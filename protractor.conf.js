var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

var basePath = __dirname + '/tests';

// An example configuration file.
exports.config = {
    // The address of a running selenium server.
    seleniumAddress: 'http://localhost:4444/wd/hub',
    //seleniumServerJar: deprecated, this should be set on node_modules/protractor/config.json

//    baseUrl: 'themachine.dev.void.pt/', //THE LAST SLASH IS IMPORTANT
    baseUrl: 'preprodyesaccount.com/',
    // baseUrl: 'luis.dev.void.pt/',
    // baseUrl: 'ivo.dev.void.pt/',
    // baseUrl: 'bh.dev.void.pt/',
    // baseUrl: 'david.dev.void.pt/',
    // baseUrl: 'yesaccount.fr/',

    framework: 'jasmine2',

    maxSessions: 1,

    // Capabilities to be passed to the webdriver instance.
    multiCapabilities: [

        /*
            MAC OS
        */


        {
            'browserName': 'firefox',
            'logName': 'Firefox OSX',
            'elementScrollBehavior': '1',
            'unexpectedAlertBehaviour': 'accept'
        },


        /*{
            'browserName': 'chrome',
            'logName': 'Chrome OSX',
            'chromeOptions': {
                // Get rid of --ignore-certificate yellow warning
                args: ['--no-sandbox', '--test-type=browser'],
                // Set download path and avoid prompting for download even though
                // this is already the default on Chrome but for completeness
                prefs: {
                    'download': {
                        'prompt_for_download': false,
                        'default_directory': './tests/e2e/storage/'
                    }
                }
            }
        },
        /*
        {
            'browserName': 'safari',
            'logName': 'Safari OSX',
        },
        */

        /*
           WINDOWS
        */

        /*{
            'browserName': 'internet explorer',
            'logName': 'IE10 Win7',
            'seleniumAddress': 'http://ie10.dev:4444/wd/hub'
        },

        /*
            CENTOS
        */

        /*{
            'browserName': 'firefox',
            'logName': 'Firefox CentOS',
            'elementScrollBehavior': '1',
            'unexpectedAlertBehaviour': 'accept',
            'seleniumAddress': 'http://centos.dev:4444/wd/hub'
        },


        {
            'browserName': 'chrome',
            'logName': 'Chrome CentOS',
            'seleniumAddress': 'http://centos.dev:4444/wd/hub',
            'chromeOptions': {
                // Get rid of --ignore-certificate yellow warning
                args: ['--no-sandbox', '--test-type=browser'],
                // Set download path and avoid prompting for download even though
                // this is already the default on Chrome but for completeness
                prefs: {
                    'download': {
                        'prompt_for_download': false,
                        'default_directory': './tests/e2e/storage/'
                    }
                }
            }
        },*/


        //phantomJS não é recomendado usar para testes
        //safari precisa de uma concdta developer para testar (driver para selenium)
        //firefox tem problemas em executar os testes ( de vez em quando em vez de selecionar um item da dropdown, faz scroll na página
    ],

    getPageTimeout: 60000,
    allScriptsTimeout: 60000,

    // Spec patterns are relative to the current working directly when
    // protractor is called.
    //specs: ['tests/e2e' + '/**/*_spec.js'],
    specs: [
            
        ],


    // comando para correr :
    //                         protractor protractor.conf.js --suite <suite_name>
    //
    suites:
    {
        //SUITES P/ CRIAÇÃO DE UTILIZADORES

        //criar partner e holding
        seed1: [
            'tests/seeds/create_scripts/1_create_partner_1.js',

            'tests/seeds/create_scripts/6_create_holding_1.js'
        ],
        //cria accounting firm para o partner e holding
        seed2: [
            'tests/seeds/create_scripts/2_create_accounting_firm_centre.js',

            'tests/seeds/create_scripts/7_create_accounting_firm_sene.js'
        ],

        //criar accountants, teams e customer firms
        seed3: [
            'tests/seeds/create_scripts/3_create_centre_accountant.js',
            'tests/seeds/create_scripts/4_create_centre_team.js',
            'tests/seeds/create_scripts/5_create_centre_customer_polo.js',

            'tests/seeds/create_scripts/8_create_sene_accountant.js',
            'tests/seeds/create_scripts/9_create_sene_team.js',
            'tests/seeds/create_scripts/10_create_sene_customer_bip.js'
        ],

        //SUITES P/ TESTES
        /*
            protractor --suite dashboard_info,helpers,docs,bank_accounts,bank_credentials,cashjournal_cashdays_delete,cashjournal_cashdays_validate,contractmanager_drafts,contractmanager_reminders,customeradmin_employees,expenses_documents_kilometers,expenses_documents_autre,expenses_documents_logement,expenses_documents_restaurant,expenses_documents_transport,expenses_documents_autre_subcats,expenses_documents_logement_subcats,expenses_documents_restaurant_subcats,expenses_documents_transport_subcats,expenses_documentstovalidate_manager,expenses_documentstovalidate_employee,invoicing_creditnotes,invoicing_customers,invoicing_families,invoicing_invoices,invoicing_recurrentinvoices,invoicing_items,invoicing_quotes,invoicing_receipts,payroll_variables,suppliers_paymentdocuments,suppliers_suppliers,documenttagging_documents,documenttagging_documents_outsource,outsource_manager
         */

        mem_leaks_check: [
            'tests/mem_leaks_check_spec.js'
        ],

        //Dashboard
        /*
            protractor --suite dashboard_info
         */
        dashboard_info: [
            'tests/app-ui/dashboard/profile_spec.js',
            'tests/app-ui/dashboard/search_spec.js',
            'tests/app-ui/dashboard/open_modules_widgets_spec.js',
            'tests/app-ui/dashboard/create_cashdays_spec.js',
            'tests/app-ui/dashboard/create_contract_draft_spec.js',
            'tests/app-ui/dashboard/create_facture_spec.js',
            'tests/app-ui/dashboard/create_devi_spec.js',
            'tests/app-ui/dashboard/sidebar_notifications_spec.js',
            'tests/app-ui/info/info_spec.js'
        ],

        //Helpers
        /*
            protractor --suite helpers
         */
        helpers: [
            'tests/app-ui/suppliers/documents/helper_spec.js',
            'tests/app-ui/docs/helper_spec.js',
            'tests/app-ui/bank/accounts/helper_spec.js',
            'tests/app-ui/bank/credentials/helper_spec.js',
            'tests/app-ui/bank/connectors/helper_spec.js'
        ],

        //Docs
        /*
            protractor --suite docs
         */
        docs: [
            'tests/app-ui/docs/bottom_bar_tasks_spec.js',
            'tests/app-ui/docs/open_recent_doc_spec.js',
            'tests/app-ui/docs/comment_document_spec.js',
            'tests/app-ui/docs/search_documents_spec.js',
            'tests/app-ui/docs/show_documents_spec.js',
        ],

        //Bank
        /*
            protractor --suite bank_credentials,bank_accounts
         */

        bank_credentials: [
            'tests/app-ui/bank/credentials/create_spec.js',
            'tests/app-ui/bank/credentials/edit_spec.js',
            'tests/app-ui/bank/credentials/edit_accounts_spec.js',
            'tests/app-ui/bank/credentials/reauthenticate_spec.js',
            'tests/app-ui/bank/credentials/export_spec.js',
            'tests/app-ui/bank/credentials/show_spec.js',
            'tests/app-ui/bank/credentials/search_spec.js',
            'tests/app-ui/bank/credentials/delete_spec.js'
        ],

        bank_accounts: [
            'tests/app-ui/bank/accounts/edit_spec.js',
            'tests/app-ui/bank/accounts/show_spec.js',
            'tests/app-ui/bank/accounts/search_spec.js',
            'tests/app-ui/bank/accounts/export_spec.js'
        ],
        //Cash journal
        /*
            protractor --suite cashjournal_cashdays_delete,cashjournal_cashdays_validate
         */
        cashjournal_cashdays_delete: [
            'tests/app-ui/cash_journal/cash_days/create_spec.js',
            'tests/app-ui/cash_journal/cash_days/edit_spec.js',
            'tests/app-ui/cash_journal/cash_days/show_spec.js',
            'tests/app-ui/cash_journal/cash_days/beside_icons_spec.js',
            'tests/app-ui/cash_journal/cash_days/delete_spec.js'
        ],
        cashjournal_cashdays_validate: [
            'tests/app-ui/cash_journal/cash_days/create_spec.js',
            'tests/app-ui/cash_journal/cash_days/validate_spec.js'
        ],
        //Contract manager
        /*
            protractor --suite contractmanager_drafts,contractmanager_contracts,contractmanager_reminders
         */
        contractmanager_drafts: [
            'tests/app-ui/contract_manager/contracts/create_draft/create_draft_societeExterne_spec.js',
            'tests/app-ui/contract_manager/contracts/create_draft/create_draft_new_societeExterne_spec.js',
            'tests/app-ui/contract_manager/contracts/create_draft/create_draft_client_spec.js',
            'tests/app-ui/contract_manager/contracts/create_draft/create_draft_new_client_spec.js',
            'tests/app-ui/contract_manager/contracts/create_draft/create_draft_employee_spec.js',
            'tests/app-ui/contract_manager/contracts/create_draft/create_draft_avec_maSociete_spec.js',
            'tests/app-ui/contract_manager/contracts/edit_spec.js',
            'tests/app-ui/contract_manager/contracts/show_spec.js',
            'tests/app-ui/contract_manager/contracts/search_spec.js',
            'tests/app-ui/contract_manager/contracts/delete_spec.js',
            'tests/app-ui/contract_manager/contracts/beside_icons_spec.js'
        ],
        contractmanager_contracts: [
            'tests/app-ui/contract_manager/contracts/create_contract/create_contract_societeExterne_spec.js',
            'tests/app-ui/contract_manager/contracts/create_contract/create_contract_new_societeExterne_spec.js',
            'tests/app-ui/contract_manager/contracts/create_contract/create_contract_client_spec.js',
            'tests/app-ui/contract_manager/contracts/create_contract/create_contract_new_client_spec.js',
            'tests/app-ui/contract_manager/contracts/create_contract/create_contract_employee_spec.js',
            'tests/app-ui/contract_manager/contracts/create_contract/create_contract_avec_maSociete_spec.js',
            'tests/app-ui/contract_manager/contracts/edit_spec.js',
            'tests/app-ui/contract_manager/contracts/show_spec.js',
            'tests/app-ui/contract_manager/contracts/search_spec.js',
            'tests/app-ui/contract_manager/contracts/delete_spec.js',
            'tests/app-ui/contract_manager/contracts/beside_icons_spec.js'
        ],
        contractmanager_reminders: [
            'tests/app-ui/contract_manager/reminders/create_spec.js',
            'tests/app-ui/contract_manager/reminders/edit_spec.js',
            'tests/app-ui/contract_manager/reminders/show_spec.js',
            'tests/app-ui/contract_manager/reminders/search_spec.js',
            'tests/app-ui/contract_manager/reminders/beside_icons_spec.js',
            'tests/app-ui/contract_manager/reminders/delete_spec.js'
        ],
        //Customer Admin
        /*
            protractor --suite customeradmin_employees
         */
        customeradmin_employees: [
            'tests/app-ui/customer_admin/employees/create_spec.js',
            'tests/app-ui/customer_admin/employees/edit_spec.js',
            'tests/app-ui/customer_admin/employees/show_spec.js',
            'tests/app-ui/customer_admin/employees/search_spec.js',
            'tests/app-ui/customer_admin/employees/delete_spec.js',
            'tests/app-ui/customer_admin/employees/modules_spec.js'
        ],
        //Expenses
        /*
            protractor --suite expenses_documents_kilometers,expenses_documents_autre,expenses_documents_hebergement,expenses_documents_alimentation,expenses_documents_transport
         */
        expenses_documents_kilometers: [
            'tests/app-ui/expenses/documents/create/create_kilometers_spec.js',
            'tests/app-ui/expenses/documents/edit/edit_kilometers_spec.js',
            'tests/app-ui/expenses/documents/show_spec.js',
            'tests/app-ui/expenses/documents/beside_icons_spec.js',
            'tests/app-ui/expenses/documents/search_spec.js',
            'tests/app-ui/expenses/documents/changepaymentstate_spec.js',
            'tests/app-ui/expenses/documents/delete_spec.js'
        ],
        expenses_documents_autre: [
            'tests/app-ui/expenses/documents/create/create_autres_spec.js',
            'tests/app-ui/expenses/documents/edit/edit_autres_spec.js',
            'tests/app-ui/expenses/documents/show_spec.js',
            'tests/app-ui/expenses/documents/beside_icons_spec.js',
            'tests/app-ui/expenses/documents/search_spec.js',
            'tests/app-ui/expenses/documents/changepaymentstate_spec.js',
            'tests/app-ui/expenses/documents/delete_spec.js'
        ],
        expenses_documents_hebergement: [
            'tests/app-ui/expenses/documents/create/create_hebergement_spec.js',
            'tests/app-ui/expenses/documents/edit/edit_hebergement_spec.js',
            'tests/app-ui/expenses/documents/show_spec.js',
            'tests/app-ui/expenses/documents/beside_icons_spec.js',
            'tests/app-ui/expenses/documents/search_spec.js',
            'tests/app-ui/expenses/documents/changepaymentstate_spec.js',
            'tests/app-ui/expenses/documents/delete_spec.js'
        ],
        expenses_documents_alimentation: [
            'tests/app-ui/expenses/documents/create/create_alimentation_spec.js',
            'tests/app-ui/expenses/documents/edit/edit_alimentation_spec.js',
            'tests/app-ui/expenses/documents/show_spec.js',
            'tests/app-ui/expenses/documents/beside_icons_spec.js',
            'tests/app-ui/expenses/documents/search_spec.js',
            'tests/app-ui/expenses/documents/changepaymentstate_spec.js',
            'tests/app-ui/expenses/documents/delete_spec.js'
        ],
        expenses_documents_transport: [
            'tests/app-ui/expenses/documents/create/create_transport_spec.js',
            'tests/app-ui/expenses/documents/edit/edit_transport_spec.js',
            'tests/app-ui/expenses/documents/show_spec.js',
            'tests/app-ui/expenses/documents/beside_icons_spec.js',
            'tests/app-ui/expenses/documents/search_spec.js',
            'tests/app-ui/expenses/documents/changepaymentstate_spec.js',
            'tests/app-ui/expenses/documents/delete_spec.js'
        ],
        //Expenses with subcats
        /*
            protractor --suite expenses_documents_autre_subcats,expenses_documents_hebergement_subcats,expenses_documents_alimentation_subcats,expenses_documents_transport_subcats
         */
        expenses_documents_autre_subcats: [
            'tests/app-ui/expenses/documents/create_subcats/create_autres_spec.js',
            'tests/app-ui/expenses/documents/edit_subcats/edit_autres_spec.js',
            'tests/app-ui/expenses/documents/show_spec.js',
            'tests/app-ui/expenses/documents/search_spec.js',
            'tests/app-ui/expenses/documents/delete_spec.js',
            'tests/app-ui/expenses/documents/changepaymentstate_spec.js',
            'tests/app-ui/expenses/documents/beside_icons_spec.js'
        ],
        expenses_documents_hebergement_subcats: [
            'tests/app-ui/expenses/documents/create_subcats/create_hebergement_spec.js',
            'tests/app-ui/expenses/documents/edit_subcats/edit_hebergement_spec.js',
            'tests/app-ui/expenses/documents/show_spec.js',
            'tests/app-ui/expenses/documents/search_spec.js',
            'tests/app-ui/expenses/documents/delete_spec.js',
            'tests/app-ui/expenses/documents/changepaymentstate_spec.js',
            'tests/app-ui/expenses/documents/beside_icons_spec.js'
        ],
        expenses_documents_alimentation_subcats: [
            'tests/app-ui/expenses/documents/create_subcats/create_alimentation_spec.js',
            'tests/app-ui/expenses/documents/edit_subcats/edit_alimentation_spec.js',
            'tests/app-ui/expenses/documents/show_spec.js',
            'tests/app-ui/expenses/documents/search_spec.js',
            'tests/app-ui/expenses/documents/delete_spec.js',
            'tests/app-ui/expenses/documents/changepaymentstate_spec.js',
            'tests/app-ui/expenses/documents/beside_icons_spec.js'
        ],
        expenses_documents_transport_subcats: [
            'tests/app-ui/expenses/documents/create_subcats/create_transport_spec.js',
            'tests/app-ui/expenses/documents/edit_subcats/edit_transport_spec.js',
            'tests/app-ui/expenses/documents/show_spec.js',
            'tests/app-ui/expenses/documents/search_spec.js',
            'tests/app-ui/expenses/documents/delete_spec.js',
            'tests/app-ui/expenses/documents/changepaymentstate_spec.js',
            'tests/app-ui/expenses/documents/beside_icons_spec.js'
        ],
        //Expenses document validation
        /*
            protractor --suite expenses_documentstovalidate_manager,expenses_documentstovalidate_employee
         */
        expenses_documentstovalidate_manager: [
            'tests/app-ui/expenses/documents_to_validate/manager/treat_facture_achat_spec.js',
            'tests/app-ui/expenses/documents_to_validate/manager/treat_avoir_achat_spec.js',
            'tests/app-ui/expenses/documents_to_validate/manager/treat_od_spec.js',
            'tests/app-ui/expenses/documents_to_validate/manager/treat_pieces_justificatives_spec.js',
            'tests/app-ui/expenses/documents_to_validate/manager/type_payment_frais/treat_frais_cheque_spec.js',
            'tests/app-ui/expenses/documents_to_validate/manager/type_payment_frais/treat_frais_credit_spec.js',
            'tests/app-ui/expenses/documents_to_validate/manager/type_payment_frais/treat_frais_personal_spec.js',
            'tests/app-ui/expenses/documents_to_validate/manager/show_spec.js',
            'tests/app-ui/expenses/documents_to_validate/manager/search_spec.js',
            'tests/app-ui/expenses/documents_to_validate/manager/delete_spec.js'
        ],
        expenses_documentstovalidate_employee: [
            'tests/app-ui/expenses/documents_to_validate/employe/treat_facture_achat_spec.js',
            'tests/app-ui/expenses/documents_to_validate/employe/treat_avoir_achat_spec.js',
            'tests/app-ui/expenses/documents_to_validate/employe/treat_od_spec.js',
            'tests/app-ui/expenses/documents_to_validate/employe/treat_pieces_justificatives_spec.js',
            'tests/app-ui/expenses/documents_to_validate/employe/type_payment_frais/treat_frais_non_concerne_spec.js',
            'tests/app-ui/expenses/documents_to_validate/employe/show_spec.js',
            'tests/app-ui/expenses/documents_to_validate/employe/search_spec.js',
            'tests/app-ui/expenses/documents_to_validate/employe/delete_spec.js'
        ],
        //Invoicing
        /*
            protractor --suite invoicing_customers,invoicing_families,invoicing_items,invoicing_quotes,invoicing_invoices,invoicing_recurrentinvoices,invoicing_creditnotes,invoicing_receipts
         */
        invoicing_creditnotes: [
            'tests/app-ui/invoicing/credit_notes/create_spec.js',
            'tests/app-ui/invoicing/credit_notes/beside_icons_spec.js',
            'tests/app-ui/invoicing/credit_notes/edit_spec.js',
            'tests/app-ui/invoicing/credit_notes/show_spec.js',
            'tests/app-ui/invoicing/credit_notes/search_spec.js',
            'tests/app-ui/invoicing/credit_notes/validate_spec.js',
            'tests/app-ui/invoicing/credit_notes/print_spec.js',
            'tests/app-ui/invoicing/credit_notes/delete_spec.js',
            'tests/app-ui/invoicing/credit_notes/duplicate_creditnotes_draft_spec.js',
            'tests/app-ui/invoicing/credit_notes/duplicate_creditnotes_validate_spec.js',
            'tests/app-ui/invoicing/credit_notes/duplicate_facture_draft_spec.js',
            'tests/app-ui/invoicing/credit_notes/duplicate_facture_validate_spec.js',
            'tests/app-ui/invoicing/credit_notes/duplicate_quote_spec.js',
            'tests/app-ui/invoicing/credit_notes/sendmail_spec.js'
        ],
        invoicing_customers: [
            'tests/app-ui/invoicing/customers/client_situation_spec.js',
            'tests/app-ui/invoicing/customers/create_spec.js',
            'tests/app-ui/invoicing/customers/edit_spec.js',
            'tests/app-ui/invoicing/customers/show_spec.js',
            'tests/app-ui/invoicing/customers/search_spec.js',
            'tests/app-ui/invoicing/customers/delete_spec.js'
        ],
        invoicing_families: [
            'tests/app-ui/invoicing/families/create_spec.js',
            'tests/app-ui/invoicing/families/beside_icons_spec.js',
            'tests/app-ui/invoicing/families/edit_spec.js',
            'tests/app-ui/invoicing/families/show_spec.js',
            'tests/app-ui/invoicing/families/search_spec.js',
            'tests/app-ui/invoicing/families/delete_spec.js'
        ],
        invoicing_invoices: [
            'tests/app-ui/invoicing/invoices/create_spec.js',
            'tests/app-ui/invoicing/invoices/beside_icons_spec.js',
            'tests/app-ui/invoicing/invoices/edit_spec.js',
            'tests/app-ui/invoicing/invoices/show_spec.js',
            'tests/app-ui/invoicing/invoices/search_spec.js',
            'tests/app-ui/invoicing/invoices/auto_liquidation_spec.js',
            'tests/app-ui/invoicing/invoices/validate_spec.js',
            'tests/app-ui/invoicing/invoices/print_spec.js',
            'tests/app-ui/invoicing/invoices/delete_spec.js',
            'tests/app-ui/invoicing/invoices/duplicate_creditnotes_draft_spec.js',
            'tests/app-ui/invoicing/invoices/duplicate_creditnotes_validate_spec.js',
            'tests/app-ui/invoicing/invoices/duplicate_facture_draft_spec.js',
            'tests/app-ui/invoicing/invoices/duplicate_facture_validate_spec.js',
            'tests/app-ui/invoicing/invoices/duplicate_facture_new_client_spec.js',
            'tests/app-ui/invoicing/invoices/duplicate_facture_private_enterprise_spec.js',
            'tests/app-ui/invoicing/invoices/duplicate_quote_spec.js',
            'tests/app-ui/invoicing/invoices/duplicate_quote_new_client_spec.js',
            'tests/app-ui/invoicing/invoices/duplicate_quote_private_enterprise_spec.js',
            'tests/app-ui/invoicing/invoices/sendmail_spec.js',
            'tests/app-ui/invoicing/invoices/create_private_customer_spec.js',
            'tests/app-ui/invoicing/invoices/create_generate_receipt_spec.js',
            'tests/app-ui/invoicing/invoices/create_subtotal_spec.js',
            'tests/app-ui/invoicing/invoices/change_to_enRecouvrement_spec.js'
        ],
        invoicing_recurrentinvoices: [
            'tests/app-ui/invoicing/invoices/createrecurrent_spec.js',
            'tests/app-ui/invoicing/invoices/showrecurrent_spec.js',
            'tests/app-ui/invoicing/invoices/searchrecurrent_spec.js',
            'tests/app-ui/invoicing/invoices/activatedeactivaterecurrent_spec.js',
            'tests/app-ui/invoicing/invoices/deleterecurrent_spec.js'
        ],
        invoicing_items: [
            'tests/app-ui/invoicing/items/create_spec.js',
            'tests/app-ui/invoicing/items/beside_icons_spec.js',
            'tests/app-ui/invoicing/items/edit_spec.js',
            'tests/app-ui/invoicing/items/show_spec.js',
            'tests/app-ui/invoicing/items/search_spec.js',
            'tests/app-ui/invoicing/items/delete_spec.js'
        ],
        invoicing_quotes: [
            'tests/app-ui/invoicing/quotes/create_spec.js',
            'tests/app-ui/invoicing/quotes/beside_icons_spec.js',
            'tests/app-ui/invoicing/quotes/edit_spec.js',
            'tests/app-ui/invoicing/quotes/show_spec.js',
            'tests/app-ui/invoicing/quotes/search_spec.js',
            'tests/app-ui/invoicing/quotes/print_spec.js',
            'tests/app-ui/invoicing/quotes/sendmail_spec.js',
            'tests/app-ui/invoicing/quotes/sendmail_phone_number_spec.js',
            'tests/app-ui/invoicing/quotes/duplicate_creditnotes_draft_spec.js',
            'tests/app-ui/invoicing/quotes/duplicate_creditnotes_validate_spec.js',
            'tests/app-ui/invoicing/quotes/duplicate_facture_draft_spec.js',
            'tests/app-ui/invoicing/quotes/duplicate_facture_validate_spec.js',
            'tests/app-ui/invoicing/quotes/duplicate_facture_new_client_spec.js',
            'tests/app-ui/invoicing/quotes/duplicate_facture_private_enterprise_spec.js',
            'tests/app-ui/invoicing/quotes/duplicate_quote_spec.js',
            'tests/app-ui/invoicing/quotes/duplicate_quote_new_client_spec.js',
            'tests/app-ui/invoicing/quotes/duplicate_quote_private_enterprise_spec.js',
            'tests/app-ui/invoicing/quotes/create_subtotal_spec.js',
            'tests/app-ui/invoicing/quotes/delete_spec.js'
        ],
        invoicing_receipts: [
            'tests/app-ui/invoicing/receipts/create_spec.js',
            'tests/app-ui/invoicing/receipts/beside_icons_spec.js',
            'tests/app-ui/invoicing/receipts/show_spec.js',
            'tests/app-ui/invoicing/receipts/search_spec.js',
            'tests/app-ui/invoicing/receipts/print_spec.js',
            'tests/app-ui/invoicing/receipts/sendmail_spec.js',
            'tests/app-ui/invoicing/receipts/delete_spec.js',
            'tests/app-ui/invoicing/receipts/create_parcial_spec.js'
        ],
        //Payroll
        /*
            protractor --suite payroll_variables
         */
        payroll_variables: [
            'tests/app-ui/payroll/variables/edit_spec.js',
            'tests/app-ui/payroll/variables/search_spec.js',
            'tests/app-ui/payroll/variables/validate_spec.js'
        ],

        //Suppliers
        /*
            protractor --suite suppliers_paymentdocuments,suppliers_suppliers,suppliers_creditnotes,suppliers_documents
         */
        suppliers_paymentdocuments: [
            'tests/app-ui/suppliers/payment_documents/create_spec.js',
            'tests/app-ui/suppliers/payment_documents/show_spec.js',
            'tests/app-ui/suppliers/payment_documents/search_spec.js',
            'tests/app-ui/suppliers/payment_documents/print_spec.js',
            'tests/app-ui/suppliers/payment_documents/sendmail_spec.js',
            'tests/app-ui/suppliers/payment_documents/delete_spec.js',
            'tests/app-ui/suppliers/payment_documents/create_parcial_spec.js'
        ],
        suppliers_suppliers: [
            'tests/app-ui/suppliers/suppliers/create_spec.js',
            'tests/app-ui/suppliers/suppliers/edit_spec.js',
            'tests/app-ui/suppliers/suppliers/show_spec.js',
            'tests/app-ui/suppliers/suppliers/search_spec.js',
            'tests/app-ui/suppliers/suppliers/delete_spec.js'
        ],
        suppliers_creditnotes: [
            'tests/app-ui/suppliers/credit_notes/search_spec.js',
            'tests/app-ui/suppliers/credit_notes/view_document_spec.js',
            'tests/app-ui/suppliers/credit_notes/view_observation_spec.js'
        ],
        suppliers_documents: [
            'tests/app-ui/suppliers/documents/search_spec.js',
            'tests/app-ui/suppliers/documents/view_document_spec.js',
            'tests/app-ui/suppliers/documents/view_observation_spec.js',
            'tests/app-ui/suppliers/documents/resend_to_tagging_spec.js'
        ],
        //Document tagging
        /*
            protractor --suite documenttagging_documents,documenttagging_crop
         */
        documenttagging_documents: [
            'tests/app-ui/document_tagging/documents/treat_facture_achat_spec.js',
            'tests/app-ui/document_tagging/documents/treat_avoir_achat_spec.js',
            'tests/app-ui/document_tagging/documents/treat_frais_spec.js',
            'tests/app-ui/document_tagging/documents/treat_od_spec.js',
            'tests/app-ui/document_tagging/documents/treat_pieces_justificatives_spec.js',
            'tests/app-ui/document_tagging/documents/treat_avoir_vente_spec.js',
            'tests/app-ui/document_tagging/documents/treat_facture_vente_spec.js',
            'tests/app-ui/document_tagging/documents/treat_contract_my_ext_spec.js',
            'tests/app-ui/document_tagging/documents/treat_contract_my_ext_new_spec.js',
            'tests/app-ui/document_tagging/documents/treat_contract_my_part_spec.js',
            'tests/app-ui/document_tagging/documents/treat_contract_my_part_new_spec.js',
            'tests/app-ui/document_tagging/documents/treat_contract_my_emp_spec.js',
            'tests/app-ui/document_tagging/documents/treat_contract_ext_my_spec.js',
            'tests/app-ui/document_tagging/documents/treat_contract_ext_new_my_spec.js',
            'tests/app-ui/document_tagging/documents/show_spec.js',
            'tests/app-ui/document_tagging/documents/search_spec.js',
            'tests/app-ui/document_tagging/documents/delete_spec.js'
        ],

        documenttagging_crop: [
            'tests/app-ui/document_tagging/documents/crop_document_tagging_spec.js',
            'tests/app-ui/document_tagging/documents/treat_facture_achat_spec.js',
            'tests/app-ui/document_tagging/documents/treat_avoir_achat_spec.js',
            'tests/app-ui/document_tagging/documents/treat_frais_spec.js',
            'tests/app-ui/document_tagging/documents/treat_od_spec.js',
            'tests/app-ui/document_tagging/documents/treat_pieces_justificatives_spec.js'
        ],
        //Document tagging (outsource tagger)
        /*
            protractor --suite documenttagging_documents_outsource
         */
        documenttagging_documents_outsource: [
            'tests/app-ui/document_tagging/documents/outsource_tagger/treat_facture_achat_spec.js',
            'tests/app-ui/document_tagging/documents/outsource_tagger/treat_avoir_achat_spec.js',
            'tests/app-ui/document_tagging/documents/outsource_tagger/treat_frais_spec.js',
            'tests/app-ui/document_tagging/documents/outsource_tagger/treat_od_spec.js',
            'tests/app-ui/document_tagging/documents/outsource_tagger/treat_pieces_justificatives_spec.js',
            'tests/app-ui/document_tagging/documents/outsource_tagger/show_spec.js',
            'tests/app-ui/document_tagging/documents/outsource_tagger/search_spec.js',
            'tests/app-ui/document_tagging/documents/outsource_tagger/delete_spec.js'
        ],
        //Outsource manager
        /*
            protractor --suite outsource_manager
         */
        outsource_manager: [
            'tests/app-ui/outsource_manager/create_spec.js',
            'tests/app-ui/outsource_manager/edit_spec.js',
            'tests/app-ui/outsource_manager/show_spec.js',
            'tests/app-ui/outsource_manager/search_spec.js',
            'tests/app-ui/outsource_manager/activate_spec.js',
            'tests/app-ui/outsource_manager/reactivate_spec.js',
            'tests/app-ui/outsource_manager/delete_spec.js',
            'tests/app-ui/outsource_manager/create_activate_mail_spec.js'
        ],

        //Customer Admin / test Palma
        /*
            protractor --suite master_plan
         */
        master_plan: [
            'tests/app-ui/customer_admin/testPalma/acc_settings/master_plan/normalCreate_spec.js',
            'tests/app-ui/customer_admin/testPalma/acc_settings/master_plan/normalEdit_spec.js',
            'tests/app-ui/customer_admin/testPalma/acc_settings/master_plan/normalSearch_spec.js',
            'tests/app-ui/customer_admin/testPalma/acc_settings/master_plan/normalDelete_spec.js',
            'tests/app-ui/customer_admin/testPalma/acc_settings/master_plan/exceptionCreate_spec.js',
            'tests/app-ui/customer_admin/testPalma/acc_settings/master_plan/exceptionEdit_spec.js',
            'tests/app-ui/customer_admin/testPalma/acc_settings/master_plan/exceptionDelete_spec.js',
            'tests/app-ui/customer_admin/testPalma/acc_settings/master_plan/exceptionSearch_spec.js',
            'tests/app-ui/customer_admin/testPalma/acc_settings/master_plan/deleteAll_spec.js',
            'tests/app-ui/customer_admin/testPalma/acc_settings/master_plan/download_import_spec.js'
        ],

        //Customer Admin / test Palma
        /*
            protractor --suite acc_plan
         */
        acc_plan: [
            'tests/app-ui/customer_admin/testPalma/acc_settings/acc_plan/create_edit_deleteAll_spec.js',
            'tests/app-ui/customer_admin/testPalma/acc_settings/acc_plan/download_import_spec.js',
            'tests/app-ui/customer_admin/testPalma/acc_settings/acc_plan/search_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite apes_journal
         */
        apes_journal: [
            'tests/app-ui/customer_admin/testPalma/acc_settings/apes/edit_spec.js',
            'tests/app-ui/customer_admin/testPalma/acc_settings/apes/download_import_spec.js',
            'tests/app-ui/customer_admin/testPalma/acc_settings/journal/add_code_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite supplier_suppliers
         */
        supplier_suppliers: [
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/suppliers/create_code_compte_achats_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/suppliers/create_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/suppliers/exist_code_compte_achats_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/suppliers/exist_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/suppliers/search_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite supplier_payment_methods
         */
        supplier_payment_methods: [
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/payment_methods/exist_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/payment_methods/create_code_compte_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite supplier_private_suppliers
         */
        supplier_private_suppliers: [
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/private_suppliers/create_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/private_suppliers/exist_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/private_suppliers/create_depenses_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/private_suppliers/exist_depenses_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/private_suppliers/create_produits_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/private_suppliers/exist_produits_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/supplier/private_suppliers/search_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite invoicing_customers
         */
        customer_admin_invoicing_customers: [
            'tests/app-ui/customer_admin/testPalma/modules_configs/invoicing/customers/create_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/invoicing/customers/exist_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/invoicing/customers/search_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite invoicing_item_families
         */
        customer_admin_invoicing_families: [
            'tests/app-ui/customer_admin/testPalma/modules_configs/invoicing/item_families/create_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/invoicing/item_families/exist_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/invoicing/item_families/search_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite invoicing_payment_methods
         */
        customer_admin_invoicing_paymentmethods: [
            'tests/app-ui/customer_admin/testPalma/modules_configs/invoicing/payment_methods/create_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/invoicing/payment_methods/exist_code_compte_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite bank_accounts
         */
        customer_admin_bank_accounts: [
            'tests/app-ui/customer_admin/testPalma/modules_configs/bank/accounts/create_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/bank/accounts/exist_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/bank/accounts/search_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite cashJournal_bank_deposits
         */
        customer_admin_cashJournal_bankdeposits: [
            'tests/app-ui/customer_admin/testPalma/modules_configs/cashJournal/bank_deposits/create_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/cashJournal/bank_deposits/exist_code_compte_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite cashJournal_products
         */
        customer_admin_cashJournal_products: [
            'tests/app-ui/customer_admin/testPalma/modules_configs/cashJournal/products/create_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/cashJournal/products/exist_code_compte_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite cashJournal_other
         */
        customer_admin_cashJournal_other: [
            'tests/app-ui/customer_admin/testPalma/modules_configs/cashJournal/other/create_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/cashJournal/other/exist_code_compte_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite expenses_categories
         */
        customer_admin_expenses_categories: [
            'tests/app-ui/customer_admin/testPalma/modules_configs/expenses/categories/create_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/expenses/categories/exist_code_compte_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite expenses_personal
         */
        customer_admin_expenses_personal: [
            'tests/app-ui/customer_admin/testPalma/modules_configs/expenses/personal/create_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/expenses/personal/exist_code_compte_spec.js'
        ],

        //Customer Admin / test Palma
        /*
         protractor --suite expenses_cash
         */
        customer_admin_expenses_cash: [
            'tests/app-ui/customer_admin/testPalma/modules_configs/expenses/cash/create_code_compte_spec.js',
            'tests/app-ui/customer_admin/testPalma/modules_configs/expenses/cash/exist_code_compte_spec.js'
        ],

         /*
         protractor --suite services
         */
        services: [
            'tests/app-ui/services/create_new_spec.js',
            'tests/app-ui/services/banners_spec.js'
        ],

    },


    onPrepare: function () {
        var width = 1920;
        var height = 1080;
        browser.driver.manage().window().setSize(width, height);

        var date = new Date();
        var month = date.getMonth() + 1;
        var reportsBasePath = './tests/reports/' + date.getFullYear() + '-' + month + '-' + date.getDate();

        browser.getCapabilities().then(function(cap){
            var browserName = cap.get('browserName');
            var osName = cap.get('platform');
            switch (browserName){
                case 'chrome':
                    browserName = 'Chrome';
                    break;
                case 'safari':
                    browserName = 'Safari';
                    break;
                case 'firefox':
                    browserName = 'Firefox';
                    break;
                case 'internet explorer':
                    browserName = 'IE10';
                    break;
            }
            switch(osName){
                case 'MAC':
                    osName = 'OSX';
                    break;
                case 'LINUX':
                    osName = 'CentOS';
                    break;
                case 'WINDOWS':
                    osName = 'Win7';
                    break;
            }
            jasmine.getEnv().addReporter(new Jasmine2HtmlReporter({
                savePath: reportsBasePath + '/' + browserName + ' - ' + osName + '/',
                filePrefix: '' + date.getHours() + 'h' + date.getMinutes() + 'm',
                takeScreenshotsOnlyOnFailures: true
            }));
        });

        global.basePath = function () {
            return basePath;
        };
        global.requireConfig = function () {
            return require(basePath + '/common/config');
        };
        global.requireCommonHelper = function (filename) {
            return require(basePath + '/common/helpers/' + filename);
        };
        global.requireAccountsConfig = function () {
            return require(basePath + '/seeds/create_scripts/config');
        };
        global.requireLoginSpec = function () {
            return require(basePath + '/login_spec');
        };
        global.requireCreateExpenseHelper = function () {
            return require(basePath + '/app-ui/expenses/helpers/create_expense_helper');
        };
        global.requireEditExpenseHelper = function () {
            return require(basePath + '/app-ui/expenses/helpers/edit_expense_helper');
        };
        global.requireBulkCashDayHelper = function() {
            return require(basePath + '/app-ui/cash_journal/helpers/bulk_helper');
        }
    },

    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        onComplete: null,
        isVerbose: true,
        showColors: true,
        includeStackTrace: true,
        defaultTimeoutInterval: 60000,
        realtimeFailure: true
    }
};
