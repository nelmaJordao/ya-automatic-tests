describe('Login', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    var user = config.users.TESTPOLO;

    var baseURL = generalHelper.baseUrlWithSubdomain(user.subdomain);

    browser.ignoreSynchronization = true;

    browser.get(baseURL);

    it('should wait for mem leaks check', function () {
        console.log('check for memory leaks now');
        browser.sleep(300000); //5 min
    });
});
