module.exports = {

    getTodayDate: function () {

        // select current date with date function

        var today = new Date();

        var day = today.getDate();

        var month = today.getMonth() + 1; //By default January count as 0

        var year = today.getFullYear();

        if (day < 10) {
            day = '0' + day
        }

        if (month < 10) {
            month = '0' + month
        }

        today = {'day': day,
            'month': month,
            'year': year
        };

        return today;

    }

};