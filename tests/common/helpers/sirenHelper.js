filepath = basePath() + '/common/siren.json';
var json = require(filepath);
var fs = require('fs');
module.exports = {
    getSiren: function () {

        var sirenArray = json['siren_numbers'];
        var sirenCount = json['siren_count'];
        var tvaArray = json['tva_numbers'];
        var tvaCount = json['tva_count'];

        if(sirenCount == 0){
            throw new Error('\n---NO SIREN NUMBERS---\n');
        }

        if(tvaCount == 0){
            console.log('\n---NO TVA NUMBERS---\n');
        }

        var siren = sirenArray.shift();
        sirenCount--;
        
        var file = {"siren_count": sirenCount,
            "tva_count": tvaCount,
            "siren_numbers": sirenArray,
            "tva_numbers": tvaArray};

        if(sirenCount < 10){
            console.log("\n---ONLY " + sirenCount + " SIREN NUMBERS REMAINING---\n");
        }

        if(tvaCount < 10){
            console.log("\n---ONLY " + tvaCount + " TVA NUMBERS REMAINING---\n");
        }

        fs.writeFileSync(filepath, JSON.stringify(file));

        return siren;
    },

    getTVA: function () {

        var sirenArray = json['siren_numbers'];
        var sirenCount = json['siren_count'];
        var tvaArray = json['tva_numbers'];
        var tvaCount = json['tva_count'];

        if(tvaCount == 0){
            throw new Error('\n---NO TVA NUMBERS---\n');
        }

        if(sirenCount == 0){
            console.log('\n---NO SIREN NUMBERS---\n');
        }

        var tva = tvaArray.shift();
        tvaCount--;

        var file = {"siren_count": sirenCount,
            "tva_count": tvaCount,
            "siren_numbers": sirenArray,
            "tva_numbers": tvaArray};

        if(sirenCount < 10){
            console.log("\n---ONLY " + sirenCount + " SIREN NUMBERS REMAINING---\n");
        }

        if(tvaCount < 10){
            console.log("\n---ONLY " + tvaCount + " TVA NUMBERS REMAINING---\n");
        }

        fs.writeFileSync(filepath, JSON.stringify(file));

        return tva;
    }
};