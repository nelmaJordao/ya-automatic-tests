var generalHelper = requireCommonHelper('generalHelper');
module.exports = {
    
    loginWith: function (url, user) {
        var emailInput,
            passwordInput,
            accessInput;
        browser.get(url);

        this.waitUrlToChange(url);
        var self = this;
        browser.getCurrentUrl().then(function (currentUrl) {
            console.log(currentUrl);
            if (currentUrl.indexOf('login') == -1) {
                self.logout();
            }
            emailInput = element(by.css('input#email'));
            passwordInput = element(by.css('input#password'));
            accessInput = element(by.css('.js-submit'));
            generalHelper.waitElementPresent(emailInput);
            expect(emailInput.isPresent()).toBe(true);

            emailInput.sendKeys(user.username);
            passwordInput.sendKeys(user.password);
            generalHelper.waitElementClickableAndClick(accessInput);
            var usernameMenu = element(by.css('#username'));
            generalHelper.waitElementPresent(usernameMenu);

            usernameMenu.getText().then(function (text) {
                console.log(text);
            });
        });
    },

    login: function (user) {
        var emailInput,
            passwordInput,
            accessInput;

        emailInput = element(by.css('input#email'));
        passwordInput = element(by.css('input#password'));
        accessInput = element(by.css('.js-submit'));
        generalHelper.waitElementPresent(emailInput);
        expect(emailInput.isPresent()).toBe(true);

        emailInput.sendKeys(user.username);
        passwordInput.sendKeys(user.password);
        generalHelper.waitElementClickableAndClick(accessInput);
        var usernameMenu = element(by.css('#username'));
        generalHelper.waitElementPresent(usernameMenu);

        usernameMenu.getText().then(function (text) {
            console.log(text);
        });
    },
    
    changeRoleTo: function (role) {
        /**
         * Exit codes:
         * 1 - Only has one role and the desired one is the selected one.
         * 2 - Has many roles and the desired one is the selected one.
         * 3 - Has many roles and the desired one exists. Selected it.
         * -1 - Does not have the desired role.
         */
        var hasManyRoles = false;
        var roleCombo = element(by.css('#usertype'));
        var self = this;
        
        generalHelper.waitElementDisplayed(roleCombo);
        roleCombo.getAttribute('data-key').then(function (currentRole) {
            //console.log('current role 1: ', currentRole);

            //1 - Only has one role and the desired one is the selected one.
            if (currentRole === role) { return 1; }
            
            hasManyRoles = (currentRole !== null); //if null then has many roles

            if (hasManyRoles) {
                //has many roles
                var selectedOption = roleCombo.element(by.css('#usertype > a'));
                generalHelper.waitElementPresent(selectedOption);
                generalHelper.waitElementDisplayed(selectedOption);

                selectedOption.getAttribute('data-key').then(function (currentRole) {
                    
                    if (currentRole === role) { 
                        //2 - Has many roles and the desired one is the selected one.
                        return 2;
                    } else {
                        //the role and is not the desired one, then look for the desired one and click
                        //console.log('current role 2: ', currentRole);
                        var otherOption = roleCombo.element(by.css('a[data-key="' + role + '"]'));
                        generalHelper.waitElementPresent(otherOption);
                        generalHelper.waitElementClickableAndClick(roleCombo);                        
                        generalHelper.waitElementDisplayed(otherOption);
                        
                        //otherOption.getAttribute('data-key').then(function(dataKey) { console.log('otherOption data-key: ', dataKey); });
                        
                        generalHelper.waitElementClickableAndClick(otherOption);

                        self.waitUrlToHave('dashboard');
                        browser.sleep(1000);

                        var roleComboAfterRefresh = element(by.css('#usertype'));
                        generalHelper.waitElementPresent(roleComboAfterRefresh);
                        generalHelper.waitElementDisplayed(roleComboAfterRefresh);
                        
                        //3 - Has many roles and the desired one exists. Selected it.
                        return 3;
                    }
                });
                
            } else {
                // -1 - Only has one role and the role is not the desired one.
                return -1;
            }
        });
    },
    
    logout: function () {
        var emailInput = element(by.css('input#email'));
        var usernameMenu = element(by.css('#username'));
        generalHelper.waitElementClickableAndClick(usernameMenu);
        var logoutButton = element.all(by.css('.js-logout')).get(0);
        generalHelper.waitElementClickableAndClick(logoutButton);
        generalHelper.waitElementPresent(emailInput);
        browser.getCurrentUrl().then(function (currentUrl) {
            expect(currentUrl.indexOf('login')).toBeGreaterThan(-1);
        });
    },
    
    waitUrlToChange: function (url) {
        browser.wait(function () {
            return browser.getCurrentUrl().then(function (currentUrl) {
                return url !== currentUrl;
            });
        }, generalHelper.waitTimeout());
    },
    
    waitUrlToHave: function (str) {
        browser.wait(function () {
            return browser.getCurrentUrl().then(function (currentUrl) {
                return (currentUrl.indexOf(str) !== -1);
            });
        }, generalHelper.waitTimeout());
    },
    
    changeCustomerCompanyTo: function (customerCompany) {
        /**
         * Exit codes:
         * 1 - Only has one customerCompany and the desired one is the selected one.
         * 2 - Has many customerCompanies and the desired one is the selected one.
         * 3 - Has many customerCompanies and the desired one exists. Selected it.
         * -1 - Does not have the desired customerCompany.
         */
        
        var hasManyCustomerCompanies = false;
        var customerCompanyCombo = element(by.css('#customercompany'));
        var self = this;
        
        generalHelper.waitElementDisplayed(customerCompanyCombo);
        customerCompanyCombo.getText().then(function (currentCustomerCompany) {
        
            console.log('current customerCompany: ', currentCustomerCompany);

            //1 - Only has one customerCompany and the desired one is the selected one.
            if (currentCustomerCompany === customerCompany) { return 1; }
            
            hasManyCustomerCompanies = (currentCustomerCompany !== null); //if not null then has many customerCompanies

            console.log('hasManyCustomerCompanies: ' + hasManyCustomerCompanies);

            if (hasManyCustomerCompanies) {
                //has many customerCompanies
                               
                generalHelper.waitElementClickableAndClick(customerCompanyCombo);
                
                var customerCompanyDesired = element(by.cssContainingText('.dropdown-toggle', 'BIG BANG'));
                                
                generalHelper.waitElementDisplayed(customerCompanyDesired);
                generalHelper.waitElementClickableAndClick(customerCompanyDesired);
                
                customerCompanyDesired.getText().then(function (customerCompanyName) {
                    console.log('selected Customer Company: ' + customerCompanyName);
                });
                
                return 2;
                
            } else {
                // -1 - Only has one customerCompany and the customerCompany is not the desired one.
                return -1;
            }
        });
    }
};