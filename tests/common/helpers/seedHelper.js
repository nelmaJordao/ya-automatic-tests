var generalHelper = requireCommonHelper('generalHelper');

module.exports = {

    //
    // Ficheiro com funções comuns para a criação da estrutura de utilizadores
    //
    //

    // funcao para criar accounting firm
    //
    createAccountingFirm: function ( accountingFirm, urlLogout )
    {

        describe('Accounting Firm', function () {

            var newButton,
                modalWindow;

            beforeEach(function()
            {

                newButton = element(by.css('button.js-new-company'));
                modalWindow = element(by.css('#company-create-modal'));

            });

            it('should open new accounting firm modal window', function ()
            {
                generalHelper.waitElementClickableAndClick(newButton);
                generalHelper.waitElementPresent(modalWindow);

                expect(modalWindow.isDisplayed()).toBe(true);
            });

            it('should fill mandatory fields and submit', function() {
                var nameInput = modalWindow.element(by.css('.js-company-autocomplete'));

                browser.sleep(500); // serve ter a certeza que insere dados na textbox
                generalHelper.selectTypeAheadCreateNew(nameInput, 'input#name');

                browser.sleep(2000);

                var sirenInput = modalWindow.element(by.css('#sirenNumber'));
                var sirenButton = modalWindow.element(by.css('div.js-siren-check button.js-check-vat'));

                //generalHelper.waitElementClickableAndClick(sirenButton);
                sirenInput.sendKeys( accountingFirm.siren );

                generalHelper.waitElementClickableAndClick(sirenButton);

                browser.sleep(2000);

                var domainInput =  modalWindow.element(by.css('#subDomain'));

                domainInput.sendKeys( accountingFirm.subDomain );

                var aliasInput = modalWindow.element(by.css('#alias'));

                aliasInput.sendKeys( accountingFirm.alias);

                var firstNameInput = modalWindow.element(by.css('#firstName'));
                var lastNameInput = modalWindow.element(by.css('#lastName'));
                var emailInput = modalWindow.element(by.css('#portalEmail'));

                firstNameInput.sendKeys( accountingFirm.firstName );
                lastNameInput.sendKeys( accountingFirm.lastName );
                emailInput.sendKeys( accountingFirm.email );

                var checkbox = modalWindow.element(by.css('input.js-send-activation-email'));

                generalHelper.waitElementClickableAndClick(checkbox);


                var saveButton = modalWindow.element(by.css('button.btn-success.ok'));

                var records = element(by.css('.js-total-res span'));
                var recordCount = 0;
                records.getText().then(function(text) {
                    recordCount = parseInt(text);
                });

                generalHelper.waitElementClickableAndClick(saveButton);
                generalHelper.waitAlertSuccessPresentAndClose();

                records.getText().then(function(text) {
                    expect(parseInt(text)).toEqual(++recordCount);
                });

                browser.get( urlLogout );

            });

        });


    },


    // funcao para criar partner e holding
    //
    createPartnerOrHolding: function ( entity, urlLogout )
    {

        describe( entity.type , function () {

            var newButton,
                modalWindow,
                saveButton,
                tabItem;


            beforeEach(function()
            {

                if  (entity.type == "holding")
                {
                    tabItem = element(by.css('#plat-admin-menu-region [data-sec="holdings"]'));
                    newButton = element(by.css('.js-new-holding'));
                    modalWindow = element(by.css('#holding-create-modal'));
                }
                else if (entity.type == "partner")
                    {
                        tabItem = element(by.css('#plat-admin-menu-region [data-sec="partners"]'));
                        newButton = element(by.css('.js-new-partner'));
                        modalWindow = element(by.css('#partner-create-modal'));
                    }


                saveButton = modalWindow.element(by.css('button.btn-success.ok'));
            });

            it('should open new partner modal window', function ()
            {
                generalHelper.waitElementClickableAndClick(tabItem);
                generalHelper.waitElementClickableAndClick(newButton);
                generalHelper.waitElementPresent(modalWindow);

                expect(modalWindow.isDisplayed()).toBe(true);
            });

            it('should fill mandatory fields and submit', function()
            {

                if  (entity.type == "holding")
                {
                    // inserir o nome da entidade
                    var nameInput = modalWindow.element(by.css('#name'));
                    nameInput.sendKeys( entity.name );
                }

                // inserir o nome
                var firstNameInput = modalWindow.element(by.css('#firstName'));
                firstNameInput.sendKeys( entity.firstName );

                // inserir o apelido
                var lastNameInput = modalWindow.element(by.css('#lastName'));
                lastNameInput.sendKeys( entity.lastName );

                // inserir o email
                var email = modalWindow.element(by.css('#email'));
                email.sendKeys( entity.email );

                // checar para enviar email de ativacao
                var checkbox = modalWindow.element(by.css('input.js-send-activation-email'));
                generalHelper.waitElementClickableAndClick(checkbox);

                //verificar se inserido
                var records = element(by.css('.js-total-res span'));
                var recordCount = 0;
                records.getText().then(function(text) {
                    recordCount = parseInt(text);
                });

                generalHelper.waitElementClickableAndClick(saveButton);
                generalHelper.waitAlertSuccessPresentAndClose();

                records.getText().then(function(text) {
                    expect(parseInt(text)).toEqual(++recordCount);
                });

                browser.get( urlLogout );

            });


        });

    },


    // funcao para criar contabilista
    //
    createAccountant: function ( accountant, urlLogout ) {

        describe('Accountant', function () {

            var newButton,
                modalWindow,
                saveButton;

            beforeEach(function () {
                newButton = element(by.css('button.js-new-accountant'));
                modalWindow = element(by.css('#accountant-create-modal'));
                saveButton = modalWindow.element(by.css('button.btn-success.ok'));
            });

            it('should open new accounting firm modal window', function () {
                generalHelper.waitElementClickableAndClick(newButton);
                generalHelper.waitElementPresent(modalWindow);


                expect(modalWindow.isDisplayed()).toBe(true);
            });

            it('should fill mandatory fields and submit', function () {

                browser.sleep(1000);

                var firstNameInput = modalWindow.element(by.css('#firstName'));
                var lastNameInput = modalWindow.element(by.css('#lastName'));
                var emailInput = modalWindow.element(by.css('#email'));

                firstNameInput.sendKeys( accountant.firstName );
                lastNameInput.sendKeys( accountant.lastName ) ;
                emailInput.sendKeys( accountant.email );

                var records = element(by.css('.js-total-res span'));
                var recordCount = 0;
                records.getText().then(function (text) {
                    recordCount = parseInt(text);
                });

                generalHelper.waitElementClickableAndClick(saveButton);
                generalHelper.waitAlertSuccessPresentAndClose();

                records.getText().then(function (text) {
                    expect(parseInt(text)).toEqual(++recordCount);
                });

                browser.get( urlLogout );

            });

        });


    },



    // funcao para criar equipa
    //
    createTeam: function ( team, urlLogout )
    {

        describe('Team', function () {

            var tabItem,
                newButton,
                modalWindow,
                saveButton;

            beforeEach(function()
            {
                tabItem = element(by.css('#portal-admin-menu-region [data-sec="teams"]'));
                newButton = element(by.css('button.js-new-team'));
                modalWindow = element(by.css('#team-update-modal'));
                saveButton = modalWindow.element(by.css('button.btn-success.ok'));
            });

            it('should open new accounting firm modal window', function () {
                generalHelper.waitElementClickableAndClick(tabItem);
                generalHelper.waitElementClickableAndClick(newButton);
                generalHelper.waitElementPresent(modalWindow);

                expect(modalWindow.isDisplayed()).toBe(true);
            });

            it('should fill mandatory fields and submit', function() {

                var nameInput = element(by.css('#name'));

                nameInput.sendKeys( team.name ) ;

                var addAccountantButton = modalWindow.element(by.css('button.js-add-accountant'));

                generalHelper.waitElementClickableAndClick(addAccountantButton);

                var records = element(by.css('.js-total-res span'));
                var recordCount = 0;
                records.getText().then(function(text) {
                    recordCount = parseInt(text);
                });

                generalHelper.waitElementClickableAndClick(saveButton);
                generalHelper.waitAlertSuccessPresentAndClose();

                records.getText().then(function(text) {
                    expect(parseInt(text)).toEqual(++recordCount);
                });

                browser.get( urlLogout );
            });


        });


    },


    // funcao para criar Costumer Company
    //
    createCostumerCompany: function ( costumerCompany, urlLogout )
    {

        describe('Costumer Company', function () {

            var tabItem,
                newButton,
                modalWindow;

            beforeEach(function() {
                tabItem = element(by.css('#portal-admin-menu-region [data-sec="customercompanies"]'));
                newButton = element(by.css('button.js-new-customer-company'));
                modalWindow = element(by.css('#company-create-modal'));
            });
            
            it('should open new accounting firm modal window', function () {
                generalHelper.waitElementClickableAndClick(tabItem);
                generalHelper.waitElementClickableAndClick(newButton);
                generalHelper.waitElementPresent(modalWindow);

                expect(modalWindow.isDisplayed()).toBe(true);
            });

            it('should fill mandatory fields and submit', function() {
                var nameInput = modalWindow.element(by.css('.js-company-autocomplete'));

                generalHelper.selectTypeAheadCreateNew(nameInput, 'input#name','a');

                browser.sleep(2000);

                var sirenInput = modalWindow.element(by.css('#sirenNumber'));
                var sirenButton = modalWindow.element(by.css('div.js-siren-check button.js-check-vat'));

                sirenInput.sendKeys( costumerCompany.siren );

                generalHelper.waitElementClickableAndClick(sirenButton);

                browser.sleep(2000);

                var domainInput =  modalWindow.element(by.css('#subDomain'));

                domainInput.sendKeys( costumerCompany.subDomain );

                var firstNameInput = modalWindow.element(by.css('#firstName'));
                var lastNameInput = modalWindow.element(by.css('#lastName'));
                var emailInput = modalWindow.element(by.css('#managerEmail'));

                firstNameInput.sendKeys( costumerCompany.firstName );
                lastNameInput.sendKeys( costumerCompany.lastName );
                emailInput.sendKeys( costumerCompany.email );

                var checkbox = modalWindow.element(by.css('input.js-send-activation-email'));

                generalHelper.waitElementClickableAndClick(checkbox);

                var teamDropdown = modalWindow.element(by.css('button.dropdown-toggle.selectpicker[data-id="team"]'));

                generalHelper.waitElementClickableAndClick(teamDropdown);

                var teamSelect = modalWindow.element(by.css('div.dropdown-menu.open ul.dropdown-menu.inner li[data-original-index="1"] a'));

                generalHelper.waitElementClickableAndClick(teamSelect);

                var saveButton = modalWindow.element(by.css('button.btn-success.ok'));

                var records = element(by.css('.js-total-res span'));
                var recordCount = 0;
                records.getText().then(function(text) {
                    recordCount = parseInt(text);
                });

                generalHelper.waitElementClickableAndClick(saveButton);
                generalHelper.waitAlertSuccessPresentAndClose();

                records.getText().then(function(text) {
                    expect(parseInt(text)).toEqual(++recordCount);
                });

                browser.get( urlLogout );

            });

        });


    }



};


