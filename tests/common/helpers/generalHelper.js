module.exports = {
    waitTimeout: function() {
        return 10000;
    },
    waitElementClickableAndClick: function (element) {
        return browser.wait(function () {
            return element.click().then(
                function () {
                    return true;
                },
                function () {
                    return false;
                });
        }, this.waitTimeout());
    },
    waitElementPresent: function (element) {
        return browser.wait(function () {
            return element.isPresent().then(function (present) {
                return present;
            });
        }, this.waitTimeout());
    },
    waitElementNotPresent: function (element) {
        browser.wait(function () {
            return element.isPresent().then(function (present) {
                return !present;
            });
        }, this.waitTimeout());
    },
    waitElementDisplayed: function (element) {
        browser.wait(function () {
            return element.isDisplayed().then(function (displayed) {
                return displayed;
            });
        }, this.waitTimeout());
    },
    waitAlertSuccessPresent: function () {
        var alertSuccess = element(by.css('.alert-success'));
        this.waitElementPresent(alertSuccess);
    },
    waitAlertDangerPresent: function () {
        var alertDanger = element(by.css('.alert-danger'));
        return this.waitElementPresent(alertDanger);
    },
    waitAlertDangerPresentAndClose: function () {
        this.waitAlertDangerPresent();
        var alertDanger = element(by.css('.alert-danger'));
        var closeNotify = alertDanger.element(by.css('.ui-pnotify-closer'));
        var usernameMenu = element(by.css('#username'));
        browser.actions().mouseMove(usernameMenu).perform();
        browser.actions().mouseMove(alertDanger).perform();
        this.waitElementClickableAndClick(closeNotify);
        this.waitElementNotPresent(alertDanger);
    },
    waitAlertSuccessPresentAndClose: function () {
        this.waitAlertSuccessPresent();
        var alertSuccess = element(by.css('.alert-success'));
        var closeNotify = alertSuccess.element(by.css('.ui-pnotify-closer'));
        var usernameMenu = element(by.css('#username'));
        browser.actions().mouseMove(usernameMenu).perform();
        browser.actions().mouseMove(alertSuccess).perform();
        this.waitElementClickableAndClick(closeNotify);
        this.waitElementNotPresent(alertSuccess);
    },
    baseUrlWithSubdomain: function (subDomain) {
        return 'http://' + subDomain + '.' + browser.baseUrl;
    },
    testProfile: function (firstNameText, lastNameText) {
        var self = this;
        return browser.wait(function () {
            //click open profile
            var usernameMenu = element(by.css('#username'));
            self.waitElementClickableAndClick(usernameMenu);
            var profileButton = element(by.css('.js-user-profile'));
            self.waitElementClickableAndClick(profileButton);
            //wait open
            var modalProfile = element(by.css('#profile-update-modal'));
            var firstNameInput = modalProfile.element(by.css('#firstName'));
            var lastNameInput = modalProfile.element(by.css('#lastName'));
            self.waitElementPresent(firstNameInput);
            self.waitElementDisplayed(firstNameInput);
            //change first and last name
            firstNameInput.clear().sendKeys(firstNameText);
            lastNameInput.clear().sendKeys(lastNameText);
            //click save
            var saveButton = modalProfile.element(by.css('.btn-success'));
            self.waitElementClickableAndClick(saveButton);
            //wait request
            self.waitAlertSuccessPresentAndClose();
            //see name changed topbar
            expect(usernameMenu.getText()).toEqual(firstNameText + " " + lastNameText);
            return true;
        }, this.waitTimeout());
    },
    waitForBothAppearDisappear: function (element1, element2) {
        var ele1Found = false; //if true element 1 found first, else element 2 found first (no timeout triggered)
        browser.wait(function () {
            return element1.isPresent().then(function (present1) {
                return element2.isPresent().then(function (present2) {
                    var anyFound = present1 || present2;
                    if (anyFound) {
                        ele1Found = present1;
                    }
                    return anyFound;
                });
            });
        }, this.waitTimeout());

        var auxEle1, auxEle2;
        if (ele1Found) {
            auxEle1 = element1;
            auxEle2 = element2;
        } else {
            auxEle1 = element2;
            auxEle2 = element1;
        }

        //wait for the one that appeared to disappear or the one that did not appear yet to disappear
        var bothPresent = false;
        browser.wait(function () {
            return auxEle1.isPresent().then(function (present1) {
                return auxEle2.isPresent().then(function (present2) {
                    var shouldPass = !present1 || present2;
                    if(shouldPass) {
                        bothPresent = present1 && present2;
                    }
                    return shouldPass;
                });
            });
        }, this.waitTimeout());

        if (bothPresent) {
            //wait for both to disappear
            browser.wait(function () {
                return auxEle1.isPresent().then(function (present1) {
                    return auxEle2.isPresent().then(function (present2) {
                        return !present1 && !present2;
                    });
                });
            }, this.waitTimeout());
        } else {
            //auxEle1 appeared and disappeared, wait for auxEle2 to appear and disappear
            this.waitElementPresent(auxEle2);
            this.waitElementNotPresent(auxEle2);
        }
    },
    selectTypeAheadItem: function (container, inputSelector, inputText, numberOfGroups) {
        var resultGroupsSelector = '.tt-dataset-results';
        var selectablesSelector = '.tt-selectable';

        var input = container.element(by.css(inputSelector));
        var resultGroups = container.all(by.css(resultGroupsSelector));

        this.waitElementPresent(input);
        this.waitElementDisplayed(input);

        input.click();

        browser.wait(function () {
            return resultGroups.count().then(function (count) {
                return count === numberOfGroups;
            })
        });

        input.sendKeys(inputText);

        var selectables = container.all(by.css(resultGroupsSelector + " " + selectablesSelector));

        if(numberOfGroups === 1) {
            this.waitElementPresent(resultGroups.get(0));
            this.waitElementDisplayed(resultGroups.get(0));
        } else if(numberOfGroups === 2) {
            //wait for both present
            browser.wait(function () {
                return resultGroups.get(0).isPresent().then(function (present1) {
                    return resultGroups.get(1).isPresent().then(function (present2) {
                        return (present1 && present2);
                    });
                });
            }, this.waitTimeout());
        } else {
            var message = '. CASE NOT FORESEEN.';
            if(numberOfGroups < 1) {
                message += 'Number of groups is less than 1 which is not possible.';
            } else {
                message += 'Number of groups is greater than 2 and this function is not prepared for more than 2 groups of results.';
            }
            pending(message);
        }

        browser.wait(function() {
            return selectables.count().then(function (count) {
                return count > 0;
            });
        });
        browser.executeScript("$('" + resultGroupsSelector + " " + selectablesSelector + "').click()");
    },

    selectTypeAheadCreateNew: function (container, inputSelector) {
        var inputText = 'a';

        var targetGroupSelector = '.tt-dataset-new';
        var targetSelector = '.tt-selectable';

        var input = container.element(by.css(inputSelector));

        this.waitElementPresent(input);
        this.waitElementDisplayed(input);

        input.click();

        input.sendKeys(inputText);

        browser.executeScript("$('" + targetGroupSelector + " " + targetSelector + "').click()");
    },
    
    returnToDashboard: function() {
        browser.getCurrentUrl().then(function(url){
            var str = url.toString();
            var array = str.split('#');
            var newURL = array[0];
            browser.get(newURL + '#dashboard');
        });
    },

    selectArea: function(document, coordsInit, coordsFinal) {

            browser.actions()
                .mouseMove(document)
                .perform();

            browser.actions()
                .mouseMove(coordsInit)
                .perform();

            browser.actions()
                .mouseDown()
                .perform();

            browser.actions()
                .mouseMove(coordsFinal)
                .perform();

            browser.actions()
                .mouseUp()
                .perform();

            browser.sleep(2000);

    }
};