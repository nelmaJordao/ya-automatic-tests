module.exports = {
    
    users: {
        //CORE
        PLATFORMADMIN:          { username: "pa@yesaccount.fr",     password: "xptoxpto",   subdomain: "admin",     role:"PLATFORMADMIN" },
        PARTNER:                { username: "test.par1@xpto.biz",   password: "xptoxpto",   subdomain: "admin",     role:"PARTNER" },
        SUPERADMIN:             { username: "holding1@xpto.biz",    password: "xptoxpto",   subdomain: "admin",     role:"SUPERADMIN" },
        //APP
        PORTALADMIN:            { username: "test.sene@xpto.biz",   password: "xptoxpto",   subdomain: "admin",     role:"PORTALADMIN" },
        OUTSOURCEMANAGER:       { username: "tag.1@xpto.biz",       password: "xptoxpto",   subdomain: "admin",     role:"OUTSOURCEMANAGER" },
        CHIEFACCOUNTANT:        { username: "sene.expert@xpto.biz", password: "xptoxpto",   subdomain: "admin",     role:"CHIEFACCOUNTANT" },
        ACCOUNTANT:             { username: "test.sene@xpto.biz",   password: "xptoxpto",   subdomain: "admin",     role:"ACCOUNTANT" },
        ASSISTANTACCOUNTANT:    { username: "test.sene@xpto.biz",   password: "xptoxpto",   subdomain: "admin",     role:"ASSISTANTACCOUNTANT" },
        MANAGER:                { username: "test.polo@xpto.biz",   password: "xptoxpto",   subdomain: "testpolo",  role:"MANAGER" },
        ASSISTANTMANAGER:       { username: "test.sene@xpto.biz",   password: "xptoxpto",   subdomain: "admin",     role:"ASSISTANTMANAGER" },
        EMPLOYEE:               { username: "wilo.emp@xpto.biz",    password: "xptoxpto",   subdomain: "admin",     role:"EMPLOYEE" },
        OUTSOURCETAGGER:        { username: "tag.1.1@xpto.biz",     password: "xptoxpto",   subdomain: "admin",     role:"OUTSOURCETAGGER" },
        //FOR CREATION SCRIPTS
        TESTCENTRE:             { username: "test.centre@xpto.biz", password: "xptoxpto",   subdomain: "testcentre",role:"PORTALADMIN" },
        TESTSENE:               { username: "test.sene@xpto.biz",   password: "xptoxpto",   subdomain: "testsene",  role:"PORTALADMIN" },
        TESTPOLO:               { username: "test.polo@xpto.biz",   password: "xptoxpto",   subdomain: "testpolo",  role:"MANAGER" },
        TESTPALMA:              { username: "test.palma@xpto.biz",  password: "xptoxpto",   subdomain: "testpalma", role:"MANAGER" },
        TESTARTECH:             { username: "test.artech@xpto.biz", password: "xptoxpto",   subdomain: "artech",    role:"MANAGER"},
        TESTTAG:                { username: "test.tag1@xpto.biz",   password: "xptoxpto",   subdomain: "tag",       role:"OUTSOURCETAGGER"},

        TESTPEDRO:              { username: "ps@void.pt",           password: "xptoxpto",   subdomain: "testpolo",  role:"EMPLOYEE" },
        
        TEST:                   { username: "accnelmah@xpto.biz",   password: "supretest",   subdomain: "acfirmholding",  role:"ACCOUNTANT" },
        TESTJACA:               { username: "test.jaca@xpto.biz",   password: "xptoxpto",   subdomain: "testjaca",  role:"MANAGER" },

        //For profile test (password change)
        TESTPOLO_PASSCHANGED:   { username: "test.polo@xpto.biz",   password: "qwerty",     subdomain: "testpolo",  role:"MANAGER" },
        TESTACCNELMA_PASSCHANGED:   { username: "accnelmah@xpto.biz",   password: "qwerty",   subdomain: "acfirmholding",  role:"ACCOUNTANT" }
    },

    inputs: {
        largeText:  'Lorem ipsum dolor sit amet consectetur adipiscing elit Integer rutrum blandit porta Cras lobortis enim lectus Nam commodo ' +
                    'magna vitae molestie fermentum quam odio interdum enim sit amet feugiat ex magna eget ipsum Proin ornare iaculis semper Mauris ' +
                    'ultricies massa massa vel mollis orci tempus ut Suspendisse potenti In lorem neque egestas nec dignissim ut sodales imperdiet tellus ' +
                    'Nam in pretium felis Proin vel augue a urna vulputate congue id sed justo Duis mi odio condimentum ac ipsum id volutpat bibendum elit ' +
                    'Cras nec lectus nec neque imperdiet dictum Cras a faucibus augue sed dignissim tortor In eu diam congue ullamcorper risus vel tincidunt sem' +
                    'Maecenas rhoncus efficitur iaculis In ut arcu nec tortor aliquam iaculis vitae in velit Vestibulum a semper magna Quisque mollis maximus luctus ' +
                    'Morbi a egestas felis Mauris sit amet nisi vestibulum fringilla eros ornare tincidunt magna Pellentesque euismod lacus in lectus blandit volutpat ' +
                    'Etiam tincidunt interdum purus vitae finibus felis ultrices ut Suspendisse nec mattis sem at tincidunt ex' +
                    'Sed at sagittis tellus Praesent aliquam libero fermentum tortor finibus placerat Sed et malesuada justo Fusce vitae accumsan arcu Sed ultrices ' +
                    'fringilla rutrum Vivamus vulputate turpis in sodales lacinia Cras ut magna ante Aenean vitae ullamcorper metus sit amet tempus justo Aliquam at ' +
                    'finibus tortor sed euismod velit Sed porttitor eu eros ultricies aliquet Nullam consequat mi at urna vestibulum sollicitudin laoreet sed nisl ' +
                    'Pellentesque auctor accumsan quam vitae tempus Cras id hendrerit orci ' +
                    'Pellentesque cursus vestibulum lorem id vestibulum Morbi eget urna ut velit mollis bibendum non a nunc Maecenas id ante ante Aliquam mollis orci ut ' +
                    'ante dictum vel finibus velit luctus Maecenas finibus arcu sed ligula facilisis non vulputate nulla fermentum Fusce scelerisque mi ut convallis ' +
                    'porttitor Nam sagittis sagittis dignissim Proin tincidunt eros sit amet turpis tincidunt dapibus ' +
                    'Etiam et lacus viverra facilisis diam eu facilisis elit Etiam eget vehicula ante Proin iaculis ipsum a nunc congue et rutrum ligula tempor Cras ' +
                    'tempus quis risus in ultrices Nulla interdum semper nunc nec ultrices magna luctus in Duis placerat quis ipsum nec feugiat Donec volutpat molestie ' +
                    'auctor Vestibulum ullamcorper vel purus eget sagittis Ut nec orci tortor Proin nec convallis enim Morbi molestie justo porta iaculis ornare Nullam ' +
                    'elit magna dapibus sit amet justo id rutrum faucibus felis Curabitur imperdiet egestas arcu tincidunt tristique In interdum accumsan dui '+
                    'Lorem ipsum dolor sit amet consectetur adipiscing elit Integer rutrum blandit porta Cras lobortis enim lectus Nam commodo ' +
                    'magna vitae molestie fermentum quam odio interdum enim sit amet feugiat ex magna eget ipsum Proin ornare iaculis semper Mauris ' +
                    'ultricies massa massa vel mollis orci tempus ut Suspendisse potenti In lorem neque egestas nec dignissim ut sodales imperdiet tellus ' +
                    'Nam in pretium felis Proin vel augue a urna vulputate congue id sed justo Duis mi odio condimentum ac ipsum id volutpat bibendum elit ' +
                    'Cras nec lectus nec neque imperdiet dictum Cras a faucibus augue sed dignissim tortor In eu diam congue ullamcorper risus vel tincidunt sem' +
                    'Maecenas rhoncus efficitur iaculis In ut arcu nec tortor aliquam iaculis vitae in velit Vestibulum a semper magna Quisque mollis maximus luctus ' +
                    'Morbi a egestas felis Mauris sit amet nisi vestibulum fringilla eros ornare tincidunt magna Pellentesque euismod lacus in lectus blandit volutpat ' +
                    'Etiam tincidunt interdum purus vitae finibus felis ultrices ut Suspendisse nec mattis sem at tincidunt ex' +
                    'Sed at sagittis tellus Praesent aliquam libero fermentum tortor finibus placerat Sed et malesuada justo Fusce vitae accumsan arcu Sed ultrices ' +
                    'fringilla rutrum Vivamus vulputate turpis in sodales lacinia Cras ut magna ante Aenean vitae ullamcorper metus sit amet tempus justo Aliquam at ' +
                    'finibus tortor sed euismod velit Sed porttitor eu eros ultricies aliquet Nullam consequat mi at urna vestibulum sollicitudin laoreet sed nisl ' +
                    'Pellentesque auctor accumsan quam vitae tempus Cras id hendrerit orci ' +
                    'Pellentesque cursus vestibulum lorem id vestibulum Morbi eget urna ut velit mollis bibendum non a nunc Maecenas id ante ante Aliquam mollis orci ut ' +
                    'ante dictum vel finibus velit luctus Maecenas finibus arcu sed ligula facilisis non vulputate nulla fermentum Fusce scelerisque mi ut convallis ' +
                    'porttitor Nam sagittis sagittis dignissim Proin tincidunt eros sit amet turpis tincidunt dapibus ' +
                    'Etiam et lacus viverra facilisis diam eu facilisis elit Etiam eget vehicula ante Proin iaculis ipsum a nunc congue et rutrum ligula tempor Cras ' +
                    'tempus quis risus in ultrices Nulla interdum semper nunc nec ultrices magna luctus in Duis placerat quis ipsum nec feugiat Donec volutpat molestie ' +
                    'auctor Vestibulum ullamcorper vel purus eget sagittis Ut nec orci tortor Proin nec convallis enim Morbi molestie justo porta iaculis ornare Nullam ' +
                    'elit magna dapibus sit amet justo id rutrum faucibus felis Curabitur imp',


        getDescription: function(length) {
            if (length !== undefined) return this.largeText.substr(0, length);
            return this.largeText;
        },
        
        getRandomName: function(length, spaces)
        {
            // length - número de caracteres do nome aleatório
            // spaces - boleano que indica se o nome deve ou não conter espaçoes


            if (length === undefined) length = 10;
            if (spaces === undefined) spaces = true;
            var append = "";

            var randomName = '';
            for (var i = 0; i < length; i++)
            {
                append = this.largeText[Math.floor(Math.random() * 1000)];      // console.log('\n append : ' + append);

                if ( spaces || (!spaces && append != " ") )
                    randomName += append;
                else
                    i--;

            }

            return randomName;
        }

    }
    
};