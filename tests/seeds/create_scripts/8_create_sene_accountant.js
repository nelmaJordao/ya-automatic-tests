describe('Portal Admin', function () {
    var config = requireConfig();
    var accounts = requireAccountsConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTSENE.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.TESTSENE.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTSENE);
    });

    /*
    var accountant = {
        firstName: "teste",
        lastName: "sene",
        email: "test.sene@xpto.biz"
    };
    */

    seedHelper.createAccountant( accounts.holdingAccountant , baseURL + '#logout');
});