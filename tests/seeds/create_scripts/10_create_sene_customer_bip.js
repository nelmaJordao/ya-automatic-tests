describe('Admin', function () {
    var config = requireConfig();
    var accounts = requireAccountsConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTSENE.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.TESTSENE.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTSENE);
    });

    /*
    var costumerCompany = { siren : '382592160',
                            subDomain : 'testbip',
                            firstName : 'test',
                            lastName : 'bip',
                            email : 'test.bip@xpto.biz' };
    */

    seedHelper.createCostumerCompany( accounts.holdingCostumerCompany, baseURL + '#logout' );

});