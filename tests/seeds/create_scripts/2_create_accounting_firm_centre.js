describe('Admin', function () {
    var config = requireConfig();
    var accounts = requireAccountsConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.PARTNER.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.PARTNER.role, function () {
        loginHelper.loginWith(baseURL, config.users.PARTNER);
    });

    /*
    var accountingFirm = {  siren : '481344687',
        alias : 'Centre',
        subDomain : 'testcentre',
        firstName : 'test',
        lastName : 'centre',
        email : 'test.centre@xpto.biz'};
        */


    seedHelper.createAccountingFirm( accounts.partnerAccountingFirm, baseURL + '#logout' );

});