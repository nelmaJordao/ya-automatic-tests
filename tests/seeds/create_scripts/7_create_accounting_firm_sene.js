describe('Admin', function () {
    var config = requireConfig();
    var accounts = requireAccountsConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.SUPERADMIN.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.SUPERADMIN.role, function () {
        loginHelper.loginWith(baseURL, config.users.SUPERADMIN);
    });

    /*
    var accountingFirm = {  siren : '524070281',
        alias : 'Sene',
        subDomain : 'testsene',
        firstName : 'test',
        lastName : 'sene',
        email : 'test.sene@xpto.biz'};
        */


    seedHelper.createAccountingFirm( accounts.holdingAccountingFirm, baseURL + '#logout'  );
    // dá timeout ao gravar :s

});