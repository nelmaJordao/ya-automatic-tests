module.exports = {


        // Informações para criação da estrutura relativa ao partner
        //
        partner: {
            firstName: "partner",
            lastName: "um",
            email: "test.par1@xpto.biz",
            type: "partner"
        },

        partnerAccountingFirm: {
            siren: '481344687',
            alias: 'Centre',
            subDomain: 'testcentre',
            firstName: 'test',
            lastName: 'centre',
            email: 'test.centre@xpto.biz',
            type: 'holding'
        },

        partnerAccountant: {
            firstName: "teste",
            lastName: "centre",
            email: "test.centre@xpto.biz"
        },

        partnerTeam: {
            name: "Centre team"
        },

        partnerCostumerCompany: {
            siren: '398528208',
            subDomain: 'testpolo',
            firstName: 'test',
            lastName: 'polo',
            email: 'test.polo@xpto.biz'
        },

        // Informações para criação da estrutura relativa á holding
        //
        holding: {
            name: "holding um",
            firstName: "holding",
            lastName: "um",
            email: "holding1@xpto.biz",
            type: 'holding'
        },

        holdingAccountingFirm: {
            siren: '524070281',
            alias: 'Sene',
            subDomain: 'testsene',
            firstName: 'test',
            lastName: 'sene',
            email: 'test.sene@xpto.biz',
            type: 'partner'
        },

        holdingAccountant: {
            firstName: "teste",
            lastName: "sene",
            email: "test.sene@xpto.biz"
        },

        holdingTeam: {
            name: "Sene team"
        },

        holdingCostumerCompany: {
            siren: '382592160',
            subDomain: 'testbip',
            firstName: 'test',
            lastName: 'bip',
            email: 'test.bip@xpto.biz'
        }

};





