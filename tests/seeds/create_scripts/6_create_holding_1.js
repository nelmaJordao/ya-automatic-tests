describe('Admin', function () {
    var config = requireConfig();
    var accounts = requireAccountsConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.PLATFORMADMIN.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.PLATFORMADMIN.role, function () {
        loginHelper.loginWith(baseURL, config.users.PLATFORMADMIN);
    });

    /*
    var holding = { name : "holding um",
                    firstName : "holding",
                    lastName : "um",
                    email : "holding1@xpto.biz",
                    type : 'holding' };
    */

    seedHelper.createPartnerOrHolding( accounts.holding, baseURL + '#logout' );

});