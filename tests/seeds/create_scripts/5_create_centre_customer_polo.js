describe('Admin', function () {
    var config = requireConfig();
    var accounts = requireAccountsConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTCENTRE.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.TESTCENTRE.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTCENTRE);
    });

    /*
    var costumerCompany = { siren : '398528208 ',
                            subDomain : 'testpolo',
                            firstName : 'test',
                            lastName : 'polo   ',
                            email : 'test.polo@xpto.biz' };
*/

    seedHelper.createCostumerCompany( accounts.partnerCostumerCompany, baseURL + '#logout' );

});