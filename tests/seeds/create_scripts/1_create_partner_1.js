describe('Admin', function () {
    var config = requireConfig();
    var accounts = requireAccountsConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.PLATFORMADMIN.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.PLATFORMADMIN.role, function () {
        loginHelper.loginWith(baseURL, config.users.PLATFORMADMIN);
    });

    /*
    var partner = { name : "partner um",
                    firstName : "partner",
                    lastName : "um",
                    email : "test.par1@xpto.biz",
                    type : 'partner'
    }
    */

    /*
    console.log("TESTE");

    console.log("\n name - " + accounts.partner.name +
                "\n firstName - " + accounts.partner.firstName +
                "\n lastName - " + accounts.partner.lastName +
                "\n email - " + accounts.partner.email +
                "\n type - " + accounts.partner.type );
    */

    
    seedHelper.createPartnerOrHolding( accounts.partner , baseURL + '#logout');

});