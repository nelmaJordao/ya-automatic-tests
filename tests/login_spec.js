describe('Login', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    var user = config.users.TEST;

    var baseURL = generalHelper.baseUrlWithSubdomain(user.subdomain);

    browser.ignoreSynchronization = true;

    browser.get(baseURL);

    it('should login with ' + user.username + ' as ' + user.role, function () {
        var login = element(by.css('js-login-window'));
        if(login !== undefined) {
            loginHelper.login(user);
            browser.sleep(1000);
        }
        //loginHelper.changeRoleTo("EMPLOYEE");
        
        loginHelper.changeCustomerCompanyTo("BIG BANG");
    });

});
 