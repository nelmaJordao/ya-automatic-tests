describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            masterPlanItem,
            invalidButton,
            company,
            newCompany,
            newButton,
            modalWindowCreate,
            codeCompte,
            description,
            okButtonCreate,
            tabInformations,
            vats,
            accPlan,
            firstRow,
            editButton,
            modalWindow,
            okButton,
            modalWindowDelete,
            deleteAllButton,
            okButtonDelete,
            searchField,
            searchButton,
            secondRowMaster,
            deleteButton,
            modalWindowDeleteElement,
            okButtonDeleteElement;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="acc_settings"]'));
            masterPlanItem = element(by.css('#customer-admin-main-region [data-name="confs"] a'));
            invalidButton = element(by.css('.js-toggle'));
            company= element(by.css('#customercompany'));
            newCompany = company.element(by.xpath('/html/body/div[1]/div[1]/div/nav/div[3]/ul/li[6]/ul/li[2]'));
            newButton = element(by.css('.js-new-acc-plan-item'));
            modalWindowCreate = element(by.css('#accounting-line-add-modal'));
            codeCompte = modalWindowCreate.element(by.css('#accCode'));
            description = modalWindowCreate.element(by.css('#description'));
            okButtonCreate = modalWindowCreate.element(by.css('.btn-success.ok'));
            tabInformations = element(by.css('#customer-admin-menu-region [data-sec="settings"]'));
            vats = element(by.css('#customer-admin-main-region [data-name="vats"]'));
            accPlan = element(by.css('#customer-admin-main-region [data-name="acc-vats"] a'));
            firstRow = element.all(by.css('#acc-firm-vat-lines-dtable tr')).get(0);
            editButton = firstRow.element(by.css('.js-edit-vat-line'));
            modalWindow = element(by.css('#edit-vat-line-code-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            modalWindowDelete = element(by.css('#accounting-configs-delete-modal'));
            deleteAllButton = element(by.css('.js-delete-vat-accounting-lines'));
            okButtonDelete = modalWindowDelete.element(by.css('.btn-success.ok'));
            searchField = element(by.css('.js-search-input'));
            searchButton = element(by.css('.js-search-acc-plan'));
            secondRowMaster = element.all(by.css('#acc-plan-table tr')).get(1);
            deleteButton = secondRowMaster.element(by.css('.js-delete-acc-plan-item'));
            modalWindowDeleteElement = element(by.css('#acc-code-delete-modal'));
            okButtonDeleteElement = modalWindowDeleteElement.element(by.css('.btn-success.ok'));



        });

        it('should open master plan modal window', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(masterPlanItem);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            generalHelper.waitElementClickableAndClick(company);

            generalHelper.waitElementClickableAndClick(newCompany);

            browser.sleep(1000);

            //generalHelper.waitElementClickableAndClick(invalidButton);

            //browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(newButton);

            generalHelper.waitElementPresent(modalWindowCreate);

            expect(modalWindowCreate.isDisplayed()).toBe(true);




        });


        it('should check for empty fields', function ()
        {

            generalHelper.waitElementClickableAndClick(okButtonCreate);

            var errors = modalWindowCreate.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(2);

        });

        it('should test field restrictions', function () {

            codeCompte.sendKeys('1');

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButtonCreate);

            var errors = modalWindowCreate.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(2);

            //---------------------//------------------------------//

            description.sendKeys('Code 1');

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButtonCreate);

            expect(errors.count()).toEqual(1);

            codeCompte.clear();
            description.clear();

        });


        it('should fill mandatory fields and submit', function()
        {

            // inserir code de compte
            codeCompte.sendKeys('44510000');

            // inserir a description
            description.sendKeys('Code 44510000');


            // verificar se foi bem introduzido
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButtonCreate);
            generalHelper.waitAlertSuccessPresentAndClose();

            browser.sleep(1000);

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(recordCount++);
            });

        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


        it('should open acc plan edit modal window', function ()
        {
            //preparation
            //action
            //assertion
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabInformations);
            generalHelper.waitElementClickableAndClick(vats);

            //Check TVAs

            var rowZero = element.all(by.css('table.table-striped.table-hover tbody tr')).get(0);
            var rowOne = element.all(by.css('table.table-striped.table-hover tbody tr')).get(1);
            var rowTwo = element.all(by.css('table.table-striped.table-hover tbody tr')).get(2);
            var rowThree = element.all(by.css('table.table-striped.table-hover tbody tr')).get(3);
            var rowFour = element.all(by.css('table.table-striped.table-hover tbody tr')).get(4);
            var rowFive = element.all(by.css('table.table-striped.table-hover tbody tr')).get(5);
            var rowSix = element.all(by.css('table.table-striped.table-hover tbody tr')).get(6);


            var checkboxZero = rowZero.element(by.css('td.options label.checkbox span'));
            var checkboxOne = rowOne.element(by.css('td.options label.checkbox span'));
            var checkboxTwo = rowTwo.element(by.css('td.options label.checkbox span'));
            var checkboxThree = rowThree.element(by.css('td.options label.checkbox span'));
            var checkboxFour = rowFour.element(by.css('td.options label.checkbox span'));
            var checkboxFive = rowFive.element(by.css('td.options label.checkbox span'));
            var checkboxSix = rowSix.element(by.css('td.options label.checkbox span'));

            //checkbox.click();
            generalHelper.waitElementClickableAndClick(checkboxZero);
            generalHelper.waitElementClickableAndClick(checkboxOne);
            generalHelper.waitElementClickableAndClick(checkboxTwo);
            generalHelper.waitElementClickableAndClick(checkboxThree);
            generalHelper.waitElementClickableAndClick(checkboxFour);
            generalHelper.waitElementClickableAndClick(checkboxFive);
            generalHelper.waitElementClickableAndClick(checkboxSix);


            //Edit

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(accPlan);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            var searchCompte = element(by.css('.js-search-input'));
            searchCompte.sendKeys('44510000');

            var searchButtonAcc = element(by.css('.js-search-vat-lines'));
            generalHelper.waitElementClickableAndClick(searchButtonAcc);


            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);


            expect(modalWindow.isDisplayed()).toBe(true);


        });



        it('should fill mandatory fields and submit', function()
        {

            var addVat = modalWindow.element(by.css('.js-add-vat'));

            generalHelper.waitElementClickableAndClick(addVat);

            var selectPickerType = modalWindow.element(by.css('[data-id="pickVat"]'));
            var selectPickerOption = modalWindow.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));

            generalHelper.waitElementClickableAndClick(selectPickerType);
            generalHelper.waitElementClickableAndClick(selectPickerOption);

            var lineCode = modalWindow.element(by.css('.js-line-code'));
            lineCode.sendKeys('TVA 20');

            browser.sleep(1000);

            var removeVat = element(by.css('.js-remove-vat'));
            generalHelper.waitElementClickableAndClick(removeVat);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(addVat);

            generalHelper.waitElementClickableAndClick(selectPickerType);
            generalHelper.waitElementClickableAndClick(selectPickerOption);

            lineCode.sendKeys('TVA 20');

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);
            
            
            
            //Apagar tvas

            generalHelper.waitElementClickableAndClick(deleteAllButton);

            browser.sleep(1000);

            generalHelper.waitElementPresent(modalWindowDelete);

            expect(modalWindowDelete.isDisplayed()).toBe(true);


            generalHelper.waitElementClickableAndClick(okButtonDelete);
            generalHelper.waitAlertSuccessPresentAndClose();

            browser.sleep(1000);


            var tvaCell = firstRow.all(by.css('td')).get(2);

            tvaCell.getAttribute('data-content').then(function (text) {
                expect(text).toEqual(null);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });



        it('should uncheck checkbox', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabInformations);
            generalHelper.waitElementClickableAndClick(vats);

            //Check TVAs

            var rowZero = element.all(by.css('table.table-striped.table-hover tbody tr')).get(0);
            var rowOne = element.all(by.css('table.table-striped.table-hover tbody tr')).get(1);
            var rowTwo = element.all(by.css('table.table-striped.table-hover tbody tr')).get(2);
            var rowThree = element.all(by.css('table.table-striped.table-hover tbody tr')).get(3);
            var rowFour = element.all(by.css('table.table-striped.table-hover tbody tr')).get(4);
            var rowFive = element.all(by.css('table.table-striped.table-hover tbody tr')).get(5);
            var rowSix = element.all(by.css('table.table-striped.table-hover tbody tr')).get(6);


            var checkboxZero = rowZero.element(by.css('td.options label.checkbox span'));
            var checkboxOne = rowOne.element(by.css('td.options label.checkbox span'));
            var checkboxTwo = rowTwo.element(by.css('td.options label.checkbox span'));
            var checkboxThree = rowThree.element(by.css('td.options label.checkbox span'));
            var checkboxFour = rowFour.element(by.css('td.options label.checkbox span'));
            var checkboxFive = rowFive.element(by.css('td.options label.checkbox span'));
            var checkboxSix = rowSix.element(by.css('td.options label.checkbox span'));

            //checkbox.click();

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(checkboxZero);
            generalHelper.waitElementClickableAndClick(checkboxOne);
            generalHelper.waitElementClickableAndClick(checkboxTwo);
            generalHelper.waitElementClickableAndClick(checkboxThree);
            generalHelper.waitElementClickableAndClick(checkboxFour);
            generalHelper.waitElementClickableAndClick(checkboxFive);
            generalHelper.waitElementClickableAndClick(checkboxSix);

        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });



        it('should open master plan delete modal window', function ()
        {
            //preparation
            //action
            //assertion

            generalHelper.waitElementClickableAndClick(menuIcon);

            generalHelper.waitElementClickableAndClick(tabItem);

            generalHelper.waitElementClickableAndClick(masterPlanItem);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            //generalHelper.waitElementClickableAndClick(invalidButton);

            browser.sleep(1000);

            searchField.clear();
            searchField.sendKeys('445');

            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementClickableAndClick(deleteButton);
            generalHelper.waitElementPresent(modalWindowDeleteElement);

            expect(modalWindowDeleteElement.isDisplayed()).toBe(true);

        });

        it('should confirm delete modal window', function()
        {
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButtonDeleteElement);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text)
            {
                expect(parseInt(text)).toEqual(--recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});
