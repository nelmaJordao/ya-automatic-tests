describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            accPlan,
            company,
            newCompany,
            searchField,
            searchButton;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="acc_settings"]'));
            accPlan = element(by.css('#customer-admin-main-region [data-name="acc-vats"] a'));
            company = element(by.css('#customercompany'));
            newCompany = company.element(by.xpath('/html/body/div[1]/div[1]/div/nav/div[3]/ul/li[6]/ul/li[2]'));
            searchField = element(by.css('.js-search-input'));
            searchButton = element(by.css('.js-search-vat-lines'));
        });

        it('should search', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(accPlan);
            generalHelper.waitElementClickableAndClick(company);
            generalHelper.waitElementClickableAndClick(newCompany);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            searchField.clear();
            searchField.sendKeys('etat');

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });


            generalHelper.waitElementClickableAndClick(searchButton);

            browser.sleep(1000);

            records.getText().then(function(text) {
                expect(parseInt(text)).not.toEqual(recordCount);
            });

        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});