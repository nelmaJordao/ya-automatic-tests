describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            vatCatSalesItem,
            invalidButton,
            okButton,
            accPlanItem;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="acc_settings"]'));
            vatCatSalesItem = element(by.css('#customer-admin-main-region [data-name="vat-cat-sales"] a'));
            invalidButton = element(by.css('.js-toggle'));
            okButton = element(by.css('.js-save-vat-cat-sales'));
            accPlanItem = element(by.css('#customer-admin-main-region [data-name="acc-vats"] a'));
        });

        it('should open vat cat sales item', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(vatCatSalesItem);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido
        });


        it('should activate compte', function ()
        {
            var compteNumber = "44520000";

            //var services = element(by.xpath('/html/body/div/div[3]/div/div[1]/div/div[2]/div/div[2]/div/div/div/div/div[3]/div/div/div'));
            //var hiddenTwoPointOne = element.all(by.css('#pick_2.1')).get(1);
            var twoPointOne = element(by.xpath('/html/body/div/div[3]/div/div[1]/div/div[2]/div/div[2]/div/div/div/div/div[3]/div/div/div/div[3]/div/div[2]/div[2]/div/div'));


            generalHelper.selectTypeAheadItem(twoPointOne, 'input.input-block-level', compteNumber, 1);

            generalHelper.waitElementClickableAndClick(okButton);

            generalHelper.waitAlertSuccessPresent();

            browser.sleep(1000);

        });


        it('should confirm activate compte', function ()
        {
            generalHelper.waitElementClickableAndClick(accPlanItem);

            browser.sleep(5000);

            var table = element.all(by.css('#acc-firm-vat-lines-dtable tr')).get(2);
            var activateButton = table.element(by.css('.btn success'));

            expect(activateButton.isPresent()).toBe(true);
        });

        it('should activate other compte', function ()
        {
            generalHelper.waitElementClickableAndClick(vatCatSalesItem);

            var compteNumber = "44500000";

            //var services = element(by.xpath('/html/body/div/div[3]/div/div[1]/div/div[2]/div/div[2]/div/div/div/div/div[3]/div/div/div'));
            //var hiddenTwoPointOne = element.all(by.css('#pick_2.1')).get(1);
            var twoPointOne = element(by.xpath('/html/body/div/div[3]/div/div[1]/div/div[2]/div/div[2]/div/div/div/div/div[3]/div/div/div/div[3]/div/div[2]/div[2]/div/div'));

            generalHelper.selectTypeAheadItem(twoPointOne, 'input.input-block-level', compteNumber, 1);

            generalHelper.waitElementClickableAndClick(okButton);

            generalHelper.waitAlertSuccessPresent();

            browser.sleep(1000);
        });

        it('should confirm desactivate first compte', function ()
        {
            generalHelper.waitElementClickableAndClick(accPlanItem);

            browser.sleep(5000);

            var table = element.all(by.css('#acc-firm-vat-lines-dtable tr')).get(2);
            var activateButton = table.element(by.css('.btn success'));

            expect(activateButton.isPresent()).toBe(false);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});
