describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            journalItem,
            firstRow,
            tags,
            save;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="acc_settings"]'));
            journalItem = element(by.css('#customer-admin-main-region [data-name="journal"] a'));
            firstRow = element.all(by.css('#acc-firm-journal-dtable tr')).get(0);
            tags = firstRow.element(by.css('.tags-input-box input'));
            save = firstRow.element(by.css('.js-save-journal-line'));
        });


        it('should count tags', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(journalItem);

            browser.sleep(5000);


        });

        it('add tag', function ()
        {

            //Contar os items
            var records = element(by.css('.tags-input-box span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            tags.sendKeys('TH');

            generalHelper.waitElementClickableAndClick(save);


            browser.sleep(1000);

            var records = element(by.css('.tags-input-box span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(recordCount++);
            });


        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});