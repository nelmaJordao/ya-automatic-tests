describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            vatCatSalesItem,
            invalidButton,
            invalidComptesButton,
            items;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="acc_settings"]'));
            vatCatSalesItem = element(by.css('#customer-admin-main-region [data-name="vat-cat-purchases"] a'));
            invalidButton = element(by.css('.js-toggle'));
            invalidComptesButton = element(by.css('.js-error-messages'));
            items = element(by.css('.list-group-item'));
        });

        it('should open vat cat sales item', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(vatCatSalesItem);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            generalHelper.waitElementClickableAndClick(invalidComptesButton);

            browser.sleep(1000);

            expect(items.isPresent()).toBe(true);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});