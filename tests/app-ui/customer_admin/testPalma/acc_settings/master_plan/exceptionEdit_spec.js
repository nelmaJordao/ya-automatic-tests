describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            masterPlanItem,
            invalidButton,
            searchField,
            searchButton,
            secondRow,
            editButton,
            modalWindow,
            okButton,
            codeCompte,
            description;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="acc_settings"]'));
            masterPlanItem = element(by.css('#customer-admin-main-region [data-name="confs"] a'));
            invalidButton = element(by.css('.js-toggle'));
            searchField = element(by.css('.js-search-input'));
            searchButton = element(by.css('.js-search-acc-plan'));
            secondRow = element.all(by.css('#acc-plan-table tr')).get(1);
            editButton = secondRow.element(by.css('.js-edit-acc-plan-item'));
            modalWindow = element(by.css('#accounting-line-edit-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            codeCompte = modalWindow.element(by.css('#accCode'));
            description = modalWindow.element(by.css('#description'));
        });

        it('should open master plan edit modal window', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(masterPlanItem);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            //generalHelper.waitElementClickableAndClick(invalidButton);

            //browser.sleep(1000);

            searchField.clear();
            searchField.sendKeys('445');

            generalHelper.waitElementClickableAndClick(searchButton);


            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

        });


        it('should fill mandatory fields and submit', function()
        {

            // editar a description
            description.clear();
            description.sendKeys('Code 445000000!');


            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});
