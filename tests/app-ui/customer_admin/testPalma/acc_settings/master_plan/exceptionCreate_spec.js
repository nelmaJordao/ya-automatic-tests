describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            accPlan,
            masterPlanItem,
            invalidButton,
            newButton,
            modalWindow,
            okButton,
            codeCompte,
            description,
            recordCountNextTab;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="acc_settings"]'));
            accPlan = element(by.css('#customer-admin-main-region [data-name="acc-vats"] a'));
            masterPlanItem = element(by.css('#customer-admin-main-region [data-name="confs"] a'));
            invalidButton = element(by.css('.js-toggle'));
            newButton = element(by.css('.js-new-acc-plan-item'));
            modalWindow = element(by.css('#accounting-line-add-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            codeCompte = modalWindow.element(by.css('#accCode'));
            description = modalWindow.element(by.css('#description'));
        });

        it('should open acc plan', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(accPlan);

            browser.sleep(5000);


            //Contar os items
            var records = element(by.css('.js-total-res span'));
            recordCountNextTab = 0;
            records.getText().then(function(text) {
                recordCountNextTab = parseInt(text);
            });


        });


        it('should open master plan modal window', function ()
        {
            //preparation
            //action
            //assertion

            generalHelper.waitElementClickableAndClick(masterPlanItem);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido


            //generalHelper.waitElementClickableAndClick(invalidButton);

            //browser.sleep(1000);
            

            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);




        });

        it('should check for empty fields', function ()
        {

            generalHelper.waitElementClickableAndClick(okButton);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(2);

        });

        it('should test field restrictions', function () {

            codeCompte.sendKeys('1');

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(2);

            //---------------------//------------------------------//

            description.sendKeys('Code 1');

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

            expect(errors.count()).toEqual(1);

            codeCompte.clear();
            description.clear();

        });


        it('should fill mandatory fields and submit', function()
        {

            // inserir code de compte
            codeCompte.sendKeys('44510000');

            // inserir a description
            description.sendKeys('Code 44510000');


            // verificar se foi bem introduzido
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            browser.sleep(1000);

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(recordCount++);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });



        it('should confirm addition compte in acc plan', function()
        {


            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(accPlan);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCountNextTab);
            });

        });




        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});
