describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            masterPlanItem,
            invalidButton,
            firstRow,
            deleteButton,
            modalWindow,
            okButton;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="acc_settings"]'));
            masterPlanItem = element(by.css('#customer-admin-main-region [data-name="confs"] a'));
            invalidButton = element(by.css('.js-toggle'));
            firstRow = element.all(by.css('#acc-plan-table tr')).get(0);
            deleteButton = firstRow.element(by.css('.js-delete-acc-plan-item'));
            modalWindow = element(by.css('#acc-code-delete-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
        });

        it('should open master plan delete modal window', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(masterPlanItem);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            //generalHelper.waitElementClickableAndClick(invalidButton);

            //browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(deleteButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should confirm delete modal window', function()
        {
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text)
            {
                expect(parseInt(text)).toEqual(--recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});

