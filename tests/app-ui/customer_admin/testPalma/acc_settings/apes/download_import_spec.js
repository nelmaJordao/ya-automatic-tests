describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            apesItem,
            company,
            newCompany,
            downloadButton,
            importButton;




        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="acc_settings"]'));
            apesItem = element(by.css('#customer-admin-main-region [data-name="apes"] a'));
            downloadButton = element(by.css('.js-download-ape-plan-template'));
            importButton = element(by.css('.js-import-ape-plan'));
        });


        it('should click in download button and import download', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(apesItem);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido


            generalHelper.waitElementClickableAndClick(downloadButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(importButton);


        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});