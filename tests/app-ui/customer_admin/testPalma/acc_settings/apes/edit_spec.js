describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            apesItem,
            company,
            newCompany,
            modalWindowDelete,
            save,
            searchField,
            searchButton,
            firstRow,
            editButton,
            modalWindow,
            okButton;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="acc_settings"]'));
            apesItem = element(by.css('#customer-admin-main-region [data-name="apes"] a'));
            company= element(by.css('#customercompany'));
            newCompany = company.element(by.xpath('/html/body/div[1]/div[1]/div/nav/div[3]/ul/li[6]/ul/li[2]'));
            modalWindowDelete = element(by.css('#accounting-configs-delete-modal'));
            save = modalWindowDelete.element(by.css('.btn-success.ok'));
            searchField = element(by.css('.js-search-input'));
            searchButton = element(by.css('.js-search-apes'));
            firstRow = element.all(by.css('#acc-firm-apes-dtable tr')).get(0);
            editButton = firstRow.element(by.css('.js-edit-ape'));
            modalWindow = element(by.css('#edit-ape-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
        });

        it('should open apes edit modal window', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(apesItem);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido


            //Mudar de empresa
            generalHelper.waitElementClickableAndClick(company);
            generalHelper.waitElementClickableAndClick(newCompany);


            //Apagar todos

            var deleteAll = element(by.css('.js-delete-apes'));
            generalHelper.waitElementClickableAndClick(deleteAll);
            generalHelper.waitElementClickableAndClick(save);


            //Procurar

            searchField.clear();
            searchField.sendKeys('0111Z');

            generalHelper.waitElementClickableAndClick(searchButton);


            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

        });


        it('should fill mandatory fields and submit', function()
        {


            var firstSelectPickerType = modalWindow.element(by.xpath('/html/body/div[2]/div[2]/div/div[2]/div/form/div/div[1]/div/div/button'));
            var firstSelectPickerOption = modalWindow.element(by.xpath('/html/body/div[2]/div[2]/div/div[2]/div/form/div/div[1]/div/div/div/ul/li[2]'));

            generalHelper.waitElementClickableAndClick(firstSelectPickerType);
            generalHelper.waitElementClickableAndClick(firstSelectPickerOption);

            browser.sleep(1000);

            var secondSelectPickerType = modalWindow.element(by.xpath('/html/body/div[2]/div[2]/div/div[2]/div/form/div/div[2]/div/div/button'));
            var secondSelectPickerOption = modalWindow.element(by.xpath('/html/body/div[2]/div[2]/div/div[2]/div/form/div/div[2]/div/div/div/ul/li[2]'));

            generalHelper.waitElementClickableAndClick(secondSelectPickerType);
            generalHelper.waitElementClickableAndClick(secondSelectPickerOption);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });



        it('should confirm edition', function ()
        {
            //preparation
            //action
            //assertion
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(apesItem);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido


            //Procurar
            searchField.sendKeys('0111Z');

            generalHelper.waitElementClickableAndClick(searchButton);

            browser.sleep(1000);

            var comptesVentesCell = firstRow.all(by.css('td')).get(2);

            comptesVentesCell.getAttribute('data-content').then(function (text) {
                expect(text).not.toEqual('-');
            });


            browser.sleep(1000);

            var comptesAchatsCell = firstRow.all(by.css('td')).get(3);

            comptesAchatsCell.getAttribute('data-content').then(function (text) {
                expect(text).not.toEqual('-');
            });

        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });



        it('should delete comptes', function ()
        {
            //preparation
            //action
            //assertion
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(apesItem);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido


            //Apagar todos

            var deleteAll = element(by.css('.js-delete-apes'));
            generalHelper.waitElementClickableAndClick(deleteAll);
            generalHelper.waitElementClickableAndClick(save);


            //Procurar

            searchField.clear();
            searchField.sendKeys('0111Z');

            generalHelper.waitElementClickableAndClick(searchButton);


            browser.sleep(1000);

            var comptesVentesCell = firstRow.all(by.css('td')).get(2);

            comptesVentesCell.getAttribute('data-content').then(function (text) {
                expect(text).toEqual(null);
            });


            browser.sleep(1000);

            var comptesAchatsCell = firstRow.all(by.css('td')).get(3);

            comptesAchatsCell.getAttribute('data-content').then(function (text) {
                expect(text).toEqual(null);
            });




        });



        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});
