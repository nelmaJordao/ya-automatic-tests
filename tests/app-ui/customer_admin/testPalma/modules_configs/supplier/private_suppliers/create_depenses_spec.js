describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItemAccSettings,
            masterPlanItem,
            tabItem,
            privateSuppliers,
            firstPrivateSuppliersTable,
            firstSecondTd,
            modalWindow,
            okButton,
            searchField,
            searchButton,
            firstRowMasterPlan,
            deleteButton,
            modalWindowDelete,
            okButtonDelete,
            records,
            recordCount;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItemAccSettings = element(by.css('#customer-admin-menu-region [data-sec="acc_settings"]'));
            masterPlanItem = element(by.css('#customer-admin-main-region [data-name="confs"] a'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="modules_configs"]'));
            privateSuppliers = element(by.css('#customer-admin-main-region [data-tab="private_suppliers"]'));
            firstPrivateSuppliersTable = element.all(by.css('#supplier-private-supplier-configs-dtable tr')).get(0);
            firstSecondTd = firstPrivateSuppliersTable.all(by.css('td')).get(2);
            modalWindow = element(by.css('#accounting-line-add-modal'));
            okButton = element(by.css('.js-configs-save'));
            searchField = element(by.css('.js-search-input'));
            searchButton = element(by.css('.js-search-acc-plan'));
            firstRowMasterPlan = element.all(by.css('#acc-plan-table tr')).get(0);
            deleteButton = firstRowMasterPlan.element(by.css('.js-delete-acc-plan-item'));
            modalWindowDelete = element(by.css('#acc-code-delete-modal'));
            okButtonDelete = modalWindowDelete.element(by.css('.btn-success.ok'));
        });

        it('should count the number of items', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItemAccSettings);
            generalHelper.waitElementClickableAndClick(masterPlanItem);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            records = element(by.css('.js-total-res span'));
            recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });
        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


        it('should add a new code compte', function ()
        {
            //preparation
            //action
            //assertion
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(privateSuppliers);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            var codeDropdownFirst = firstSecondTd.element(by.css('div.js-acc-code-costs'));
            var newCode = codeDropdownFirst.element(by.css('.add-more'));

            generalHelper.waitElementClickableAndClick(codeDropdownFirst);
            generalHelper.waitElementClickableAndClick(newCode);
        });


        it('should test restrictions', function () {


            browser.sleep(1000);

            //Campos a null

            var saveButton = modalWindow.element(by.css('.btn-success.ok'));

            generalHelper.waitElementClickableAndClick(saveButton);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(2);

            browser.sleep(1000);

            //Código inválido e descrição a null

            var code = modalWindow.element(by.css('#accCode'));

            code.sendKeys('LLLLLLL');
            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(1000);

            //Código válido mas descrição a null

            code.clear();
            code.sendKeys('11LL');

            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(1000);

            //Código válido e descrição válida

            code.clear();
            code.sendKeys('11LL');

            var descriptionCompte = modalWindow.element(by.css('#description'));

            descriptionCompte.sendKeys('LL');

            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);
        });


        it('should confirm additional item', function () {
            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItemAccSettings);
            generalHelper.waitElementClickableAndClick(masterPlanItem);

            browser.sleep(1000);

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });
        });


        it('should search', function ()
        {
            generalHelper.waitElementClickableAndClick(tabItemAccSettings);
            generalHelper.waitElementClickableAndClick(masterPlanItem);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            searchField.clear();
            searchField.sendKeys('611LL');

            generalHelper.waitElementClickableAndClick(searchButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(deleteButton);
            generalHelper.waitElementPresent(modalWindowDelete);

            expect(modalWindowDelete.isDisplayed()).toBe(true);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButtonDelete);
            generalHelper.waitAlertSuccessPresentAndClose();

            browser.sleep(1000);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});