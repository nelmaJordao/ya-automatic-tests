describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            suppliers,
            firstSupplierTable,
            firstSecondTd,
            okButton,
            secondSupplierTable,
            secondSecondTd;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="modules_configs"]'));
            suppliers = element(by.css('#customer-admin-main-region [data-tab="suppliers"]'));
            firstSupplierTable = element.all(by.css('#suppliers-suppliers-dtable tr')).get(0);
            firstSecondTd = firstSupplierTable.all(by.css('td')).get(2);
            okButton = element(by.css('.js-configs-save'));
            secondSupplierTable = element.all(by.css('#suppliers-suppliers-dtable tr')).get(1);
            secondSecondTd = secondSupplierTable.all(by.css('td')).get(2);
        });

        it('should pick a code compte achat for first row', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(suppliers);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            var codeDropdownFirst = firstSecondTd.element(by.css('div.js-acc-code'));
            var codeOptionOneFirst = codeDropdownFirst.element(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a span.text'));

            generalHelper.waitElementClickableAndClick(codeDropdownFirst);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(codeOptionOneFirst);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

            generalHelper.waitAlertSuccessPresent();
        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });





        /*it('should confirm first row td changes', function ()
         {
             generalHelper.waitElementClickableAndClick(menuIcon);
             generalHelper.waitElementClickableAndClick(tabItem);
             generalHelper.waitElementClickableAndClick(suppliers);

             browser.sleep(5000); // para garantir que não falha por ser demasiado rápido


             var codeDropdownFirst = firstSecondTd.element(by.css('div.js-purchase-acc-code'));

             codeDropdownFirst.getTextContent('title').then(function (text) {
                expect(text).toEqual('60110000 - Matieres (ou groupe) a');
             });

             expect(codeDropdownFirst.getText()).toEqual('60110000 - Matieres (ou groupe) a ');

             codeDropdownFirst.getText().then(function(text) {
                expect(text).toEqual('60110000 - Matieres (ou groupe) a ');
             });
         });


         it('should return to dashboard', function () {
             generalHelper.returnToDashboard();
             browser.refresh();
         });*/



        it('should pick a code compte achat for second row', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(suppliers);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            var codeDropdownSecond = secondSecondTd.element(by.css('div.js-acc-code'));
            var codeOptionOneSecond = codeDropdownSecond.element(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a span.text'));

            generalHelper.waitElementClickableAndClick(codeDropdownSecond);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(codeOptionOneSecond);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

            generalHelper.waitAlertDangerPresent();


        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });



        it('should confirm second row td changes', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(suppliers);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            secondSecondTd.getAttribute('data-content').then(function (text) {
                expect(text).toEqual('60110000 - Matieres (ou groupe) a');
            });


         });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });



        it('should return to zero', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(suppliers);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            var codeDropdownFirst = firstSecondTd.element(by.css('div.js-acc-code'));
            var codeOptionThreeFirst = codeDropdownFirst.element(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="0"] a span.text'));

            var codeDropdownSecond = secondSecondTd.element(by.css('div.js-acc-code'));
            var codeOptionThreeSecond = codeDropdownSecond.element(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="0"] a span.text'));

            generalHelper.waitElementClickableAndClick(codeDropdownFirst);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(codeOptionThreeFirst);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(codeDropdownSecond);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(codeOptionThreeSecond);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

            generalHelper.waitAlertSuccessPresent();
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});