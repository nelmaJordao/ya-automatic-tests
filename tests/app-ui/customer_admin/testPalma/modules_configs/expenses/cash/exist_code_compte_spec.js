describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            expense,
            cash,
            firstCashTable,
            firstSecondTd,
            okButton;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="modules_configs"]'));
            expense = element(by.css('.js-configs-expenses'));
            cash = element(by.css('#customer-admin-main-region [data-tab="cash"]'));
            firstCashTable = element.all(by.css('#expenses-category-configs-dtable tr')).get(0);
            firstSecondTd = firstCashTable .all(by.css('td')).get(1);
            okButton = element(by.css('.js-configs-save'));
        });

        it('should pick a code compte for first row', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(expense);
            generalHelper.waitElementClickableAndClick(cash);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            var codeDropdownFirst = firstSecondTd.element(by.css('div.js-acc-code'));
            var codeOptionOneFirst = codeDropdownFirst.element(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a span.text'));

            generalHelper.waitElementClickableAndClick(codeDropdownFirst);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(codeOptionOneFirst);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

            generalHelper.waitAlertSuccessPresent();
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

        it('should return to zero', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(expense);
            generalHelper.waitElementClickableAndClick(cash);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            var codeDropdownFirst = firstSecondTd.element(by.css('div.js-acc-code'));
            var codeOptionThreeFirst = codeDropdownFirst.element(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="0"] a span.text'));


            generalHelper.waitElementClickableAndClick(codeDropdownFirst);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(codeOptionThreeFirst);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

            generalHelper.waitAlertSuccessPresent();
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });
});

