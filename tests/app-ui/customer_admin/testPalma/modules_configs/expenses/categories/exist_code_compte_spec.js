describe('Customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTPALMA.subdomain);

    it('should login with ' + config.users.TESTPALMA.username + ' as ' + config.users.TESTPALMA.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTPALMA);

    });

    describe('Test Palma', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            expense,
            categories,
            firstCategoriesTable,
            firstSecondTd,
            okButton,
            secondCategoriesTable,
            secondSecondTd,
            number;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="modules_configs"]'));
            expense = element(by.css('.js-configs-expenses'));
            categories = element(by.css('#customer-admin-main-region [data-tab="categories"]'));
            firstCategoriesTable = element.all(by.css('#expenses-category-configs-dtable tr')).get(0);
            firstSecondTd = firstCategoriesTable .all(by.css('td')).get(1);
            okButton = element(by.css('.js-configs-save'));
            secondCategoriesTable = element.all(by.css('#expenses-category-configs-dtable tr')).get(1);
            secondSecondTd = secondCategoriesTable.all(by.css('td')).get(1);
        });

        it('should pick a code compte for first row', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(expense);
            generalHelper.waitElementClickableAndClick(categories);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            var codeDropdownFirst = firstSecondTd.element(by.css('div.js-acc-code'));
            var codeOptionOneFirst = codeDropdownFirst.element(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a span.text'));

            generalHelper.waitElementClickableAndClick(codeDropdownFirst);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(codeOptionOneFirst);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

            generalHelper.waitAlertSuccessPresent();
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

        it('should pick a code compte for second row', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(expense);
            generalHelper.waitElementClickableAndClick(categories);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            var codeDropdownSecond = secondSecondTd.element(by.css('div.js-acc-code'));
            var codeOptionOneSecond = codeDropdownSecond.element(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a span.text'));

            generalHelper.waitElementClickableAndClick(codeDropdownSecond);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(codeOptionOneSecond);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

            generalHelper.waitAlertSuccessPresent();
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

        it('should return to zero', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(expense);
            generalHelper.waitElementClickableAndClick(categories);

            browser.sleep(5000); // para garantir que não falha por ser demasiado rápido

            var codeDropdownFirst = firstSecondTd.element(by.css('div.js-acc-code'));
            var codeOptionThreeFirst = codeDropdownFirst.element(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="0"] a span.text'));

            var codeDropdownSecond = secondSecondTd.element(by.css('div.js-acc-code'));
            var codeOptionThreeSecond = codeDropdownSecond.element(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="0"] a span.text'));

            generalHelper.waitElementClickableAndClick(codeDropdownFirst);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(codeOptionThreeFirst);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(codeDropdownSecond);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(codeOptionThreeSecond);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

            generalHelper.waitAlertSuccessPresent();
        });

        it('click first radio button and count the items', function () {
            browser.sleep(1000);
            var radio = element(by.css('#expenseCategoriesMacro'));
            generalHelper.waitElementClickableAndClick(radio);

            var records = element(by.css('.js-total-res span'));
            number = 0;
            records.getText().then(function(text) {
                number = parseInt(text);
            });
        });

        it('click second radio button and count the items and confirm that is not equal', function () {
            browser.sleep(1000);
            var radio = element(by.css('#expenseCategoriesMicro'));
            generalHelper.waitElementClickableAndClick(radio);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).not.toEqual(number);
            });

            browser.sleep(1000);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });
});

