describe('customer admin', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('settings / costumer', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            tabItem2,
            okButton,
            region;

        var phoneInput,
            phoneSecInput,
            emailInput,
            websiteInput;

        var phone = parseInt( (Math.random() * 900000000) +  100000000 ); phone = phone + ""; // e converte para texto para depois poder comparar
        var phoneSec = parseInt( (Math.random() * 900000000) +  100000000); phoneSec = phone + "";
        var email = config.inputs.getRandomName(7, false) + "@xpto.biz";
        var baseURL = browser.getCurrentUrl().then(function(url){
            var str = url.toString();
            var array = str.split('#');
            return array[0];
        });

        beforeEach(function()
        {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="settings"]'));
            region = element(by.css('#customer-region'));
            tabItem2 = element(by.css('#customer-admin-main-region [data-name="customer"] a'));
            okButton = region.element(by.css('.btn-success.ok'));
            phoneInput =  region.element(by.css('#phone'));
            phoneSecInput = region.element(by.css('#phoneSecondary'));
            emailInput = region.element(by.css('#email'));
            websiteInput = region.element(by.css('#website'));
        });

        it('should open costumer tab in settings and clear fields', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(tabItem2);

            // limpa os campos
            phoneInput.clear();
            phoneSecInput.clear();
            emailInput.clear();
            websiteInput.clear();

            expect( okButton.isPresent() ) . toBe( true );

        });

        it('should fill mandatory fields and submit', function()
        {
            // Preencher nº de telefone fixo
            phoneInput.sendKeys( phone );

            // Preencher nº de telefone fixo
            phoneSecInput.sendKeys( phoneSec );

            // Preencher o email
            emailInput.sendKeys( email );

            // Preencher o website
            websiteInput.sendKeys( baseURL );
            
            
            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose(); // se encontrar este modal significa que gravou com sucesso

        });

        it('should loggout, login and open costumer tab in settings ', function()
        {
            browser.get( baseURL + "#logout");
            requireLoginSpec();

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(tabItem2);

            expect( okButton.isPresent() ) . toBe( true );


        });

        it('should  check if the values were saved correct', function()
        {

            expect( phoneInput.getAttribute('value') ) . toEqual( phone );
            expect( phoneSecInput.getAttribute('value') ) . toEqual( phoneSec );
            expect( emailInput.getAttribute('value') ) . toEqual( email );
            expect( websiteInput.getAttribute('value') ) . toEqual( baseURL );

            generalHelper.returnToDashboard();

        });

    });

});