describe('customer admin', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('settings / invoicing', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            tabItem2,
            okButton,
            region;

        beforeEach(function()
        {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="settings"]'));
            tabItem2 = element(by.css('#customer-admin-main-region [data-name="invoicing"] a'));
            region = element(by.css('#invoicing-configs-region'));
            okButton = region.element(by.css('.btn-success.js-save-current-series'));
        });

        it('should open invoicing tab in settings', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(tabItem2);

            browser.sleep(500); // para garantir que não falha por ser demasiado rápido
            expect( okButton.isPresent() ) . toBe( true );

        });

        it('should fill mandatory fields and submit', function()
        {
            var list = region.all( by.css('.table-striped tbody tr:not(.warning)') );

            // seleciona o primeiro item da lista independentemente de já estar selecionado ou não
            var radioButton = list.first().element(by.css('input[name="optionsRadiosSalesReceipt"]'));
            generalHelper.waitElementClickableAndClick(radioButton);

            generalHelper.waitElementClickableAndClick(okButton);
            // se encontrar este modal significa que gravou com sucesso
            generalHelper.waitAlertSuccessPresentAndClose();

            generalHelper.returnToDashboard();

        });
        
    });

});