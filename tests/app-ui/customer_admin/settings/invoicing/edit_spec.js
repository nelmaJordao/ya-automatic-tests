describe('customer admin', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.MANAGER.role, function () {
        loginHelper.loginWith(baseURL, config.users.MANAGER);
    });

    describe('settings / invoicing', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            tabItem2,
            okButton,
            region;

        var list;
        
        var serieInput,
            selectpickerDocType,
            initialCountInput;

        beforeEach(function()
        {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="settings"]'));
            tabItem2 = element(by.css('#customer-admin-main-region [data-name="invoicing"] a'));
            region = element(by.css('#invoicing-configs-region'));
            okButton = region.element(by.css('.btn-success.js-save-current-series'));

            list = region.all( by.css('.table-striped tbody tr:not(.warning)') );

            serieInput = list.last().element(by.css('.form-control.js-serie'));
            selectpickerDocType = list.last();
            initialCountInput = list.last().element(by.css('.form-control.js-initial-count'));

        });

        it('should open invoicing tab in settings', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(tabItem2);

            browser.sleep(500); // para garantir que não falha por ser demasiado rápido
            expect( okButton.isPresent() ) . toBe( true );

        });

        it('should check for empty fields', function ()
        {
            // limpa os campos e seleciona o primeiro item da dropbox
            serieInput.clear();

            var selectpickerDocTypeOption = selectpickerDocType.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="0"]'));
            generalHelper.waitElementClickableAndClick(selectpickerDocType);
            generalHelper.waitElementClickableAndClick(selectpickerDocTypeOption);

            initialCountInput.clear();

            generalHelper.waitElementClickableAndClick(okButton);

            var errors = list.last().all(by.css('.form-group.error'));
            expect( errors.count() ) . toEqual(2);

        });


        it('should fill mandatory fields and submit', function()
        {
            
            var n; // variavel auxiliar para a geração de números aleatórios

            // preencher a serie com uma seria aleatoria
            n = parseInt( (Math.random() * 4) +  1 );
            serieInput.sendKeys( config.inputs.getRandomName(n, false) );

            // escolher um tipo de decomento aleatorio
            n = parseInt( (Math.random() * 3) +  1 );
            
            var selectpickerDocTypeOption = selectpickerDocType.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="' + n + '"]'));
            generalHelper.waitElementClickableAndClick(selectpickerDocType);
            generalHelper.waitElementClickableAndClick(selectpickerDocTypeOption);

            // preencher a contagem inicial com um número aleatório
            n = parseInt( (Math.random() * 100) +  1 );
            initialCountInput.sendKeys( n );

            browser.sleep(5000);

            generalHelper.waitElementClickableAndClick(okButton);
            // se encontrar este modal significa que gravou com sucesso
            generalHelper.waitAlertSuccessPresentAndClose();

            generalHelper.returnToDashboard();

        });

    });

});