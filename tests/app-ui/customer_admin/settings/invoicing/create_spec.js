describe('customer admin', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('settings / invoicing', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            tabItem2,
            okButton,
            region;

        beforeEach(function()
        {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="settings"]'));
            tabItem2 = element(by.css('#customer-admin-main-region [data-name="invoicing"] a'));
            region = element(by.css('#invoicing-configs-region'));
            okButton = region.element(by.css('.btn-success.js-save-current-series'));
        });

        it('should open invoicing tab in settings', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(tabItem2);

            browser.sleep(500); // para garantir que não falha por ser demasiado rápido
            expect( okButton.isPresent() ) . toBe( true );

        });

        it('should fill mandatory fields and submit', function()
        {
            var serieInput;
            var initialCountInput;
            var list = region.all( by.css('.table-striped tbody tr'));
            var addButton = region.element(by.css('.btn-default.js-new-series'));

            generalHelper.waitElementClickableAndClick(addButton);
            serieInput = list.last().element(by.css('.form-control.js-serie'));
            var n; // variavel auxiliar para a geração de números aleatórios

            // preencher a serie com uma seria aleatoria
            n = parseInt( (Math.random() * 3) +  1 );
            serieInput.sendKeys( config.inputs.getRandomName(n, false) );

            // escolher um tipo de decomento aleatorio
            n = parseInt( (Math.random() * 2) +  1 );

            var row = list.last();
            var selectpickerDocType = row.element(by.css('div button'));
            var selectpickerDocTypeOption = row.element(by.css('div.dropdown-menu ul li[data-original-index="' + n + '"] a'));
            generalHelper.waitElementClickableAndClick(selectpickerDocType);
            generalHelper.waitElementClickableAndClick(selectpickerDocTypeOption);

            // preencher a contagem inicial com um número aleatório
            n = parseInt( (Math.random() * 100) +  1 );
            initialCountInput = list.last().element(by.css('.form-control.js-initial-count'));
            initialCountInput.sendKeys( n );
            

            generalHelper.waitElementClickableAndClick(okButton);
            // se encontrar este modal significa que gravou com sucesso
            generalHelper.waitAlertSuccessPresentAndClose();

            generalHelper.returnToDashboard();

        });

    });

});