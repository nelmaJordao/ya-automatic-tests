describe('Custumer', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Employees', function ()
    {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            moduleButton,
            modalWindow,
            okButton;

        beforeEach(function()
        {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="employees"]'));
            moduleButton = element.all(by.css('#customer-admin-employees-dtable a.js-choose-modules-employee')).get(5);
            modalWindow = element(by.css('#employee-choose-modules-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
        });

        it('should open modules employee modal window', function () {

            loginHelper.logout();

            browser.sleep(1000);

            loginHelper.login(config.users.TESTPEDRO);

            browser.sleep(5000);

            loginHelper.logout();

            browser.sleep(1000);

            loginHelper.login(config.users.TESTPOLO);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(moduleButton);

            browser.sleep(1000);

            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });


        it('should check modules and submit', function()
        {
            var secondRow = modalWindow.all(by.css('table.table-striped.table-hover tbody tr')).get(2);
            var undoSelectEVP = secondRow.element(by.css('td.center label.checkbox'));
            generalHelper.waitElementClickableAndClick(undoSelectEVP);

            browser.sleep(2000);

            var saveButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(2000);

            loginHelper.logout();

            browser.sleep(1000);

            loginHelper.login(config.users.TESTPEDRO);

            browser.refresh();

            browser.sleep(4000);

            loginHelper.logout();

            browser.sleep(1000);

            loginHelper.login(config.users.TESTPOLO);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(moduleButton);

            browser.sleep(1000);

            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

            var firstRow = modalWindow.all(by.css('table.table-striped.table-hover tbody tr')).get(1);
            var radioFirst = firstRow.element(by.css('td.center label.radio span'));
            generalHelper.waitElementClickableAndClick(radioFirst);

            browser.sleep(2000);

            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(1000);

            loginHelper.logout();

            browser.sleep(1000);

            loginHelper.login(config.users.TESTPEDRO);

            browser.sleep(4000);

            var frais = 'expenses';


            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');
                var arr = array[1].split('/');
                expect(arr[0]).toEqual(frais);
            });

            browser.sleep(4000);

            loginHelper.logout();

            browser.sleep(1000);

            loginHelper.login(config.users.TESTPOLO);

            browser.sleep(1000);

            var firstRowOriginal = modalWindow.all(by.css('table.table-striped.table-hover tbody tr')).get(0);
            var radioFirstOriginal = firstRowOriginal.element(by.css('td.center label.radio span'));
            generalHelper.waitElementClickableAndClick(radioFirstOriginal);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(undoSelectEVP);
            generalHelper.waitElementClickableAndClick(saveButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});

