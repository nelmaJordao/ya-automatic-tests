describe('customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('employees', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow;
        
        beforeEach(function()
        {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="employees"]'));
            modalWindow = element(by.css('#employee-read-modal'));
        });

        it('should open show credentials modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var firstShowButton = element.all(by.css('#customer-admin-employees-dtable a.js-show-employee')).get(0);
            generalHelper.waitElementClickableAndClick(firstShowButton);

            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);

        });


        it('should check for empty fields', function ()
        {
            //verificar nome
            var firstName = modalWindow.all(by.css('.form-group p.value')).get(0);
            expect(firstName.getText()).not.toEqual('');

            //verificar o aplido
            var lastName = modalWindow.all(by.css('.form-group p.value')).get(1);
            expect(lastName.getText()).not.toEqual('');

            //verificar email
            var email = modalWindow.all(by.css('.form-group p.value')).get(2);
            expect(email.getText()).not.toEqual('');

            //verificar o role
            var role = modalWindow.all(by.css('.form-group p.value')).get(3);
            expect(role.getText()).not.toEqual('');

        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default'));
            generalHelper.waitElementClickableAndClick(closeButton);
            
            expect(modalWindow.isPresent()).toBe(false);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });
    
});