describe('customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    
    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('employees', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow,
            firstRow,
            firstAtivation;
        
        beforeEach(function()
        {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="employees"]'));
            modalWindow = element(by.css('.modal.in'));
            firstRow = element.all(by.css('#customer-admin-employees-dtable tr')).get(0);
            firstAtivation = firstRow.all(by.css('.options a')).first();
        });

        it('should open send activation modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(2000);

            generalHelper.waitElementClickableAndClick(firstAtivation);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        
        it('should confirm send ativation modal window', function()
        {
            var okButton = modalWindow.element(by.css('.btn-success'));

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            var reSendButton = firstRow.element(by.css('.options a.js-email-employee i'));

            expect(reSendButton.isPresent() ).toBe( true );

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
      
    });
    
});