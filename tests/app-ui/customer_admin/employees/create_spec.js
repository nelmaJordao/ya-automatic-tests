describe('customer admin', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('employees', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            okButton,
            modalWindow,
            firstNameInput,
            lastNameInput,
            emailInput;

        beforeEach(function()
        {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="employees"]'));
            newButton = element(by.css('.js-new-employee'));
            modalWindow = element(by.css('#employee-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            firstNameInput = modalWindow.element(by.css('#firstName'));
            lastNameInput = modalWindow.element(by.css('#lastName'));
            emailInput = modalWindow.element(by.css('#email'));
        });

        it('should open new employee modal window', function ()
        {
            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

        });

        it('should check for empty fields', function ()
        {
            generalHelper.waitElementClickableAndClick(okButton);

            var errors = modalWindow.all(by.css('.form-group.error'));

            browser.sleep(1000);

            expect(errors.count()).toEqual(3);
        });

        it('should test field restrictions', function () {
            firstNameInput.sendKeys('123');
            lastNameInput.sendKeys('123');
            emailInput.sendKeys('a');

            firstNameInput.clear();
            lastNameInput.clear();
            emailInput.clear();
        });

        it('should fill mandatory fields and submit', function()
        {

            // inserir o nome
            var firstName = config.inputs.getRandomName(6).replace(/ /g, '');
            firstNameInput.sendKeys( firstName );

            // inserir o apelido
            var lastName = config.inputs.getRandomName(6).replace(/ /g, '');
            lastNameInput.sendKeys( lastName );

            // inserir o email
            emailInput.sendKeys( firstName + '.' + lastName + '@xpto.biz'); // @xpto.biz

            // inserir numero da matricula
            var matriculeNumberInput = modalWindow.element(by.css('#matriculeNumber'));
            matriculeNumberInput.sendKeys( parseInt( (Math.random() * 100000000) +  100 ) );

            // inserir codigo de afliliado
            var filialCodeInput = modalWindow.element(by.css('#filialCode'));
            filialCodeInput.sendKeys( parseInt( (Math.random() * 100000000) +  100 ) );

            // verificar se foi bem introduzido
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

            generalHelper.waitAlertSuccessPresentAndClose();

            browser.sleep(1000);

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });
    
});