describe('Custumer', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Employees', function ()
    {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstEditButton,
            modalWindow,
            okButton,
            closeButton,
            firstNameInput,
            lastNameInput;

        beforeEach(function()
        {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="employees"]'));
            firstEditButton = element.all(by.css('#customer-admin-employees-dtable a.js-edit-employee')).get(0);
            modalWindow = element(by.css('#employee-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            firstNameInput = modalWindow.element(by.css('#firstName'));
            lastNameInput = modalWindow.element(by.css('#lastName'));
        });
        
        it('should open edit employee modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(firstEditButton);
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should cancel edit employee', function () {
            var cancelButton = element(by.css('button.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            browser.sleep(1000);
        });

        it('should open edit employee modal window', function () {
            generalHelper.waitElementClickableAndClick(firstEditButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function ()
        {
            clearFields();

            var errors = modalWindow.all(by.css('.form-group.error'));
            expect(errors.count()).toEqual(2);

        });

        it('should test field restrictions', function () {
            clearFields();

            firstNameInput.sendKeys('123');
            lastNameInput.sendKeys('123');

            var errors = modalWindow.all(by.css('.form-group.error'));
            expect(errors.count()).toEqual(2);

            clearFields();
        });


        it('should close and reopen edit credentials modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(firstEditButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });


        it('should fill mandatory fields and submit', function()
        {
            clearFields();

            // editar o nome
            var firstName = config.inputs.getRandomName(6).replace(/ /g, '');
            firstNameInput.sendKeys( firstName );

            // editar o apelido
            var lastName = config.inputs.getRandomName(6).replace(/ /g, '');
            lastNameInput.sendKeys( lastName );

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            browser.refresh();

            browser.sleep(3000);

            var resFirstName = firstEditButton.element(by.xpath("../..")).all(by.css('td')).get(1);
            var resLastName = firstEditButton.element(by.xpath("../..")).all(by.css('td')).get(2);

            resFirstName.getText().then(function(text) {
                expect(text).toEqual( firstName );
            });

            resLastName.getText().then(function(text) {
                expect(text).toEqual( lastName );
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


        function clearFields()
        {
            // tem que estar aqui porque por vezes apenas limpava um dos campos
            browser.sleep(500);

            //limpar campos
            firstNameInput.clear();
            lastNameInput.clear();

        }

    });

    
});