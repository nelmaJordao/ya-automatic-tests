describe('customer admin', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('employees', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow;
        
        beforeEach(function()
        {
            menuIcon = element(by.css('#customer-admin-module'));
            tabItem = element(by.css('#customer-admin-menu-region [data-sec="employees"]'));
            modalWindow = element(by.css('#employee-delete-modal'));
        });

        it('should open delete employee modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var filterButton = element(by.css('button[data-id="js-multi-filter"]'));
            var filterOption = element(by.css('li[data-original-index="3"]'));
            generalHelper.waitElementClickableAndClick(filterButton);
            generalHelper.waitElementClickableAndClick(filterOption);

            browser.sleep(1000);

            var firstDeleteButton = element.all(by.css('#customer-admin-employees-dtable a.js-delete-employee')).get(0);
            generalHelper.waitElementClickableAndClick(firstDeleteButton);

            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should confirm delete modal window', function()
        {
            var confirmDeleteButton = modalWindow.element(by.css('.btn-success'));
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(confirmDeleteButton);

            browser.sleep(1000);

            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text)
            {
                expect(parseInt(text)).toEqual(--recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
      
    });
    
});