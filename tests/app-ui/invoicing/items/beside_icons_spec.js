describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Items', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            showButton,
            modalWindow,
            editModal;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="items"]'));
            firstRow = element.all(by.css('#invoicing-items-dtable tr')).get(0);
            showButton = firstRow.element(by.css('.options .js-show-item'));
            modalWindow = element(by.css('#invoicing-read-item-modal'));
            editModal = element(by.css('#invoicing-update-item-modal'));
        });

        it('should open show item modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should open edit modal window', function () {
            var crudEdit = modalWindow.element(by.css('.crud [data-trigger="invoicing:items:update:item"]'));
            generalHelper.waitElementClickableAndClick(crudEdit);

            browser.sleep(1000);

            expect(editModal.isDisplayed()).toBe(true);

        });


        it('should open show modal window again', function () {
            var crudShow = editModal.element(by.css('.crud [data-trigger="invoicing:items:read:item"]'));
            generalHelper.waitElementClickableAndClick(crudShow);

            expect(modalWindow.isDisplayed()).toBe(true);

        });


        it('should close show modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);

        });



        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});