describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Items', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow;
            
        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="items"]'));
            firstRow = element.all(by.css('#invoicing-items-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-read-item-modal'));
        });

        it('should open show item modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            var showButton = firstRow.element(by.css('.options .js-show-item'));
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });
        
        it('should check for empty fields', function () {
            //type
            var typeEl = modalWindow.all(by.css('.form-group p.value')).get(0);
            expect(typeEl.getText()).not.toEqual('');
            
            //family
            var familyEl = modalWindow.all(by.css('.form-group p.value')).get(1);
            expect(familyEl.getText()).not.toEqual('');
            
            //code
            var codeEl = modalWindow.all(by.css('.form-group p.value')).get(2);
            expect(codeEl.getText()).not.toEqual('');
            
            //description
            var descriptionEl = modalWindow.all(by.css('.form-group p.value')).get(3);
            expect(descriptionEl.getText()).not.toEqual('');

            //price
            var priceEl = modalWindow.all(by.css('.form-group p.value')).get(4);
            expect(priceEl.getText()).not.toEqual('');

            //vat
            var vatEl = modalWindow.all(by.css('.form-group p.value')).get(5);
            expect(vatEl.getText()).not.toEqual('');
        });
        
        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);
            
            expect(modalWindow.isPresent()).toBe(false);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
    
    });

});