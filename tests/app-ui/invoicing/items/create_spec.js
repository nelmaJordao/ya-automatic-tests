describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Items', function () {
        
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButton;
            
        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="items"]'));
            newButton = element(by.css('.js-new-item'));
            modalWindow = element(by.css('#invoicing-create-item-modal'));
            okButton = modalWindow.element(by.css('.btn-success'));
        });
            
        it('should open new item modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });
        
        it('should check for empty fields', function () {

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);
            
            var errors = modalWindow.all(by.css('.form-group.error'));
            
            expect(errors.count()).toEqual(5);
            
        });        

        it('should fill mandatory fields and submit', function() {

            //select family category
            var selectpickerFamilyCategory = modalWindow.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = modalWindow.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);

            //select item family
            var selectpickerItemFamily = modalWindow.element(by.css('[data-id="itemFamily"]'));
            var selectpickerItemFamilyOption = modalWindow.all(by.css('.dropdown-menu.open')).get(1).element(by.css('li[data-original-index="5"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemFamily);
            generalHelper.waitElementClickableAndClick(selectpickerItemFamilyOption);
            
            //code
            var codeInput = modalWindow.element(by.css('#code'));
            codeInput.sendKeys(Math.floor((Math.random() * 1000) + 1));
            
            //description
            var descriptionTextarea = modalWindow.element(by.css('#description'));
            descriptionTextarea.sendKeys('AAAAAAAAAAAAAAAAAAAAAAAAa');
            
            //price
            browser.executeScript('$("#invoicing-create-item-modal #price").val(' + (Math.floor(Math.random() * 1000) + 1) + ')');
            browser.executeScript('$("#invoicing-create-item-modal #price").blur()');

            //select family category
            var selectpickerItemVat = modalWindow.element(by.css('[data-id="itemVat"]'));
            var selectpickerItemVatOption = modalWindow.all(by.css('.dropdown-menu.open')).get(2).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemVat);
            generalHelper.waitElementClickableAndClick(selectpickerItemVatOption);

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();
            
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });

        });
        
        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
            
    });
        
});