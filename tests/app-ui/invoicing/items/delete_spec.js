describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Items', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow,
            okButton;
        
        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="items"]'));
            modalWindow = element(by.css('#invoicing-delete-item-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
        });
        
        it('should open delete item modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            var deleteButton = element.all(by.css('.options .js-delete-item')).get(0);
            generalHelper.waitElementClickableAndClick(deleteButton);

            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });
    
        it('should confirm delete modal window', function() {
            var okButton = modalWindow.element(by.css('.btn-success.ok'));
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();
            
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(--recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
    
    });
    
});