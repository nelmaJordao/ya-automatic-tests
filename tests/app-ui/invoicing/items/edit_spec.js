describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Items', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow,
            okButton,
            closeButton;
        
        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="items"]'));
            firstRow = element.all(by.css('#invoicing-items-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-update-item-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            closeButton = modalWindow.element(by.css('.btn-default.cancel'));
        });
        
        it('should open edit item modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            var editButton = firstRow.element(by.css('.options .js-edit-item'));
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should cancel edit item', function () {
            var cancelButton = element(by.css('button.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            browser.sleep(1000);
        });


        it('should open edit item modal window', function () {
            var editButton = firstRow.element(by.css('.options .js-edit-item'));
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });
        
        it('should check for empty fields', function () {
            //select family category
            var selectpickerFamilyCategory = modalWindow.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = modalWindow.all(by.css('.dropdown-menu')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);

            browser.sleep(1000);

            var selectpickerFamily = modalWindow.element(by.css('[data-id="itemFamily"]'));
            // var selectpickerFamilyOption = modalWindow.all(by.css('.dropdown-menu')).get(1).element(by.css('li[data-original-index="1"]'));
            var selectpickerFamilyOption = modalWindow.element(by.xpath('/html/body/div[2]/div[2]/div/div[2]/div/form/div[1]/div[2]/div/div/div/ul/li[2]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamily);
            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyOption);

            browser.sleep(10000);

            var familyModal = element(by.css('#invoicing-create-family-modal'));

            generalHelper.waitElementPresent(familyModal);

            var typeSelect = familyModal.element(by.css('button[data-id="familyCategory"]'));
            var typeOption = familyModal.element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(typeSelect);
            generalHelper.waitElementClickableAndClick(typeOption);

            var shortDescription = familyModal.element(by.css('#shortDescription'));
            shortDescription.sendKeys(config.inputs.getDescription(10));

            var description = familyModal.element(by.css('#description'));
            description.sendKeys(config.inputs.getDescription(100));

            var familyOkButton = familyModal.element(by.css('.btn-success'));
            generalHelper.waitElementClickableAndClick(familyOkButton);

            //description
            var descriptionTextarea = modalWindow.element(by.css('#description'));
            descriptionTextarea.clear();


            generalHelper.waitElementClickableAndClick(okButton);
            
            var errors = modalWindow.all(by.css('.form-group.error'));
            
            expect(errors.count()).toEqual(1);
        });

        it('should close and reopen edit item modal window', function () {
            generalHelper.waitElementClickableAndClick(closeButton);
            var editButton = firstRow.element(by.css('.options .js-edit-item'));
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });
        
        it('should fill mandatory fields and submit', function() {
            //description
           
            var descriptionTextarea = modalWindow.element(by.css('#description'));
            descriptionTextarea.clear();
            descriptionTextarea.sendKeys('AAAAAAAAA');
            
            //price
            
            browser.executeScript('$("#invoicing-update-item-modal #price").val(' + (Math.floor(Math.random() * 1000) + 1) + ')');
            browser.executeScript('$("#invoicing-update-item-modal #price").blur()');

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            var descriptionCell = firstRow.all(by.css('td')).get(2);

            descriptionCell.getText().then(function(text) {
                expect(text).toEqual('AAAAAAAAA');
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
    
    });
    
});