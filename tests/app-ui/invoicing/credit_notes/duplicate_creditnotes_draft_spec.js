describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Creditnotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            creditnotesTabItem,
            firstRow,
            modalWindow,
            creditnotesCount;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            creditnotesTabItem = element(by.css('#invoicing-menu-region [data-sec="creditnotes"]'));
            firstRow = element.all(by.css('#invoicing-creditnotes-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-create-creditnote-modal'));
        });

        it('should check number of creditnotes', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);



            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(creditnotesTabItem);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            creditnotesCount = 0;
            records.getText().then(function(text) {
                creditnotesCount = parseInt(text);
            });
        });

        it('should clone creditnote as creditnote', function () {
            //preparation
            //action
            //assertion
            generalHelper.waitElementClickableAndClick(creditnotesTabItem);

            var duplicateDiv = firstRow.element(by.css('.options div.dropdown[data-original-title="Dupliquer"]'));
            var duplicateButton = duplicateDiv.element(by.css('.dropdown-toggle'));
            var duplicateOption = duplicateDiv.element(by.css('ul.dropdown-menu li a[data-clone-target="creditnotes"]'))

            browser.sleep(1000);
            duplicateButton.click();
            duplicateOption.click();
            browser.sleep(1000);

            var saveButton = modalWindow.element(by.css('div.custom-invoicing-modal-row-2 .btn-success'));
            generalHelper.waitElementClickableAndClick(saveButton);
        });

        it('should confirm duplicated creditnote', function() {
            generalHelper.waitElementClickableAndClick(creditnotesTabItem);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++creditnotesCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});