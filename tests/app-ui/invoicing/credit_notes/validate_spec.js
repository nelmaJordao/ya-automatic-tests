describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Creditnotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow,
            okButton,
            issueDate,
            creditnotesTBody,
            creditnotesList,
            editButton;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="creditnotes"]'));
            modalWindow = element(by.css('#invoicing-update-creditnote-modal'));
            issueDate = modalWindow.element(by.css('#issuedDate'));
            okButton = modalWindow.element(by.css('div [data-original-title="Valider et imprimer"]'));
            creditnotesTBody = element(by.css('#invoicing-creditnotes-dtable'));
            creditnotesList = creditnotesTBody.all(by.css('tr'));
            editButton = creditnotesList.get(0).element(by.css('.js-edit-creditnote'));
        });

        it('should open edit credit note modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var stateType = element(by.css('button[data-id="state"]'));
            var stateOption = element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(stateType);
            generalHelper.waitElementClickableAndClick(stateOption);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should validate credit note', function() {
            generalHelper.waitElementClickableAndClick(okButton);

            var confirmModal = element(by.css('#invoicing-confirm-print-creditnote-modal'));
            var confirmButton = confirmModal.element(by.css('.btn.btn-success.ladda-button.ok'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(confirmButton);

            var printModal = element(by.css('#credit-note-print-pdf'));
            var closePrintButton = printModal.element(by.css('.btn.btn-default.cancel'));

            generalHelper.waitElementClickableAndClick(closePrintButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});