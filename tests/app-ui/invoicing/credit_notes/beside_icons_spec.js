describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Creditnotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            showButton,
            modalWindow,
            editModal,
            deleteModal;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="creditnotes"]'));
            firstRow = element.all(by.css('#invoicing-creditnotes-dtable tr')).get(1);
            showButton = firstRow.element(by.css('.options .js-show-creditnote'));
            modalWindow = element(by.css('#invoicing-read-creditnote-modal'));
            editModal = element(by.css('#invoicing-update-creditnote-modal'));
            deleteModal = element(by.css('#invoicing-delete-creditnote-modal'));
        });

        it('should open show receipt modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });


        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);

        });

        it('should open show receipt modal window', function () {
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should open edit modal window', function () {
            var crudEdit = modalWindow.element(by.css('.crud [data-trigger="invoicing:creditnotes:update:creditnote"]'));
            generalHelper.waitElementClickableAndClick(crudEdit);

            browser.sleep(1000);

            expect(editModal.isDisplayed()).toBe(true);

        });

        it('should open show modal window again', function () {
            var crudShow = editModal.element(by.css('.crud [data-trigger="invoicing:creditnotes:read:creditnote"]'));
            generalHelper.waitElementClickableAndClick(crudShow);

            browser.sleep(1000);

            expect(modalWindow.isDisplayed()).toBe(true);

        });


        it('should open delete modal window', function() {
            var crudDelete = modalWindow.element(by.css('.crud [data-trigger="invoicing:creditnotes:delete:creditnote"]'));
            generalHelper.waitElementClickableAndClick(crudDelete);

            browser.sleep(1000);

            expect(deleteModal.isDisplayed()).toBe(true);

            var cancelButton = deleteModal.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            expect(deleteModal.isPresent()).toBe(false);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});