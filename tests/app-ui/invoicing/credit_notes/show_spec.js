describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Creditnotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow,
            invoiceDiv;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="creditnotes"]'));
            firstRow = element.all(by.css('#invoicing-creditnotes-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-read-creditnote-modal'));
            invoiceDiv = modalWindow.element(by.css('#invoicing-creditnote-form-items'));
        });

        it('should open show receipt modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            var showButton = firstRow.element(by.css('.options .js-show-creditnote'));
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {
            //client name
            var name = modalWindow.element(by.css('.form-group .name'));
            expect(name.getText()).not.toBe('');

            //date
            var date = modalWindow.all(by.css('.form-group .value')).get(0);
            expect(date.getText()).not.toBe('');

            browser.sleep(1000);

        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);
            
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});