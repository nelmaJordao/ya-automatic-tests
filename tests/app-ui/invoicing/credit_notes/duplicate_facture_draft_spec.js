describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Creditnotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            creditnotesTabItem,
            invoicesTabItem,
            firstRow,
            modalWindow,
            invoicesCount;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            creditnotesTabItem = element(by.css('#invoicing-menu-region [data-sec="creditnotes"]'));
            invoicesTabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
            firstRow = element.all(by.css('#invoicing-creditnotes-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-create-invoice-modal'));
        });

        it('should check number of invoices', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(invoicesTabItem);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            invoicesCount = 0;
            records.getText().then(function(text) {
                invoicesCount = parseInt(text);
            });
        });

        it('should clone creditnote as invoice', function () {

            generalHelper.waitElementClickableAndClick(creditnotesTabItem);

            var duplicateDiv = firstRow.element(by.css('.options div.dropdown[data-original-title="Dupliquer"]'));
            var duplicateButton = duplicateDiv.element(by.css('.dropdown-toggle'));
            var duplicateOption = duplicateDiv.element(by.css('ul.dropdown-menu li a[data-clone-target="invoices"]'))

            browser.sleep(1000);
            duplicateButton.click();
            duplicateOption.click();
            browser.sleep(1000);

            var saveButton = modalWindow.element(by.css('div.custom-invoicing-modal-row-2 .btn-success'));
            generalHelper.waitElementClickableAndClick(saveButton);
        });

        it('should confirm duplicated invoice', function() {
            generalHelper.waitElementClickableAndClick(invoicesTabItem);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++invoicesCount);
            });

        });
        
        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});