describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Creditnotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButtonCreditNote,
            creditnoteIssueDate;


        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="creditnotes"]'));
            newButton = element(by.css('.js-new-creditnote'));
            modalWindow = element(by.css('#invoicing-create-creditnote-modal'));
            creditnoteIssueDate = modalWindow.element(by.css('#issuedDate'));
            okButtonCreditNote = modalWindow.element(by.css('[data-original-title="Brouillon"]'));

        });

        it('should open new credit note modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for zero value', function () {

            var table = element.all(by.css('#invoicing-creditnote-form-items tr')).get(1);

            var deleteFirst = table.element(by.css('.js-delete-item'));
            
            generalHelper.waitElementClickableAndClick(deleteFirst);
            
            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButtonCreditNote);

            generalHelper.waitAlertDangerPresent();

            generalHelper.waitAlertDangerPresentAndClose();

        });

        it('should fill mandatory fields and submit', function () {
            var customerName = "ALTERNA";

            var customerSelectContainer = element(by.css('div[data-key="customer"]'));
            generalHelper.selectTypeAheadItem(customerSelectContainer, 'input#customer', customerName, 1);

            var editClientButton = modalWindow.element(by.css('.js-edit-customer'));

            generalHelper.waitElementClickableAndClick(editClientButton);

            browser.sleep(1000);

            var editClientModal = element(by.css('#company-update-modal'));
            var emailInput = editClientModal.element(by.css('#email'));
            var phoneInput = editClientModal.element(by.css('#phone'));
            emailInput.clear();
            phoneInput.clear();
            emailInput.sendKeys('xpto@xpto.biz');
            phoneInput.sendKeys('123456789');

            var editClientSave = editClientModal.element(by.css('.btn-success'));

            generalHelper.waitElementClickableAndClick(editClientSave);

            generalHelper.waitElementNotPresent(editClientModal);

            var checkEmail = modalWindow.element(by.css('table[data-sidepanel="company"] tbody tr td.values[data-field="email"]'));
            var checkPhone = modalWindow.element(by.css('table[data-sidepanel="company"] tbody tr td.values[data-field="phone"]'));

            expect(checkEmail.getText()).toEqual('xpto@xpto.biz');
            expect(checkPhone.getText()).toEqual('123456789');

            browser.sleep(1000);

            var invoicePickerType = modalWindow.element(by.css('div.bootstrap-select.form-control'));
            var invoicePickerOption = modalWindow.element(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"]'));

            generalHelper.waitElementClickableAndClick(invoicePickerType);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(invoicePickerOption);

            browser.sleep(1000);

            var itemDiv = modalWindow.element(by.css('#invoicing-creditnote-form-items'));

            var addVat = modalWindow.element(by.css('.js-add-item'));

            generalHelper.waitElementClickableAndClick(addVat);

            browser.sleep(1000);

            generalHelper.selectTypeAheadCreateNew(itemDiv, "input.tt-input");

            var newArticle = element(by.css('#invoicing-create-item-modal'));

            //select family category
            var selectpickerFamilyCategory = newArticle.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = newArticle.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);

            //code
            var codeInput = newArticle.element(by.css('#code'));
            codeInput.sendKeys(config.inputs.getRandomName(10));

            //description
            var descriptionTextarea = newArticle.element(by.css('#description'));
            descriptionTextarea.sendKeys(config.inputs.getDescription(1000));

            //price
            browser.executeScript('$("#invoicing-create-item-modal #price").val(' + (Math.floor(Math.random() * 1000) + 1) + ')');
            browser.executeScript('$("#invoicing-create-item-modal #price").blur()');

            //select family category
            var selectpickerItemVat = newArticle.element(by.css('[data-id="itemVat"]'));
            var selectpickerItemVatOption = newArticle.all(by.css('.dropdown-menu.open')).get(2).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemVat);
            generalHelper.waitElementClickableAndClick(selectpickerItemVatOption);

            //select item family
            var selectpickerItemFamily = newArticle.element(by.css('[data-id="itemFamily"]'));
            var selectpickerItemFamilyOption = newArticle.all(by.css('.dropdown-menu.open')).get(1).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemFamily);
            generalHelper.waitElementClickableAndClick(selectpickerItemFamilyOption);

            browser.sleep(1000);

            var newFamilyArticle = element(by.css('#invoicing-create-family-modal'));

            //select family category
            var selectpickerFamilyCategory = newFamilyArticle.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = newFamilyArticle.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);


            var shortDescription = newFamilyArticle.element(by.css('#shortDescription'));
            shortDescription.sendKeys("teste");

            var description = newFamilyArticle.element(by.css('#description'));
            description.sendKeys("Teste1");


            var okButtonFamily = newFamilyArticle.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButtonFamily);
            generalHelper.waitAlertSuccessPresentAndClose();

            var okButtonArticle = newArticle.element(by.css('.btn-success.ok'));

            browser.executeScript('$(".btn-success.ok.hidden").removeClass("hidden")');

            generalHelper.waitElementClickableAndClick(okButtonArticle);
            generalHelper.waitAlertSuccessPresentAndClose();

            // var newClient = element(by.css('#company-create-modal'));

            generalHelper.waitElementClickableAndClick(addVat);

            browser.sleep(1000);

            generalHelper.selectTypeAheadCreateNew(itemDiv, "input.tt-input");

            var newArticle = element(by.css('#invoicing-create-item-modal'));

            //select family category
            var selectpickerFamilyCategory = newArticle.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = newArticle.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="2"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);

            //code
            var codeInput = newArticle.element(by.css('#code'));
            codeInput.sendKeys(config.inputs.getRandomName(10));

            //description
            var descriptionTextarea = newArticle.element(by.css('#description'));
            descriptionTextarea.sendKeys(config.inputs.getDescription(1000));

            //price
            browser.executeScript('$("#invoicing-create-item-modal #price").val(' + (Math.floor(Math.random() * 1000) + 1) + ')');
            browser.executeScript('$("#invoicing-create-item-modal #price").blur()');

            //select family category
            var selectpickerItemVat = newArticle.element(by.css('[data-id="itemVat"]'));
            var selectpickerItemVatOption = newArticle.all(by.css('.dropdown-menu.open')).get(2).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemVat);
            generalHelper.waitElementClickableAndClick(selectpickerItemVatOption);

            //select item family
            var selectpickerItemFamily = newArticle.element(by.css('[data-id="itemFamily"]'));
            var selectpickerItemFamilyOption = newArticle.all(by.css('.dropdown-menu.open')).get(1).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemFamily);
            generalHelper.waitElementClickableAndClick(selectpickerItemFamilyOption);

            browser.sleep(1000);

            var newFamilyArticle = element(by.css('#invoicing-create-family-modal'));

            //select family category
            var selectpickerFamilyCategory = newFamilyArticle.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = newFamilyArticle.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="2"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);


            var shortDescription = newFamilyArticle.element(by.css('#shortDescription'));
            shortDescription.sendKeys("teste");

            var description = newFamilyArticle.element(by.css('#description'));
            description.sendKeys("Teste1");


            var okButtonFamily = newFamilyArticle.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButtonFamily);
            generalHelper.waitAlertSuccessPresentAndClose();

            var okButtonArticle = newArticle.element(by.css('.btn-success.ok'));

            browser.executeScript('$(".btn-success.ok.hidden").removeClass("hidden")');

            generalHelper.waitElementClickableAndClick(okButtonArticle);
            generalHelper.waitAlertSuccessPresentAndClose();

            // var newClient = element(by.css('#company-create-modal'));


            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            //save quote
            generalHelper.waitElementClickableAndClick(okButtonCreditNote);
            

            //check number of credit notes increased by 1
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(recordCount++);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });

});