describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Creditnotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow,
            receiptsTBody,
            receiptsList,
            sendmailButton,
            resendmailButton,
            emailInput;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="creditnotes"]'));
            modalWindow = element(by.css('#credit-note-send-email'));
            receiptsTBody = element(by.css('#invoicing-creditnotes-dtable'));
            receiptsList = receiptsTBody.all(by.css('tr'));
            sendmailButton = receiptsList.get(0).element(by.css('.js-send-creditnote'));
            resendmailButton = receiptsList.get(0).element(by.css('.js-send-creditnote.active'));
            emailInput = modalWindow.element(by.css('#email'));
        });

        it('should open send email modal', function () {

            var stateType = element(by.css('button[data-id="state"]'));
            var stateOption = element(by.css('li[data-original-index="2"]'));
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);


            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(stateType);
            generalHelper.waitElementClickableAndClick(stateOption);
            generalHelper.waitElementClickableAndClick(sendmailButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should send email', function () {
            var email = 'xpto@xpto.biz';
            var sendButton = modalWindow.element(by.css('.btn-success.ok'));

            emailInput.clear().sendKeys(email);

            var emailComment = modalWindow.element(by.css('#emailComment'));
            emailComment.sendKeys('Email email!');

            generalHelper.waitElementClickableAndClick(sendButton);

            generalHelper.waitAlertSuccessPresentAndClose();
        });

        it('should see resend button', function () {
            browser.refresh();
            browser.sleep(10000);
            expect(resendmailButton.isPresent()).toBe(true);
            expect(resendmailButton.isDisplayed()).toBe(true);
        });

        it('should see email filled when resending', function () {
            generalHelper.waitElementClickableAndClick(resendmailButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

            generalHelper.waitElementPresent(emailInput);
            generalHelper.waitElementDisplayed(emailInput);

            expect(emailInput.getAttribute('value')).not.toEqual('');

            var emailComment = modalWindow.element(by.css('#emailComment'));

            expect(emailComment.getAttribute('value')).toEqual('');

            emailComment.sendKeys('Email email update!');

            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));

            generalHelper.waitElementClickableAndClick(closeButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });

});