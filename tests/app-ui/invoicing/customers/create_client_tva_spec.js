describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var sirenHelper = requireCommonHelper('sirenHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Customers', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButton;


        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
            newButton = element(by.css('.js-new-invoice'));
            modalWindow = element(by.css('#invoicing-create-invoice-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
        });

        it('should open new family modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should create new entreprise client', function () {

            var itemDiv = element(by.css('#invoicing-invoice-form-items'));

            browser.sleep(1000);

            generalHelper.selectTypeAheadItem(itemDiv, '.js-item.tt-input', 'A', 1);

            browser.sleep(1000);

            var nameInput = modalWindow.element(by.css('.js-auto-complete'));
            generalHelper.selectTypeAheadCreateNew(nameInput, 'input#customer');

            browser.sleep(1000);

            var createNewModalWindow = element(by.css('#company-create-modal'));
            var nameFiled = createNewModalWindow.element(by.css('#name'));
            nameFiled.clear();
            nameFiled.sendKeys("Test Void");

            browser.sleep(1000);

            var tvaOption = createNewModalWindow.element(by.css('#radio-vat'));
            generalHelper.waitElementClickableAndClick(tvaOption);

            var tvaInputField = createNewModalWindow.element(by.css('#vatNumber'));
            tvaInputField.sendKeys(sirenHelper.getTVA());

            var checkTVA = createNewModalWindow.element(by.css('.js-base-company-set.js-check-vat'));
            generalHelper.waitElementClickableAndClick(checkTVA);

            var okButton = createNewModalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(2000);

            var saveButton = modalWindow.all(by.css('.btn-success.btn-ok')).get(1);
            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(1000);

            var validationModalWindow = element(by.css('#invoicing-confirm-print-invoice-modal'));
            var validationButton = validationModalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(validationButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});