describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var sirenHelper = requireCommonHelper('sirenHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Customers', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButton;


        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="customers"]'));
            newButton = element(by.css('.js-new-customer'));
            modalWindow = element(by.css('#company-create-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
        });

        it('should open new family modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should create new entreprise client', function () {

            var companyName = 'a';
            var nameInput = modalWindow.element(by.css('div.js-typeahead-company'));

            generalHelper.selectTypeAheadItem(nameInput, 'input#name', companyName, 1);

            browser.sleep(1000);

            var nameFiled = modalWindow.element(by.css('#name'));
            nameFiled.sendKeys("Test Void");

            browser.sleep(3000);

            var sirenInputField = modalWindow.element(by.css('#sirenNumber'));
            sirenInputField.sendKeys(sirenHelper.getSiren());

            browser.sleep(3000);

            generalHelper.waitElementClickableAndClick(okButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});