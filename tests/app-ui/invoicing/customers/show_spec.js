describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Customers', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="customers"]'));
            firstRow = element.all(by.css('.table.table-striped.table-hover tr')).get(1);
            modalWindow = element(by.css('div#company-read-modal'));
        });

        it('should open show family modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            var showButton = firstRow.element(by.css('.options .js-show-customer'));
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {
            var name = modalWindow.element(by.css('h3.name'));
            expect(name.getText()).not.toEqual('');

            var tva = modalWindow.all(by.css('.form-group p.value')).get(0);
            expect(tva.getText()).not.toEqual('');

            var siren = modalWindow.all(by.css('.form-group p.value')).get(1);
            expect(siren.getText()).not.toEqual('');

            var siret = modalWindow.all(by.css('.form-group p.value')).get(2);
            expect(siret.getText()).not.toEqual('');

            var address = modalWindow.all(by.css('.form-group p.value')).get(3);
            expect(address.getText()).not.toEqual('');

            var country = modalWindow.all(by.css('.form-group p.value')).get(5);
            expect(country.getText()).not.toEqual('');

            var delayReglement = modalWindow.all(by.css('.form-group p.value')).get(9);
            expect(delayReglement.getText()).not.toEqual('');

        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});