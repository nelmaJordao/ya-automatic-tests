describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Customers', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButton,
            emailInput,
            siteInput;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="customers"]'));
            newButton = element(by.css('.js-new-customer'));
            modalWindow = element(by.css('#company-create-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ladda-button.ok'));
            emailInput = modalWindow.element(by.css('input#email'));
            siteInput = modalWindow.element(by.css('input#website'));
        });

        it('should open new family modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should test field restrictions', function () {
            var companyName = 'a';

            var nameInput = element(by.css('div.js-typeahead-company'));

            generalHelper.selectTypeAheadItem(nameInput, 'input#name', companyName, 1);

            var expandButton =  modalWindow.all(by.css('.js-collapse-fields-address')).last();
            generalHelper.waitElementClickableAndClick(expandButton);

            browser.sleep(1000);

            emailInput.clear();
            siteInput.clear();

            emailInput.sendKeys('a');
            siteInput.sendKeys('a');

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

            var errors = modalWindow.all(by.css('.form-group.error'));
            expect(errors.count()).toEqual(2);
        });


        it('should fill mandatory fields and submit', function() {

            emailInput.clear();
            siteInput.clear();
            emailInput.sendKeys('email@xpto.biz');
            siteInput.sendKeys('www.xpto.biz');

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            browser.sleep(1000);

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});