describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Families', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow,
            okButton,
            emailInput,
            siteInput;

        beforeEach(function() {

            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="customers"]'));
            firstRow = element.all(by.css('#invoicing-customers-dtable tr')).first();
            modalWindow = element(by.css('div#company-update-modal'));
            okButton = modalWindow.element(by.css('.btn-success'));
            emailInput = modalWindow.element(by.css('input#email'));
            siteInput = modalWindow.element(by.css('input#website'));
        });

        it('should open edit customer modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            // var searchField = element(by.css('#searchTable'));
            // searchField.sendKeys('CUIR SHOP');
            //
            // var searchButton = element(by.css('.js-search-btn'));
            // generalHelper.waitElementClickableAndClick(searchButton);
            //

            browser.sleep(1000);

            var editButton = firstRow.element(by.css('.js-edit-customer'));
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should cancel edit customer', function () {
            var cancelButton = element(by.css('button.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            browser.sleep(1000);
        });

        it('should open edit family modal window', function () {
            var editButton = firstRow.element(by.css('.js-edit-customer'));
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should test field restrictions', function () {
            emailInput.clear();
            siteInput.clear();

            emailInput.sendKeys('a');
            siteInput.sendKeys('a');

            generalHelper.waitElementClickableAndClick(okButton);
            
            var errors = modalWindow.all(by.css('.form-group.error'));
            expect(errors.count()).toEqual(2);
        });

        it('should fill mandatory fields and submit', function() {

            var streetInput = modalWindow.element(by.css('#streetLine1'));
            streetInput.clear();
            streetInput.sendKeys("Test street");

            var addressCompleteInput = modalWindow.element(by.css('#streetLine2'));
            addressCompleteInput.clear();
            addressCompleteInput.sendKeys("Test address");

            var postalCodeInput = modalWindow.element(by.css('#postalCode'));
            postalCodeInput.clear();
            postalCodeInput.sendKeys("1234-567");

            var cityInput = modalWindow.element(by.css('#city'));
            cityInput.clear();
            cityInput.sendKeys("Test town");

            var telephoneInput = modalWindow.element(by.css('#phone'));
            telephoneInput.clear();
            telephoneInput.sendKeys("123456789");

            //teste de erros nos fields de email e website

            emailInput.clear();
            siteInput.clear();
            emailInput.sendKeys('xpto@xpto.biz');
            siteInput.sendKeys('www.xpto.biz');

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            var telephoneCell = firstRow.all(by.css('td')).get(3);

            telephoneCell.getText().then(function(text) {
                expect(text).toEqual("123456789");
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});