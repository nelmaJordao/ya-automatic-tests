describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Families', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="customers"]'));
            firstRow = element.all(by.css('#invoicing-customers-dtable tr')).first();
        });

        it('should navigate to customer situation screen', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var balanceLink = firstRow.element(by.css('.js-show-customer-balance'));
            generalHelper.waitElementClickableAndClick(balanceLink);

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var string1 = array[1].split('/')[1];
                var string2 = array[1].split('/')[2];

                expect(string1).toEqual('customers');
                expect(string2).toEqual('balance');
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});