describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Families', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow,
            okButton;
        
        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="families"]'));
            firstRow = element.all(by.css('#invoicing-itemfamilies-dtable tr')).first();
            modalWindow = element(by.css('#invoicing-update-family-modal'));
            okButton = modalWindow.element(by.css('.btn-success'));
        });

        it('should open edit family modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            var editButton = firstRow.element(by.css('.js-edit-family'));

            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should cancel edit family', function () {
            var cancelButton = element(by.css('button.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            browser.sleep(1000);
        });

        it('should open edit family modal window', function () {
            var editButton = firstRow.element(by.css('.js-edit-family'));

            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });
        
        it('should check for empty fields', function () {
            //select short description
            var shortDescriptionInput = modalWindow.element(by.css('#shortDescription'));
            shortDescriptionInput.clear();
            
            //select description
            var descriptionInput = modalWindow.element(by.css('#description'));
            descriptionInput.clear();
            
            generalHelper.waitElementClickableAndClick(okButton);
            
            var errors = modalWindow.all(by.css('.form-group.error'));
            
            expect(errors.count()).toEqual(2);
        });
        
        it('should fill mandatory fields and submit', function() {
            
            //select short description
            var shortDescriptionInput = modalWindow.element(by.css('#shortDescription'));
            shortDescriptionInput.clear();
            shortDescriptionInput.sendKeys('AAA');
            
            //select description
            var descriptionInput = modalWindow.element(by.css('#description'));
            descriptionInput.clear();
            descriptionInput.sendKeys('AA');

            generalHelper.waitElementClickableAndClick(okButton);
            browser.sleep(1000);
            generalHelper.waitAlertSuccessPresentAndClose();
            
            var shortDescriptionCell = firstRow.all(by.css('td')).get(2);

            browser.sleep(1000);
            
            shortDescriptionCell.getText().then(function(text) {
                expect(text).toEqual('AAA');
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });
    
});