describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Families', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            showButton,
            modalWindow,
            editModal,
            deleteModal;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="families"]'));
            firstRow = element.all(by.css('#invoicing-itemfamilies-dtable tr')).first();
            showButton = firstRow.element(by.css('.js-show-family'));
            modalWindow = element(by.css('#invoicing-read-family-modal'));
            editModal = element(by.css('#invoicing-update-family-modal'));
            deleteModal = element(by.css('#invoicing-delete-family-modal'));
        });

        it('should open show family modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });


        it('should close show modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);

        });


        it('should open show modal window', function () {
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });


        it('should open edit modal window', function () {
            var crudEdit = modalWindow.element(by.css('.crud [data-trigger="invoicing:families:update:family"]'));
            generalHelper.waitElementClickableAndClick(crudEdit);

            expect(editModal.isDisplayed()).toBe(true);


        });


        it('should open show modal window again', function () {
            var crudShow = editModal.element(by.css('.crud [data-trigger="invoicing:families:read:family"]'));
            generalHelper.waitElementClickableAndClick(crudShow);

            browser.sleep(1000);

            expect(modalWindow.isDisplayed()).toBe(true);


        });


        it('should open delete modal window', function() {
            var crudDelete = modalWindow.element(by.css('.crud [data-trigger="invoicing:families:delete:family"]'));
            generalHelper.waitElementClickableAndClick(crudDelete);

            browser.sleep(1000);


            expect(deleteModal.isDisplayed()).toBe(true);

            var cancelButton = deleteModal.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            expect(deleteModal.isPresent()).toBe(false);

        });



        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});