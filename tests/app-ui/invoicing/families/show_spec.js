describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Families', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow;
        
        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="families"]'));
            firstRow = element.all(by.css('#invoicing-itemfamilies-dtable tr')).first();
            modalWindow = element(by.css('#invoicing-read-family-modal'));
        });

        it('should open show family modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            var showButton = firstRow.element(by.css('.js-show-family'));
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });
        
        it('should check for empty fields', function () {
            //type
            var typeEl = modalWindow.all(by.css('.form-group p.value')).get(0);
            expect(typeEl.getText()).not.toEqual('');
            
            //short description
            var shortDescriptionEl = modalWindow.all(by.css('.form-group p.value')).get(1);
            expect(shortDescriptionEl.getText()).not.toEqual('');
            
            //description
            var descriptionEl = modalWindow.all(by.css('.form-group p.value')).get(2);
            expect(descriptionEl.getText()).not.toEqual('');
        });
        
        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default'));
            generalHelper.waitElementClickableAndClick(closeButton);
            
            expect(modalWindow.isPresent()).toBe(false);

            });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });
    
});