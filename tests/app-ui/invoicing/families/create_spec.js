describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Families', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButton;
        
        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="families"]'));
            newButton = element(by.css('.js-new-family'));
            modalWindow = element(by.css('#invoicing-create-family-modal'));
            okButton = modalWindow.element(by.css('.btn-success'));
        });

        it('should open new family modal window', function () {

            browser.sleep(2000);
            generalHelper.waitElementClickableAndClick(menuIcon);
            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);
           
           expect(modalWindow.isDisplayed()).toBe(true);
        });
        
        it('should check for empty fields', function () {
            generalHelper.waitElementClickableAndClick(okButton);
            
            var errors = modalWindow.all(by.css('.form-group.error'));
            
            expect(errors.count()).toEqual(3);
        });
        
        it('should fill mandatory fields and submit', function() {
            //select type field
            var selectPickerType = modalWindow.element(by.css('[data-id="familyCategory"]'));
            var selectPickerOption = modalWindow.element(by.css('.dropdown-menu.open [data-original-index="1"]'));
            
            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(selectPickerType);
            generalHelper.waitElementClickableAndClick(selectPickerOption);
            
            //select short description
            var shortDescriptionInput = modalWindow.element(by.css('#shortDescription'));
            shortDescriptionInput.sendKeys('AA');
            
            //select description
            var descriptionInput = modalWindow.element(by.css('#description'));
            descriptionInput.sendKeys('A');
            
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();
            
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });
    
});