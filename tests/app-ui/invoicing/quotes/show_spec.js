describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Quotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow,
            closeButton,
            itemsDiv;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="quotes"]'));
            modalWindow = element(by.css('#invoicing-read-quote-modal'));
            closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            itemsDiv = modalWindow.element(by.css('#invoicing-quote-form-items'));
        });

        it('should open show quote modal window', function () {
            var quotesTBody = element.all(by.css('#invoicing-quotes-dtable tr')).first();
            var showButton = quotesTBody.element(by.css('.js-show-quote'));

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {
            //client
            var clientEl = modalWindow.all(by.css('.form-group h3.name')).get(0);
            expect(clientEl.getText()).not.toBe('');

            //date
            var dateEl = modalWindow.all(by.css('.form-group p.value')).get(0);
            expect(dateEl.getText()).not.toBe('');

            //expiration days
            var expDateEl = modalWindow.all(by.css('.form-group p.value')).get(1);
            expect(expDateEl.getText()).not.toBe('');

            //tva exception
            var tvaExceptionEl = modalWindow.all(by.css('.form-group p.value')).get(2);
            expect(tvaExceptionEl.getText()).not.toBe('');
            

            //products
            var productsList = itemsDiv.all(by.css('tr'));

            browser.sleep(1000);

            expect(productsList.count()).toBeGreaterThan(0);
        });

        it('should close show modal window', function() {
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});