describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Quotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow,
            quotesTBody,
            quotesList,
            sendmailButton,
            resendmailButton,
            emailInput;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="quotes"]'));
            modalWindow = element(by.css('#quote-send-email'));
            quotesTBody = element(by.css('#invoicing-quotes-dtable'));
            quotesList = quotesTBody.all(by.css('tr'));
            sendmailButton = quotesList.get(0).element(by.css('.js-send-quote'));
            resendmailButton = quotesList.get(0).element(by.css('.js-send-quote.active'));
            emailInput = modalWindow.element(by.css('#email'));
        });

        it('should open send email modal', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(sendmailButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should send email', function () {

            var cleanEmailInput = modalWindow.element(by.css('.js-reset-typeahead'));
            generalHelper.waitElementClickableAndClick(cleanEmailInput);

            browser.sleep(1000);

            var email = 'xpto@xpto.biz';
            emailInput.sendKeys(email);

            var emailComment = modalWindow.element(by.css('#emailComment'));
            emailComment.sendKeys('Email email!');

            var name = modalWindow.element(by.css('#name'));
            name.sendKeys('Diana');

            var countryButton = modalWindow.element(by.css('button[data-id="country"]'));
            var countryInput = modalWindow.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="179"] a'));
            generalHelper.waitElementClickableAndClick(countryButton);
            generalHelper.waitElementClickableAndClick(countryInput);

            var phoneNumber = modalWindow.element(by.css('#phoneNumber'));
            phoneNumber.sendKeys('914166467');

            var sendButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(sendButton);

            generalHelper.waitAlertSuccessPresentAndClose();

            browser.refresh();
            browser.sleep(2000);
        });

        it('should see email filled when resending', function () {
            generalHelper.waitElementClickableAndClick(resendmailButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

            generalHelper.waitElementPresent(emailInput);
            generalHelper.waitElementDisplayed(emailInput);

            expect(emailInput.getAttribute('value')).not.toEqual('');

            var emailComment = modalWindow.element(by.css('#emailComment'));

            expect(emailComment.getAttribute('value')).toEqual('');

            emailComment.sendKeys('Email email update!');

            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });

});