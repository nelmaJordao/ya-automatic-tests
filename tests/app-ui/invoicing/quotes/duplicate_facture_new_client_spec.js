describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Quotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            quotesTabItem,
            invoicesTabItem,
            firstRow,
            modalWindow,
            invoicesCount;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            quotesTabItem = element(by.css('#invoicing-menu-region [data-sec="quotes"]'));
            invoicesTabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
            firstRow = element.all(by.css('#invoicing-quotes-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-create-invoice-modal'));
        });

        it('should check number of invoices', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(invoicesTabItem);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            invoicesCount = 0;
            records.getText().then(function(text) {
                invoicesCount = parseInt(text);
            });
        });

        it('should clone quote as invoice', function () {
            generalHelper.waitElementClickableAndClick(quotesTabItem);

            var duplicateDiv = firstRow.element(by.css('.options div.dropdown[data-original-title="Dupliquer"]'));
            var duplicateButton = duplicateDiv.element(by.css('.dropdown-toggle'));
            var duplicateOption = duplicateDiv.element(by.css('ul.dropdown-menu li a[data-clone-target="invoices"]'))

            browser.sleep(1000);
            duplicateButton.click();
            duplicateOption.click();
            browser.sleep(1000);

            var clientName = firstRow.all(by.css('td')).get(3);
            var isPrivate = false;

            clientName.getText().then(function(text) {
                if(text.includes('private'))
                {
                    isPrivate = true;
                }

                var resetButton = modalWindow.element(by.css('.js-reset-typeahead'));
                generalHelper.waitElementClickableAndClick(resetButton);

                browser.sleep(1000);

                var clientContainer = modalWindow.element(by.css('div[data-key="customer"]'));

                generalHelper.selectTypeAheadCreateNew(clientContainer, '#customer');

                var createCustomerModal = element(by.css('#company-create-modal'));
                var saveButton;

                if(isPrivate)
                {
                    browser.sleep(1000);

                    var sirenInput = createCustomerModal.element(by.css('#sirenNumber'));
                    var sirenButton = createCustomerModal.all(by.css('.js-check-vat')).get(1);

                    sirenInput.sendKeys(450776968);

                    generalHelper.waitElementClickableAndClick(sirenButton);

                    saveButton = createCustomerModal.element(by.css('.btn-success'));

                    generalHelper.waitElementClickableAndClick(saveButton);
                }
                else
                {
                    browser.sleep(1000);

                    var nameInput = createCustomerModal.element(by.css('#name'));

                    nameInput.clear();

                    nameInput.sendKeys('private');

                    var radioPrivate = createCustomerModal.all(by.css('.js-radio span')).get(1);

                    generalHelper.waitElementClickableAndClick(radioPrivate);

                    saveButton = createCustomerModal.element(by.css('.btn-success'));
                }

                generalHelper.waitElementClickableAndClick(saveButton);

                generalHelper.waitElementNotPresent(createCustomerModal);

                browser.sleep(1000);

                saveButton = modalWindow.element(by.css('div.custom-invoicing-modal-row-2 .btn-success[data-original-title="Brouillon"]'));
                generalHelper.waitElementClickableAndClick(saveButton);

            });

            browser.sleep(1000);

        });

        it('should confirm duplicated invoice', function() {
            generalHelper.waitElementClickableAndClick(invoicesTabItem);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++invoicesCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});