describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Quotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow,
            firstRow,
            secondRow,
            sendmailButton,
            resendmailButton,
            emailInput;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="quotes"]'));
            modalWindow = element(by.css('#quote-send-email'));
            firstRow = element.all(by.css('#invoicing-quotes-dtable tr')).first();
            secondRow = element.all(by.css('#invoicing-quotes-dtable tr')).get(1);
            sendmailButton = firstRow.element(by.css('.js-send-quote'));
            resendmailButton = firstRow.element(by.css('.js-send-quote.active'));
            emailInput = modalWindow.element(by.css('#email'));
        });

        it('should open send email modal', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(sendmailButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should send email', function () {
            var email = 'xpto@xpto.biz';
            var sendButton = modalWindow.element(by.css('.btn-success.ok'));
            
            emailInput.clear().sendKeys(email);

            var emailComment = modalWindow.element(by.css('#emailComment'));
            emailComment.sendKeys('Email email!');

            var checkbox = modalWindow.element(by.css('div.js-approve-checkbox label.checkbox span'));
            generalHelper.waitElementClickableAndClick(checkbox);


            generalHelper.waitElementClickableAndClick(sendButton);

            generalHelper.waitAlertSuccessPresentAndClose();

            browser.refresh();
        });

        it('should see email filled when resending', function () {

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(resendmailButton);

            browser.sleep(1000);

            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

            generalHelper.waitElementPresent(emailInput);
            generalHelper.waitElementDisplayed(emailInput);

            expect(emailInput.getAttribute('value')).not.toEqual('');

            var emailComment = modalWindow.element(by.css('#emailComment'));

            expect(emailComment.getAttribute('value')).toEqual('');

            emailComment.sendKeys('Email email update!');

            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });

});