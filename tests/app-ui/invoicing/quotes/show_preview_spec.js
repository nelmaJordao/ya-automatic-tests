describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Recurrent Invoices', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            editButton,
            previewModal,
            tabItem,
            modalWindow;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="quotes"]'));
            editButton = element.all(by.css('.js-edit-quote')).get(0);
            modalWindow = element(by.css('#invoicing-update-quote-modal'));
            previewModal = element(by.css('#quote-preview-pdf'));

        });

        it('should open show receipt modal window', function () {

            //Abre o Invoice
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            //Muda para a tab das Quotes
            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            //Vai ao primeiro butao ativo de editar
            generalHelper.waitElementClickableAndClick(editButton);

            //Espera pelo modal
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

        });

        it('should show a preview of document', function () {

            //Carrega no Aperçu
            var previewButton = modalWindow.element(by.css('.js-preview'));
            generalHelper.waitElementClickableAndClick(previewButton);

            browser.sleep(1000);

            //Carrega em Ferme para fechar a janela de preview
            var closePreview = previewModal.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closePreview);

        });

        it('should close modal window', function() {
            //Carrega na cruz para fechar a janela de editar
            var closeButton = modalWindow.element(by.css('.close'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});