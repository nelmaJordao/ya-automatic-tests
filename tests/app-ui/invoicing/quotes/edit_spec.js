describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Quotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow,
            okButton,
            quoteIssueDate,
            itemDiv,
            selectItems;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="quotes"]'));
            firstRow = element.all(by.css('#invoicing-quotes-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-update-quote-modal'));
            quoteIssueDate = modalWindow.element(by.css('#issuedDate'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            itemDiv = modalWindow.element(by.css('#invoicing-quote-form-items'));
            selectItems = itemDiv.all(by.css('table tbody tr'));
        });

        it('should open edit quote modal window', function () {
            var quotesTBody = element(by.css('#invoicing-quotes-dtable'));
            var quotesList = quotesTBody.all(by.css('tr'));
            var editButton = quotesList.get(0).element(by.css('.js-edit-quote'));
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should cancel edit quote', function () {
            var cancelButton = element(by.css('button.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            browser.sleep(1000);
        });

        it('should open edit quote modal window', function () {
            var quotesTBody = element(by.css('#invoicing-quotes-dtable'));
            var quotesList = quotesTBody.all(by.css('tr'));
            var editButton = quotesList.get(0).element(by.css('.js-edit-quote'));
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should fill mandatory fields and submit', function() {

            var observationsInput = modalWindow.element(by.css('#observations'));
            observationsInput.clear();
            observationsInput.sendKeys('Editted');

            var addItemButton = itemDiv.element(by.css('.js-add-item'));

            generalHelper.waitElementClickableAndClick(addItemButton);

            browser.sleep(1000);

            generalHelper.selectTypeAheadItem(itemDiv, "input.tt-input", "s", 1);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            var observationCell = firstRow.all(by.css('td')).get(8);

            var observationButton = observationCell.element(by.css('a[data-original-title="Observation"]'));

            observationButton.getAttribute('data-content').then(function (text) {
                expect(text).toEqual('Editted');
            });

        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});