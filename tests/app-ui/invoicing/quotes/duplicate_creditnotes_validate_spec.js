describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Quotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            quotesTabItem,
            creditnotesTabItem,
            firstRow,
            modalWindow,
            modalWindow2,
            modalWindow3,
            creditnotesCount;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            quotesTabItem = element(by.css('#invoicing-menu-region [data-sec="quotes"]'));
            creditnotesTabItem = element(by.css('#invoicing-menu-region [data-sec="creditnotes"]'));
            firstRow = element.all(by.css('#invoicing-quotes-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-create-creditnote-modal'));
            modalWindow2 = element(by.css('#invoicing-confirm-print-creditnote-modal'));
            modalWindow3 = element(by.css('#credit-note-print-pdf'));
        });

        it('should check number of creditnotes', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(creditnotesTabItem);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            creditnotesCount = 0;
            records.getText().then(function(text) {
                creditnotesCount = parseInt(text);
            });
        });

        it('should clone quote as creditnote', function () {

            generalHelper.waitElementClickableAndClick(quotesTabItem);

            var duplicateDiv = firstRow.element(by.css('.options div.dropdown[data-original-title="Dupliquer"]'));
            var duplicateButton = duplicateDiv.element(by.css('.dropdown-toggle'));
            var duplicateOption = duplicateDiv.element(by.css('ul.dropdown-menu li a[data-clone-target="creditnotes"]'))

            browser.sleep(1000);
            duplicateButton.click();
            duplicateOption.click();
            browser.sleep(1000);

            var saveButton = modalWindow.element(by.css('div.custom-invoicing-modal-row-2 .btn-success[data-original-title="Sauvegarder et valider"]'));
            generalHelper.waitElementClickableAndClick(saveButton);

            var okButton = modalWindow2.element(by.css('div.modal-footer .btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButton);

            var cancelButton = modalWindow3.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

        });

        it('should confirm duplicated creditnote', function() {
            generalHelper.waitElementClickableAndClick(creditnotesTabItem);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++creditnotesCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});