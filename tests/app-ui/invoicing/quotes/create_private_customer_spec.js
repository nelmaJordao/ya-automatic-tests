describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Quotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButtonQuote,
            quoteIssueDate,
            inputCustomer,
            createCustomerModal;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="quotes"]'));
            newButton = element(by.css('.js-new-quote'));
            modalWindow = element(by.css('#invoicing-create-quote-modal'));
            inputCustomer = element(by.css('div[data-key="customer"]'));
            quoteIssueDate = modalWindow.element(by.css('#issuedDate'));
            okButtonQuote = modalWindow.element(by.css('.btn-success.ok'));
            createCustomerModal = element(by.css('#company-create-modal'));
        });

        it('should open new quote modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {

            var table = element.all(by.css('#invoicing-quote-form-items tr')).get(1);

            var deleteFirst = table.element(by.css('.js-delete-item'));

            generalHelper.waitElementClickableAndClick(deleteFirst);

            generalHelper.waitElementClickableAndClick(okButtonQuote);

            generalHelper.waitElementPresent(quoteIssueDate); //espera que a data seja preenchida para não dar erro de data

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).not.toEqual(0); //client and items are mandatory -> 2
        });

        it('should fill mandatory fields and submit', function() {

            //Selecionar um item já existente na lista

            var itemDiv = element(by.css('#invoicing-quote-form-items'));

            var addItem = modalWindow.element(by.css('.js-add-item'));

            generalHelper.waitElementClickableAndClick(addItem);

            browser.sleep(1000);

            generalHelper.selectTypeAheadItem(itemDiv, '.js-item.tt-input', 'A', 1);

            browser.sleep(1000);

            //Editar os números desse item

            var quantityField = itemDiv.element(by.css('.js-item-quantity'));

            quantityField.sendKeys('1,50');

            var discountField = itemDiv.element(by.css('.js-item-discount'));
            discountField.clear();
            discountField.sendKeys('40');

            //Adicionar linha em branco
            var addLine = modalWindow.element(by.css('.js-add-blank'));

            generalHelper.waitElementClickableAndClick(addLine);

            browser.sleep(1000);

            //Adicionar um comentário
            var addComment = modalWindow.element(by.css('.js-add-comment'));

            generalHelper.waitElementClickableAndClick(addComment);

            browser.sleep(1000);

            var writeComment = modalWindow.element(by.css('.js-line-observations'));

            writeComment.sendKeys("Comentário!");

            //Adicionar linha em branco

            generalHelper.waitElementClickableAndClick(addLine);

            //Criar um item novo

            generalHelper.waitElementClickableAndClick(addItem);

            browser.sleep(1000);

            generalHelper.selectTypeAheadCreateNew(itemDiv, "input.tt-input");

            var newArticle = element(by.css('#invoicing-create-item-modal'));

            //select family category
            var selectpickerFamilyCategory = newArticle.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = newArticle.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);

            //code
            var codeInput = newArticle.element(by.css('#code'));
            codeInput.sendKeys(config.inputs.getRandomName(10));

            //description
            var descriptionTextarea = newArticle.element(by.css('#description'));
            descriptionTextarea.sendKeys(config.inputs.getDescription(1000));

            //price
            browser.executeScript('$("#invoicing-create-item-modal #price").val(' + (Math.floor(Math.random() * 1000) + 1) + ')');
            browser.executeScript('$("#invoicing-create-item-modal #price").blur()');

            //select family category
            var selectpickerItemVat = newArticle.element(by.css('[data-id="itemVat"]'));
            var selectpickerItemVatOption = newArticle.all(by.css('.dropdown-menu.open')).get(2).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemVat);
            generalHelper.waitElementClickableAndClick(selectpickerItemVatOption);

            //select item family
            var selectpickerItemFamily = newArticle.element(by.css('[data-id="itemFamily"]'));
            var selectpickerItemFamilyOption = newArticle.all(by.css('.dropdown-menu.open')).get(1).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemFamily);
            generalHelper.waitElementClickableAndClick(selectpickerItemFamilyOption);

            browser.sleep(1000);

            var newFamilyArticle = element(by.css('#invoicing-create-family-modal'));

            //select family category
            var selectpickerFamilyCategory = newFamilyArticle.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = newFamilyArticle.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);


            var shortDescription = newFamilyArticle.element(by.css('#shortDescription'));
            shortDescription.sendKeys("teste");

            var description = newFamilyArticle.element(by.css('#description'));
            description.sendKeys("Teste1");


            var okButtonFamily = newFamilyArticle.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButtonFamily);
            generalHelper.waitAlertSuccessPresentAndClose();

            var okButtonArticle = newArticle.element(by.css('.btn-success.ok'));

            browser.executeScript('$(".btn-success.ok.hidden").removeClass("hidden")');

            generalHelper.waitElementClickableAndClick(okButtonArticle);
            generalHelper.waitAlertSuccessPresentAndClose();

            generalHelper.waitElementClickableAndClick(addItem);

            browser.sleep(1000);

            generalHelper.selectTypeAheadCreateNew(itemDiv, "input.tt-input");

            //select family category
            var selectpickerFamilyCategory = newArticle.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = newArticle.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="2"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);

            //code
            var codeInput = newArticle.element(by.css('#code'));
            codeInput.sendKeys(config.inputs.getRandomName(10));

            //description
            var descriptionTextarea = newArticle.element(by.css('#description'));
            descriptionTextarea.sendKeys(config.inputs.getDescription(1000));

            //price
            browser.executeScript('$("#invoicing-create-item-modal #price").val(' + (Math.floor(Math.random() * 1000) + 1) + ')');
            browser.executeScript('$("#invoicing-create-item-modal #price").blur()');

            //select family category
            var selectpickerItemVat = newArticle.element(by.css('[data-id="itemVat"]'));
            var selectpickerItemVatOption = newArticle.all(by.css('.dropdown-menu.open')).get(2).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemVat);
            generalHelper.waitElementClickableAndClick(selectpickerItemVatOption);

            //select item family
            var selectpickerItemFamily = newArticle.element(by.css('[data-id="itemFamily"]'));
            var selectpickerItemFamilyOption = newArticle.all(by.css('.dropdown-menu.open')).get(1).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemFamily);
            generalHelper.waitElementClickableAndClick(selectpickerItemFamilyOption);

            browser.sleep(1000);

            var newFamilyArticle = element(by.css('#invoicing-create-family-modal'));

            //select family category
            var selectpickerFamilyCategory = newFamilyArticle.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = newFamilyArticle.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="2"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);


            var shortDescription = newFamilyArticle.element(by.css('#shortDescription'));
            shortDescription.sendKeys("teste");

            var description = newFamilyArticle.element(by.css('#description'));
            description.sendKeys("Teste1");


            var okButtonFamily = newFamilyArticle.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButtonFamily);
            generalHelper.waitAlertSuccessPresentAndClose();

            var okButtonArticle = newArticle.element(by.css('.btn-success.ok'));

            browser.executeScript('$(".btn-success.ok.hidden").removeClass("hidden")');

            generalHelper.waitElementClickableAndClick(okButtonArticle);
            generalHelper.waitAlertSuccessPresentAndClose();
            generalHelper.selectTypeAheadCreateNew(inputCustomer, "input#customer");

            browser.sleep(1000);

            var nameInput = createCustomerModal.element(by.css('#name'));

            nameInput.clear();

            nameInput.sendKeys('private');

            var radioPrivate = createCustomerModal.all(by.css('.js-radio span')).get(1);

            generalHelper.waitElementClickableAndClick(radioPrivate);

            var saveButton = createCustomerModal.element(by.css('.btn-success'));

            generalHelper.waitElementClickableAndClick(saveButton);

            generalHelper.waitElementNotPresent(createCustomerModal);

            //get number of existing quotes
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            //save quote
            generalHelper.waitElementClickableAndClick(okButtonQuote);

            //check number of quotes increased by 1
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(recordCount++);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});