describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Quotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow,
            modalWindow2,
            modalWindow3;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="quotes"]'));
            firstRow = element.all(by.css('#invoicing-quotes-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-create-invoice-modal'));
            modalWindow2 = element(by.css('#invoicing-confirm-print-invoice-modal'));
            modalWindow3 = element(by.css('#invoice-print-pdf'));
        });

        it('should clone quote as invoice', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var generateButton = firstRow.element(by.css('.js-convert-quote'));

            generalHelper.waitElementClickableAndClick(generateButton);

            var saveButton = modalWindow.element(by.css('div.custom-invoicing-modal-row-2 .btn-success[data-original-title="Sauvegarder et valider"]'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(saveButton);


            var okButton = modalWindow2.element(by.css('.btn-success'));
            generalHelper.waitElementClickableAndClick(okButton);

            var cancelButton = modalWindow3.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);
        });

        it('should generate invoice again', function() {
            var generateButton2 = firstRow.element(by.css('div[data-original-title="Convertir en facture"] a.dropdown-toggle'));

            generalHelper.waitElementClickableAndClick(generateButton2);

            var generateInfo = firstRow.element(by.css('div[data-original-title="Convertir en facture"] ul li.text-center'));

            expect(generateInfo.isPresent()).toBe(true);

            var regenerateButton = firstRow.element(by.css('div[data-original-title="Convertir en facture"] ul li.text-center button.js-convert-quote'));
            generalHelper.waitElementClickableAndClick(regenerateButton);

            var saveButton = modalWindow.element(by.css('div.custom-invoicing-modal-row-2 .btn-success[data-original-title="Sauvegarder et valider"]'));
            generalHelper.waitElementClickableAndClick(saveButton);

            var okButton = modalWindow2.element(by.css('div.modal-footer .btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButton);

            var cancelButton = modalWindow3.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});