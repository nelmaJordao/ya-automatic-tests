describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Quotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            searchField,
            searchButton;

        beforeEach(function() {

            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="quotes"]'));

        });

        it('should search', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            searchField = element(by.css('#searchTable'));
            searchButton = element(by.css('.js-search-btn'));

            searchField.sendKeys('96');

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(searchButton);

            browser.sleep(1000);

            records.getText().then(function(text) {
                expect(parseInt(text)).not.toEqual(recordCount);
            });

            });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});