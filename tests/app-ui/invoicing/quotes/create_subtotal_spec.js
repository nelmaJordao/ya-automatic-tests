describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Quotes', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButtonQuote,
            quoteIssueDate;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="quotes"]'));
            newButton = element(by.css('.js-new-quote'));
            modalWindow = element(by.css('#invoicing-create-quote-modal'));
            quoteIssueDate = modalWindow.element(by.css('#issuedDate'));
            okButtonQuote = modalWindow.element(by.css('.btn-success.ok'));
        });

        it('should open new quote modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {

            var table = element.all(by.css('#invoicing-quote-form-items tr')).get(1);

            var deleteFirst = table.element(by.css('.js-delete-item'));

            generalHelper.waitElementClickableAndClick(deleteFirst);

            generalHelper.waitElementClickableAndClick(okButtonQuote);

            generalHelper.waitElementPresent(quoteIssueDate); //espera que a data seja preenchida para não dar erro de data

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).not.toEqual(0); //client and items are mandatory -> 2
        });

        it('should fill mandatory fields and submit', function() {


            var customerName = 'bip';

            //select client
            var clientSelectContainer = element(by.css('div[data-key="customer"]'));
            generalHelper.selectTypeAheadItem(clientSelectContainer, 'input#customer', customerName, 2);

            var editClientButton = modalWindow.element(by.css('.js-edit-customer'));

            generalHelper.waitElementClickableAndClick(editClientButton);

            browser.sleep(1000);

            var editClientModal = element(by.css('#company-update-modal'));

            var emailInput = editClientModal.element(by.css('#email'));

            var phoneInput = editClientModal.element(by.css('#phone'));

            emailInput.clear();
            phoneInput.clear();
            emailInput.sendKeys('xpto@xpto.biz');
            phoneInput.sendKeys('123456789');

            var editClientSave = editClientModal.element(by.css('.btn-success'));

            generalHelper.waitElementClickableAndClick(editClientSave);

            generalHelper.waitElementNotPresent(editClientModal);

            var checkEmail = modalWindow.element(by.css('table[data-sidepanel="company"] tbody tr td.values[data-field="email"]'));
            var checkPhone = modalWindow.element(by.css('table[data-sidepanel="company"] tbody tr td.values[data-field="phone"]'));

            expect(checkEmail.getText()).toEqual('xpto@xpto.biz');
            expect(checkPhone.getText()).toEqual('123456789');

            //Selecionar um item já existente na lista

            var itemDiv = element(by.css('#invoicing-quote-form-items'));

            var addItem = modalWindow.element(by.css('.js-add-item'));

            generalHelper.waitElementClickableAndClick(addItem);

            browser.sleep(1000);

            generalHelper.selectTypeAheadItem(itemDiv, '.js-item.tt-input', 'A', 1);

            browser.sleep(1000);

            //Editar os números desse item

            var quantityField = itemDiv.element(by.css('.js-item-quantity'));

            quantityField.sendKeys('1,50');

            var discountField = itemDiv.element(by.css('.js-item-discount'));
            discountField.clear();
            discountField.sendKeys('40');

            generalHelper.waitElementClickableAndClick(addItem);

            browser.sleep(1000);

            generalHelper.selectTypeAheadItem(itemDiv, '.js-item.tt-input', 'A', 1);

            browser.sleep(1000);

            var addSubtotal = modalWindow.element(by.css('.js-add-subtotal'));
            generalHelper.waitElementClickableAndClick(addSubtotal);

            browser.sleep(1000);

            //Subtotal
            var itemSub1 = element.all(by.css('.js-item-subtotal')).get(0);
            var itemSub2 = element.all(by.css('.js-item-subtotal')).get(1);
            var sub = element(by.css('.js-item-lines-subtotal'));

            var itemSubt1 = 0;
            itemSub1.getText().then(function(text) {
                text = text.substring(0, text.length - 1).replace(',', '.').replace(' ', '');
                itemSubt1 = parseFloat(text);
            });

            var itemSubt2 = 0;
            itemSub2.getText().then(function(text) {
                text = text.substring(0, text.length - 1).replace(',', '.').replace(' ', '');
                itemSubt2 = parseFloat(text);
            });

            sub.getText().then(function(text) {
                var total = itemSubt1 + itemSubt2;
                text = text.substring(0, text.length - 1).replace(',', '.').replace(' ', '');
                expect(parseFloat(text)).toEqual(total);

            });

            browser.sleep(1000);

            //get number of existing quotes
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            //save quote
            generalHelper.waitElementClickableAndClick(okButtonQuote);


            //check number of quotes increased by 1
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(recordCount++);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});