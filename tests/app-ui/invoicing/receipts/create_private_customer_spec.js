describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Receipts', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButton,
            receiptIssueDate,
            checkboxLabel,
            checkbox;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="receipts"]'));
            newButton = element(by.css('.js-new-receipt'));
            modalWindow = element(by.css('#invoicing-create-receipt-modal'));
            receiptIssueDate = modalWindow.element(by.css('#issuedDate'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            checkboxLabel = element.all(by.css('.checkbox')).get(1);
            checkbox = checkboxLabel.element(by.css('input'));
        });

        it('should open new receipt modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {
            var companyName = "private";

            var companySelectContainer = element(by.css('div[data-key="customer"]'));
            generalHelper.selectTypeAheadItem(companySelectContainer, 'input#company', companyName, 1);

            generalHelper.waitElementClickableAndClick(okButton);

            generalHelper.waitElementPresent(receiptIssueDate); //espera que a data seja preenchida para não dar erro de data

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(2);
        });

        it('should fill mandatory fields and submit', function () {
            //Select payment method
            var paymentPickerType = modalWindow.element(by.css('[data-id="paymentMethod"]'));
            var paymentPickerOption = modalWindow.element(by.css('.dropdown-menu.open [data-original-index="5"]'));
            generalHelper.waitElementClickableAndClick(paymentPickerType);
            generalHelper.waitElementClickableAndClick(paymentPickerOption);
            browser.sleep(1000);

            var numberInput = modalWindow.element(by.css('#paymentNumber'));
            numberInput.sendKeys('123456789');

            generalHelper.waitElementClickableAndClick(checkbox);

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});