describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Receipts', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButton,
            receiptIssueDate,
            checkboxLabel,
            checkbox,
            faturesTabItem;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="receipts"]'));
            newButton = element(by.css('.js-new-receipt'));
            modalWindow = element(by.css('#invoicing-create-receipt-modal'));
            receiptIssueDate = modalWindow.element(by.css('#issuedDate'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            checkboxLabel = element.all(by.css('.checkbox')).get(1);
            checkbox = checkboxLabel.element(by.css('input'));
            faturesTabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
        });

        it('should open new receipt modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });


        it('should fill mandatory fields and submit', function () {

            var supplier = "a";

            //selecionar o fornecedor
            var supplierSelectContainer = element(by.css('div[data-key="customer"]'));
            generalHelper.selectTypeAheadItem(supplierSelectContainer, 'input#company', supplier, 1);

            //Select payment method
            var paymentPickerType = modalWindow.element(by.css('[data-id="paymentMethod"]'));
            var paymentPickerOption = modalWindow.element(by.css('.dropdown-menu.open [data-original-index="5"]'));
            generalHelper.waitElementClickableAndClick(paymentPickerType);
            generalHelper.waitElementClickableAndClick(paymentPickerOption);

            var numberInput = modalWindow.element(by.css('#paymentNumber'));
            numberInput.sendKeys('123456789');

            generalHelper.waitElementClickableAndClick(checkbox);

            var money = modalWindow.element(by.css('.js-invoice-value-pay'));
            money.clear();
            money.sendKeys('1');

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });

        });

        it('should check facture state', function()
        {
            generalHelper.waitElementClickableAndClick(faturesTabItem);

            browser.sleep(1000);

            var filterType = element(by.css('[data-id="state"]'));
            var filterOption = element.all(by.css('.dropdown-menu.open')).get(1).element(by.css('li[data-original-index="4"]'));

            generalHelper.waitElementClickableAndClick(filterType);

            generalHelper.waitElementClickableAndClick(filterOption);

            browser.sleep(1000);

            var paymentState = 'PARTIELLEMENT PAYÉ';

            var secondRow = element.all(by.css('#invoicing-invoices-dtable tr')).last();

            var etat = secondRow.element(by.xpath("../..")).all(by.css('td')).get(5);

            etat.getText().then(function(text) {
                expect(text).toEqual(paymentState);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});