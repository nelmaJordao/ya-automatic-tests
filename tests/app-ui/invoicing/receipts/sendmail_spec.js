describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Receipts', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow,
            firstRow,
            sendmailButton,
            resendmailButton,
            emailInput;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="receipts"]'));
            modalWindow = element(by.css('#receipt-send-email'));
            firstRow = element.all(by.css('#invoicing-receipts-dtable tr')).first();
            sendmailButton = firstRow.element(by.css('.js-send-receipt'));
            resendmailButton = firstRow.element(by.css('.active.js-send-receipt'));
            emailInput = modalWindow.element(by.css('#email'));
        });

        it('should open send email modal', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(sendmailButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should send email', function () {
            var email = 'xpto@xpto.biz';
            var sendButton = modalWindow.element(by.css('.btn-success.ok'));

            emailInput.clear().sendKeys(email);

            var emailComment = modalWindow.element(by.css('#emailComment'));
            emailComment.sendKeys('Email email!');

            generalHelper.waitElementClickableAndClick(sendButton);

            browser.refresh();

        });

        it('should see email filled when resending', function () {
            generalHelper.waitElementClickableAndClick(resendmailButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

            generalHelper.waitElementPresent(emailInput);
            generalHelper.waitElementDisplayed(emailInput);

            expect(emailInput.getAttribute('value')).not.toEqual('');

            var emailComment = modalWindow.element(by.css('#emailComment'));

            expect(emailComment.getAttribute('value')).toEqual('');

            emailComment.sendKeys('Email email update!');

            var okButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });

});