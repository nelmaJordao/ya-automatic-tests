describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Receipts', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow,
            invoiceDiv;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="receipts"]'));
            firstRow = element.all(by.css('#invoicing-receipts-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-read-receipt-modal'));
            invoiceDiv = modalWindow.element(by.css('#invoicing-receipt-form-items'));
        });

        it('should open show receipt modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            var showButton = firstRow.element(by.css('.options .js-show-receipt'));
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {
            //client name
            var name = modalWindow.element(by.css('.form-group .name'));
            expect(name.getText()).not.toBe('');

            //date
            var date = modalWindow.all(by.css('.form-group .value')).get(0);
            expect(date.getText()).not.toBe('');

            //payment method
            var paymentMethod = modalWindow.all(by.css('.form-group .value')).get(1);
            expect(paymentMethod.getText()).not.toBe('');

            //invoice list
            var invoiceList = invoiceDiv.all(by.css('tr'));
            expect(invoiceList.count()).toBeGreaterThan(0);
        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});