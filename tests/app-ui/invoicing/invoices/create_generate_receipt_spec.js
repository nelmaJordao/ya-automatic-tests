describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Invoices', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow,
            firstRow,
            sendmailButton;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
            modalWindow = element(by.css('div#invoicing-create-receipt-modal'));
            firstRow = element.all(by.css('#invoicing-invoices-dtable tr')).get(0);
            sendmailButton = firstRow.element(by.css('.js-generate-receipt'));
        });

        it('should open generate receipt modal', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var etatPickerType = element(by.css('[data-id="state"]'));
            var etatPickerOption = element.all(by.css('.dropdown-menu.open')).get(1).element(by.css('[data-original-index="2"]'));
            generalHelper.waitElementClickableAndClick(etatPickerType);
            generalHelper.waitElementClickableAndClick(etatPickerOption);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(sendmailButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should submit', function () {
            var paymentPickerType = modalWindow.element(by.css('[data-id="paymentMethod"]'));
            var paymentPickerOption = modalWindow.element(by.css('.dropdown-menu.open [data-original-index="1"]'));

            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(paymentPickerType);

            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(paymentPickerOption);

            var money = modalWindow.element(by.css('.js-invoice-value-pay'));
            money.clear();
            money.sendKeys('1');

            var okButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

            generalHelper.waitAlertSuccessPresentAndClose();

            browser.sleep(1000);

        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

        it('should check facture state', function() {

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);

            var etatPickerType = element(by.css('[data-id="state"]'));
            var etatPickerOption = element.all(by.css('.dropdown-menu.open')).get(1).element(by.css('[data-original-index="3"]'));

            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(etatPickerType);
            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(etatPickerOption);

            var paymentState = 'PAYÉ';

            var etat = firstRow.element(by.xpath("../..")).all(by.css('td')).get(5);

            etat.getText().then(function(text) {
                expect(text).toEqual(paymentState);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});