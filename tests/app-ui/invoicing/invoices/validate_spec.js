describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Invoices', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow,
            okButton;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
            firstRow = element.all(by.css('#invoicing-invoices-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-update-invoice-modal'));
            okButton = modalWindow.element(by.css('.btn-success[data-original-title="Les données de la facture seront enregistrées et validées, aucune autre correction ne sera possible"]'));
        });

        it('should open edit invoice modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            var editButton = firstRow.element(by.css('.options .js-edit-invoice'));

            var stateType = element(by.css('button[data-id="state"]'));
            var stateOption = element.all(by.css('li[data-original-index="1"]')).get(1);

            generalHelper.waitElementClickableAndClick(stateType);
            generalHelper.waitElementClickableAndClick(stateOption);

            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should validate invoice', function() {
            generalHelper.waitElementClickableAndClick(okButton);

            var confirmModal = element(by.css('#invoicing-confirm-print-invoice-modal'));

            var confirmButton = confirmModal.element(by.css('.btn-success'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(confirmButton);

            var printModal = element(by.css('#invoice-print-pdf'));

            var closeButton = printModal.element(by.css('.btn.btn-default.cancel'));

            generalHelper.waitElementClickableAndClick(closeButton);

            browser.refresh();
            browser.sleep(3000);

            var stateType = element(by.css('button[data-id="state"]'));
            var stateOption = element.all(by.css('li[data-original-index="0"]')).get(1);

            generalHelper.waitElementClickableAndClick(stateType);
            generalHelper.waitElementClickableAndClick(stateOption);

            browser.sleep(1000);

            firstRow = element.all(by.css('#invoicing-invoices-dtable tr')).get(0);

            var statusCell = firstRow.all(by.css('td')).get(5);

            var statusSpan = statusCell.all(by.css('.label.label-warning'));

            expect(statusSpan.getText()).toEqual([ 'PAIEMENT EN ATTENTE' ]);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});