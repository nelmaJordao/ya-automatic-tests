describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Invoices', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            invoicesTabItem,
            firstRow,
            modalWindow,
            modalWindow2,
            modalWindow3,
            invoicesCount;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            invoicesTabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
            firstRow = element.all(by.css('#invoicing-invoices-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-create-invoice-modal'));
            modalWindow2 = element(by.css('#invoicing-confirm-print-invoice-modal'));
            modalWindow3 = element(by.css('#invoice-print-pdf'));
        });

        it('should check number of invoices', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            generalHelper.waitElementClickableAndClick(invoicesTabItem);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            invoicesCount = 0;
            records.getText().then(function(text) {
                invoicesCount = parseInt(text);
            });
        });

        it('should clone invoice as invoice', function () {

            generalHelper.waitElementClickableAndClick(invoicesTabItem);

            var duplicateDiv = firstRow.element(by.css('.options div.dropdown[data-original-title="Dupliquer"]'));
            var duplicateButton = duplicateDiv.element(by.css('.dropdown-toggle'));
            var duplicateOption = duplicateDiv.element(by.css('ul.dropdown-menu li a[data-clone-target="invoices"]'))

            browser.sleep(1000);
            duplicateButton.click();
            duplicateOption.click();
            browser.sleep(1000);

            var saveButton = modalWindow.element(by.css('div.custom-invoicing-modal-row-2 .btn-success[data-original-title="Sauvegarder et valider"]'));
            generalHelper.waitElementClickableAndClick(saveButton);

            var okButton = modalWindow2.element(by.css('div.modal-footer .btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButton);

            var cancelButton = modalWindow3.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);
        });

        it('should confirm duplicated invoice', function() {
            generalHelper.waitElementClickableAndClick(invoicesTabItem);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++invoicesCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});