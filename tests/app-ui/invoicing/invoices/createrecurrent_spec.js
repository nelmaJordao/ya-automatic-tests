describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Create Invoice', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            appRegion,
            newButton,
            modalWindow,
            inputCustomer,
            okButton;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            appRegion = element(by.css('#invoicing-menu-region'));
            newButton = element(by.css('.js-new-invoice'));
            modalWindow = element(by.css('#invoicing-create-invoice-modal'));
            inputCustomer = element(by.css('div[data-key="customer"]'));
            okButton = modalWindow.element(by.css('[data-original-title="Sauvegarder et valider"]'));
        });

        it('should open new invoice modal', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementPresent(appRegion);
            generalHelper.waitElementDisplayed(appRegion);

            generalHelper.waitElementClickableAndClick(newButton);

            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {

            var table = element.all(by.css('#invoicing-invoice-form-items tr')).get(1);

            var deleteFirst = table.element(by.css('.js-delete-item'));

            generalHelper.waitElementClickableAndClick(deleteFirst);

            generalHelper.waitElementClickableAndClick(okButton);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).not.toEqual(0);
        });

        it('should fill mandatory fields and submit', function() {
            var customerName = "bip";

            generalHelper.selectTypeAheadItem(inputCustomer, "input#customer", customerName, 2);

            //Escolher fatura recurrente
            var recurrentCheck = modalWindow.element(by.css('#recurrent'));

            generalHelper.waitElementClickableAndClick(recurrentCheck);

            var periodicityInput = modalWindow.element(by.css("#periodicity"));
            var periodicityChoiceInput = modalWindow.element(by.css('#periodicityChoice'));
            periodicityInput.sendKeys('7');
            periodicityChoiceInput.sendKeys('10');

            //Selecionar um item já existente na lista

            var itemDiv = element(by.css('#invoicing-invoice-form-items'));
            var addItem = modalWindow.element(by.css('.js-add-item'));
            generalHelper.waitElementClickableAndClick(addItem);

            browser.sleep(1000);

            generalHelper.selectTypeAheadItem(itemDiv, '.js-item.tt-input', 's', 1);

            browser.sleep(1000);


            //Editar os números desse item

            var quantityField = itemDiv.element(by.css('.js-item-quantity'));
            quantityField.sendKeys('3,50');

            var discountField = itemDiv.element(by.css('.js-item-discount'));
            discountField.clear();
            discountField.sendKeys('30');

            generalHelper.waitElementClickableAndClick(okButton);

            var confirmModal = element(by.css('#invoicing-confirm-print-invoice-modal'));
            var confirmButton = confirmModal.element(by.css('.modal-footer .btn.btn-success.ok'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(confirmButton);

            var printModal = element(by.css('#invoice-print-pdf'));
            var closeButton = printModal.element(by.css('.btn.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });


});