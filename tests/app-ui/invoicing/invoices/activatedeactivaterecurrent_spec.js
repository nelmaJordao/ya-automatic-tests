describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Invoices', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow;

        beforeEach(function () {

            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="invoicesrecurrent"]'));
            firstRow = element.all(by.css('#invoicing-invoicesrecurrent-dtable tr')).get(0);
            modalWindow = element(by.css('.modal-dialog.modal-invoicing'));
        });

        it('should open activate/deactivate invoice modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            var deleteButton = firstRow.element(by.css('.options .js-enable-activate-invoice-recurrent'));
            generalHelper.waitElementClickableAndClick(deleteButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should confirm activate/deactivate invoice modal window', function() {

            var okButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();
            
            });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});
