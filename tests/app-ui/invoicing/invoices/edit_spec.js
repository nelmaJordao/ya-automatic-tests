describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Invoices', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow,
            okButton;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
            firstRow = element.all(by.css('#invoicing-invoices-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-update-invoice-modal'));
            okButton = modalWindow.element(by.css('.btn-success[data-original-title="Les données de la facture seront enregistrées et validées, aucune autre correction ne sera possible"]'));
        });

        it('should open edit invoice modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var stateType = element(by.css('button[data-id="state"]'));
            var stateOption = element.all(by.css('li[data-original-index="1"]')).get(1);

            generalHelper.waitElementClickableAndClick(stateType);
            generalHelper.waitElementClickableAndClick(stateOption);

            var editButton = firstRow.element(by.css('.options .js-edit-invoice'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(editButton);
            
            
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should cancel edit invoice', function () {
            var cancelButton = element(by.css('button.cancel'));

            browser.sleep(1000);
            
            generalHelper.waitElementClickableAndClick(cancelButton);

            
        });

        it('should open edit invoice modal window', function () {
            var editButton = firstRow.element(by.css('.options .js-edit-invoice'));
            
            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(editButton);
            
            browser.sleep(1000);
            
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should fill mandatory fields and submit', function() {

            var observationsInput = modalWindow.element(by.css('#observations'));
            observationsInput.clear();
            observationsInput.sendKeys('Editted');

            var itemDiv = modalWindow.element(by.css('#invoicing-invoice-form-items'));
            var addItemButton = itemDiv.element(by.css('.js-add-item'));

            generalHelper.waitElementClickableAndClick(addItemButton);

            generalHelper.selectTypeAheadItem(itemDiv, "input.tt-input", "s", 1);

            generalHelper.waitElementClickableAndClick(okButton);

            var confirmButton = element(by.css('#invoicing-confirm-print-invoice-modal .btn-success.ok'));

            generalHelper.waitElementClickableAndClick(confirmButton);

            var printModal = element(by.css('#invoice-print-pdf'));

            generalHelper.waitElementPresent(printModal);

            var cancelButton = printModal.element(by.css('button.cancel'));

            generalHelper.waitElementClickableAndClick(cancelButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});