describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Recurrent Invoices', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow,
            itemDiv;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
            firstRow = element.all(by.css('#invoicing-invoices-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-read-invoice-modal'));
            itemDiv = modalWindow.element(by.css('#invoicing-invoice-form-items'));
        });

        it('should open show receipt modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var stateType = element(by.css('button[data-id="state"]'));
            var stateOption = element.all(by.css('li[data-original-index="2"]')).get(1);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(stateType);
            generalHelper.waitElementClickableAndClick(stateOption);

            browser.sleep(1000);

            var showButton = firstRow.element(by.css('.options .js-show-invoice'));
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

        });

        it('should click on auto-liquidation button', function () {

            var autoLiquidationButton = element(by.css('.js-auto-liquidate'));
            generalHelper.waitElementClickableAndClick(autoLiquidationButton);

            browser.sleep(1500);

            var liquidationModalWindow = element(by.css('#invoicing-auto-liquidate'));
            var confirmationButton = liquidationModalWindow.element(by.css('.btn-success.ok'))
            generalHelper.waitElementClickableAndClick(confirmationButton);

            browser.sleep(1000);

            var closeButton = element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});