describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Invoices', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
            modalWindow = element(by.css('#invoicing-delete-invoice-modal'));
        });

        it('should open delete invoice modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var stateType = element(by.css('button[data-id="state"]'));
            var stateOption = element.all(by.css('li[data-original-index="1"]')).get(1);

            generalHelper.waitElementClickableAndClick(stateType);
            generalHelper.waitElementClickableAndClick(stateOption);

            firstRow = element.all(by.css('#invoicing-invoices-dtable tr')).get(0);

            var deleteButton = firstRow.element(by.css('.options .js-delete-invoice'));
            generalHelper.waitElementClickableAndClick(deleteButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should confirm delete invoice modal window', function() {
            var okButton = modalWindow.element(by.css('.btn-success.ok'));
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(--recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});