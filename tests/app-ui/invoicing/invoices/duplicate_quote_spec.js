describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Invoices', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            invoicesTabItem,
            quotesTabItem,
            firstRow,
            modalWindow,
            quotesCount;

        beforeEach(function () {
            menuIcon = element(by.css('#invoicing-module'));
            invoicesTabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
            quotesTabItem = element(by.css('#invoicing-menu-region [data-sec="quotes"]'));
            firstRow = element.all(by.css('#invoicing-invoices-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-create-quote-modal'));
        });

        it('should check number of quotes', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(quotesTabItem);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            quotesCount = 0;
            records.getText().then(function(text) {
                quotesCount = parseInt(text);
            });
        });

        it('should clone invoice as quote', function () {

            generalHelper.waitElementClickableAndClick(invoicesTabItem);
            
            var duplicateDiv = firstRow.element(by.css('.options div.dropdown[data-original-title="Dupliquer"]'));
            var duplicateButton = duplicateDiv.element(by.css('.dropdown-toggle'));
            var duplicateOption = duplicateDiv.element(by.css('ul.dropdown-menu li a[data-clone-target="quotes"]'))
            
            browser.sleep(1000);
            duplicateButton.click();
            duplicateOption.click();
            browser.sleep(1000);
            
            var saveButton = modalWindow.element(by.css('div.modal-footer .btn-success'));
            generalHelper.waitElementClickableAndClick(saveButton);
        });

        it('should confirm duplicated quote', function() {
            generalHelper.waitElementClickableAndClick(quotesTabItem);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++quotesCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});