describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Invoices', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow,
            invoicesTBody,
            invoicesList,
            sendmailButton,
            resendmailButton,
            emailInput;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
            modalWindow = element(by.css('div#invoicing-send-email'));
            invoicesTBody = element(by.css('#invoicing-invoices-dtable'));
            invoicesList = invoicesTBody.all(by.css('tr'));
            sendmailButton = invoicesList.get(0).element(by.css('.js-send-invoice'));
            resendmailButton = invoicesList.get(0).element(by.css('.js-send-invoice.active'));
            emailInput = modalWindow.element(by.css('#email'));
        });

        it('should open send email modal', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var stateType = element(by.css('button[data-id="state"]'));
            var stateOption = element.all(by.css('li[data-original-index="3"]')).get(1);
            generalHelper.waitElementClickableAndClick(stateType);
            generalHelper.waitElementClickableAndClick(stateOption);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(sendmailButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should send email', function () {
            var email = 'xpto@xpto.biz';
            var sendButton = modalWindow.element(by.css('.btn-success.ok'));
            emailInput.clear().sendKeys(email);

            var emailComment = modalWindow.element(by.css('#emailComment'));
            emailComment.sendKeys('Email email!');

            generalHelper.waitElementClickableAndClick(sendButton);

            generalHelper.waitAlertSuccessPresentAndClose();
            browser.refresh();
        });

        it('should see email filled when resending', function () {
            generalHelper.waitElementClickableAndClick(resendmailButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

            browser.sleep(1000);

            generalHelper.waitElementPresent(emailInput);

            browser.sleep(1000);
            
            generalHelper.waitElementDisplayed(emailInput);

            browser.sleep(1000);

            expect(emailInput.getAttribute('value')).not.toEqual('');

            var emailComment = modalWindow.element(by.css('#emailComment'));

            expect(emailComment.getAttribute('value')).toEqual('');

            emailComment.sendKeys('Email email update!');

            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });

});