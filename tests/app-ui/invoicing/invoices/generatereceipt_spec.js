describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Invoices', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow,
            invoicesTBody,
            invoicesList,
            sendmailButton;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
            modalWindow = element(by.css('div#invoicing-create-receipt-modal'));
            invoicesTBody = element(by.css('.table.table-striped.table-hover'));
            invoicesList = invoicesTBody.all(by.css('tr'));
            sendmailButton = invoicesList.get(1).element(by.css('.js-generate-receipt'));
        });

        it('should open generate receipt modal', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var stateType = element(by.css('button[data-id="state"]'));
            var stateOption = element.all(by.css('li[data-original-index="2"]')).get(1);

            generalHelper.waitElementClickableAndClick(stateType);
            generalHelper.waitElementClickableAndClick(stateOption);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(sendmailButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should submit', function () {
            var paymentPickerType = modalWindow.element(by.css('[data-id="paymentMethod"]'));
            var paymentPickerOption = modalWindow.element(by.css('.dropdown-menu.open [data-original-index="1"]'));

            generalHelper.waitElementClickableAndClick(paymentPickerType);
            generalHelper.waitElementClickableAndClick(paymentPickerOption);

            var okButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButton);

            generalHelper.waitAlertSuccessPresentAndClose();

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});