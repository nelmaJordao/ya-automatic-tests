describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Invoices', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow,
            okButton;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            tabItem = element(by.css('#invoicing-menu-region [data-sec="invoices"]'));
            firstRow = element.all(by.css('#invoicing-invoices-dtable tr')).get(0);
            modalWindow = element(by.css('#invoicing-update-invoice-modal'));
            okButton = modalWindow.element(by.css('.btn-success[data-original-title="Les données de la facture seront enregistrées et validées, aucune autre correction ne sera possible"]'));
        });

        it('should open edit invoice modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var stateType = element(by.css('button[data-id="state"]'));
            var stateOption = element.all(by.css('li[data-original-index="1"]')).get(1);
            generalHelper.waitElementClickableAndClick(stateType);
            generalHelper.waitElementClickableAndClick(stateOption);

            var editButton = firstRow.element(by.css('.options .js-edit-invoice'));
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should validate invoice', function() {

            generalHelper.waitElementClickableAndClick(okButton);

            var confirmModal = element(by.css('#invoicing-confirm-print-invoice-modal'));

            var confirmButton = confirmModal.element(by.css('.btn-success'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(confirmButton);

            var printModal = element(by.css('#invoice-print-pdf'));

            var closeButton = printModal.element(by.css('.btn.btn-default.cancel'));

            generalHelper.waitElementClickableAndClick(closeButton);

            browser.refresh();
            browser.sleep(2000);

            var stateType = element(by.css('button[data-id="state"]'));
            var stateOption = element.all(by.css('li[data-original-index="0"]')).get(1);

            generalHelper.waitElementClickableAndClick(stateType);
            generalHelper.waitElementClickableAndClick(stateOption);

        });

        it('should send an email', function() {
            browser.sleep(1000);

            var sendEmail = element(by.css('.js-send-invoice'));
            generalHelper.waitElementClickableAndClick(sendEmail);

            var emailWindow = element(by.css('#invoicing-send-email'));
            generalHelper.waitElementPresent(emailWindow);

            expect(emailWindow.isDisplayed()).toBe(true);

            var email = emailWindow.element(by.css('#email'));
            email.clear();
            email.sendKeys('xpto@xpto.biz');

            var checkBoxEmail = emailWindow.element(by.css('#isRecover'));
            generalHelper.waitElementClickableAndClick(checkBoxEmail);

            var acceptButton = emailWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(acceptButton);

            browser.sleep(1000);

            expect(emailWindow.isPresent()).toBe(false);

            browser.sleep(1000);

            var cellChange = firstRow.all(by.css('td')).get(5);

            var status = cellChange.element(by.css('.label.label-warning'));

            expect(status.getText()).toEqual('EN RECOUVREMENT');

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});