describe('Cash journal', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Cash days', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            firstRow,
            modalWindow;

        beforeEach(function() {
            menuIcon = element(by.css('#cash-journal-module'));
            firstRow = element.all(by.css('#cash-journal-cashdays-dtable tr')).get(0);
            modalWindow = element(by.css('#cash-day-validate-modal'));
        });

        it('should validate cash day', function() {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            var validateButton = firstRow.element(by.css('td .js-validate-cash-day'));
            generalHelper.waitElementClickableAndClick(validateButton);
            generalHelper.waitElementPresent(modalWindow);

            var confirmButton = modalWindow.element(by.css('div.modal-footer button.btn-success'));
            generalHelper.waitElementClickableAndClick(confirmButton);
            
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});