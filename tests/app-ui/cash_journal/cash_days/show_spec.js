describe('Cash journal', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Cash days', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            firstRow,
            modalWindow;

        beforeEach(function() {
            menuIcon = element(by.css('#cash-journal-module'));
            firstRow = element.all(by.css('#cash-journal-cashdays-dtable tr')).get(0);
            modalWindow = element(by.css('#cash-day-create-modal'));
        });

        it('should open show family modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            var showButton = firstRow.element(by.css('.options .js-show-cash-day'));
            generalHelper.waitElementClickableAndClick(showButton);

            browser.sleep(1000);
            
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});