describe('Cash journal', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Cash days', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            firstRow,
            modalWindow;

        beforeEach(function() {
            menuIcon = element(by.css('#cash-journal-module'));
            modalWindow = element(by.css('#cash-day-delete-modal'));
            firstRow = element.all(by.css('#cash-journal-cashdays-dtable tr')).first();
        });

        it('should open delete cash day modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            var deleteButton = firstRow.element(by.css('.options .js-delete-cash-day'));
            generalHelper.waitElementClickableAndClick(deleteButton);

            browser.sleep(1000);
            
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should confirm delete modal window', function() {
            var okButton = modalWindow.element(by.css('.btn-success.ok'));
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);
            
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(--recordCount);
            });
            
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});