describe('Cash journal', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Cash days', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            showButton,
            modalWindow,
            deleteModal;

        beforeEach(function() {
            menuIcon = element(by.css('#cash-journal-module'));
            showButton = element.all(by.css('.js-show-cash-day')).get(0);
            modalWindow = element(by.css('#cash-day-create-modal'));
            deleteModal = element(by.css('#cash-day-delete-modal'));
        });

        it('should open show family modal window', function () {

            browser.sleep(2000);
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(2000);

            generalHelper.waitElementClickableAndClick(showButton);

            browser.sleep(2000);

            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });


        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);
        });

        it('should open show modal window', function () {
            generalHelper.waitElementClickableAndClick(showButton);

            generalHelper.waitElementPresent(modalWindow);
            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should open edit modal window', function () {
            var crudEdit = modalWindow.element(by.css('.modal-header .crud [data-trigger="cash:journal:edit:cashDay"]'));
            generalHelper.waitElementClickableAndClick(crudEdit);

            browser.sleep(500);

            expect(modalWindow.isDisplayed()).toBe(true);

            browser.sleep(10000);
        });

        it('should open show modal window again', function () {
            var crudShow = modalWindow.element(by.css('.crud [data-trigger="cash:journal:show:cashDay"]'));
            generalHelper.waitElementClickableAndClick(crudShow);

            browser.sleep(500);

            // expect(modalWindow.isDisplayed()).toBe(true);

            browser.sleep(1000);
        });

        it('should open delete modal window', function() {
            var crudDelete = modalWindow.element(by.css('.crud [data-trigger="cash:journal:delete:cashDay"] span'));
            generalHelper.waitElementClickableAndClick(crudDelete);

            browser.sleep(1000);

            expect(deleteModal.isDisplayed()).toBe(true);

            browser.sleep(1000);

            var cancelButton = deleteModal.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            expect(deleteModal.isPresent()).toBe(false);
        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});