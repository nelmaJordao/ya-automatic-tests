describe('Cash journal', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var sirenHelper = requireCommonHelper('sirenHelper');


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Cash days', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            newButton,
            modalWindow,
            saveButton,
            numberTransactionsinput;

        beforeEach(function() {
            menuIcon = element(by.css('#cash-journal-module'));
            newButton = element(by.css('.js-new-cash-day'));
            modalWindow = element(by.css('#cash-day-create-modal'));
            saveButton = modalWindow.element(by.css('.btn.btn-success.btn-block.ladda-button.ok'));
            numberTransactionsinput = modalWindow.element(by.css('#numberOfTransactions'));
        });

        it('should open new family modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should fill mandatory fields and submit', function() {

            var inputTicketNumber = modalWindow.element(by.css('#zNumber'));
            var ticketNumber = Math.floor(Math.random() * 1000);
            inputTicketNumber.sendKeys(ticketNumber);

            var numberTransactions = Math.floor(Math.random() * 1000);
            numberTransactionsinput.sendKeys(numberTransactions);

            //Income tab

            var tab = modalWindow.element(by.css('div.tab-pane.active[role="tabpanel"]'));

            var salesInput = modalWindow.element(by.css('div.form-group input.js-cash-input.js-sales-input'));
            salesInput.clear();

            var salesAmmount;
            if(modalWindow.isElementPresent(by.css('#CLIENT_CREDIT'))){
                salesAmmount = 360;
            }
            else{
                salesAmmount = 310;
            }

            salesInput.sendKeys(salesAmmount);

            var modesInput = tab.element(by.css('#CASH'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(10);

            modesInput = tab.element(by.css('#CHECK'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(20);

            modesInput = tab.element(by.css('#BANK_CARD'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(30);

            modesInput = tab.element(by.css('#TICKET_RESTAURANT'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(40);

            if(modalWindow.isElementPresent(by.css('#CLIENT_CREDIT'))){
                modesInput = tab.element(by.css('#CLIENT_CREDIT'));
                modesInput.sendKeys('asd');
                expect(modesInput.getText()).not.toEqual('asd');
                modesInput.clear();
                modesInput.sendKeys(50);
            }

            modesInput = tab.element(by.css('#WIRE_TRANSFER'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(60);

            modesInput = tab.element(by.css('#TICKET_HOLIDAY'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(70);

            modesInput = tab.element(by.css('#GIFT_CARD'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(80);

            var cashReinforcementInput = tab.element(by.css('#cashReinforcement'));
            var cashReinforcement = Math.floor(Math.random() * 10);
            cashReinforcementInput.sendKeys('asd');
            expect(cashReinforcementInput.getText()).not.toEqual('asd');
            cashReinforcementInput.clear();
            cashReinforcementInput.sendKeys(cashReinforcement);

            //Expenses tab

            var expensesTabClick = modalWindow.element(by.css('li[role="cash-outputs"]'));
            generalHelper.waitElementClickableAndClick(expensesTabClick);

            tab = modalWindow.element(by.css('#cj-outs'));

            generalHelper.waitElementDisplayed(tab);

            modesInput = tab.element(by.css('#CASH'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(10);

            modesInput = tab.element(by.css('#CHECK'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(20);

            modesInput = tab.element(by.css('#TICKET_RESTAURANT'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(30);

            modesInput = tab.element(by.css('#WIRE_TRANSFER'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(40);

            modesInput = tab.element(by.css('#TICKET_HOLIDAY'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(50);

            modesInput = tab.element(by.css('#GIFT_CARD'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(60);


            //Add supplier purchase
            var supplierPurchaseButton = tab.element(by.css('button.js-add-supplier-purchase'));
            generalHelper.waitElementClickableAndClick(supplierPurchaseButton);

            var tr = tab.all(by.css('#supplier-purchases-region tr')).last();

            var supplierInput = tr.element(by.css('div.js-auto-complete'));
            generalHelper.selectTypeAheadCreateNew(supplierInput, 'input[name="supplierBaseCompanyName"]');

            expect(modalWindow.isDisplayed()).toBe(true);

            browser.sleep(2000);

            var newSupplierModal = element(by.css('#company-create-modal'));
            var name = newSupplierModal.element(by.css('#name'));
            name.clear();
            name.sendKeys('XPTO ALBERT');

            //O SIREN só dá para utilizar uma vez, dois testes corretamente concluidos não podem utilizar o mesmo SIREN
            var sirenInput = newSupplierModal.element(by.css('#sirenNumber'));
            sirenInput.sendKeys(sirenHelper.getSiren());

            var verifiedSirenButton = newSupplierModal.all(by.css('.input-group .js-check-vat')).get(1);
            generalHelper.waitElementClickableAndClick(verifiedSirenButton);

            browser.sleep(2000);

            var okButton = newSupplierModal.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

            var supplierInvoiceInput = tr.element(by.css('input[name="supplierDocId"]'));
            supplierInvoiceInput.clear();
            var invoiceNumber = Math.floor(Math.random() * 100);
            supplierInvoiceInput.sendKeys(invoiceNumber);

            var supplierComment = tr.element(by.css('input[name="observations"]'));
            supplierComment.sendKeys("test");

            var supplierAmmountInput = tr.element(by.css('input[name="totalInCash"]'));
            supplierAmmountInput.clear();
            var supplierAmmount = 10;
            supplierAmmountInput.sendKeys(supplierAmmount);

            //Add cash withdrawal
            var cashWithdrawalButton = tab.element(by.css('button.js-add-cash-withdrawal'));
            generalHelper.waitElementClickableAndClick(cashWithdrawalButton);

            tr = tab.all(by.css('#cash-withdrawals-region tr')).last();

            var withdrawalDescription = tr.all(by.css('input.js-description')).first();
            withdrawalDescription.sendKeys("test");

            var withdrawalTypeDrop = tr.element(by.css('button'));
            var withdrawalTypeSelect = tr.element(by.css('div.dropdown-menu ul.dropdown-menu li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(withdrawalTypeDrop);
            generalHelper.waitElementClickableAndClick(withdrawalTypeSelect);

            var withdrawalAmmountInput = tr.element(by.css('input[name="value"]'));
            withdrawalAmmountInput.sendKeys(10);

            //add client credit
            if(modalWindow.isElementPresent(by.css('#CLIENT_CREDIT'))) {

                var clientCreditButton = tab.element(by.css('button.js-add-client-credit'));
                generalHelper.waitElementClickableAndClick(clientCreditButton);

                tr = tab.all(by.css('#client-credit-region tr')).last();

                var clientInput = tr.element(by.css('input.js-customer-name'));
                clientInput.sendKeys("test");

                var commentsInput = tr.element(by.css('input.js-observations'));
                commentsInput.sendKeys("test");

                var clientCreditAmmountInput = tr.element(by.css('input.js-client-credit'));
                clientCreditAmmountInput.sendKeys(50);
            }

            var observationsInput = modalWindow.element(by.css('input.js-edit-client-credit.js-observations'));
            observationsInput.sendKeys('Test');

            browser.sleep(1500);

            generalHelper.waitElementClickableAndClick(saveButton);
            generalHelper.waitAlertSuccessPresentAndClose();

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});

