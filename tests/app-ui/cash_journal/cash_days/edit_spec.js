describe('Cash journal', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Cash days', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            modalWindow,
            firstRow,
            okButton,
            editButton,
            cancelButton;

        beforeEach(function() {
            menuIcon = element(by.css('#cash-journal-module'));
            modalWindow = element(by.css('#cash-day-create-modal'));
            okButton = modalWindow.element(by.css('.btn-success.btn-block.ok'));
            firstRow = element.all(by.css('#cash-journal-cashdays-dtable tr')).get(0);
            editButton = firstRow.element(by.css('.js-edit-cash-day'));
            cancelButton = modalWindow.element(by.css('.btn-default.btn-block.cancel'));
        });

        it('should open new family modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should cancel edit expense', function () {
            var cancelButton = element(by.css('button.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            browser.sleep(1000);
        });

        it('should open new family modal window', function () {
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should fill mandatory fields and submit', function() {
            var inputTicketNumber = modalWindow.element(by.css('#zNumber'));
            var ticketNumber = Math.floor(Math.random() * 1000);
            inputTicketNumber.sendKeys(ticketNumber);

            var numberTransactionsinput = modalWindow.element(by.css('#numberOfTransactions'));
            var numberTransactions = Math.floor(Math.random() * 1000);
            numberTransactionsinput.clear();
            numberTransactionsinput.sendKeys(numberTransactions);

            //Income tab

            var tab = modalWindow.element(by.css('div.tab-pane.active[role="tabpanel"]'));

            var salesInput = modalWindow.element(by.css('#numberOfTransactions'));
            salesInput.clear();
            var salesAmmount = numberTransactions * 10;
            salesInput.sendKeys(salesAmmount);

            var modesAmmount = salesAmmount / 8;
            var modesInput = tab.element(by.css('#CASH'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#CHECK'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#BANK_CARD'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#TICKET_RESTAURANT'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#CLIENT_CREDIT'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#WIRE_TRANSFER'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#TICKET_HOLIDAY'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#GIFT_CARD'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            var cashReinforcementInput = tab.element(by.css('#cashReinforcement'));
            var cashReinforcement = Math.floor(Math.random() * 10);
            cashReinforcementInput.sendKeys('asd');
            expect(cashReinforcementInput.getText()).not.toEqual('asd');
            cashReinforcementInput.clear();
            cashReinforcementInput.sendKeys(cashReinforcement);

            //Expenses tab

            var expensesTabClick = modalWindow.element(by.css('li[role="cash-outputs"'));
            generalHelper.waitElementClickableAndClick(expensesTabClick);

            tab = modalWindow.element(by.css('#cj-outs'));

            generalHelper.waitElementDisplayed(tab);

            var bankExpenses = Math.floor(Math.random() * 500);
            var expenseModesAmmount = bankExpenses / 6;

            modesInput = tab.element(by.css('#CASH'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(expenseModesAmmount);

            modesInput = tab.element(by.css('#CHECK'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(expenseModesAmmount);

            modesInput = tab.element(by.css('#TICKET_RESTAURANT'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(expenseModesAmmount);

            modesInput = tab.element(by.css('#WIRE_TRANSFER'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(expenseModesAmmount);

            modesInput = tab.element(by.css('#TICKET_HOLIDAY'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(expenseModesAmmount);

            modesInput = tab.element(by.css('#GIFT_CARD'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(expenseModesAmmount);

            //Add supplier purchase

            var supplierPurchaseButton = tab.element(by.css('button.js-add-supplier-purchase'));
            generalHelper.waitElementClickableAndClick(supplierPurchaseButton);

            var tr = tab.all(by.css('#supplier-purchases-region tr')).last();

            var supplierInput = tr.element(by.css('div.js-auto-complete'));
            generalHelper.selectTypeAheadItem(supplierInput, 'input[name="supplierBaseCompanyName"]', 'alteau', 1);

            var supplierInvoiceInput = tr.element(by.css('input[name="supplierDocId"]'));
            supplierInvoiceInput.clear();
            var invoiceNumber = Math.floor(Math.random() * 100);
            supplierInvoiceInput.sendKeys(invoiceNumber);

            var supplierComment = tr.element(by.css('input[name="observations"]'));
            supplierComment.sendKeys("test");

            var supplierAmmountInput = tr.element(by.css('input[name="totalInCash"'));
            supplierAmmountInput.clear();
            var supplierAmmount = Math.floor(Math.random() * 100);
            supplierAmmountInput.sendKeys(supplierAmmount);

            //Add cash withdrawal

            var cashWithdrawalButton = tab.element(by.css('button.js-add-cash-withdrawal'));
            generalHelper.waitElementClickableAndClick(cashWithdrawalButton);

            tr = tab.all(by.css('#cash-withdrawals-region tr')).last();

            var withdrawalDescription = tr.element(by.css('input.js-description'));
            withdrawalDescription.sendKeys("test");

            var withdrawalTypeDrop = tr.element(by.css('button'));
            var withdrawalTypeSelect = tr.element(by.css('div.dropdown-menu ul.dropdown-menu li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(withdrawalTypeDrop);
            generalHelper.waitElementClickableAndClick(withdrawalTypeSelect);

            var withdrawalAmmountInput = tr.element(by.css('input[name="value"]'));
            withdrawalAmmountInput.sendKeys(Math.floor(Math.random() * 10));

            //add client credit

            var clientCreditButton = tab.element(by.css('button.js-add-client-credit'));
            generalHelper.waitElementClickableAndClick(clientCreditButton);

            browser.sleep(1000);

            tr = tab.all(by.css('#client-credit-region tr')).last();

            var clientInput = tr.element(by.css('input.js-customer-name'));
            clientInput.sendKeys("test");

            var commentsInput = tr.element(by.css('input.js-observations'));
            commentsInput.sendKeys("test");

            var clientAmmountInput = tr.element(by.css('input.js-client-credit'));
            clientAmmountInput.sendKeys(modesAmmount);

            var observationsInput = modalWindow.element(by.css('#observations'));
            observationsInput.sendKeys('Test');

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});