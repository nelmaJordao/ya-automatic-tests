describe('Portal Admin', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTCENTRE.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.TESTCENTRE.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTCENTRE);
    });


    describe('Teams', function () {
        //declarar variaveis necessarias em mais do que um teste
        var tabItem,
            firstRow,
            modalWindow;

        beforeEach(function() {
            tabItem = element(by.css('#portal-admin-menu-region [data-sec="teams"]'));
            firstRow = element.all(by.css('#portal-admin-teams-dtable tr')).first();
            modalWindow = element(by.css('div#team-read-modal'));
        });

        it('should open show team modal window', function () {
            generalHelper.waitElementClickableAndClick(tabItem);

            var showButton = firstRow.element(by.css('.options .js-show-team'));
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {
            var name = modalWindow.all(by.css('.form-group .value')).get(0);

            expect(name.getText()).not.toEqual('');
        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});