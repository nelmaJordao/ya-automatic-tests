describe('Portal Admin', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTCENTRE.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.TESTCENTRE.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTCENTRE);
    });


    describe('Teams', function () {
        //declarar variaveis necessarias em mais do que um teste
        var tabItem,
            firstRow,
            modalWindow,
            okButton,
            nameInput;

        beforeEach(function() {
            tabItem = element(by.css('#portal-admin-menu-region [data-sec="teams"]'));
            firstRow = element.all(by.css('#portal-admin-teams-dtable tr')).first();
            modalWindow = element(by.css('div#team-update-modal'));
            okButton = modalWindow.element(by.css('.btn-success'));
            nameInput = modalWindow.element(by.css('input#name'));
        });

        it('should open edit team modal window', function () {
            generalHelper.waitElementClickableAndClick(tabItem);
            var editButton = firstRow.element(by.css('.js-edit-team'));
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should cancel edit team', function () {
            var cancelButton = element(by.css('button.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            browser.sleep(1000);
        });

        it('should open edit team modal window', function () {
            var editButton = firstRow.element(by.css('.js-edit-team'));
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should fill mandatory fields and submit', function() {

            nameInput.clear();

            nameInput.sendKeys('test team');

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});