describe('Portal Admin', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTCENTRE.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.TESTCENTRE.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTCENTRE);
    });


    describe('Teams', function () {
        //declarar variaveis necessarias em mais do que um teste
        var tabItem,
            firstRow,
            modalWindow;

        beforeEach(function() {
            tabItem = element(by.css('#portal-admin-menu-region [data-sec="teams"]'));
            firstRow = element.all(by.css('#portal-admin-teams-dtable tr')).first();
            modalWindow = element(by.css('div#team-delete-modal'));
        });

        it('should open delete accountant modal window', function () {
            generalHelper.waitElementClickableAndClick(tabItem);

            var deleteButton = firstRow.element(by.css('.js-delete-team'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(deleteButton);

            browser.sleep(1000);

            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should confirm delete modal window', function() {
            var okButton = modalWindow.element(by.css('.btn-success.ok'));
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(--recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});