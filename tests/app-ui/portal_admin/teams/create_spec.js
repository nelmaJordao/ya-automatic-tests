describe('Portal Admin', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTCENTRE.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.TESTCENTRE.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTCENTRE);
    });

    var team = {  name : "equipa teste"
    };

    seedHelper.createTeam( team, baseURL + '#logout' );

    /*
    describe('Team', function () {

        var tabItem,
            newButton,
            modalWindow,
            saveButton;

        beforeEach(function() {
            tabItem = element(by.css('#portal-admin-menu-region [data-sec="teams"]'));
            newButton = element(by.css('button.js-new-team'));
            modalWindow = element(by.css('#team-update-modal'));
            saveButton = modalWindow.element(by.css('button.btn-success.ok'));
        });

        it('should open new accounting firm modal window', function () {
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);


            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {
            generalHelper.waitElementClickableAndClick(saveButton);

            generalHelper.waitAlertDangerPresentAndClose();
        });

        it('should fill mandatory fields and submit', function() {

            var nameInput = element(by.css('#name'));

            nameInput.sendKeys('Centre team');

            var addAccountantButton = modalWindow.element(by.css('button.js-add-accountant'));

            generalHelper.waitElementClickableAndClick(addAccountantButton);

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(saveButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });

        });

    });
    */

});