describe('Admin', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTCENTRE.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.TESTCENTRE.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTCENTRE);
    });

    describe('Customer Companies', function () {
        //declarar variaveis necessarias em mais do que um teste
        var tabItem,
            firstRow,
            modalWindow;

        beforeEach(function() {
            tabItem = element(by.css('#portal-admin-menu-region [data-sec="customercompanies"]'));
            firstRow = element.all(by.css('#portal-admin-companies-dtable tr')).first();
            modalWindow = element(by.css('div#company-read-modal'));
        });

        it('should open show accountant modal window', function () {
            generalHelper.waitElementClickableAndClick(tabItem);

            var showButton = firstRow.element(by.css('.options .js-show-customer-company'));
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {
            browser.sleep(1000);

            var TVA = modalWindow.all(by.css('.form-group .value')).get(2);
            var siren = modalWindow.all(by.css('.form-group .value')).get(3);
            var address = modalWindow.all(by.css('.form-group .value')).get(5);

            expect(TVA.getText()).not.toEqual('');
            expect(siren.getText()).not.toEqual('');
            expect(address.getText()).not.toEqual('');
        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});