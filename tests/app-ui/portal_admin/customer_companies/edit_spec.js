describe('Admin', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTCENTRE.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.TESTCENTRE.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTCENTRE);
    });

    describe('Customer Company', function () {

        var tabItem,
            firstRow,
            modalWindow;

        beforeEach(function() {
            tabItem = element(by.css('#portal-admin-menu-region [data-sec="customercompanies"]'));
            firstRow = element.all(by.css('#portal-admin-companies-dtable tr')).first();
            modalWindow = element(by.css('#company-update-modal'));
        });

        it('should open new accounting firm modal window', function () {
            generalHelper.waitElementClickableAndClick(tabItem);

            var editButton = firstRow.element(by.css('.js-edit-customer-company'));
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should fill form fields and submit', function() {
            var inputClientNumber = modalWindow.element(by.css('#clientNumber'));
            var inputEmail = modalWindow.element(by.css('#email'));
            var inputPhone = modalWindow.element(by.css('#phone'));
            var inputWebsite = modalWindow.element(by.css('#website'));
            var okButton = modalWindow.element(by.css('.btn-success'));

            inputClientNumber.clear();
            inputEmail.clear();
            inputPhone.clear();
            inputWebsite.clear();

            inputClientNumber.sendKeys('123456789');
            inputEmail.sendKeys('test.artec@xpto.biz');
            inputWebsite.sendKeys('http://testartec.themachine.dev.void.pt');
            inputPhone.sendKeys('987654321');

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            browser.sleep(1000);

            var tdClientNumber = firstRow.all(by.css('td')).get(1);
            var tdEmail = firstRow.all(by.css('td')).get(5);
            var tdPhone = firstRow.all(by.css('td')).get(6);
            var tdWebsite = firstRow.all(by.css('td a')).get(0);

            expect(tdClientNumber.getText()).toEqual('123456789');
            expect(tdEmail.getText()).toEqual('test.artec@xpto.biz');
            expect(tdWebsite.getText()).toEqual('http://testartec.themachine.dev.void.pt');
            expect(tdPhone.getText()).toEqual('987654321');

        });

    });

});