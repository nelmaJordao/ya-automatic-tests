describe('Admin', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTCENTRE.subdomain);
    var sirenHelper = requireCommonHelper('sirenHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.TESTCENTRE.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTCENTRE);
    });

    describe('Customer Company', function () {

        var tabItem,
            newButton,
            modalWindow;

        beforeEach(function() {
            tabItem = element(by.css('#portal-admin-menu-region [data-sec="customercompanies"]'));
            newButton = element(by.css('button.js-new-customer-company'));
            modalWindow = element(by.css('#company-create-modal'));
        });

        it('should open new customer company modal window', function () {
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should fill mandatory fields and submit', function() {
            var nameInput = modalWindow.element(by.css('.js-company-autocomplete'));

            browser.sleep(500);

            generalHelper.selectTypeAheadCreateNew(nameInput, 'input#name','a');

            var sirenInput = modalWindow.element(by.css('#sirenNumber'));
            var sirenButton = modalWindow.element(by.css('div.js-siren-check button.js-check-vat'));

            browser.sleep(500);

            sirenInput.sendKeys(sirenHelper.getSiren());

            generalHelper.waitElementClickableAndClick(sirenButton);

            browser.sleep(2000);

            var domainInput =  modalWindow.element(by.css('#subDomain'));

            domainInput.sendKeys('testartec');

            var firstNameInput = modalWindow.element(by.css('#firstName'));
            var lastNameInput = modalWindow.element(by.css('#lastName'));
            var emailInput = modalWindow.element(by.css('#managerEmail'));

            firstNameInput.sendKeys('test');
            lastNameInput.sendKeys('artec');
            emailInput.sendKeys('test.artec@xpto.biz');

            var checkbox = modalWindow.element(by.css('input.js-send-activation-email'));

            generalHelper.waitElementClickableAndClick(checkbox);

            var teamDropdown = modalWindow.element(by.css('button.dropdown-toggle.selectpicker[data-id="team"]'));

            generalHelper.waitElementClickableAndClick(teamDropdown);

            var teamSelect = modalWindow.element(by.css('div.dropdown-menu.open ul.dropdown-menu.inner li[data-original-index="1"] a'));

            generalHelper.waitElementClickableAndClick(teamSelect);

            var saveButton = modalWindow.element(by.css('button.btn-success.ok'));

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(1000);

            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });

        });

    });

});