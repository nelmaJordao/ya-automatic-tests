describe('Portal Admin', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTCENTRE.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.TESTCENTRE.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTCENTRE);
    });


    var accountant = {  firstName : "teste",
                        lastName : "senetres",
                        email : "test.sene3@xpto.biz"};

    seedHelper.createAccountant( accountant, baseURL + '#logout' );
    
});