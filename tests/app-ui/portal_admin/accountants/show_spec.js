describe('Portal Admin', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTCENTRE.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.TESTCENTRE.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTCENTRE);
    });


    describe('Accountants', function () {
        //declarar variaveis necessarias em mais do que um teste
        var firstRow,
            modalWindow;

        beforeEach(function() {
            firstRow = element.all(by.css('#portal-admin-accountants-dtable tr')).first();
            modalWindow = element(by.css('div#accountant-read-modal'));
        });

        it('should open show accountant modal window', function () {
            var showButton = firstRow.element(by.css('.options .js-show-accountant'));
            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {
            var firstName = modalWindow.all(by.css('.form-group .value')).get(0);
            var lastName = modalWindow.all(by.css('.form-group .value')).get(1);
            var email = modalWindow.all(by.css('.form-group .value')).get(2);

            expect(firstName.getText()).not.toEqual('');
            expect(lastName.getText()).not.toEqual('');
            expect(email.getText()).not.toEqual('');
        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});