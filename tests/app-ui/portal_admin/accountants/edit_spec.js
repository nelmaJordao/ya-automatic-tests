describe('Portal Admin', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTCENTRE.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.TESTCENTRE.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTCENTRE);
    });

    describe('Accountants', function () {
        //declarar variaveis necessarias em mais do que um teste
        var firstRow,
            modalWindow,
            okButton,
            firstNameInput,
            lastNameInput;

        beforeEach(function() {
            firstRow = element.all(by.css('#portal-admin-accountants-dtable tr')).first();
            modalWindow = element(by.css('div#accountant-update-modal'));
            okButton = modalWindow.element(by.css('.btn-success'));
            firstNameInput = modalWindow.element(by.css('input#firstName'));
            lastNameInput = modalWindow.element(by.css('input#lastName'));
        });

        it('should open edit accountant modal window', function () {
            var editButton = firstRow.element(by.css('.js-edit-accountant'));
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should cancel edit accountant', function () {
            var cancelButton = element(by.css('button.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            browser.sleep(1000);
        });

        it('should open edit accountant modal window', function () {
            var editButton = firstRow.element(by.css('.js-edit-accountant'));
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should fill mandatory fields and submit', function() {

            firstNameInput.clear();
            lastNameInput.clear();

            firstNameInput.sendKeys('accountant');
            lastNameInput.sendKeys('um');

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});