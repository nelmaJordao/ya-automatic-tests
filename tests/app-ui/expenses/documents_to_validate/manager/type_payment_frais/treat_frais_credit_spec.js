describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents to validate', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            documentsTabItem,
            closeButton,
            firstRow,
            editButton,
            modalWindow,
            stringEmploye,
            stringManager,
            documentsCount,
            documentsManagerCount;

        beforeEach(function() {
            menuIcon = element(by.css('#expenses-module'));
            documentsTabItem = element(by.css('#expenses-menu-region [data-sec="documentstovalidate"]'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            firstRow = element.all(by.css('#docs-to-validate-dtable tr')).first();
            editButton = firstRow.element(by.css('.js-edit-doc-to-validate'));
            modalWindow = element(by.css('#document-tagging-edit-document-modal'));
            stringEmploye = 'EMPLOYEE';
            stringManager = 'MANAGER';
        });

        it('should check number of documents in manager', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(documentsTabItem);
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            documentsManagerCount = 0;
            records.getText().then(function(text) {
                documentsManagerCount = parseInt(text);
            });
        });

        it('should check number of documents in employe', function () {

            loginHelper.changeRoleTo(stringEmploye);

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(documentsTabItem);
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            documentsCount = 0;
            records.getText().then(function(text) {
                documentsCount = parseInt(text);
            });
        });

        it('should fill form and submit', function () {

            loginHelper.changeRoleTo(stringManager);

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(documentsTabItem);
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(editButton);

            browser.sleep(2000);

            var docTypeSelect = modalWindow.element(by.css('button.selectpicker[data-id="docType"]'));
            var docTypeOption = modalWindow.element(by.css('li[data-original-index="2"] a.js-doctype-option'));
            generalHelper.waitElementClickableAndClick(docTypeSelect);
            generalHelper.waitElementClickableAndClick(docTypeOption);

            var expenseTypeSelect = modalWindow.element(by.css('button[data-id="expenseType"]'));
            var expenseTypeOption = modalWindow.element(by.css('li[data-original-index="2"] a.js-expensetype-option'));
            generalHelper.waitElementClickableAndClick(expenseTypeSelect);
            generalHelper.waitElementClickableAndClick(expenseTypeOption);

            var documentDate = modalWindow.element(by.css('#calfield-documentDate'));
            documentDate.clear();
            documentDate.sendKeys('29-05-2016');

            var employeeSelect = modalWindow.element(by.css('button.selectpicker[data-id="user"]'));
            var employeeOption = modalWindow.element(by.css('li[data-original-index="5"] a.js-user-option'));
            generalHelper.waitElementClickableAndClick(employeeSelect);
            generalHelper.waitElementClickableAndClick(employeeOption);

            var keywordsInput = modalWindow.element(by.css('div.tags-input-box span input.tt-input'));
            keywordsInput.sendKeys('asdasdasd');
            var keywordSuggest = modalWindow.element(by.css('div.tags-input-box span div.tt-menu div div.tt-suggestion'));
            generalHelper.waitElementClickableAndClick(keywordSuggest);

            var paymentSelect = modalWindow.element(by.css('button[data-id="paymentMethod"]'));
            var paymentOption = modalWindow.element(by.css('li[data-original-index="3"] a.js-paymethod-option'));
            generalHelper.waitElementClickableAndClick(paymentSelect);
            generalHelper.waitElementClickableAndClick(paymentOption);

            var compteSelect = modalWindow.element(by.css('button[data-id="paymentBank"]'));
            var compteOption = modalWindow.element(by.css('li[data-original-index="1"] a.js-paymentBank-option'));
            generalHelper.waitElementClickableAndClick(compteSelect);
            generalHelper.waitElementClickableAndClick(compteOption);

            var addVatButton = modalWindow.element(by.css('.js-add-vat'));
            var vatDropdown = modalWindow.element(by.css('button[data-id="pickVat"]'));
            var vatOption = modalWindow.element(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="4"] a.js-choose-vat span.text'));
            generalHelper.waitElementClickableAndClick(addVatButton);
            generalHelper.waitElementClickableAndClick(vatDropdown);
            generalHelper.waitElementClickableAndClick(vatOption);

            var firstRowHT = modalWindow.all(by.css('input.js-ht-value')).get(0);
            var secondRowHT = modalWindow.all(by.css('input.js-ht-value')).get(1);
            var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
            firstRowHT.clear();
            secondRowHT.clear();
            thirdRowHT.clear();
            firstRowHT.sendKeys('100');
            secondRowHT.sendKeys('200');
            thirdRowHT.sendKeys('300');


            //Testar rotação
            var rotateButton = modalWindow.element(by.css('a.js-rotate-image'));

            for (var i = 0; i <= 3; i++) {

                generalHelper.waitElementClickableAndClick(rotateButton);

                browser.sleep(500);
            }

            var designationInput = modalWindow.element(by.css('#description'));
            designationInput.clear();
            designationInput.sendKeys('Descrição1');

            //Testar Associação de páginas
            if(modalWindow.element(by.css('a.js-open-pages')).isDisplayed()){
                var btnPages = modalWindow.element(by.css('a.js-open-pages'));
                generalHelper.waitElementClickableAndClick(btnPages);

                var associatePage = modalWindow.all(by.css('a.js-thumb-check')).get(0);
                generalHelper.waitElementClickableAndClick(associatePage);

                var closePages = modalWindow.element(by.css('a.js-viewer-editing-close'));
                generalHelper.waitElementClickableAndClick(closePages);
            }

            //Guardar
            var saveButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            });

        it('should confirm duplicated document', function() {

            loginHelper.changeRoleTo(stringEmploye);
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(documentsTabItem);
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++documentsCount);
            });

        });

        it('should confirm reduction document in manager', function() {
            loginHelper.changeRoleTo(stringManager);
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(documentsTabItem);
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(--documentsManagerCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
    });

});



