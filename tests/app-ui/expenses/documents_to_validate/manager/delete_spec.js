describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents to validate', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            documentsTabItem,
            closeButton,
            modalWindow,
            okButton,
            editButton,
            searchButton;

        beforeEach(function() {
            menuIcon = element(by.css('#expenses-module'));
            documentsTabItem = element(by.css('#expenses-menu-region [data-sec="documentstovalidate"]'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            modalWindow = element(by.css('#document-tagging-delete-document-modal'));
            okButton = element(by.css('.btn-success.ok'));
            editButton = element.all(by.css('#docs-to-validate-dtable .js-edit-doc-to-validate'));
            searchButton = element(by.css('.js-search-btn'));
        });


        it('should open delete document modal window', function ()
        {
            var firstDeleteButton = element.all(by.css('#docs-to-validate-dtable a.js-delete-doc-to-validate')).get(0);

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(documentsTabItem);
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(searchButton);
            generalHelper.waitElementClickableAndClick(editButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(searchButton);
            firstDeleteButton.click();
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });


        it('should confirm delete modal window', function()
        {
            var confirmDeleteButton = modalWindow.element(by.css('.btn-success'));
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(confirmDeleteButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text)
            {
                expect(parseInt(text)).toEqual(--recordCount);
            });

            });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});