describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents to validate', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            documentsTabItem,
            closeButton,
            modalWindow,
            stringEmploye,
            firstShowButton,
            close;

        beforeEach(function() {
            menuIcon = element(by.css('#expenses-module'));
            documentsTabItem = element(by.css('#expenses-menu-region [data-sec="documentstovalidate"]'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            modalWindow = element(by.css('#document-tagging-view-document-modal'));
            firstShowButton = element.all(by.css('#docs-to-validate-dtable a.js-show-doc-to-validate')).get(0);
            close = element(by.css('.btn-default.cancel'));
            stringEmploye = 'EMPLOYEE';
        });

        it('should open show document to validate modal window', function ()
        {
            loginHelper.changeRoleTo(stringEmploye);

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(documentsTabItem);
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(firstShowButton);
            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(2000);

            expect(modalWindow.isDisplayed()).toBe(true);

        });


        it('should close modal window', function() {
            generalHelper.waitElementClickableAndClick(close);

            expect(modalWindow.isPresent()).toBe(false);

            });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});