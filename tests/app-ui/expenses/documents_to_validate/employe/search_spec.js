describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents to Validate', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            searchField,
            closeButton,
            searchButton,
            stringEmploye;


        beforeEach(function() {
            menuIcon = element(by.css('#expenses-module'));
            tabItem = element(by.css('#expenses-menu-region [data-sec="documentstovalidate"]'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            searchField = element(by.css('#searchTable'));
            searchButton = element(by.css('.js-search-btn'));
            stringEmploye = 'EMPLOYEE';
        });

        it('should search', function () {

            loginHelper.changeRoleTo(stringEmploye);

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            searchField.clear();
            searchField.sendKeys('1');

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(searchButton);
            browser.sleep(1000);

            records.getText().then(function(text) {
                expect(parseInt(text)).not.toEqual(recordCount);
            });

            });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});