describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents to validate', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            documentsTabItem,
            closeButton,
            firstRow,
            editButton,
            modalWindow;

        var stringEmploye;

        beforeEach(function() {
            menuIcon = element(by.css('#expenses-module'));
            documentsTabItem = element(by.css('#expenses-menu-region [data-sec="documentstovalidate"]'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            firstRow = element.all(by.css('#docs-to-validate-dtable tr')).first();
            editButton = firstRow.element(by.css('.js-edit-doc-to-validate'));
            modalWindow = element(by.css('#document-tagging-edit-document-modal'));
            stringEmploye = 'EMPLOYEE';
        });

        it('should fill form and submit', function () {

            loginHelper.changeRoleTo(stringEmploye);

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(documentsTabItem);

            generalHelper.waitElementClickableAndClick(closeButton);

            generalHelper.waitElementClickableAndClick(editButton);


            browser.sleep(1000);

            var docTypeSelect = modalWindow.element(by.css('button.selectpicker[data-id="docType"]'));
            var docTypeOption = modalWindow.element(by.css('li[data-original-index="1"] a.js-doctype-option'));
            generalHelper.waitElementClickableAndClick(docTypeSelect);
            generalHelper.waitElementClickableAndClick(docTypeOption);

            //Testar rotação
            var rotateButton = modalWindow.element(by.css('a.js-rotate-image'));

            for (var i = 0; i <= 3; i++) {

                generalHelper.waitElementClickableAndClick(rotateButton);

                browser.sleep(500);
            }

            //Testar Associação de páginas
            if(modalWindow.element(by.css('a.js-open-pages')).isDisplayed()){
                var btnPages = modalWindow.element(by.css('a.js-open-pages'));
                generalHelper.waitElementClickableAndClick(btnPages);

                var associatePage = modalWindow.all(by.css('a.js-thumb-check')).get(0);
                generalHelper.waitElementClickableAndClick(associatePage);

                var closePages = modalWindow.element(by.css('a.js-viewer-editing-close'));
                generalHelper.waitElementClickableAndClick(closePages);
            }

            //Guardar
            var saveButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);
            generalHelper.waitAlertSuccessPresentAndClose();
            generalHelper.waitElementClickableAndClick(closeButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});