describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents to validate', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            documentsTabItem,
            closeButton,
            firstRow,
            editButton,
            modalWindow,
            modalWindow2,
            okButton,
            stringEmploye,
            stringManager,
            documentsCount,
            documentsManagerCount;

        beforeEach(function() {
            menuIcon = element(by.css('#expenses-module'));
            documentsTabItem = element(by.css('#expenses-menu-region [data-sec="documentstovalidate"]'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            firstRow = element.all(by.css('#docs-to-validate-dtable tr')).first();
            editButton = firstRow.element(by.css('.js-edit-doc-to-validate'));
            modalWindow = element(by.css('#document-tagging-edit-document-modal'));
            modalWindow2 = element.all(by.css('html body.modal-open div.modal.in')).get(1);
            okButton = modalWindow2.element(by.css('.btn-success.ok'));
            stringEmploye = 'EMPLOYEE';
            stringManager = 'MANAGER';
        });

        it('should check number of documents in employe', function () {

            loginHelper.changeRoleTo(stringEmploye);

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(documentsTabItem);
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            documentsCount = 0;
            records.getText().then(function(text) {
                documentsCount = parseInt(text);
            });
        });

        it('should check number of documents in manager', function () {

            loginHelper.changeRoleTo(stringManager);
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(documentsTabItem);
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            documentsManagerCount = 0;
            records.getText().then(function(text) {
                documentsManagerCount = parseInt(text);
            });
        });

        it('should fill form and submit', function () {

            loginHelper.changeRoleTo(stringEmploye);

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(documentsTabItem);

            browser.sleep(2000);

            var redirectButton = modalWindow.element(by.css('.input-group-btn .js-user-disassociate'));
            generalHelper.waitElementClickableAndClick(redirectButton);
            generalHelper.waitElementClickableAndClick(okButton);

        });

        it('should confirm duplicated document in manager', function() {

            loginHelper.changeRoleTo(stringManager);
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(documentsTabItem);
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++documentsManagerCount);
            });

        });

        it('should confirm reduction document in employe', function() {

            loginHelper.changeRoleTo(stringEmploye);
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(documentsTabItem);
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(--documentsCount);
            });


        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});

