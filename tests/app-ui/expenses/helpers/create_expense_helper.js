module.exports = {

    createAutre: function (numPayment) {

        describe('Expenses', function () {
            var generalHelper = requireCommonHelper('generalHelper');
            var loginHelper = requireCommonHelper('loginHelper');
            var config = requireConfig();

            beforeEach(function () {
                browser.ignoreSynchronization = true;
            });

            afterEach(function () {
                browser.ignoreSynchronization = false;
            });

            requireLoginSpec();

            describe('Documents', function () {

                //declarar variaveis necessarias em mais do que um teste
                var menuIcon,
                    newButton,
                    modalWindow,
                    okButton;

                beforeEach(function() {
                            menuIcon = element(by.css('#expenses-module'));
                    newButton = element(by.css('.js-new-expense'));
                    modalWindow = element(by.css('#expense-create-modal'));
                    okButton = modalWindow.element(by.css('.btn-success.ok'));
                });

                it('should open new expense modal window', function () {

                    generalHelper.waitElementClickableAndClick(menuIcon);

                    browser.sleep(1000);

                    generalHelper.waitElementClickableAndClick(newButton);
                    generalHelper.waitElementPresent(modalWindow);

                    browser.sleep(1000);

                    expect(modalWindow.isDisplayed()).toBe(true);
                });

                it('should check for empty fields', function () {
                    var typeSelect = modalWindow.element(by.css('button.selectpicker[data-id="expenseType"]'));
                    var typeOption = modalWindow.element(by.css('ul.inner li[data-original-index="3"] a.js-expense-type-option'));
                    generalHelper.waitElementClickableAndClick(typeSelect);
                    generalHelper.waitElementClickableAndClick(typeOption);

                    generalHelper.waitElementClickableAndClick(okButton);

                    var errors = modalWindow.all(by.css('.form-group.error'));

                    expect(errors.count()).toEqual(3);
                });

                it('should test field restrictions', function () {

                    var addVatButton = modalWindow.element(by.css('.js-add-vat'));
                    var vatDropdown = modalWindow.all(by.css('button[data-id="pickVat"]')).get(2);
                    var vatOption = modalWindow.all(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-choose-vat span.text')).get(2);
                    generalHelper.waitElementClickableAndClick(addVatButton);
                    generalHelper.waitElementClickableAndClick(vatDropdown);
                    generalHelper.waitElementClickableAndClick(vatOption);

                    browser.sleep(1000);

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('asd');
                    secondRowHT.sendKeys('fgh');
                    thirdRowHT.sendKeys('jkl');
                });

                it('should fill mandatory fields and submit', function() {

                    var employeeSelect = modalWindow.element(by.css('button.selectpicker[data-id="user"]'));
                    var employeeOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-user-option'));
                    generalHelper.waitElementClickableAndClick(employeeSelect);
                    generalHelper.waitElementClickableAndClick(employeeOption);

                    var descriptionInput = modalWindow.element(by.css('#description'));
                    descriptionInput.sendKeys(config.inputs.getDescription(260));

                    if (numPayment != 0) {
                        var paymentSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentMethod"]'));
                        var paymentOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="' + numPayment + '"] a.js-paymethod-option'));
                        generalHelper.waitElementClickableAndClick(paymentSelect);
                        generalHelper.waitElementClickableAndClick(paymentOption);
                    }

                    var accountSelect;
                    var accountOption;

                    if (numPayment == 2) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));

                        browser.sleep(1000);

                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                    }
                    if (numPayment == 3) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                        var checkNumberInput = modalWindow.element(by.css('#checkNumber'));
                        checkNumberInput.sendKeys("123456789");
                    }

                    var keywordsInput = modalWindow.element(by.css('div.tags-input-box span input.tt-input'));
                    keywordsInput.sendKeys('asdasdasd');


                    var noteInput = modalWindow.element(by.css('#comment'));
                    noteInput.sendKeys(config.inputs.getDescription(260));

                    var table = modalWindow.all(by.css('.table-striped.table-hover tr'));
                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();

                    browser.sleep(2000);

                    firstRowHT.sendKeys('100');

                    browser.sleep(2000);

                    secondRowHT.sendKeys('200');

                    browser.sleep(2000);

                    thirdRowHT.sendKeys('300');

                    browser.sleep(1000);

                    browser.executeScript('$(\'input[type="file"]\').removeClass("hidden");');

                    var inputFile = modalWindow.element(by.css('#documentFileUpload'));
                    inputFile.sendKeys('/Volumes/WORK/ui-tests/docs/13_5_2016.pdf');

                    var records = element(by.css('.js-total-res span'));
                    var recordCount = 0;
                    records.getText().then(function(text) {
                        recordCount = parseInt(text);
                    });

                    generalHelper.waitElementClickableAndClick(okButton);
                    generalHelper.waitAlertSuccessPresentAndClose();

                    records.getText().then(function(text) {
                        expect(parseInt(text)).toEqual(++recordCount);
                    });

                });

                it('should return to dashboard', function () {
                    generalHelper.returnToDashboard();
                    browser.refresh();
                });

            });

        });

    },

    createLogement: function (numPayment) {

        describe('Expenses', function () {
            var generalHelper = requireCommonHelper('generalHelper');
            var loginHelper = requireCommonHelper('loginHelper');
            var config = requireConfig();

            beforeEach(function () {
                browser.ignoreSynchronization = true;
            });

            afterEach(function () {
                browser.ignoreSynchronization = false;
            });

            requireLoginSpec();

            describe('Documents', function () {

                //declarar variaveis necessarias em mais do que um teste
                var menuIcon,
                    newButton,
                    modalWindow,
                    okButton;

                beforeEach(function() {
                    menuIcon = element(by.css('#expenses-module'));
                    newButton = element(by.css('.js-new-expense'));
                    modalWindow = element(by.css('#expense-create-modal'));
                    okButton = modalWindow.element(by.css('.btn-success.ok'));
                });

                it('should open new expense modal window', function () {

                    generalHelper.waitElementClickableAndClick(menuIcon);
                    generalHelper.waitElementClickableAndClick(newButton);
                    generalHelper.waitElementPresent(modalWindow);

                    browser.sleep(1000);

                    expect(modalWindow.isDisplayed()).toBe(true);
                });

                it('should check for empty fields', function () {

                    var typeSelect = modalWindow.element(by.css('button.selectpicker[data-id="expenseType"]'));
                    var typeOption = modalWindow.element(by.css('ul.inner li[data-original-index="1"] a.js-expense-type-option'));
                    generalHelper.waitElementClickableAndClick(typeSelect);
                    generalHelper.waitElementClickableAndClick(typeOption);
                    generalHelper.waitElementClickableAndClick(okButton);

                    var errors = modalWindow.all(by.css('.form-group.error'));

                    expect(errors.count()).toEqual(2);
                });

                it('should test field restrictions', function () {
                    var addVatButton = modalWindow.element(by.css('.js-add-vat'));
                    var vatDropdown = modalWindow.all(by.css('button[data-id="pickVat"]')).get(2);
                    var vatOption = modalWindow.all(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-choose-vat span.text')).get(2);
                    generalHelper.waitElementClickableAndClick(addVatButton);
                    generalHelper.waitElementClickableAndClick(vatDropdown);
                    generalHelper.waitElementClickableAndClick(vatOption);

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('asd');
                    secondRowHT.sendKeys('fgh');
                    thirdRowHT.sendKeys('jkl');

                });

                it('should fill mandatory fields and submit', function() {

                    var employeeSelect = modalWindow.element(by.css('button.selectpicker[data-id="user"]'));
                    var employeeOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-user-option'));
                    generalHelper.waitElementClickableAndClick(employeeSelect);
                    generalHelper.waitElementClickableAndClick(employeeOption);

                    var descriptionInput = modalWindow.element(by.css('#description'));
                    descriptionInput.sendKeys(config.inputs.getDescription(260));

                    var accountSelect;
                    var accountOption;

                    if(numPayment != 0){
                        var paymentSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentMethod"]'));
                        var paymentOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="' + numPayment + '"] a.js-paymethod-option'));
                        generalHelper.waitElementClickableAndClick(paymentSelect);
                        generalHelper.waitElementClickableAndClick(paymentOption);
                    }
                    if(numPayment == 2){
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                    }
                    if(numPayment == 3){
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                        var checkNumberInput = modalWindow.element(by.css('#checkNumber'));
                        checkNumberInput.sendKeys("123456789");
                    }

                    var keywordsInput = modalWindow.element(by.css('div.tags-input-box span input.tt-input'));
                    keywordsInput.sendKeys('asdasdasd');


                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('100');
                    secondRowHT.sendKeys('200');
                    thirdRowHT.sendKeys('300');

                    browser.executeScript('$(\'input[type="file"]\').removeClass("hidden");');

                    var inputFile = modalWindow.element(by.css('#documentFileUpload'));
                    inputFile.sendKeys('/Volumes/WORK/ui-tests/docs/13_5_2016.pdf');

                    var records = element(by.css('.js-total-res span'));
                    var recordCount = 0;
                    records.getText().then(function(text) {
                        recordCount = parseInt(text);
                    });

                    generalHelper.waitElementClickableAndClick(okButton);
                    generalHelper.waitAlertSuccessPresentAndClose();

                    records.getText().then(function(text) {
                        expect(parseInt(text)).toEqual(++recordCount);
                    });

                });

                it('should return to dashboard', function () {
                    generalHelper.returnToDashboard();
                    browser.refresh();
                });

            });

        });
    },

    createRestaurant: function (numPayment) {
        describe('Expenses', function () {
            var generalHelper = requireCommonHelper('generalHelper');
            var loginHelper = requireCommonHelper('loginHelper');
            var config = requireConfig();

            beforeEach(function () {
                browser.ignoreSynchronization = true;
            });

            afterEach(function () {
                browser.ignoreSynchronization = false;
            });

            requireLoginSpec();

            describe('Documents', function () {

                //declarar variaveis necessarias em mais do que um teste
                var menuIcon,
                    newButton,
                    modalWindow,
                    okButton;

                beforeEach(function() {
                    menuIcon = element(by.css('#expenses-module'));
                    newButton = element(by.css('.js-new-expense'));
                    modalWindow = element(by.css('#expense-create-modal'));
                    okButton = modalWindow.element(by.css('.btn-success.ok'));
                });

                it('should open new expense modal window', function () {

                    generalHelper.waitElementClickableAndClick(menuIcon);
                    generalHelper.waitElementClickableAndClick(newButton);
                    generalHelper.waitElementPresent(modalWindow);

                    browser.sleep(1000);

                    expect(modalWindow.isDisplayed()).toBe(true);
                });

                it('should check for empty fields', function () {
                    var typeSelect = modalWindow.element(by.css('button.selectpicker[data-id="expenseType"]'));
                    var typeOption = modalWindow.element(by.css('ul.inner li[data-original-index="2"] a.js-expense-type-option'));
                    generalHelper.waitElementClickableAndClick(typeSelect);
                    generalHelper.waitElementClickableAndClick(typeOption);
                    generalHelper.waitElementClickableAndClick(okButton);

                    var errors = modalWindow.all(by.css('.form-group.error'));

                    expect(errors.count()).toEqual(2);
                });

                it('should test field restrictions', function () {
                    var addVatButton = modalWindow.element(by.css('.js-add-vat'));
                    var vatDropdown = modalWindow.all(by.css('button[data-id="pickVat"]')).get(2);
                    var vatOption = modalWindow.all(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-choose-vat span.text')).get(2);
                    generalHelper.waitElementClickableAndClick(addVatButton);
                    generalHelper.waitElementClickableAndClick(vatDropdown);
                    generalHelper.waitElementClickableAndClick(vatOption);

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('asd');
                    secondRowHT.sendKeys('fgh');
                    thirdRowHT.sendKeys('jkl');
                });

                it('should fill mandatory fields and submit', function() {

                    var employeeSelect = modalWindow.element(by.css('button.selectpicker[data-id="user"]'));
                    var employeeOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-user-option'));
                    generalHelper.waitElementClickableAndClick(employeeSelect);
                    generalHelper.waitElementClickableAndClick(employeeOption);

                    var descriptionInput = modalWindow.element(by.css('#description'));
                    descriptionInput.sendKeys(config.inputs.getDescription(260));

                    if (numPayment != 0) {
                        var paymentSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentMethod"]'));
                        var paymentOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="' + numPayment + '"] a.js-paymethod-option'));
                        generalHelper.waitElementClickableAndClick(paymentSelect);
                        generalHelper.waitElementClickableAndClick(paymentOption);
                    }

                    var accountSelect;
                    var accountOption;

                    if (numPayment == 2) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                    }
                    if (numPayment == 3) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                        var checkNumberInput = modalWindow.element(by.css('#checkNumber'));
                        checkNumberInput.sendKeys("123456789");
                    }

                    var keywordsInput = modalWindow.element(by.css('div.tags-input-box span input.tt-input'));
                    keywordsInput.sendKeys('asdasdasd');


                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('100');
                    secondRowHT.sendKeys('200');
                    thirdRowHT.sendKeys('300');

                    browser.executeScript('$(\'input[type="file"]\').removeClass("hidden");');

                    var inputFile = modalWindow.element(by.css('#documentFileUpload'));
                    inputFile.sendKeys('/Volumes/WORK/ui-tests/docs/13_5_2016.pdf');

                    var records = element(by.css('.js-total-res span'));
                    var recordCount = 0;
                    records.getText().then(function(text) {
                        recordCount = parseInt(text);
                    });

                    generalHelper.waitElementClickableAndClick(okButton);
                    generalHelper.waitAlertSuccessPresentAndClose();

                    records.getText().then(function(text) {
                        expect(parseInt(text)).toEqual(++recordCount);
                    });

                });

                it('should return to dashboard', function () {
                    generalHelper.returnToDashboard();
                    browser.refresh();
                });

            });

        });
    },

    createTransport: function (numPayment) {
        describe('Expenses', function () {
            var generalHelper = requireCommonHelper('generalHelper');
            var loginHelper = requireCommonHelper('loginHelper');
            var config = requireConfig();

            beforeEach(function () {
                browser.ignoreSynchronization = true;
            });

            afterEach(function () {
                browser.ignoreSynchronization = false;
            });

            requireLoginSpec();

            describe('Documents', function () {

                //declarar variaveis necessarias em mais do que um teste
                var menuIcon,
                    newButton,
                    modalWindow,
                    okButton;

                beforeEach(function() {
                    menuIcon = element(by.css('#expenses-module'));
                    newButton = element(by.css('.js-new-expense'));
                    modalWindow = element(by.css('#expense-create-modal'));
                    okButton = modalWindow.element(by.css('.btn-success.ok'));
                });

                it('should open new expense modal window', function () {

                    generalHelper.waitElementClickableAndClick(menuIcon);
                    generalHelper.waitElementClickableAndClick(newButton);
                    generalHelper.waitElementPresent(modalWindow);

                    browser.sleep(1000);

                    expect(modalWindow.isDisplayed()).toBe(true);
                });

                it('should check for empty fields', function () {
                    var typeSelect = modalWindow.element(by.css('button.selectpicker[data-id="expenseType"]'));
                    var typeOption = modalWindow.element(by.css('ul.inner li[data-original-index="0"] a.js-expense-type-option'));
                    generalHelper.waitElementClickableAndClick(typeSelect);
                    generalHelper.waitElementClickableAndClick(typeOption);
                    generalHelper.waitElementClickableAndClick(okButton);

                    var errors = modalWindow.all(by.css('.form-group.error'));

                    expect(errors.count()).toEqual(2);
                });

                it('should test field restrictions', function () {
                    var addVatButton = modalWindow.element(by.css('.js-add-vat'));
                    var vatDropdown = modalWindow.all(by.css('button[data-id="pickVat"]')).get(2);
                    var vatOption = modalWindow.all(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-choose-vat span.text')).get(2);
                    generalHelper.waitElementClickableAndClick(addVatButton);
                    generalHelper.waitElementClickableAndClick(vatDropdown);
                    generalHelper.waitElementClickableAndClick(vatOption);

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('asd');
                    secondRowHT.sendKeys('fgh');
                    thirdRowHT.sendKeys('jkl');
                });

                it('should fill mandatory fields and submit', function() {

                    var employeeSelect = modalWindow.element(by.css('button.selectpicker[data-id="user"]'));
                    var employeeOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-user-option'));
                    generalHelper.waitElementClickableAndClick(employeeSelect);
                    generalHelper.waitElementClickableAndClick(employeeOption);

                    var descriptionInput = modalWindow.element(by.css('#description'));
                    descriptionInput.sendKeys(config.inputs.getDescription(260));

                    if (numPayment != 0) {
                        var paymentSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentMethod"]'));
                        var paymentOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="' + numPayment + '"] a.js-paymethod-option'));
                        generalHelper.waitElementClickableAndClick(paymentSelect);
                        generalHelper.waitElementClickableAndClick(paymentOption);
                    }

                    var accountSelect;
                    var accountOption;

                    if (numPayment == 2) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                    }
                    if (numPayment == 3) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                        var checkNumberInput = modalWindow.element(by.css('#checkNumber'));
                        checkNumberInput.sendKeys("123456789");
                    }

                    var keywordsInput = modalWindow.element(by.css('div.tags-input-box span input.tt-input'));
                    keywordsInput.sendKeys('asdasdasd');

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('100');
                    secondRowHT.sendKeys('200');
                    thirdRowHT.sendKeys('300');

                    browser.executeScript('$(\'input[type="file"]\').removeClass("hidden");');

                    var inputFile = modalWindow.element(by.css('#documentFileUpload'));
                    inputFile.sendKeys('/Volumes/WORK/ui-tests/docs/13_5_2016.pdf');

                    var records = element(by.css('.js-total-res span'));
                    var recordCount = 0;
                    records.getText().then(function(text) {
                        recordCount = parseInt(text);
                    });

                    generalHelper.waitElementClickableAndClick(okButton);
                    generalHelper.waitAlertSuccessPresentAndClose();

                    records.getText().then(function(text) {
                        expect(parseInt(text)).toEqual(++recordCount);
                    });

                });

                it('should return to dashboard', function () {
                    generalHelper.returnToDashboard();
                    browser.refresh();
                });

            });

        });
    },

    createAutreSubcats: function (numSubcat, numPayment) {

        describe('Expenses', function () {
            var generalHelper = requireCommonHelper('generalHelper');
            var loginHelper = requireCommonHelper('loginHelper');
            var config = requireConfig();

            beforeEach(function () {
                browser.ignoreSynchronization = true;
            });

            afterEach(function () {
                browser.ignoreSynchronization = false;
            });

            requireLoginSpec();

            describe('Documents', function () {

                //declarar variaveis necessarias em mais do que um teste
                var menuIcon,
                    newButton,
                    modalWindow,
                    okButton;

                beforeEach(function() {
                            menuIcon = element(by.css('#expenses-module'));
                    newButton = element(by.css('.js-new-expense'));
                    modalWindow = element(by.css('#expense-create-modal'));
                    okButton = modalWindow.element(by.css('.btn-success.ok'));
                });

                it('should open new expense modal window', function () {

                    generalHelper.waitElementClickableAndClick(menuIcon);
                    generalHelper.waitElementClickableAndClick(newButton);
                    generalHelper.waitElementPresent(modalWindow);

                    browser.sleep(1000);

                    expect(modalWindow.isDisplayed()).toBe(true);
                });

                it('should check for empty fields', function () {
                    var typeSelect = modalWindow.element(by.css('button.selectpicker[data-id="expenseType"]'));
                    var typeOption = modalWindow.element(by.css('ul.inner li[data-original-index="3"] a.js-expense-type-option'));
                    generalHelper.waitElementClickableAndClick(typeSelect);
                    generalHelper.waitElementClickableAndClick(typeOption);

                    var categorySelect = modalWindow.element(by.css('button.selectpicker[data-id="expenseSubType"]'));
                    var categoryOption = modalWindow.element(by.css('ul.inner li[data-original-index="' + numSubcat + '"] a.js-expense-sub-type-option'));
                    generalHelper.waitElementClickableAndClick(categorySelect);
                    generalHelper.waitElementClickableAndClick(categoryOption);
                    generalHelper.waitElementClickableAndClick(okButton);

                    var errors = modalWindow.all(by.css('.form-group.error'));

                    expect(errors.count()).toEqual(3);
                });

                it('should test field restrictions', function () {
                    var addVatButton = modalWindow.element(by.css('.js-add-vat'));
                    var vatDropdown = modalWindow.all(by.css('button[data-id="pickVat"]')).get(2);
                    var vatOption = modalWindow.all(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-choose-vat span.text')).get(2);
                    generalHelper.waitElementClickableAndClick(addVatButton);
                    generalHelper.waitElementClickableAndClick(vatDropdown);
                    generalHelper.waitElementClickableAndClick(vatOption);

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('asd');
                    secondRowHT.sendKeys('fgh');
                    thirdRowHT.sendKeys('jkl');
                });

                it('should fill mandatory fields and submit', function() {

                    var employeeSelect = modalWindow.element(by.css('button.selectpicker[data-id="user"]'));
                    var employeeOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-user-option'));
                    generalHelper.waitElementClickableAndClick(employeeSelect);
                    generalHelper.waitElementClickableAndClick(employeeOption);

                    var descriptionInput = modalWindow.element(by.css('#description'));
                    descriptionInput.sendKeys(config.inputs.getDescription(260));

                    if (numPayment != 0) {
                        var paymentSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentMethod"]'));
                        var paymentOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="' + numPayment + '"] a.js-paymethod-option'));
                        generalHelper.waitElementClickableAndClick(paymentSelect);
                        generalHelper.waitElementClickableAndClick(paymentOption);
                    }

                    var accountSelect;
                    var accountOption;

                    if (numPayment == 2) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                    }
                    if (numPayment == 3) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                        var checkNumberInput = modalWindow.element(by.css('#checkNumber'));
                        checkNumberInput.sendKeys("123456789");
                    }

                    var keywordsInput = modalWindow.element(by.css('div.tags-input-box span input.tt-input'));
                    keywordsInput.sendKeys('asdasdasd');


                    var noteInput = modalWindow.element(by.css('#comment'));
                    noteInput.sendKeys(config.inputs.getDescription(260));

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);

                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();

                    firstRowHT.sendKeys('100');
                    secondRowHT.sendKeys('200');
                    thirdRowHT.sendKeys('300');

                    browser.executeScript('$(\'input[type="file"]\').removeClass("hidden");');

                    var inputFile = modalWindow.element(by.css('#documentFileUpload'));
                    inputFile.sendKeys('/Volumes/WORK/ui-tests/docs/13_5_2016.pdf');

                    var records = element(by.css('.js-total-res span'));
                    var recordCount = 0;
                    records.getText().then(function(text) {
                        recordCount = parseInt(text);
                    });

                    generalHelper.waitElementClickableAndClick(okButton);
                    generalHelper.waitAlertSuccessPresentAndClose();

                    records.getText().then(function(text) {
                        expect(parseInt(text)).toEqual(++recordCount);
                    });

                });

                it('should return to dashboard', function () {
                    generalHelper.returnToDashboard();
                    browser.refresh();
                });

            });

        });

    },

    createLogementSubcats: function (numSubcat, numPayment) {
        describe('Expenses', function () {
            var generalHelper = requireCommonHelper('generalHelper');
            var loginHelper = requireCommonHelper('loginHelper');
            var config = requireConfig();

            beforeEach(function () {
                browser.ignoreSynchronization = true;
            });

            afterEach(function () {
                browser.ignoreSynchronization = false;
            });

            requireLoginSpec();

            describe('Documents', function () {

                //declarar variaveis necessarias em mais do que um teste
                var menuIcon,
                    newButton,
                    modalWindow,
                    okButton;

                beforeEach(function() {
                            menuIcon = element(by.css('#expenses-module'));
                    newButton = element(by.css('.js-new-expense'));
                    modalWindow = element(by.css('#expense-create-modal'));
                    okButton = modalWindow.element(by.css('.btn-success.ok'));

                });

                it('should open new expense modal window', function () {

                    generalHelper.waitElementClickableAndClick(menuIcon);
                    generalHelper.waitElementClickableAndClick(newButton);
                    generalHelper.waitElementPresent(modalWindow);

                    browser.sleep(1000);

                    expect(modalWindow.isDisplayed()).toBe(true);
                });

                it('should check for empty fields', function () {
                    var typeSelect = modalWindow.element(by.css('button.selectpicker[data-id="expenseType"]'));
                    var typeOption = modalWindow.element(by.css('ul.inner li[data-original-index="1"] a.js-expense-type-option'));
                    generalHelper.waitElementClickableAndClick(typeSelect);
                    generalHelper.waitElementClickableAndClick(typeOption);

                    var categorySelect = modalWindow.element(by.css('button.selectpicker[data-id="expenseSubType"]'));
                    var categoryOption = modalWindow.element(by.css('ul.inner li[data-original-index="' + numSubcat + '"] a.js-expense-sub-type-option'));
                    generalHelper.waitElementClickableAndClick(categorySelect);
                    generalHelper.waitElementClickableAndClick(categoryOption);
                    generalHelper.waitElementClickableAndClick(okButton);

                    var errors = modalWindow.all(by.css('.form-group.error'));

                    expect(errors.count()).toEqual(2);
                });

                it('should test field restrictions', function () {
                    var addVatButton = modalWindow.element(by.css('.js-add-vat'));
                    var vatDropdown = modalWindow.all(by.css('button[data-id="pickVat"]')).get(2);
                    var vatOption = modalWindow.all(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-choose-vat span.text')).get(2);
                    generalHelper.waitElementClickableAndClick(addVatButton);
                    generalHelper.waitElementClickableAndClick(vatDropdown);
                    generalHelper.waitElementClickableAndClick(vatOption);

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('asd');
                    secondRowHT.sendKeys('fgh');
                    thirdRowHT.sendKeys('jkl');
                });

                it('should fill mandatory fields and submit', function() {

                    var employeeSelect = modalWindow.element(by.css('button.selectpicker[data-id="user"]'));
                    var employeeOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-user-option'));
                    generalHelper.waitElementClickableAndClick(employeeSelect);
                    generalHelper.waitElementClickableAndClick(employeeOption);

                    var descriptionInput = modalWindow.element(by.css('#description'));
                    descriptionInput.sendKeys(config.inputs.getDescription(260));

                    if (numPayment != 0) {
                        var paymentSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentMethod"]'));
                        var paymentOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="' + numPayment + '"] a.js-paymethod-option'));
                        generalHelper.waitElementClickableAndClick(paymentSelect);
                        generalHelper.waitElementClickableAndClick(paymentOption);
                    }

                    var accountSelect;
                    var accountOption;

                    if (numPayment == 2) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                    }
                    if (numPayment == 3) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                        var checkNumberInput = modalWindow.element(by.css('#checkNumber'));
                        checkNumberInput.sendKeys("123456789");
                    }

                    var keywordsInput = modalWindow.element(by.css('div.tags-input-box span input.tt-input'));
                    keywordsInput.sendKeys('asdasdasd');

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('100');
                    secondRowHT.sendKeys('200');
                    thirdRowHT.sendKeys('300');

                    browser.executeScript('$(\'input[type="file"]\').removeClass("hidden");');

                    var inputFile = modalWindow.element(by.css('#documentFileUpload'));
                    inputFile.sendKeys('/Volumes/WORK/ui-tests/docs/13_5_2016.pdf');

                    var records = element(by.css('.js-total-res span'));
                    var recordCount = 0;
                    records.getText().then(function(text) {
                        recordCount = parseInt(text);
                    });

                    generalHelper.waitElementClickableAndClick(okButton);
                    generalHelper.waitAlertSuccessPresentAndClose();

                    records.getText().then(function(text) {
                        expect(parseInt(text)).toEqual(++recordCount);
                    });

                });

                it('should return to dashboard', function () {
                    generalHelper.returnToDashboard();
                    browser.refresh();
                });

            });

        });
    },

    createRestaurantSubcats: function (numSubcat, numPayment) {
        describe('Expenses', function () {
            var generalHelper = requireCommonHelper('generalHelper');
            var loginHelper = requireCommonHelper('loginHelper');
            var config = requireConfig();

            beforeEach(function () {
                browser.ignoreSynchronization = true;
            });

            afterEach(function () {
                browser.ignoreSynchronization = false;
            });

            requireLoginSpec();

            describe('Documents', function () {

                //declarar variaveis necessarias em mais do que um teste
                var menuIcon,
                    newButton,
                    modalWindow,
                    okButton;

                beforeEach(function() {
                            menuIcon = element(by.css('#expenses-module'));
                    newButton = element(by.css('.js-new-expense'));
                    modalWindow = element(by.css('#expense-create-modal'));
                    okButton = modalWindow.element(by.css('.btn-success.ok'));
                });

                it('should open new expense modal window', function () {

                    generalHelper.waitElementClickableAndClick(menuIcon);
                    generalHelper.waitElementClickableAndClick(newButton);
                    generalHelper.waitElementPresent(modalWindow);

                    browser.sleep(1000);

                    expect(modalWindow.isDisplayed()).toBe(true);
                });

                it('should check for empty fields', function () {
                    var typeSelect = modalWindow.element(by.css('button.selectpicker[data-id="expenseType"]'));
                    var typeOption = modalWindow.element(by.css('ul.inner li[data-original-index="2"] a.js-expense-type-option'));
                    generalHelper.waitElementClickableAndClick(typeSelect);
                    generalHelper.waitElementClickableAndClick(typeOption);

                    var categorySelect = modalWindow.element(by.css('button.selectpicker[data-id="expenseSubType"]'));
                    var categoryOption = modalWindow.element(by.css('ul.inner li[data-original-index="' + numSubcat + '"] a.js-expense-sub-type-option'));
                    generalHelper.waitElementClickableAndClick(categorySelect);
                    generalHelper.waitElementClickableAndClick(categoryOption);
                    generalHelper.waitElementClickableAndClick(okButton);

                    var errors = modalWindow.all(by.css('.form-group.error'));

                    expect(errors.count()).toEqual(2);
                });

                it('should test field restrictions', function () {
                    var addVatButton = modalWindow.element(by.css('.js-add-vat'));
                    var vatDropdown = modalWindow.all(by.css('button[data-id="pickVat"]')).get(2);
                    var vatOption = modalWindow.all(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-choose-vat span.text')).get(2);
                    generalHelper.waitElementClickableAndClick(addVatButton);
                    generalHelper.waitElementClickableAndClick(vatDropdown);
                    generalHelper.waitElementClickableAndClick(vatOption);

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('asd');
                    secondRowHT.sendKeys('fgh');
                    thirdRowHT.sendKeys('jkl');
                });

                it('should fill mandatory fields and submit', function() {

                    var employeeSelect = modalWindow.element(by.css('button.selectpicker[data-id="user"]'));
                    var employeeOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-user-option'));
                    generalHelper.waitElementClickableAndClick(employeeSelect);
                    generalHelper.waitElementClickableAndClick(employeeOption);

                    var descriptionInput = modalWindow.element(by.css('#description'));
                    descriptionInput.sendKeys(config.inputs.getDescription(260));

                    if (numPayment != 0) {
                        var paymentSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentMethod"]'));
                        var paymentOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="' + numPayment + '"] a.js-paymethod-option'));
                        generalHelper.waitElementClickableAndClick(paymentSelect);
                        generalHelper.waitElementClickableAndClick(paymentOption);
                    }

                    var accountSelect;
                    var accountOption;

                    if (numPayment == 2) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                    }
                    if (numPayment == 3) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                        var checkNumberInput = modalWindow.element(by.css('#checkNumber'));
                        checkNumberInput.sendKeys("123456789");
                    }

                    var keywordsInput = modalWindow.element(by.css('div.tags-input-box span input.tt-input'));
                    keywordsInput.sendKeys('asdasdasd');


                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);

                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();

                    firstRowHT.sendKeys('100');
                    secondRowHT.sendKeys('200');
                    thirdRowHT.sendKeys('300');

                    browser.executeScript('$(\'input[type="file"]\').removeClass("hidden");');

                    var inputFile = modalWindow.element(by.css('#documentFileUpload'));
                    inputFile.sendKeys('/Volumes/WORK/ui-tests/docs/13_5_2016.pdf');

                    var records = element(by.css('.js-total-res span'));
                    var recordCount = 0;
                    records.getText().then(function(text) {
                        recordCount = parseInt(text);
                    });

                    generalHelper.waitElementClickableAndClick(okButton);
                    generalHelper.waitAlertSuccessPresentAndClose();

                    records.getText().then(function(text) {
                        expect(parseInt(text)).toEqual(++recordCount);
                    });

                });

                it('should return to dashboard', function () {
                    generalHelper.returnToDashboard();
                    browser.refresh();
                });

            });

        });
    },

    createTransportSubcats: function (numSubcat, numPayment) {
        describe('Expenses', function () {
            var generalHelper = requireCommonHelper('generalHelper');
            var loginHelper = requireCommonHelper('loginHelper');
            var config = requireConfig();

            beforeEach(function () {
                browser.ignoreSynchronization = true;
            });

            afterEach(function () {
                browser.ignoreSynchronization = false;
            });

            requireLoginSpec();

            describe('Documents', function () {

                //declarar variaveis necessarias em mais do que um teste
                var menuIcon,
                    newButton,
                    modalWindow,
                    okButton;

                beforeEach(function() {
                            menuIcon = element(by.css('#expenses-module'));
                    newButton = element(by.css('.js-new-expense'));
                    modalWindow = element(by.css('#expense-create-modal'));
                    okButton = modalWindow.element(by.css('.btn-success.ok'));
                });

                it('should open new expense modal window', function () {

                    generalHelper.waitElementClickableAndClick(menuIcon);

                    browser.actions()
                        .mouseMove({x: 960, y: 540})
                        .perform();

                    generalHelper.waitElementClickableAndClick(newButton);
                    generalHelper.waitElementPresent(modalWindow);

                    browser.sleep(1000);

                    expect(modalWindow.isDisplayed()).toBe(true);
                });

                it('should check for empty fields', function () {
                    var typeSelect = modalWindow.element(by.css('button.selectpicker[data-id="expenseType"]'));
                    var typeOption = modalWindow.element(by.css('ul.inner li[data-original-index="0"] a.js-expense-type-option'));
                    generalHelper.waitElementClickableAndClick(typeSelect);
                    generalHelper.waitElementClickableAndClick(typeOption);

                    var categorySelect = modalWindow.element(by.css('button.selectpicker[data-id="expenseSubType"]'));
                    var categoryOption = modalWindow.element(by.css('ul.inner li[data-original-index="' + numSubcat + '"] a.js-expense-sub-type-option'));
                    generalHelper.waitElementClickableAndClick(categorySelect);
                    generalHelper.waitElementClickableAndClick(categoryOption);
                    generalHelper.waitElementClickableAndClick(okButton);

                    var errors = modalWindow.all(by.css('.form-group.error'));

                    expect(errors.count()).toEqual(2);
                });

                it('should test field restrictions', function () {
                    var addVatButton = modalWindow.element(by.css('.js-add-vat'));
                    var vatDropdown = modalWindow.all(by.css('button[data-id="pickVat"]')).get(2);
                    var vatOption = modalWindow.all(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-choose-vat span.text')).get(2);
                    generalHelper.waitElementClickableAndClick(addVatButton);
                    generalHelper.waitElementClickableAndClick(vatDropdown);
                    generalHelper.waitElementClickableAndClick(vatOption);

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('asd');
                    secondRowHT.sendKeys('fgh');
                    thirdRowHT.sendKeys('jkl');
                });

                it('should fill mandatory fields and submit', function() {

                    var employeeSelect = modalWindow.element(by.css('button.selectpicker[data-id="user"]'));
                    var employeeOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-user-option'));
                    generalHelper.waitElementClickableAndClick(employeeSelect);
                    generalHelper.waitElementClickableAndClick(employeeOption);

                    var descriptionInput = modalWindow.element(by.css('#description'));
                    descriptionInput.sendKeys(config.inputs.getDescription(260));

                    if (numPayment != 0) {
                        var paymentSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentMethod"]'));
                        var paymentOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="' + numPayment + '"] a.js-paymethod-option'));
                        generalHelper.waitElementClickableAndClick(paymentSelect);
                        generalHelper.waitElementClickableAndClick(paymentOption);
                    }

                    var accountSelect;
                    var accountOption;

                    if (numPayment == 2) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                    }
                    if (numPayment == 3) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                        var checkNumberInput = modalWindow.element(by.css('#checkNumber'));
                        checkNumberInput.sendKeys("123456789");
                    }

                    var keywordsInput = modalWindow.element(by.css('div.tags-input-box span input.tt-input'));
                    keywordsInput.sendKeys('asdasdasd');

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('100');
                    secondRowHT.sendKeys('200');
                    thirdRowHT.sendKeys('300');

                    browser.executeScript('$(\'input[type="file"]\').removeClass("hidden");');

                    var inputFile = modalWindow.element(by.css('#documentFileUpload'));
                    inputFile.sendKeys('/Volumes/WORK/ui-tests/docs/13_5_2016.pdf');

                    var records = element(by.css('.js-total-res span'));
                    var recordCount = 0;
                    records.getText().then(function(text) {
                        recordCount = parseInt(text);
                    });

                    generalHelper.waitElementClickableAndClick(okButton);
                    generalHelper.waitAlertSuccessPresentAndClose();

                    records.getText().then(function(text) {
                        expect(parseInt(text)).toEqual(++recordCount);
                    });
                    
                });

                it('should return to dashboard', function () {
                    generalHelper.returnToDashboard();
                    browser.refresh();
                });

            });

        });
    }
};
