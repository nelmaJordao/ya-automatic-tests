module.exports = {
    edit: function (numType, numPayment) {
        describe('Expenses', function ()
        {
            var generalHelper = requireCommonHelper('generalHelper');
            var loginHelper = requireCommonHelper('loginHelper');
            var config = requireConfig();

            beforeEach(function () {
                browser.ignoreSynchronization = true;
            });

            afterEach(function () {
                browser.ignoreSynchronization = false;
            });

            requireLoginSpec();

            describe('Documents', function ()
            {

                //declarar variaveis necessarias em mais do que um teste
                var menuIcon,
                    tabItem,
                    firstRow,
                    editButton,
                    modalWindow,
                    okButton,
                    closeButton,
                    table,
                    cancelButton;

                beforeEach(function()
                {
                    menuIcon = element(by.css('#expenses-module'));
                    tabItem = element(by.css('#expenses-menu-region [data-sec="documents"]'));
                    firstRow = element.all(by.css('#expenses-dtable tr')).get(0);
                    editButton = firstRow.element(by.css('a.js-edit-expense'));
                    modalWindow = element(by.css('#expense-edit-modal'));
                    okButton = modalWindow.element(by.css('.btn-success.ok'));
                    closeButton = modalWindow.element(by.css('.btn-default.cancel'));
                    table = element.all(by.css('#expenses-dtable tr')).get(0);
                    cancelButton = modalWindow.element(by.css('.btn-default.cancel'));

                });

                it('should open edit documents modal window', function () {

                    generalHelper.waitElementClickableAndClick(menuIcon);

                    browser.sleep(1000);

                    generalHelper.waitElementClickableAndClick(tabItem);

                    browser.sleep(1000);

                    var stateType = element(by.css('button[data-id="expenseTypeFilter"]'));
                    var stateOption = element.all(by.css('li[data-original-index="' + numType + '"]')).get(0);

                    generalHelper.waitElementClickableAndClick(stateType);
                    generalHelper.waitElementClickableAndClick(stateOption);

                    browser.sleep(1000);

                    var paymentType = element(by.css('.dropdown-toggle.selectpicker[data-id="payStateFilter"]'));
                    var paymentOption = element.all(by.css('li[data-original-index="2"]')).get(1);

                    generalHelper.waitElementClickableAndClick(paymentType);
                    generalHelper.waitElementClickableAndClick(paymentOption);

                    browser.sleep(1000);
                    
                    generalHelper.waitElementClickableAndClick(editButton);
                    generalHelper.waitElementPresent(modalWindow);

                    expect(modalWindow.isDisplayed()).toBe(true);
                });

                it('should cancel edit invoice', function () {
                    var cancelButton = element(by.css('button.cancel'));
                    generalHelper.waitElementClickableAndClick(cancelButton);

                    browser.sleep(1000);

                });

                it('should open edit documents modal window', function () {
                    generalHelper.waitElementClickableAndClick(editButton);

                    browser.sleep(1000);

                    generalHelper.waitElementDisplayed(modalWindow);

                    expect(modalWindow.isDisplayed()).toBe(true);
                });

                it('should check for empty fields', function () {

                    var descriptionInput = modalWindow.element(by.css('#description'));
                    descriptionInput.clear();

                    generalHelper.waitElementClickableAndClick(okButton);

                    var errors = modalWindow.all(by.css('.form-group.error'));

                    expect(errors.count()).not.toEqual(0);
                });

                it('should test field restrictions', function () {

                    var vatDropdown = modalWindow.all(by.css('button[data-id="pickVat"]')).get(2);
                    var vatOption = modalWindow.all(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-choose-vat span.text')).get(2);
                    generalHelper.waitElementClickableAndClick(vatDropdown);
                    generalHelper.waitElementClickableAndClick(vatOption);

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('asd');
                    secondRowHT.sendKeys('fgh');
                    thirdRowHT.sendKeys('jkl');
                });

                it('should fill mandatory fields and submit', function() {

                    var employeeSelect = modalWindow.element(by.css('button.selectpicker[data-id="user"]'));
                    var employeeOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="2"] a.js-user-option'));
                    generalHelper.waitElementClickableAndClick(employeeSelect);

                    var employeeName = "";

                    employeeOption.getText().then(function (text) {
                        employeeName = text;
                    });
                    generalHelper.waitElementClickableAndClick(employeeOption);

                    browser.sleep(100);
                    var descriptionInput = modalWindow.element(by.css('#description'));
                    descriptionInput.clear();
                    descriptionInput.sendKeys(config.inputs.getDescription(260));

                    if (numPayment != 0) {
                        var paymentSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentMethod"]'));
                        var paymentOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="' + numPayment + '"] a.js-paymethod-option'));
                        generalHelper.waitElementClickableAndClick(paymentSelect);
                        generalHelper.waitElementClickableAndClick(paymentOption);
                    }

                    var accountSelect;
                    var accountOption;
                    
                    if (numPayment == 2) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                    }
                    if (numPayment == 3) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                        var checkNumberInput = modalWindow.element(by.css('#checkNumber'));
                        checkNumberInput.sendKeys("123456789");
                    }

                    var keywordsInput = modalWindow.element(by.css('div.tags-input-box span input.tt-input'));
                    keywordsInput.sendKeys('asdasdasd');

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('100');
                    secondRowHT.sendKeys('200');
                    thirdRowHT.sendKeys('300');

                    var saveButton = modalWindow.element(by.css('.btn-success.ok'));
                    generalHelper.waitElementClickableAndClick(saveButton);

                    browser.sleep(1000);

                    var resEmployee = table.all(by.css('td')).get(3);
                    resEmployee.getText().then(function (text) {
                        expect(text).toEqual(employeeName);
                    });

                });

                it('should return to dashboard', function () {
                    generalHelper.returnToDashboard();
                    browser.refresh();
                });

            });

        });
    },

    editSubcats: function (numType, numSubcat, numPayment) {
        describe('Expenses', function ()
        {
            var generalHelper = requireCommonHelper('generalHelper');
            var loginHelper = requireCommonHelper('loginHelper');
            var config = requireConfig();

            beforeEach(function () {
                browser.ignoreSynchronization = true;
            });

            afterEach(function () {
                browser.ignoreSynchronization = false;
            });

            requireLoginSpec();

            describe('Documents', function ()
            {

                //declarar variaveis necessarias em mais do que um teste
                var menuIcon,
                    tabItem,
                    firstRow,
                    editButton,
                    modalWindow,
                    okButton,
                    closeButton,
                    cancelButton;

                beforeEach(function()
                {
                    menuIcon = element(by.css('#expenses-module'));
                    tabItem = element(by.css('#expenses-menu-region [data-sec="documents"]'));
                    firstRow = element.all(by.css('#expenses-dtable tr')).get(0);
                    editButton = firstRow.element(by.css('a.js-edit-expense'));
                    modalWindow = element(by.css('#expense-edit-modal'));
                    okButton = modalWindow.element(by.css('.btn-success.ok'));
                    closeButton = modalWindow.element(by.css('.btn-default.cancel'));
                    cancelButton = modalWindow.element(by.css('.btn-default.cancel'));
                });

                it('should open edit documents modal window', function () {

                    generalHelper.waitElementClickableAndClick(menuIcon);
                    generalHelper.waitElementClickableAndClick(tabItem);

                    browser.sleep(1000);

                    var stateType = element(by.css('button[data-id="expenseTypeFilter"]'));
                    var stateOption = element(by.css('li[data-original-index="' + numType + '"]'));

                    generalHelper.waitElementClickableAndClick(stateType);
                    generalHelper.waitElementClickableAndClick(stateOption);

                    browser.sleep(1000);

                    generalHelper.waitElementClickableAndClick(editButton);
                    generalHelper.waitElementPresent(modalWindow);

                    expect(modalWindow.isDisplayed()).toBe(true);
                });

                it('should cancel edit invoice', function () {
                    var cancelButton = element(by.css('button.cancel'));
                    generalHelper.waitElementClickableAndClick(cancelButton);

                    browser.sleep(1000);
                });

                it('should open edit documents modal window', function () {
                    generalHelper.waitElementClickableAndClick(editButton);
                    generalHelper.waitElementPresent(modalWindow);

                    expect(modalWindow.isDisplayed()).toBe(true);
                });

                it('should check for empty fields', function () {
                    browser.sleep(500);
                    var descriptionInput = modalWindow.element(by.css('#description'));
                    descriptionInput.clear();

                    var categorySelect = modalWindow.element(by.css('button.selectpicker[data-id="expenseSubType"]'));
                    var categoryOption = modalWindow.element(by.css('ul.inner li[data-original-index="' + numSubcat + '"] a.js-expense-sub-type-option'));
                    generalHelper.waitElementClickableAndClick(categorySelect);
                    generalHelper.waitElementClickableAndClick(categoryOption);

                    generalHelper.waitElementClickableAndClick(okButton);

                    var errors = modalWindow.all(by.css('.form-group.error'));

                    expect(errors.count()).not.toEqual(0);
                });

                it('should test field restrictions', function () {
                    var vatDropdown = modalWindow.all(by.css('button[data-id="pickVat"]')).get(2);
                    var vatOption = modalWindow.all(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-choose-vat span.text')).get(2);

                    generalHelper.waitElementClickableAndClick(vatDropdown);
                    generalHelper.waitElementClickableAndClick(vatOption);

                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();
                    firstRowHT.sendKeys('asd');
                    secondRowHT.sendKeys('fgh');
                    thirdRowHT.sendKeys('jkl');
                });

                it('should fill mandatory fields and submit', function() {
                    var employeeSelect = modalWindow.element(by.css('button.selectpicker[data-id="user"]'));
                    var employeeOption = modalWindow.all(by.css('li[data-original-index="1"]')).get(2);
                    generalHelper.waitElementClickableAndClick(employeeSelect);
                    generalHelper.waitElementClickableAndClick(employeeOption);

                    var employeeName = "";

                    employeeOption.element(by.css('span.text')).getText().then(function (text) {
                        employeeName = text;
                    });
                    generalHelper.waitElementClickableAndClick(employeeOption);

                    var descriptionInput = modalWindow.element(by.css('#description'));
                    descriptionInput.clear();
                    descriptionInput.sendKeys(config.inputs.getDescription(260));

                    if (numPayment != 0) {
                        var paymentSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentMethod"]'));
                        var paymentOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="' + numPayment + '"] a.js-paymethod-option'));
                        generalHelper.waitElementClickableAndClick(paymentSelect);
                        generalHelper.waitElementClickableAndClick(paymentOption);
                    }

                    var accountSelect;
                    var accountOption;

                    if (numPayment == 2) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                    }
                    if (numPayment == 3) {
                        accountSelect = modalWindow.element(by.css('button.selectpicker[data-id="paymentBank"]'));
                        accountOption = modalWindow.element(by.css('div.btn-group.bootstrap-select.form-control.open div.dropdown-menu.open ul.dropdown-menu.inner.selectpicker li[data-original-index="1"] a.js-paymentBank-option'));
                        generalHelper.waitElementClickableAndClick(accountSelect);
                        generalHelper.waitElementClickableAndClick(accountOption);
                        var checkNumberInput = modalWindow.element(by.css('#checkNumber'));
                        checkNumberInput.sendKeys("123456789");
                    }

                    var keywordsInput = modalWindow.element(by.css('div.tags-input-box span input.tt-input'));
                    keywordsInput.sendKeys('asdasdasd');


                    var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
                    var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
                    var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);

                    firstRowHT.clear();
                    secondRowHT.clear();
                    thirdRowHT.clear();

                    firstRowHT.sendKeys('100');
                    secondRowHT.sendKeys('200');
                    thirdRowHT.sendKeys('300');

                    var saveButton = modalWindow.element(by.css('.btn-success.ok'));

                    generalHelper.waitElementClickableAndClick(saveButton);
                    generalHelper.waitAlertSuccessPresentAndClose();

                    var resEmployee = firstRow.all(by.css('td')).get(3);

                    resEmployee.getText().then(function (text) {
                        expect(text).toEqual(employeeName);
                    });

                });

                it('should return to dashboard', function () {
                    generalHelper.returnToDashboard();
                    browser.refresh();
                });

            });

        });
    }
};
