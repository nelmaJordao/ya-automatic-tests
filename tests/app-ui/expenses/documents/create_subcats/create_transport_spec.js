describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var createHelper = requireCreateExpenseHelper();

    for(var i = 0; i < 10; i += 4){
        createHelper.createTransportSubcats(i, 0);
        createHelper.createTransportSubcats(i + 1, 1);
        createHelper.createTransportSubcats(i + 2, 2);
        createHelper.createTransportSubcats(i + 3, 3);
    }
});