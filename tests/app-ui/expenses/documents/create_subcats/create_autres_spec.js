describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var createHelper = requireCreateExpenseHelper();
    
    for(var i = 0; i < 28; i += 4){
        createHelper.createAutreSubcats(i, 0);
        createHelper.createAutreSubcats(i + 1, 1);
        createHelper.createAutreSubcats(i + 2, 2);
        createHelper.createAutreSubcats(i + 3, 3);
    }
    createHelper.createAutreSubcats(28, 0);
    
});