describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            modalWindow,
            validationDocuments,
            firstRow;

        beforeEach(function() {
            menuIcon = element(by.css('#expenses-module'));
            modalWindow = element(by.css('#document-tagging-edit-document-modal'));
            validationDocuments = element(by.css('.js-menu .js-menu-btn[data-sec="documentstovalidate"]'));
            firstRow = element.all(by.css('#docs-to-validate-dtable tr')).get(0);
        });

        it('should open tab validation documents and show family modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(validationDocuments);

            browser.sleep(1000);

            var editButton = firstRow.element(by.css('.options .js-edit-doc-to-validate'));
            generalHelper.waitElementClickableAndClick(editButton);

            browser.sleep(1000);

        });

        it('should select crop and select two areas to crop and save', function () {

            var cropButton = modalWindow.element(by.css('.js-crop-image'));
            generalHelper.waitElementClickableAndClick(cropButton);

            browser.sleep(1000);

            var document = modalWindow.element(by.css('.js-page-preview'));

            generalHelper.selectArea(document,{x: -100, y: 100},{x: -400, y: -300});

            generalHelper.selectArea(document,{x: 100, y: 100},{x: 400, y: -300});

            var cropButton = modalWindow.element(by.css('.js-viewer-cropping-close'));
            generalHelper.waitElementClickableAndClick(cropButton);


            var confimationCropModal = element(by.css('#document-tagging-crop-document-modal'));
            expect(confimationCropModal.isDisplayed()).toBe(true);

            var saveCropButton = confimationCropModal.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveCropButton);

            generalHelper.waitAlertSuccessPresentAndClose();

            browser.sleep(1000);

        });

        it('should tag crop documents', function() {

            browser.sleep(2000);

            var firstEditButton = firstRow.element(by.css('.js-edit-doc-to-validate'));
            generalHelper.waitElementClickableAndClick(firstEditButton);

            browser.sleep(1000);

            var docTypeSelect = modalWindow.element(by.css('button.selectpicker[data-id="docType"]'));
            var docTypeOption = modalWindow.element(by.css('li[data-original-index="2"] a.js-doctype-option'));
            generalHelper.waitElementClickableAndClick(docTypeSelect);
            generalHelper.waitElementClickableAndClick(docTypeOption);

            var emplyoeeTypeSelect = modalWindow.element(by.css('.dropdown-toggle[data-id="user"]'));
            generalHelper.waitElementClickableAndClick(emplyoeeTypeSelect);

            var employeeTypeOption = modalWindow.all(by.css('.dropdown-menu li[data-original-index="1"]')).get(2);
            generalHelper.waitElementClickableAndClick(employeeTypeOption);

            browser.sleep(1000);

            var dateField = modalWindow.element(by.css('#calfield-documentDate'));
            dateField.clear();

            browser.sleep(1000);

            dateField.sendKeys(24-10-2016);

            browser.sleep(1000);

            var saveButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(2000);

        });


        it('confirm document on account employee', function() {

            loginHelper.logout();

            browser.sleep(1000);

            loginHelper.login(config.users.TESTPEDRO);

            browser.refresh();

            browser.sleep(2000);

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(validationDocuments);

            browser.sleep(1000);

            var closeButton = modalWindow.element(by.css('.close'));
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            var showFirstDocument = element(by.css('.options .js-show-doc-to-validate'));
            generalHelper.waitElementClickableAndClick(showFirstDocument);

            browser.sleep(2300);

            loginHelper.logout();

            browser.sleep(1000);

            loginHelper.login(config.users.TESTPOLO);

            browser.sleep(1000);


        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});

