describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            firstRow,
            showButton,
            modalWindow,
            editModal,
            deleteModal;

        beforeEach(function() {
            menuIcon = element(by.css('#expenses-module'));
            firstRow = element.all(by.css('#expenses-dtable tr')).get(0);
            showButton = firstRow.element(by.css('.options .js-show-expense'));
            modalWindow = element(by.css('#expense-show-modal'));
            editModal = element(by.css('#expense-edit-modal'));
            deleteModal = element(by.css('#expense-delete-modal'));
        });

        it('should open show family modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should close modal window', function() {

            var closeButton = modalWindow.element(by.css('.btn-default'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);

        });

        it('should open show modal window', function () {

            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should open edit modal window', function () {
            
            var crudEdit = modalWindow.all(by.css('.crud [data-trigger="expenses:edit:expense"]'));

            if(crudEdit.length > 0) {
                generalHelper.waitElementClickableAndClick(crudEdit);
            }

            browser.sleep(1000);

        });

        it('should open show modal window again', function () {

            var crudShow = editModal.all(by.css('.crud [data-trigger="expenses:show:expense"]'));

            if(crudShow.length > 0 ) {
                generalHelper.waitElementClickableAndClick(crudShow);
            }

        });


        it('should open delete modal window', function() {

            var crudDelete = modalWindow.all(by.css('.crud [data-trigger="expenses:delete:expense"]'));

            if(crudDelete.length > 0) {
                generalHelper.waitElementClickableAndClick(crudDelete);


                expect(deleteModal.isDisplayed()).toBe(true);


                var cancelButton = deleteModal.element(by.css('.btn-default.cancel'));
                generalHelper.waitElementClickableAndClick(cancelButton);

            }
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});