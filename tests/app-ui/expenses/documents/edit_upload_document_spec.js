describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Expenses', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            modalWindow;

        beforeEach(function() {
            menuIcon = element(by.css('#expenses-module'));
            modalWindow = element(by.css('#expense-edit-modal'));

            browser.sleep(1000);

        });

        it('should open expenses modal', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            var editFirstExpense = element.all(by.css('#expenses-dtable .js-edit-expense')).get(0);
            generalHelper.waitElementClickableAndClick(editFirstExpense);

        });

        it('should fill mandatory fields and submit', function() {

            var calendar = modalWindow.element(by.css('#expense-date'));
            calendar.clear();
            calendar.sendKeys('20-10-2016');

            var descriptionField = modalWindow.element(by.css('#description'));
            descriptionField.clear();
            descriptionField.sendKeys(config.inputs.getDescription(100));

            browser.sleep(2000);

            var deleteDocumentButton = modalWindow.element(by.css('.js-see-or-remove-upload .js-remove-document'));
            generalHelper.waitElementClickableAndClick(deleteDocumentButton);

            browser.sleep(3000);

            browser.executeScript('$(\'input[type="file"]\').removeClass("hidden");');

            var inputFile = modalWindow.element(by.css('#documentFileUpload'));
            inputFile.sendKeys('/Volumes/WORK/ui-tests/docs/document.pdf');

            var saveButton = modalWindow.element(by.css('.ladda-button.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);
        });

        it('should open the last expense saved', function () {

            var sortByDate = element(by.css('.js-sort[data-field="date"]'))
            generalHelper.waitElementClickableAndClick(sortByDate);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(sortByDate);

            browser.sleep(1000);

            var showFirstExpense = element.all(by.css('#expenses-dtable .js-show-expense')).get(0);
            generalHelper.waitElementClickableAndClick(showFirstExpense);

            browser.sleep(3000);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});
