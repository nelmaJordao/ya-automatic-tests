describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            exportButton;


        beforeEach(function() {
            menuIcon = element(by.css('#expenses-module'));
            exportButton = element(by.css('button.js-list-export'));
        });

        it('should export', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(exportButton);

            browser.sleep(1000);

            expect(element(by.css('#iframe-download')).isPresent()).toBe(true);

            });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});