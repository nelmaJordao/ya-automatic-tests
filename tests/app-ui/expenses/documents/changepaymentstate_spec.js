describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            firstRow,
            modalWindow;

        beforeEach(function() {
            menuIcon = element(by.css('#expenses-module'));
            modalWindow = element(by.css('#expense-set-paid-state-modal'));
            firstRow = element.all(by.css('#expenses-dtable tr')).first();
        });

        it('should open change payment state window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            var stateButton = firstRow.element(by.css('.options .js-set-payed-state-expense'));
            generalHelper.waitElementClickableAndClick(stateButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should confirm change payment state modal window', function() {
            var okButton = modalWindow.element(by.css('.btn-success.ok'));
            var statusSpan = firstRow.element(by.css('td.nobreak span.label'));
            var previousText;

            previousText = statusSpan.getText();

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            if(previousText == 'NON RÉGLÉ'){
                expect(statusSpan.getText()).toEqual('RÉGLÉ');
            }
            else if(previousText == 'RÉGLÉ'){
                expect(statusSpan.getText()).toEqual('NON RÉGLÉ');
            }

            browser.sleep(1000);

        });

        it('should open change payment state window', function () {

            var stateButton = firstRow.element(by.css('.js-set-payed-state-expense'));
            generalHelper.waitElementClickableAndClick(stateButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should change payment state back', function() {
            var okButton = modalWindow.element(by.css('.btn-success.ok'));
            var statusSpan = firstRow.element(by.css('td.nobreak span.label'));
            var previousText;

            previousText = statusSpan.getText();

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            if(previousText == 'NON RÉGLÉ'){
                expect(statusSpan.getText()).toEqual('RÉGLÉ');
            }
            else if(previousText == 'RÉGLÉ'){
                expect(statusSpan.getText()).toEqual('NON RÉGLÉ');
            }

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});