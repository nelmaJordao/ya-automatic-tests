describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            newButton,
            modalWindow,
            okButton,
            inputKms;

        beforeEach(function() {
            menuIcon = element(by.css('#expenses-module'));
            newButton = element(by.css('.js-new-expense'));
            modalWindow = element(by.css('#expense-create-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            inputKms = modalWindow.element(by.css('#totalKilometers'));
        });

        it('should open new expense modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(1000);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {
            generalHelper.waitElementClickableAndClick(okButton);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(1);
        });

        it('should test field restrictions', function () {

            generalHelper.waitElementClickableAndClick(okButton);
            inputKms.sendKeys('1258');

            expect(inputKms.getText()).not.toEqual('asd');

            inputKms.clear();
        });

        it('should fill mandatory fields and submit', function() {

            var numKms = Math.floor(Math.random() * 500);
            inputKms.clear();
            inputKms.sendKeys(numKms);

            var descriptionInput = modalWindow.element(by.css('#description'));
            descriptionInput.sendKeys('Test');

            var startLocationInput = modalWindow.element(by.css('#startLocation'));
            startLocationInput.sendKeys('Test Town');

            var endLocationInput = modalWindow.element(by.css('#endLocation'));
            endLocationInput.sendKeys('Some Other Town');

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });

            generalHelper.returnToDashboard();

        });

    });

});