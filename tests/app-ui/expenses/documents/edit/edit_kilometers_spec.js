describe('Expenses', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function ()
    {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            editButton,
            modalWindow,
            okButton,
            closeButton,
            kilometres,
            description,
            cancelButton;

        beforeEach(function()
        {
            menuIcon = element(by.css('#expenses-module'));
            tabItem = element(by.css('#expenses-menu-region [data-sec="documents"]'));
            modalWindow = element(by.css('#expense-edit-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            kilometres = modalWindow.element(by.css('#totalKilometers'));
            description = modalWindow.element(by.css('#description'));
            cancelButton = modalWindow.element(by.css('.btn-default.cancel'));

        });

        it('should open edit documents modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementPresent(tabItem);

            var filter = element(by.css('button.selectpicker[data-id="expenseTypeFilter"'));
            var filterOption = element(by.css('div.dropdown-menu.open ul.selectpicker li[data-original-index="2"]'));

            generalHelper.waitElementClickableAndClick(filter);
            generalHelper.waitElementClickableAndClick(filterOption);

            browser.sleep(1000);

            firstRow = element.all(by.css('#expenses-dtable tr')).get(1);
            editButton = firstRow.element(by.css('a.js-edit-expense'));

            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should cancel edit expense', function () {
            var cancelButton = element(by.css('button.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            browser.sleep(1000);
        });

        it('should open edit documents modal window', function () {
            firstRow = element.all(by.css('#expenses-dtable tr')).get(1);
            editButton = firstRow.element(by.css('a.js-edit-expense'));

            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function ()
        {

            browser.sleep(2000);
            
            //limpar campos
            kilometres.clear();
            description.clear();

            generalHelper.waitElementClickableAndClick(okButton);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(2);

        });

        it('should close and reopen edit documents modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

        });

        it('should fill mandatory fields and submit', function()
        {
            browser.sleep(2000);

            //editar quilometros
            kilometres.clear();
            kilometres.sendKeys('500');

            //editar descrição
            description.clear();
            description.sendKeys('Teste1');

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            generalHelper.returnToDashboard();

        });

    });

});