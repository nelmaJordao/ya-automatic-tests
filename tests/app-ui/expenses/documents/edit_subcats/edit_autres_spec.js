describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var editHelper = requireEditExpenseHelper();
    
    for(var i = 0; i < 28;i += 4){
        editHelper.editSubcats(5, i, 0);
        editHelper.editSubcats(5, i + 1, 1);
        editHelper.editSubcats(5, i + 2, 2);
        editHelper.editSubcats(5, i + 3, 3);
    }
    editHelper.editSubcats(5, 28, 0);
    
});