describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var editHelper = requireEditExpenseHelper();

    for(var i = 0; i < 10; i += 4){
        editHelper.editSubcats(4, i, 0);
        editHelper.editSubcats(4, i + 1, 1);
        editHelper.editSubcats(4, i + 2, 2);
        editHelper.editSubcats(4, i + 3, 3);
    }

});