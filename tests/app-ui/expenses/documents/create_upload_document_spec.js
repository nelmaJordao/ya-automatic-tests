describe('Expenses', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Expenses', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            newExpenseButton,
            modalWindow;

        beforeEach(function() {
            menuIcon = element(by.css('#expenses-module'));
            modalWindow = element(by.css('#expense-create-modal'));
            newExpenseButton = element(by.css('.js-new-expense'));

            browser.sleep(1000);

        });

        it('should open expenses modal', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(newExpenseButton);

        });

        it('should fill mandatory fields and submit', function() {

            var expenseTypeButton = modalWindow.all(by.css('.dropdown-toggle.selectpicker')).get(0);
            generalHelper.waitElementClickableAndClick(expenseTypeButton);

            var expenseTypeOption = modalWindow.all(by.css('.dropdown-menu li[data-original-index="2"]')).get(0);
            generalHelper.waitElementClickableAndClick(expenseTypeOption);

            var calendar = modalWindow.element(by.css('#expense-date'));
            calendar.sendKeys('20-10-2016');

            var employeeTypeButton = modalWindow.all(by.css('.dropdown-toggle.selectpicker')).get(2);
            generalHelper.waitElementClickableAndClick(employeeTypeButton);

            var employeeTypeOption = modalWindow.all(by.css('.dropdown-menu li[data-original-index="1"]')).get(2);
            generalHelper.waitElementClickableAndClick(employeeTypeOption);

            var descriptionField = modalWindow.element(by.css('#description'));
            descriptionField.sendKeys(config.inputs.getDescription(200));


            browser.executeScript('$(\'input[type="file"]\').removeClass("hidden");');

            var inputFile = modalWindow.element(by.css('#documentFileUpload'));
            inputFile.sendKeys('/Volumes/WORK/ui-tests/docs/13_5_2016.pdf');

            var keywordsInput = modalWindow.element(by.css('div.tags-input-box span input.tt-input'));
            keywordsInput.sendKeys('xpto');

            var firstRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
            var secondRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
            firstRowHT.clear();
            secondRowHT.clear();
            firstRowHT.sendKeys('100');
            secondRowHT.sendKeys('200');

            var saveButton = modalWindow.element(by.css('.ladda-button.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);
        });

        it('should open the last expense saved', function () {

            var sortByDate = element(by.css('.js-sort[data-field="date"]'))
            generalHelper.waitElementClickableAndClick(sortByDate);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(sortByDate);

            browser.sleep(1000);

            var showFirstExpense = element.all(by.css('#expenses-dtable .js-show-expense')).get(0);
            generalHelper.waitElementClickableAndClick(showFirstExpense);

            browser.sleep(3000);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});
