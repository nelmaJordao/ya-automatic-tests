describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var 
            menuIcon;

        beforeEach(function() {
            menuIcon = element(by.css('#services-module'));

        });

        it('should click on banners and open a new browser tab', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(10000);

            var bannersTab = element(by.css('.js-menu .js-menu-btn[data-sec="banners"]'));
            generalHelper.waitElementClickableAndClick(bannersTab);

            browser.sleep(3000);

            var demoBottomBar = element(by.css('.services-a4u a[href="http://www.yesaccount.fr"]'));
            generalHelper.waitElementClickableAndClick(demoBottomBar);

            browser.sleep(15000);

        });

    });

});