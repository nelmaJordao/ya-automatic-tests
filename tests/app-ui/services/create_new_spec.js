describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            modalWindow;

        beforeEach(function() {
            menuIcon = element(by.css('#services-module'));
            modalWindow = element(by.css('#services-create-campaign-modal'));

        });

        it('should click on services and initialize the creation of a new campaign', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(10000);

            var newCampaignButton = element(by.css('.js-new-campaign'));
            generalHelper.waitElementClickableAndClick(newCampaignButton);

            browser.sleep(1000);

        });

        it('should fill form campagne', function(){

            var logo = modalWindow.element(by.css('#campaignPicture'));

            browser.executeScript('$(\'input[type="file"]\').removeClass("hidden");');

            logo.sendKeys('/Volumes/WORK/ui-tests/docs/Screen Shot 2016-09-23 at 12.54.13.png');

            browser.sleep(2000);

            var campaignTitleField = modalWindow.element(by.css('#campaignTitle'));
            campaignTitleField.sendKeys('Announcement');

            var lineOneField = modalWindow.element(by.css('#campaignHighlight1'));
            lineOneField.sendKeys('First line of Campaign!');

            var lineTwoField = modalWindow.element(by.css('#campaignHighlight2'));
            lineTwoField.sendKeys('Second Line of Campaign!');

            var emailField = modalWindow.element(by.css('#campaignServiceUrl'));
            emailField.sendKeys('www.XptoXpto.biz');

            var descriptionField = modalWindow.element(by.css('#campaignDescription'));
            descriptionField.sendKeys(config.inputs.getDescription(500));

            browser.sleep(2000);
        });

        it('should fill the target form', function(){

            var tagerTab = modalWindow.element(by.css('.nav #link-target-region'));
            generalHelper.waitElementClickableAndClick(tagerTab);

            var defineTarget = modalWindow.element(by.css('.js-radio-region'));
            generalHelper.waitElementClickableAndClick(defineTarget);

            var regionButton = modalWindow.element(by.css('.dropdown-toggle.selectpicker'));
            generalHelper.waitElementClickableAndClick(regionButton);

            var chooseRegionOption = modalWindow.all(by.css('.dropdown-menu li')).get(2);
            generalHelper.waitElementClickableAndClick(chooseRegionOption);

            var locationRadioButton = modalWindow.element(by.css('.js-radio-location'));
            generalHelper.waitElementClickableAndClick(locationRadioButton);

            var inputLocation = modalWindow.element(by.css('#city'));
            inputLocation.clear();
            inputLocation.sendKeys('Agen');

            var rangeField = modalWindow.element(by.css('#rangeKm'));
            rangeField.clear();
            rangeField.sendKeys('100');

            browser.sleep(1000);

            inputLocation.clear();
            inputLocation.sendKeys('Nantes');

            rangeField.clear();
            rangeField.sendKeys('25');

            browser.sleep(2000);

        });

        it('should fill the Options form', function(){

            var optionsTab = modalWindow.element(by.css('.nav li[data-name="options"]'));
            generalHelper.waitElementClickableAndClick(optionsTab);

            var durationRadioButton = modalWindow.all(by.css('.table-striped label')).get(1);
            generalHelper.waitElementClickableAndClick(durationRadioButton);

            browser.sleep(1000);

            var campaignTypeRegularPage = modalWindow.element(by.css('#regularLogin'));
            var campaignTypeRegularApp = modalWindow.element(by.css('#regularMod'));

            generalHelper.waitElementClickableAndClick(campaignTypeRegularPage);
            generalHelper.waitElementClickableAndClick(campaignTypeRegularApp);

            browser.sleep(1000);

            var campaignTypePremiumPage = modalWindow.element(by.css('#premiumLogin'));
            var campaignTypeDashPremium = modalWindow.element(by.css('#premiumDash'));

            generalHelper.waitElementClickableAndClick(campaignTypePremiumPage);
            generalHelper.waitElementClickableAndClick(campaignTypeDashPremium);

            browser.sleep(1000);

        });

        it('should fill the Options form', function(){

            var optionsTab = modalWindow.element(by.css('.nav li[data-name="payment"]'));
            generalHelper.waitElementClickableAndClick(optionsTab);

            browser.sleep(2000);

        });

        it('should save', function(){
            var saveButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(1000);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});