describe('dashboard', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('should jump from tab to tab', function () {

        it('should open bank module', function ()
        {
            var recivedButton = element(by.css('.js-table-switch[data-table="received"]'));
            var EmittedButton = element(by.css('.js-table-switch[data-table="emitted"]'));
            var salesButton = element(by.css('.js-table-switch[data-table="sales"]'));
            var debtsButton = element(by.css('.js-table-switch[data-table="debts"]'));
            var overDueButton = element(by.css('.js-table-switch[data-table="overdue"]'));
            var graphButton = element(by.css('.js-graph-paging.active'));


            for (var i = 0; i < 3; i++) {

                generalHelper.waitElementClickableAndClick(recivedButton);

                browser.sleep(1000);

                generalHelper.waitElementClickableAndClick(debtsButton);

                browser.sleep(1000);

                generalHelper.waitElementClickableAndClick(overDueButton);

                browser.sleep(1000);

                generalHelper.waitElementClickableAndClick(graphButton);

                browser.sleep(1500);

                generalHelper.waitElementClickableAndClick(EmittedButton);

                browser.sleep(1000);

                generalHelper.waitElementClickableAndClick(salesButton);

                browser.sleep(1000);

            }
        });

    });

});