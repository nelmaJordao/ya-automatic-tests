describe('Dashboard', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Create Devi', function () {

        //declarar variaveis necessarias em mais do que um teste
        var tabItemQuotes,
            menuIcon,
            newQuote,
            modalWindow,
            okButtonQuote,
            quoteIssueDate,
            quotesCount;


        beforeEach(function() {
            tabItemQuotes = element(by.css('#invoicing-menu-region [data-sec="quotes"]'));
            menuIcon = element(by.css('#invoicing-module'));
            newQuote = element(by.css('.js-new-quote-shortcut'));
            modalWindow = element(by.css('#invoicing-create-quote-modal'));
            quoteIssueDate = modalWindow.element(by.css('#issuedDate'));
            okButtonQuote = modalWindow.element(by.css('.btn-success.ok'));
        });


        it('should check number of quotes', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItemQuotes);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            quotesCount = 0;
            records.getText().then(function(text) {
                quotesCount = parseInt(text);
            });
        });


        it('should open new quote modal', function () {

            generalHelper.returnToDashboard();

            browser.sleep(1000);

            var showDashboardArea = element(by.css('.knob .js-show-dashboard'));

            browser.actions()
                .mouseMove(showDashboardArea)
                .perform();

            browser.sleep(1000);

            var ventesArea = element(by.css('.js-app-menu-item[href="#invoicing/invoices"]'))
            browser.actions()
                .mouseMove(ventesArea)
                .perform();

            var newQuoteShort = element(by.css('.js-new-quote-shortcut'));
            generalHelper.waitElementClickableAndClick(newQuoteShort);

            browser.sleep(1000);

        });


        it('should check for empty fields', function () {

            generalHelper.waitElementClickableAndClick(okButtonQuote);

            generalHelper.waitElementPresent(quoteIssueDate); //espera que a data seja preenchida para não dar erro de data

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).not.toEqual(0); //client and items are mandatory -> 2
        });

        it('should fill mandatory fields and submit', function() {
            var customerName = 'bip';

            //Selecionar um item já existente na lista

            var itemDiv = element(by.css('#invoicing-quote-form-items'));

            var addItem = modalWindow.element(by.css('.js-add-item'));

            generalHelper.waitElementClickableAndClick(addItem);

            browser.sleep(1000);

            generalHelper.selectTypeAheadItem(itemDiv, '.js-item.tt-input', 'A', 1);

            browser.sleep(1000);


            //Editar os números desse item

            var quantityField = itemDiv.element(by.css('.js-item-quantity'));

            quantityField.sendKeys('1,50');

            var discountField = itemDiv.element(by.css('.js-item-discount'));
            discountField.clear();
            discountField.sendKeys('40');


            //Adicionar linha em branco
            var addLine = modalWindow.element(by.css('.js-add-blank'));

            generalHelper.waitElementClickableAndClick(addLine);

            browser.sleep(1000);


            //Adicionar um comentário
            var addComment = modalWindow.element(by.css('.js-add-comment'));

            generalHelper.waitElementClickableAndClick(addComment);

            browser.sleep(1000);

            var writeComment = modalWindow.element(by.css('.js-line-observations'));

            writeComment.sendKeys("Comentário!");


            //Adicionar linha em branco

            generalHelper.waitElementClickableAndClick(addLine);


            //Criar um item novo

            generalHelper.waitElementClickableAndClick(addItem);

            browser.sleep(1000);

            generalHelper.selectTypeAheadCreateNew(itemDiv, "input.tt-input");

            var newArticle = element(by.css('#invoicing-create-item-modal'));

            //select family category
            var selectpickerFamilyCategory = newArticle.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = newArticle.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);

            //code
            var codeInput = newArticle.element(by.css('#code'));
            codeInput.sendKeys(config.inputs.getRandomName(10));

            //description
            var descriptionTextarea = newArticle.element(by.css('#description'));
            descriptionTextarea.sendKeys(config.inputs.getDescription(1000));

            //price
            browser.executeScript('$("#invoicing-create-item-modal #price").val(' + (Math.floor(Math.random() * 1000) + 1) + ')');
            browser.executeScript('$("#invoicing-create-item-modal #price").blur()');

            //select family category
            var selectpickerItemVat = newArticle.element(by.css('[data-id="itemVat"]'));
            var selectpickerItemVatOption = newArticle.all(by.css('.dropdown-menu.open')).get(2).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemVat);
            generalHelper.waitElementClickableAndClick(selectpickerItemVatOption);

            //select item family
            var selectpickerItemFamily = newArticle.element(by.css('[data-id="itemFamily"]'));
            var selectpickerItemFamilyOption = newArticle.all(by.css('.dropdown-menu.open')).get(1).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemFamily);
            generalHelper.waitElementClickableAndClick(selectpickerItemFamilyOption);

            browser.sleep(1000);

            var newFamilyArticle = element(by.css('#invoicing-create-family-modal'));

            //select family category
            var selectpickerFamilyCategory = newFamilyArticle.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = newFamilyArticle.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);


            var shortDescription = newFamilyArticle.element(by.css('#shortDescription'));
            shortDescription.sendKeys("teste");

            var description = newFamilyArticle.element(by.css('#description'));
            description.sendKeys("Teste1");


            var okButtonFamily = newFamilyArticle.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButtonFamily);
            generalHelper.waitAlertSuccessPresentAndClose();

            var okButtonArticle = newArticle.element(by.css('.btn-success.ok'));

            browser.executeScript('$(".btn-success.ok.hidden").removeClass("hidden")');

            generalHelper.waitElementClickableAndClick(okButtonArticle);

            generalHelper.waitAlertSuccessPresentAndClose();

            generalHelper.waitElementClickableAndClick(addItem);

            browser.sleep(1000);

            generalHelper.selectTypeAheadCreateNew(itemDiv, "input.tt-input");

            var newArticle = element(by.css('#invoicing-create-item-modal'));

            //select family category
            var selectpickerFamilyCategory = newArticle.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = newArticle.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="2"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);

            //code
            var codeInput = newArticle.element(by.css('#code'));
            codeInput.sendKeys(config.inputs.getRandomName(10));

            //description
            var descriptionTextarea = newArticle.element(by.css('#description'));
            descriptionTextarea.sendKeys(config.inputs.getDescription(1000));

            //price
            browser.executeScript('$("#invoicing-create-item-modal #price").val(' + (Math.floor(Math.random() * 1000) + 1) + ')');
            browser.executeScript('$("#invoicing-create-item-modal #price").blur()');

            //select family category
            var selectpickerItemVat = newArticle.element(by.css('[data-id="itemVat"]'));
            var selectpickerItemVatOption = newArticle.all(by.css('.dropdown-menu.open')).get(2).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemVat);
            generalHelper.waitElementClickableAndClick(selectpickerItemVatOption);

            //select item family
            var selectpickerItemFamily = newArticle.element(by.css('[data-id="itemFamily"]'));
            var selectpickerItemFamilyOption = newArticle.all(by.css('.dropdown-menu.open')).get(1).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemFamily);
            generalHelper.waitElementClickableAndClick(selectpickerItemFamilyOption);

            browser.sleep(1000);

            var newFamilyArticle = element(by.css('#invoicing-create-family-modal'));

            //select family category
            var selectpickerFamilyCategory = newFamilyArticle.element(by.css('[data-id="familyCategory"]'));
            var selectpickerFamilyCategoryOption = newFamilyArticle.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="2"]'));
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategory);
            generalHelper.waitElementClickableAndClick(selectpickerFamilyCategoryOption);


            var shortDescription = newFamilyArticle.element(by.css('#shortDescription'));
            shortDescription.sendKeys("teste");

            var description = newFamilyArticle.element(by.css('#description'));
            description.sendKeys("Teste1");


            var okButtonFamily = newFamilyArticle.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButtonFamily);
            generalHelper.waitAlertSuccessPresentAndClose();

            var okButtonArticle = newArticle.element(by.css('.btn-success.ok'));

            browser.executeScript('$(".btn-success.ok.hidden").removeClass("hidden")');

            generalHelper.waitElementClickableAndClick(okButtonArticle);
            generalHelper.waitAlertSuccessPresentAndClose();


            //select client
            var clientSelectContainer = element(by.css('div[data-key="customer"]'));
            generalHelper.selectTypeAheadItem(clientSelectContainer, 'input#customer', customerName, 2);

            var editClientButton = modalWindow.element(by.css('.js-edit-customer'));

            generalHelper.waitElementClickableAndClick(editClientButton);

            browser.sleep(1000);

            var editClientModal = element(by.css('#company-update-modal'));

            var emailInput = editClientModal.element(by.css('#email'));

            var phoneInput = editClientModal.element(by.css('#phone'));

            emailInput.clear();
            phoneInput.clear();
            emailInput.sendKeys('xpto@xpto.biz');
            phoneInput.sendKeys('123456789');

            var editClientSave = editClientModal.element(by.css('.btn-success'));

            generalHelper.waitElementClickableAndClick(editClientSave);

            generalHelper.waitElementNotPresent(editClientModal);

            var checkEmail = modalWindow.element(by.css('table[data-sidepanel="company"] tbody tr td.values[data-field="email"]'));
            var checkPhone = modalWindow.element(by.css('table[data-sidepanel="company"] tbody tr td.values[data-field="phone"]'));

            expect(checkEmail.getText()).toEqual('xpto@xpto.biz');
            expect(checkPhone.getText()).toEqual('123456789');

            generalHelper.waitElementClickableAndClick(okButtonQuote);

        });

        it('should confirm addition quote', function() {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItemQuotes);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++quotesCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});