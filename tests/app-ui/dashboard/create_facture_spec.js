describe('Dashboard', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Create Invoice', function () {

        //declarar variaveis necessarias em mais do que um teste
        var tabItemInvoices,
            menuIcon,
            newFacture,
            modalWindow,
            inputCustomer,
            okButton,
            facturesCount;

        beforeEach(function() {
            menuIcon = element(by.css('#invoicing-module'));
            newFacture = element(by.css('.js-new-invoice[data-original-title="Créer une facture"]'));
            modalWindow = element(by.css('#invoicing-create-invoice-modal'));
            inputCustomer = element(by.css('div[data-key="customer"]'));
            okButton = modalWindow.element(by.css('[data-original-title="Brouillon"]'));
        });

        it('should check number of cashdays', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            facturesCount = 0;
            records.getText().then(function(text) {
                facturesCount = parseInt(text);
            });

            generalHelper.returnToDashboard();

            browser.sleep(1000);
        });

        it('should open new invoice modal', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(newFacture);

            browser.sleep(1000);

            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });


        it('should check for empty fields', function () {

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(2000);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).not.toEqual(0);
        });

        it('should fill mandatory fields and submit', function() {

            var customerName = "bip";
            generalHelper.selectTypeAheadItem(inputCustomer, "input#customer", customerName, 2);

            var editClientButton = modalWindow.element(by.css('.js-edit-customer'));
            generalHelper.waitElementClickableAndClick(editClientButton);

            browser.sleep(1000);

            var editClientModal = element(by.css('#company-update-modal'));
            var emailInput = editClientModal.element(by.css('#email'));
            var phoneInput = editClientModal.element(by.css('#phone'));
            emailInput.clear();
            phoneInput.clear();
            emailInput.sendKeys('xpto@xpto.biz');
            phoneInput.sendKeys('123456789');

            var editClientSave = editClientModal.element(by.css('.btn-success'));
            generalHelper.waitElementClickableAndClick(editClientSave);
            generalHelper.waitElementNotPresent(editClientModal);

            var checkEmail = modalWindow.element(by.css('table[data-sidepanel="company"] tbody tr td.values[data-field="email"]'));
            var checkPhone = modalWindow.element(by.css('table[data-sidepanel="company"] tbody tr td.values[data-field="phone"]'));
            expect(checkEmail.getText()).toEqual('xpto@xpto.biz');
            expect(checkPhone.getText()).toEqual('123456789');

            //Selecionar um item já existente na lista
            var itemDiv = element(by.css('#invoicing-invoice-form-items'));
            var addItem = modalWindow.element(by.css('.js-add-item'));
            generalHelper.waitElementClickableAndClick(addItem);

            browser.sleep(1000);

            generalHelper.selectTypeAheadItem(itemDiv, '.js-item.tt-input', 'A', 1);

            browser.sleep(1000);

            //Editar os números desse item

            var quantityField = itemDiv.element(by.css('.js-item-quantity'));
            quantityField.sendKeys('2,50');

            var discountField = itemDiv.element(by.css('.js-item-discount'));
            discountField.clear();
            discountField.sendKeys('20');

            //Adicionar linha em branco
            var addLine = modalWindow.element(by.css('.js-add-blank'));
            generalHelper.waitElementClickableAndClick(addLine);

            browser.sleep(1000);

            //Adicionar um comentário
            var addComment = modalWindow.element(by.css('.js-add-comment'));
            generalHelper.waitElementClickableAndClick(addComment);

            browser.sleep(1000);

            var writeComment = modalWindow.element(by.css('.js-line-observations'));
            writeComment.sendKeys("Comentário novo!");

            //Adicionar linha em branco
            generalHelper.waitElementClickableAndClick(addLine);

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

        });

        it('should confirm addition facture', function() {

            var showDashboardArea = element(by.css('.knob .js-show-dashboard'));
            var invoincingModuleButton = element(by.css('.js-app-menu-item[href="#invoicing/invoices"]'));

            browser.actions()
                .mouseMove(showDashboardArea)
                .perform();

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(invoincingModuleButton);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++facturesCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });
    
});