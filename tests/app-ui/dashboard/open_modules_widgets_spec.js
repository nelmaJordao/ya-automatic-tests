describe('dashboard', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('open modules from widgets', function () {

        it('should open bank module', function ()
        {
            browser.sleep(2000);
            
            var link = element(by.css('.js-module-btn[data-mod="bank"]'));
            generalHelper.waitElementClickableAndClick(link);

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual("bank");

                var newURL = array[0];
                browser.get(newURL + '#dashboard');
            });

        });

        it('should open payroll module', function ()
        {
            browser.sleep(2000);
            
            var link = element(by.css('.js-module-btn[data-mod="payroll"]'));

            generalHelper.waitElementClickableAndClick(link);

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual("payroll");

                var newURL = array[0];
                browser.get(newURL + '#dashboard');
            });

        });

        it('should open invoicing module', function ()
        {
            browser.sleep(2000);

            var link = element.all(by.css('.js-module-btn[data-mod="invoicing"]')).get(0);
            
            generalHelper.waitElementClickableAndClick(link);

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual("invoicing");

                var newURL = array[0];
                browser.get(newURL + '#dashboard');
            });

        });

        it('should open document tagging module', function ()
        {
            browser.sleep(2000);

            var link = element.all(by.css('.js-module-btn[data-mod="document:tagging"]')).get(0);
            generalHelper.waitElementClickableAndClick(link);

            browser.sleep(1000);

            var modalWindow = element(by.css('#document-tagging-edit-document-modal'));
            var closeButton = modalWindow.element(by.css('button.close[data-dismiss="modal"]'));
            
            browser.sleep(1000);
            
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.getCurrentUrl().then(function(url){
                
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual("document_tagging");
                
                var newURL = array[0];
                browser.get(newURL + '#dashboard');
            });

        });

        it('should open contract manager module', function ()
        {
            browser.sleep(2000);
            
            var link = element(by.css('.js-module-btn[data-mod="contract:manager"]'));
            generalHelper.waitElementClickableAndClick(link);

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual("contract_manager");

                var newURL = array[0];
                browser.get(newURL + '#dashboard');
            });

        });

        it('should open invoicing module', function ()
        {
            browser.sleep(2000);
            
            var link = element.all(by.css('.js-module-btn[data-mod="invoicing"]')).get(1);
            generalHelper.waitElementClickableAndClick(link);

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual("invoicing");

                var newURL = array[0];
                browser.get(newURL + '#dashboard');
            });

        });

        it('should open invoicing module', function ()
        {
            browser.sleep(2000);
            
            var link = element.all(by.css('.js-module-btn[data-mod="invoices"]')).first();
            generalHelper.waitElementClickableAndClick(link);

            browser.sleep(4500);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual("invoicing");

                var newURL = array[0];
                browser.get(newURL + '#dashboard');
            });

        });

        it('should open cash journal module', function ()
        {
            browser.sleep(2000);
            
            var link = element.all(by.css('.js-module-btn[data-mod="cash:journal"]')).get(0);
            generalHelper.waitElementClickableAndClick(link);

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual("cash_journal");

                var newURL = array[0];
                browser.get(newURL + '#dashboard');
            });

        });

    });

});