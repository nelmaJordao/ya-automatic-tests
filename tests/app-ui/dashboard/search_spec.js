describe('dashboard', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('search', function () {

        var searchButton,
            searchField;

        beforeEach(function() {
            searchButton = element(by.css('.js-aside-search-prompt-btn'));
            searchField = element(by.css('#search'));
        });

        it('should open search field', function ()
        {
            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(searchButton);
                        
            generalHelper.waitElementPresent(searchField);
            browser.sleep(1000);
        });
        


        it('should search for purchases module', function () {
            searchField.sendKeys('Achats : Accédez au module\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('supplier');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for purchases module, documents to validate', function () {
            searchField.sendKeys('Achats : Accédez aux factures\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('supplier');
                expect(testArray[1]).toEqual('documents');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for invoicing module', function () {
            searchField.sendKeys('Ventes : Accédez au module\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('invoicing');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for invoicing module, credit notes', function () {
            searchField.sendKeys('Ventes : Accédez aux avoirs\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('invoicing');
                expect(testArray[1]).toEqual('creditnotes');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for expenses module', function () {
            searchField.sendKeys('Frais : Accédez au module\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('expenses');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for document tagging module', function () {
            
            browser.sleep(2000);
            
            searchField.sendKeys('Traitement : Accédez au module\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('document_tagging');
            });

            var closeButton = element(by.css('#document-tagging-edit-document-modal button.close'));
            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(closeButton);
            browser.sleep(1000);
        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for bank module', function () {
            searchField.sendKeys('Banque : Accédez aux comptes\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('bank');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for bank module, connectors', function () {
            searchField.sendKeys('Banque : Accédez aux connecteurs\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('bank');
                expect(testArray[1]).toEqual('connectors');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for client admin module', function () {
            searchField.sendKeys('Admin clients : Accédez au module\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('customer_admin');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for cash journal module', function () {
            searchField.sendKeys('Caisse : Accédez au module\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('cash_journal');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for payroll module', function () {
            searchField.sendKeys('EVP-Paye : Accédez au module\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('payroll');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for contracts module', function () {
            searchField.sendKeys('Contrats : Accédez au module\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('contract_manager');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for contracts module, reminders', function () {
            searchField.sendKeys('Contrats : Accédez aux alertes\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('contract_manager');
                expect(testArray[1]).toEqual('reminders');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for legal info', function () {
            searchField.sendKeys('Informations légales : Accédez au module\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('legal_info');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for docs module', function () {
            searchField.sendKeys('Docs : Accédez au module\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('document_viewer');
            });

        });

        it('should open search field', function ()
        {
            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementPresent(searchField);

            browser.sleep(1000);
        });

        it('should search for dashboard', function () {
            searchField.sendKeys('Tableau de Bord : Accédez au tableau de bord\n');

            browser.sleep(1000);

            browser.getCurrentUrl().then(function(url){
                var str = url.toString();
                var array = str.split('#');

                var testArray = array[1].split('/');
                expect(testArray[0]).toEqual('dashboard');
            });

        });

    });

});