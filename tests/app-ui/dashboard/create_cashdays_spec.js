describe('Dashboard', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Create CashDay', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            newCashDay,
            modalWindow,
            firstRow,
            deleteModal,
            okButton,
            numberTransactionsinput;

        var cashDayCount;


        beforeEach(function() {
            menuIcon = element(by.css('#cash-journal-module'));
            newCashDay = element(by.css('.js-new-cash-day'));
            modalWindow = element(by.css('#cash-day-create-modal'));
            firstRow = element.all(by.css('#cash-journal-cashdays-dtable tr')).get(0);
            deleteModal = element(by.css('#cash-day-delete-modal'));
            okButton = modalWindow.element(by.css('.btn-success.btn-block.ok'));
            numberTransactionsinput = modalWindow.element(by.css('#numberOfTransactions'));
        });


        it('should check number of cashdays', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            cashDayCount = 0;
            records.getText().then(function(text) {
                cashDayCount = parseInt(text);
            });
        });

        it('should open new cashday modal', function () {
            generalHelper.returnToDashboard();

            browser.sleep(1000);

            var showDashboardArea = element(by.css('.knob .js-show-dashboard'));

            browser.actions()
                .mouseMove(showDashboardArea)
                .perform();

            browser.sleep(1000);

            var caisseArea = element(by.css('.js-app-menu-item[href="#cash_journal/cashdays"]'))
            browser.actions()
                .mouseMove(caisseArea)
                .perform();

            browser.sleep(1000);

            var newCashDayShort = element(by.css('.js-new-cash-journal-shortcut'));
            generalHelper.waitElementClickableAndClick(newCashDayShort);

            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });


        it('should test field restrictions', function () {

            numberTransactionsinput.sendKeys('asd');

            expect(numberTransactionsinput.getText()).not.toEqual('asd');
        });

        it('should fill mandatory fields and submit', function() {

            var inputTicketNumber = modalWindow.element(by.css('#zNumber'));
            var ticketNumber = Math.floor(Math.random() * 1000);
            inputTicketNumber.sendKeys(ticketNumber);

            var numberTransactions = Math.floor(Math.random() * 1000);
            numberTransactionsinput.sendKeys(numberTransactions);

            //Income tab

            var tab = modalWindow.element(by.css('div.tab-pane.active[role="tabpanel"]'));

            var salesInput = modalWindow.element(by.css('div.form-group input.js-cash-input.js-sales-input'));
            salesInput.clear();
            var salesAmmount = numberTransactions * 10;
            salesInput.sendKeys(salesAmmount);

            var modesAmmount = salesAmmount / 8;
            var modesInput = tab.element(by.css('#CASH'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#CHECK'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#BANK_CARD'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#TICKET_RESTAURANT'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#CLIENT_CREDIT'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#WIRE_TRANSFER'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#TICKET_HOLIDAY'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            modesInput = tab.element(by.css('#GIFT_CARD'));
            modesInput.sendKeys('asd');
            expect(modesInput.getText()).not.toEqual('asd');
            modesInput.clear();
            modesInput.sendKeys(modesAmmount);

            var cashReinforcementInput = tab.element(by.css('#cashReinforcement'));
            var cashReinforcement = Math.floor(Math.random() * 10);
            cashReinforcementInput.sendKeys('asd');
            expect(cashReinforcementInput.getText()).not.toEqual('asd');
            cashReinforcementInput.clear();
            cashReinforcementInput.sendKeys(cashReinforcement);

            //Expenses tab

            var expensesTabClick = modalWindow.element(by.css('li[role="cash-outputs"'));
            generalHelper.waitElementClickableAndClick(expensesTabClick);

            tab = modalWindow.element(by.css('#cj-outs'));

            generalHelper.waitElementDisplayed(tab);

            var bankExpenses = Math.floor(Math.random() * 500);
            var expenseModesAmmount = bankExpenses / 6;

            var modesOutput = tab.element(by.css('#CASH'));
            modesOutput.sendKeys('asd');
            expect(modesOutput.getText()).not.toEqual('asd');
            modesOutput.clear();
            modesOutput.sendKeys(expenseModesAmmount);

            modesOutput = tab.element(by.css('#CHECK'));
            modesOutput.sendKeys('asd');
            expect(modesOutput.getText()).not.toEqual('asd');
            modesOutput.clear();
            modesOutput.sendKeys(expenseModesAmmount);

            modesOutput = tab.element(by.css('#TICKET_RESTAURANT'));
            modesOutput.sendKeys('asd');
            expect(modesOutput.getText()).not.toEqual('asd');
            modesOutput.clear();
            modesOutput.sendKeys(expenseModesAmmount);

            modesOutput = tab.element(by.css('#WIRE_TRANSFER'));
            modesOutput.sendKeys('asd');
            expect(modesOutput.getText()).not.toEqual('asd');
            modesOutput.clear();
            modesOutput.sendKeys(expenseModesAmmount);

            modesOutput = tab.element(by.css('#TICKET_HOLIDAY'));
            modesOutput.sendKeys('asd');
            expect(modesOutput.getText()).not.toEqual('asd');
            modesOutput.clear();
            modesOutput.sendKeys(expenseModesAmmount);

            modesOutput = tab.element(by.css('#GIFT_CARD'));
            modesOutput.sendKeys('asd');
            expect(modesOutput.getText()).not.toEqual('asd');
            modesOutput.clear();
            modesOutput.sendKeys(expenseModesAmmount);

            //Add supplier purchase

            var supplierPurchaseButton = tab.element(by.css('button.js-add-supplier-purchase'));
            generalHelper.waitElementClickableAndClick(supplierPurchaseButton);

            var tr = tab.all(by.css('#supplier-purchases-region tr')).last();

            var supplierInput = tr.element(by.css('div.js-auto-complete'));
            generalHelper.selectTypeAheadItem(supplierInput, 'input[name="supplierBaseCompanyName"]', 'alterna', 1);

            var supplierInvoiceInput = tr.element(by.css('input[name="supplierDocId"]'));
            supplierInvoiceInput.clear();

            var invoiceNumber = Math.floor(Math.random() * 100);
            supplierInvoiceInput.sendKeys(invoiceNumber);

            var supplierComment = tr.element(by.css('input[name="observations"]'));
            supplierComment.sendKeys("test");

            var supplierAmmountInput = tr.element(by.css('input[name="totalInCash"'));
            supplierAmmountInput.clear();
            var supplierAmmount = Math.floor(Math.random() * 100);
            supplierAmmountInput.sendKeys(supplierAmmount);

            //Add cash withdrawal

            var cashWithdrawalButton = tab.element(by.css('button.js-add-cash-withdrawal'));
            generalHelper.waitElementClickableAndClick(cashWithdrawalButton);

            tr = tab.all(by.css('#cash-withdrawals-region tr')).last();

            var withdrawalDescription = tr.element(by.css('input.js-description'));
            withdrawalDescription.sendKeys("test");

            var withdrawalTypeDrop = tr.element(by.css('button'));
            var withdrawalTypeSelect = tr.element(by.css('div.dropdown-menu ul.dropdown-menu li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(withdrawalTypeDrop);
            generalHelper.waitElementClickableAndClick(withdrawalTypeSelect);

            var withdrawalAmmountInput = tr.element(by.css('input[name="value"]'));
            withdrawalAmmountInput.sendKeys(Math.floor(Math.random() * 100));

            //add client credit

            var clientCreditButton = tab.element(by.css('button.js-add-client-credit'));
            generalHelper.waitElementClickableAndClick(clientCreditButton);

            tr = tab.all(by.css('#client-credit-region tr')).last();

            var clientInput = tr.element(by.css('input.js-customer-name'));
            clientInput.sendKeys("test");

            var commentsInput = tr.element(by.css('input.js-observations'));
            commentsInput.sendKeys("test");

            var reimbursementInput = tab.element(by.css('input.js-edit-client-credit.js-cash-output'));
            reimbursementInput.sendKeys(modesAmmount);

            var observationsInput = modalWindow.element(by.css('input.js-edit-client-credit.js-observations'));
            observationsInput.sendKeys('Test');

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

        });

        it('should confirm addition cashday', function() {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++cashDayCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

        //Apaga a cashday que foi criada para se puder voltar a repetir o teste sem se ter de apagar a cashday manualmente

        it('should open delete cashday window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            var deleteButton = firstRow.element(by.css('.js-delete-cash-day'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(deleteButton);
            generalHelper.waitElementPresent(deleteModal);

            expect(deleteModal.isDisplayed()).toBe(true);
        });

        it('should confirm delete cashday', function() {
            var okButton = deleteModal.element(by.css('.btn-success.ok'));
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(--recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });
});