describe('Dashboard', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Create Contract Draft', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIconContract,
            tabItem,
            menuIcon,
            newContractDraft,
            modalWindow,
            contractsCount,
            saveDraftButton;

        beforeEach(function() {
            
            menuIconContract = element(by.css('#contract-manager-module'));
            tabItem = element(by.css('#contract-manager-menu-region [data-sec="contracts"]'));
            menuIcon = element(by.css('#contract-manager-module'));
            newContractDraft = element(by.css('.js-new-draft'));
            modalWindow = element(by.css('#cm-draft-create-modal'));
            saveDraftButton = modalWindow.element(by.css('.btn-success.ok'));

        });

        it('should check number of contracts', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            var records = element(by.css('.js-total-res span'));
            contractsCount = 0;
            records.getText().then(function(text) {
                contractsCount = parseInt(text);
            });
        });

        it('should open new draft modal window', function () {

            generalHelper.returnToDashboard();
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(newContractDraft);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {
            generalHelper.waitElementClickableAndClick(saveDraftButton);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(5);
        });

        it('should test field restrictions', function () {
            var tabValues = element(by.css('li[role="values"]'));
            generalHelper.waitElementClickableAndClick(tabValues);

            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").val("asd")');
            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").blur()');

            //select payment interval unit time type :: Opcao selecionada "DAYS"
            var selectpickerPaymentIntervalUnitTimeType = modalWindow.element(by.css('#cm-values [data-id="paymentIntervalUnitTimeType"]'));
            var selectpickerPaymentIntervalUnitTimeTypeOption = modalWindow.all(by.css('#cm-values .dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeType);
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeTypeOption);

            //value amount
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").val("asd")');
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").blur()');

            generalHelper.waitElementClickableAndClick(saveDraftButton);

            var errors = modalWindow.all(by.css('.form-group.error'));
            expect(errors.count()).not.toEqual(0);

        });

        it('should fill mandatory fields and submit', function() {
            var tabGeneral = element(by.css('li[role="information"]'));
            generalHelper.waitElementClickableAndClick(tabGeneral);


            var name = config.inputs.getRandomName(10).replace(/ /g, '');
            //NOTA: tem que ser um cliente que exista e ja tenha sido adicionado anteriormente
            var customer = 'Alterna';

            //title
            var titleInput = modalWindow.element(by.css('#title'));
            generalHelper.waitElementDisplayed(titleInput);
            titleInput.sendKeys('Contract ' + name);

            //reference
            var referenceInput = modalWindow.element(by.css('#reference'));
            referenceInput.sendKeys('REF' + name);

            var tabContractors = element(by.css('li[role="contractors"]'));
            generalHelper.waitElementClickableAndClick(tabContractors);

            //select client
            var clientSelectContainer = element(by.css('div.js-customer-external-company.js-customer-external-company-input'));
            generalHelper.selectTypeAheadItem(clientSelectContainer, 'input#toExternal', customer, 1);

            var tabValues = element(by.css('li[role="values"]'));
            generalHelper.waitElementClickableAndClick(tabValues);

            //payment interval
            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").val(40)');
            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").blur()');

            //select payment interval unit time type :: Opcao selecionada "DAYS"
            var selectpickerPaymentIntervalUnitTimeType = modalWindow.element(by.css('#cm-values [data-id="paymentIntervalUnitTimeType"]'));
            var selectpickerPaymentIntervalUnitTimeTypeOption = modalWindow.all(by.css('#cm-values .dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeType);
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeTypeOption);

            //value amount
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").val(200)');
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").blur()');

            generalHelper.waitElementClickableAndClick(saveDraftButton);

            browser.sleep(1000);

        });

        it('should confirm addition contract', function() {

            var records = element(by.css('.js-total-res span'));

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(contractsCount++);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});