describe('dashboard', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('notifications', function () {

        var notificationsIcon,
            notificationsDiv,
            firstGroupHeader,
            firstGroupElement,
            notificationsCountElement,
            notificationsCount;

        beforeEach(function() {
            notificationsIcon = element(by.css('.js-aside-notifications-btn'));
            notificationsDiv = element(by.css('#aside-notifications'));
            firstGroupHeader = notificationsDiv.all(by.css('.list-group-item.header-group')).get(0);
            firstGroupElement = notificationsDiv.all(by.css('.notification-row-item div')).get(0);
        });

        it('should open notifications sidebar', function ()
        {
            browser.sleep(1000);

            notificationsCountElement = element(by.css('#total-notifications'));

            notificationsCountElement.getText().then(function(text){
                notificationsCount = parseInt(text);
            });

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(notificationsIcon);

            browser.sleep(1000);

            expect(notificationsDiv.isDisplayed()).toBe(true);
        });
        
        it('should click on the first notification group header', function () {

            generalHelper.waitElementClickableAndClick(firstGroupHeader);

            expect(firstGroupElement.isDisplayed()).toBe(true);

            browser.sleep(1000);

        });


        it('should delete first element from first group', function () {

            var firstGroupElementDelete = notificationsDiv.all(by.css('#info-notifications-position .sublinks.collapse .js-item-delete.notification-delete-btn')).get(0);
            generalHelper.waitElementClickableAndClick(firstGroupElementDelete);

            browser.sleep(5000);

        });

        it('should delete first group', function () {

            var icon = notificationsDiv.element(by.css('.list-group-item .js-occurrences-number'));
            var firstGroupDelete = notificationsDiv.all(by.css('.list-group-item .js-trash-group')).get(0);

            browser.actions()
                .mouseMove(icon)
                .perform();

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(firstGroupDelete);

            browser.sleep(2000);

        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();

        });

    });

});
