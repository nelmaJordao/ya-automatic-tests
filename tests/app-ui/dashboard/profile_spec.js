describe('dashboard', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('change profile info', function () {

        var userMenu,
            profileLink,
            firstNameField,
            lastNameField,
            expandButton,
            passwordField,
            confirmPassField,
            saveButton;

        beforeEach(function() {
            
            userMenu = element(by.css('#username'));
            profileLink = element(by.css('li.dropdown.username-dropdown ul.dropdown-menu li a.js-user-account'));
            firstNameField = element(by.css('#firstName'));
            lastNameField = element(by.css('#lastName'));
            expandButton = element(by.css('.js-change-password.action'));
            passwordField = element(by.css('#newPassword'));
            confirmPassField = element(by.css('#confirmPassword'));
            saveButton = element(by.css('button.btn-success'));
        });

        it('should open profile window', function ()
        {
            generalHelper.waitElementClickableAndClick(userMenu);
            generalHelper.waitElementClickableAndClick(profileLink);
            browser.sleep(2000);
        });

        it('should check empty fields and field restrictions', function ()
        {
            firstNameField.clear();
            lastNameField.clear();

            generalHelper.waitElementClickableAndClick(saveButton);

            var errors = element.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(2);

            browser.sleep(1000);

            firstNameField.sendKeys('123');
            lastNameField.sendKeys('123');

            generalHelper.waitElementClickableAndClick(saveButton);

            errors = element.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(2);
        });

        it('should change name', function ()
        {
            var firstName = config.inputs.getRandomName(4,false);
            var lastName = config.inputs.getRandomName(4,false);

            browser.sleep(1000);

            firstNameField.sendKeys(firstName);

            browser.sleep(1000);

            lastNameField.sendKeys(lastName);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(1000);

            var usernameDisplay = userMenu.element(by.css('#userNameOnHeader'));

            usernameDisplay.getText().then(function(text){
                expect(text).toEqual('' + firstName + ' ' + lastName);
            });
        });

        it('should open profile window', function ()
        {
            generalHelper.waitElementClickableAndClick(userMenu);
            generalHelper.waitElementClickableAndClick(profileLink);
            browser.sleep(2000);
        });

        it('should change name back to Accountant Nelma', function ()
        {
            var firstName = 'Accountant';
            var lastName = 'Nelma';
            firstNameField.clear();
            lastNameField.clear();
            firstNameField.sendKeys(firstName);
            lastNameField.sendKeys(lastName);

            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(1000);

            var usernameDisplay = userMenu.element(by.css('#userNameOnHeader'));

            usernameDisplay.getText().then(function(text){
                expect(text).toEqual('Accountant Nelma');
            });
        });

        it('should open profile modal', function ()
        {
            generalHelper.waitElementClickableAndClick(userMenu);
            generalHelper.waitElementClickableAndClick(profileLink);
            browser.sleep(2000);
        });

        it('should change password', function ()
        {
            generalHelper.waitElementClickableAndClick(expandButton);

            browser.sleep(1000);

            generalHelper.waitElementPresent(passwordField);

            browser.sleep(1000);

            passwordField.sendKeys('asdf');
            confirmPassField.sendKeys('wqer');

            generalHelper.waitElementClickableAndClick(saveButton);

            var errors = element.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(1);

            passwordField.clear();
            confirmPassField.clear();

            passwordField.sendKeys('qwerty');
            confirmPassField.sendKeys('qwerty');

            generalHelper.waitElementClickableAndClick(saveButton);
            browser.sleep(4000);
        });

        it('should login with new password and change password back to xptoxpto', function ()
        {
            browser.sleep(2000);
            loginHelper.logout();

            loginHelper.login(config.users.TESTACCNELMA_PASSCHANGED);

            generalHelper.waitElementClickableAndClick(userMenu);
            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(profileLink);
            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(expandButton);
            browser.sleep(1000);

            generalHelper.waitElementPresent(passwordField);

            passwordField.sendKeys('xptoxpto');
            confirmPassField.sendKeys('xptoxpto');

            generalHelper.waitElementClickableAndClick(saveButton);

            loginHelper.logout();

            loginHelper.login(config.users.TEST);

            browser.getCurrentUrl().then(function(url){
                expect(url).toEqual('http://acfirmholding.preprodyesaccount.com/#dashboard');
            });
        });

    });

});