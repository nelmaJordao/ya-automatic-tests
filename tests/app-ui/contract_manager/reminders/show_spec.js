describe('Contracts', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Reminders', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow;
        
        beforeEach(function()
        {
            menuIcon = element(by.css('#contract-manager-module'));
            tabItem = element(by.css('#contract-manager-menu-region [data-sec="reminders"]'));
            modalWindow = element(by.css('#cm-reminder-show-modal'));
        });


        it('should open show reminder modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);

            generalHelper.waitElementClickableAndClick(tabItem);

            var firstShowButton = element.all(by.css('#contract-manager-reminders-dtable a.js-show-reminder')).get(0);
            generalHelper.waitElementClickableAndClick(firstShowButton);
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);

        });

        it('should check for empty fields', function ()
        {
            //titulo do alerta
            var name = modalWindow.all(by.css('.form-group p.value')).get(0);
            expect(name.getText()).not.toEqual('');

            // nome do contrato
            var contract = modalWindow.all(by.css('.form-group p.value')).get(1);
            expect(contract.getText()).not.toEqual('');

            // data do alerta
            var date = modalWindow.all(by.css('.form-group p.value')).get(4);
            expect(date.getText()).not.toEqual('');

        });

        it('should close modal window', function()
        {
            var closeButton = modalWindow.element(by.css('.btn-default'));
            generalHelper.waitElementClickableAndClick(closeButton);
            
            expect(modalWindow.isPresent()).toBe(false);

            });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });
    
});