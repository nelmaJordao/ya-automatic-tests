describe('Contract Manager', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Reminders', function () {
    
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstEditButton,
            modalWindow,
            okButton,
            closeButton;
        
        beforeEach(function() {
            menuIcon = element(by.css('#contract-manager-module'));
            tabItem = element(by.css('#contract-manager-menu-region [data-sec="reminders"]'));
            firstEditButton = element.all(by.css('.js-edit-reminder')).get(0);
            modalWindow = element(by.css('#cm-reminder-edit-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            closeButton = modalWindow.element(by.css('.btn-default.cancel'));
        });
        
        it('should open edit reminder modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var filterButton = element(by.css('button[data-id="contractRelationType"]'));
            var filterOption = element.all(by.css('li[data-original-index="2"]')).get(0);
            generalHelper.waitElementClickableAndClick(filterButton);
            generalHelper.waitElementClickableAndClick(filterOption);
            generalHelper.waitElementClickableAndClick(firstEditButton);
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);

            browser.sleep(1000);
        });
        
        it('should check for empty fields', function () {
            //name
            var nameInput = modalWindow.element(by.css('#name'));
            nameInput.clear();
            
            generalHelper.waitElementClickableAndClick(okButton);
            
            var errors = modalWindow.all(by.css('.form-group.error'));
            
            expect(errors.count()).toEqual(1);
        });
        
        it('should close and reopen edit reminder modal window', function () {
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(firstEditButton);
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });
            
        it('should fill mandatory fields and submit', function() {
            var reminderTitle = 'Reminder ' + config.inputs.getRandomName(10).replace(/ /g, '');
            
            //name
            var nameInput = modalWindow.element(by.css('#name'));
            nameInput.clear();
            nameInput.sendKeys(reminderTitle);
            
            var quoteIssueDate = modalWindow.element(by.css('#specificDatePicker'))
            generalHelper.waitElementPresent(quoteIssueDate);

            browser.sleep(1000);
            
            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();
            
            var reminderTitleCell = firstEditButton.element(by.xpath("../..")).all(by.css('td')).get(1);
            
            reminderTitleCell.getText().then(function(text) {
                expect(text).toEqual(reminderTitle);
            });
            
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });
    
});