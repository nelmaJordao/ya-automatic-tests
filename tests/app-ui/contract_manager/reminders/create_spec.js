describe('Contract Manager', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Reminders', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButton;
        
        beforeEach(function() {
            menuIcon = element(by.css('#contract-manager-module'));
            tabItem = element(by.css('#contract-manager-menu-region [data-sec="reminders"]'));
            newButton = element(by.css('.js-new-reminder'));
            modalWindow = element(by.css('#cm-reminder-create-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
        });

        it('should open new reminder modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });
        
        it('should check for empty fields', function () {
            generalHelper.waitElementClickableAndClick(okButton);
            
            var errors = modalWindow.all(by.css('.form-group.error'));
            
            expect(errors.count()).not.toEqual(0);
        });

        it('should fill mandatory fields and submit', function() {
            var name = config.inputs.getRandomName(10).replace(/ /g, '');
            //NOTA: tem que ser um contrato que exista e ja tenha sido adicionado anteriormente
            var contract = 'Contract';
            
            //name
            var nameInput = modalWindow.element(by.css('#name'));
            nameInput.sendKeys('Reminder ' + name);
            
            //contract typeahead
            var contractTypeaheadContainer = element(by.css('div.js-typeahead-contract'));
            generalHelper.selectTypeAheadItem(contractTypeaheadContainer, 'input#contract', contract, 1);

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            browser.sleep(1000);
            
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });
            
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });
        
});