describe('Contract Manager', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Reminders', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow;
        
        beforeEach(function()
        {
            menuIcon = element(by.css('#contract-manager-module'));
            tabItem = element(by.css('#contract-manager-menu-region [data-sec="reminders"]'));
            firstRow = element.all(by.css('#contract-manager-reminders-dtable tr')).get(0);
            modalWindow = element(by.css('#cm-reminder-delete-modal'));
        });
        
        
        it('should open delete reminder modal window', function ()
        {
            var reminderTBody = element(by.css('#contract-manager-reminders-dtable'));
            var remindersList = reminderTBody.all(by.css('tr'));
            var deleteButton = remindersList.get(0).element(by.css('.js-delete-reminder'));
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var filterButton = element(by.css('button[data-id="contractRelationType"]'));
            var filterOption = element.all(by.css('li[data-original-index="1"]')).get(0);
            generalHelper.waitElementClickableAndClick(filterButton);
            generalHelper.waitElementClickableAndClick(filterOption);
            generalHelper.waitElementClickableAndClick(deleteButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should confirm delete modal window', function()
        {
            var confirmDeleteButton = modalWindow.element(by.css('.btn-success'));
            generalHelper.waitElementClickableAndClick(confirmDeleteButton);
            generalHelper.waitAlertSuccessPresentAndClose();

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
      
    });
    
});