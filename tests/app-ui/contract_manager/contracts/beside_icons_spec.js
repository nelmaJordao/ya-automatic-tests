describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Contracts', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            showButton,
            modalWindow,
            editModal,
            deleteModal;

        beforeEach(function() {
            menuIcon = element(by.css('#contract-manager-module'));
            tabItem = element(by.css('#contract-manager-menu-region [data-sec="contracts"]'));
            firstRow = element.all(by.css('#contract-manager-contracts-dtable tr')).get(1);
            showButton = firstRow.element(by.css('.options .js-show-contract'));
            modalWindow = element(by.css('#cm-contract-show-modal'));
            editModal = element(by.css('#cm-contract-edit-modal'));
            deleteModal = element(by.css('#cm-contract-delete-modal'));
        });

        it('should open show item modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(showButton);

            browser.sleep(1000);

            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);
        });

        it('should open show modal window', function () {

            var stateType = element(by.css('button[data-id="contractStatus"]'));
            var stateOption = element.all(by.css('li[data-original-index="2"]')).get(1);

            generalHelper.waitElementClickableAndClick(stateType);
            generalHelper.waitElementClickableAndClick(stateOption);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });


        it('should open edit modal window', function () {

            browser.sleep(1000);

            var crudEdit = modalWindow.element(by.css('.crud [data-trigger="contract:manager:edit:contract"]'));
            generalHelper.waitElementClickableAndClick(crudEdit);

            browser.sleep(1000);
            expect(editModal.isDisplayed()).toBe(true);
        });

        it('should open show modal window again', function () {
            var crudShow = editModal.element(by.css('.crud [data-trigger="contract:manager:show:contract"]'));
            generalHelper.waitElementClickableAndClick(crudShow);

            browser.sleep(1000);
            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should open delete modal window', function() {
            var crudDelete = modalWindow.element(by.css('.crud [data-trigger="contract:manager:delete:contract"]'));
            generalHelper.waitElementClickableAndClick(crudDelete);

            browser.sleep(1000);
            expect(deleteModal.isDisplayed()).toBe(true);

            var cancelButton = deleteModal.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            expect(deleteModal.isPresent()).toBe(false);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});