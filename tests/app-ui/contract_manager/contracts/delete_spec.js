describe('Contract Manager', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Contracts', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow;
        
        beforeEach(function() {
            menuIcon = element(by.css('#contract-manager-module'));
            tabItem = element(by.css('#contract-manager-menu-region [data-sec="contracts"]'));
            firstRow = element.all(by.css('#contract-manager-contracts-dtable tr')).get(0);
            modalWindow = element(by.css('#cm-contract-delete-modal'));
        });

        it('should open delete contract modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var filterButton = element(by.css('button[data-id="contractStatus"]'));
            var filterOption = element.all(by.css('li[data-original-index="1"]')).get(1);
            generalHelper.waitElementClickableAndClick(filterButton);
            generalHelper.waitElementClickableAndClick(filterOption);

            var deleteButton = firstRow.element(by.css('.options .js-delete-contract'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(deleteButton);

            browser.sleep(1000);

            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(1000);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });
        
        it('should confirm delete modal window', function() {
            var okButton = modalWindow.element(by.css('.btn-success.ok'));
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();
            
            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(--recordCount);
            });
            
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
    
    });
    
});