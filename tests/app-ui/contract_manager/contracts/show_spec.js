describe('Invoicing', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Contracts', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            modalWindow;
            
        beforeEach(function() {
            menuIcon = element(by.css('#contract-manager-module'));
            tabItem = element(by.css('#contract-manager-menu-region [data-sec="contracts"]'));
            firstRow = element.all(by.css('#contract-manager-contracts-dtable tr')).get(0);
            modalWindow = element(by.css('#cm-contract-show-modal'));
        });

        it('should open show item modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            var showButton = firstRow.element(by.css('.options .js-show-contract'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(showButton);

            browser.sleep(1000);
            
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });
        
        it('should check for empty fields', function () {
            //title
            var titleEl = modalWindow.all(by.css('h2.view')).get(0);
            expect(titleEl.getText()).not.toEqual('');
            
            //reference
            var referenceEl = modalWindow.all(by.css('.form-group p.value')).get(0);
            expect(referenceEl.getText()).not.toEqual('');
            
            //category
            var categoryEl = modalWindow.all(by.css('.form-group p.value')).get(2);
            expect(categoryEl.getText()).not.toEqual('');

            //from
            var fromEl = modalWindow.all(by.css('h3.name')).get(0);
            expect(fromEl.getText()).not.toEqual('');

            //to
            var toEl = modalWindow.all(by.css('h3.name')).get(1);
            expect(toEl.getText()).not.toEqual('');
        });
    
        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);
            
            expect(modalWindow.isPresent()).toBe(false);
            
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
    
    });
    
});