describe('Contract Manager', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Contracts', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstEditButton,
            modalWindow,
            okButton,
            closeButton,
            paymentIntervalInput;
        
        beforeEach(function() {
            menuIcon = element(by.css('#contract-manager-module'));
            tabItem = element(by.css('#contract-manager-menu-region [data-sec="contracts"]'));
            firstEditButton = element.all(by.css('#contract-manager-contracts-dtable a.js-edit-contract')).get(0);
            modalWindow = element(by.css('#cm-contract-edit-modal'));
            okButton = modalWindow.all(by.css('.btn-success')).last();
            closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            paymentIntervalInput = modalWindow.element(by.css('#paymentInterval'));
        });
        
        it('should open edit draft modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var filterButton = element(by.css('button[data-id="contractStatus"]'));
            var filterOption = element.all(by.css('li[data-original-index="1"]')).get(1);
            generalHelper.waitElementClickableAndClick(filterButton);
            generalHelper.waitElementClickableAndClick(filterOption);

            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(firstEditButton);
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should cancel edit draft', function () {
            var cancelButton = element(by.css('button.cancel'));

            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(cancelButton);

            browser.sleep(1000);
        });

        it('should open edit draft modal window', function () {
            generalHelper.waitElementClickableAndClick(firstEditButton);

            browser.sleep(1000);

            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });
        
        it('should check for empty fields', function () {
            //title
            var titleInput = modalWindow.element(by.css('#title'));
            titleInput.clear();

            var tabValues = element(by.css('li[role="values"]'));
            generalHelper.waitElementClickableAndClick(tabValues);

            generalHelper.waitElementDisplayed(paymentIntervalInput);

            //payment interval
            paymentIntervalInput.clear();


            //select payment interval unit time type
            var selectpickerPaymentIntervalUnitTimeType = modalWindow.element(by.css('#cm-values [data-id="paymentIntervalUnitTimeType"]'));
            var selectpickerPaymentIntervalUnitTimeTypeOption = modalWindow.all(by.css('#cm-values .dropdown-menu.open')).get(0).element(by.css('li[data-original-index="0"]'));
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeType);
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeTypeOption);

            generalHelper.waitElementClickableAndClick(okButton);
            
            var errors = modalWindow.all(by.css('.form-group.error'));
            
            expect(errors.count()).not.toEqual(0);
        });

        it('should test field restrictions', function () {
            var tabValues = element(by.css('li[role="values"]'));
            generalHelper.waitElementClickableAndClick(tabValues);

            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").val("asd")');
            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").blur()');

            //select payment interval unit time type :: Opcao selecionada "DAYS"
            var selectpickerPaymentIntervalUnitTimeType = modalWindow.element(by.css('#cm-values [data-id="paymentIntervalUnitTimeType"]'));
            var selectpickerPaymentIntervalUnitTimeTypeOption = modalWindow.all(by.css('#cm-values .dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeType);
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeTypeOption);

            //value amount
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").val("asd")');
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").blur()');

            generalHelper.waitElementClickableAndClick(okButton);

            var errors = modalWindow.all(by.css('.form-group.error'));
            expect(errors.count()).not.toEqual(0);

        });
        
        it('should close and reopen edit draft modal window', function () {
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(firstEditButton);
            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });
        
        it('should fill mandatory fields and submit', function() {
            var contractTitle = 'Contract ' + config.inputs.getRandomName(10).replace(/ /g, '');

            //title
            var titleInput = modalWindow.element(by.css('#title'));
            titleInput.clear();
            titleInput.sendKeys(contractTitle);

            //select category
            var selectpickerCategory = modalWindow.element(by.css('#cm-information [data-id="category"]'));
            var selectpickerCategoryOption = modalWindow.all(by.css('#cm-information .dropdown-menu.open')).get(1).element(by.css('li[data-original-index="2"]'));
            generalHelper.waitElementClickableAndClick(selectpickerCategory);
            generalHelper.waitElementClickableAndClick(selectpickerCategoryOption);


            var tabDates = element(by.css('li[role="dates"]'));
            generalHelper.waitElementClickableAndClick(tabDates);


            var dateCheckbox = modalWindow.element(by.css('tr.js-contract-term-duration'));

            var checkboxDate1,
                checkboxDate2;

            browser.sleep(1000);

            dateCheckbox.isDisplayed().then(function (displayed) {

                if(!displayed){
                    //Specificites Date1

                    checkboxDate1 = modalWindow.all(by.css('td.values label.checkbox span')).get(0);
                    generalHelper.waitElementClickableAndClick(checkboxDate1);

                    //Specificites Date2

                    checkboxDate2 = modalWindow.all(by.css('td.values label.checkbox span')).get(1);
                    generalHelper.waitElementClickableAndClick(checkboxDate2);

                    browser.sleep(1000);

                    var time = modalWindow.element(by.css('#duration'));
                    time.sendKeys('1');

                    var selectPickerDurationType = modalWindow.element(by.css('#cm-dates [data-id="durationUnitTimeType"]'));
                    var selectPickerDurationOption = modalWindow.all(by.css('#cm-dates .dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
                    generalHelper.waitElementClickableAndClick(selectPickerDurationType);
                    generalHelper.waitElementClickableAndClick(selectPickerDurationOption);

                    var renewalLimit = modalWindow.element(by.css('#renewalLimit'));
                    renewalLimit.sendKeys('1');

                }else{
                    checkboxDate2 = modalWindow.all(by.css('td.values label.checkbox span')).get(1);
                    generalHelper.waitElementClickableAndClick(checkboxDate2);

                    checkboxDate1 = modalWindow.all(by.css('td.values label.checkbox span')).get(0);
                    generalHelper.waitElementClickableAndClick(checkboxDate1);

                    browser.sleep(1000);

                }
            });


            var tabValues = element(by.css('li[role="values"]'));
            generalHelper.waitElementClickableAndClick(tabValues);
            
            //payment interval
            generalHelper.waitElementDisplayed(paymentIntervalInput);
            paymentIntervalInput.clear();
            paymentIntervalInput.sendKeys(1);


            //select payment interval unit time type :: Opcao selecionada "MONTHS"
            var selectpickerPaymentIntervalUnitTimeType = modalWindow.element(by.css('#cm-values [data-id="paymentIntervalUnitTimeType"]'));
            var selectpickerPaymentIntervalUnitTimeTypeOption = modalWindow.all(by.css('#cm-values .dropdown-menu.open')).get(0).element(by.css('li[data-original-index="2"]'));
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeType);
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeTypeOption);
            
            //value amount
            var amountInput = modalWindow.element(by.css('#valueAmount'));
            amountInput.clear();
            amountInput.sendKeys(1000);

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

        });
        
        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });
    
});