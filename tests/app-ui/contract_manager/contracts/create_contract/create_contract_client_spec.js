describe('Contract Manager', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Contracts', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            finalizeContractButton;


        beforeEach(function() {
            menuIcon = element(by.css('#contract-manager-module'));
            tabItem = element(by.css('#contract-manager-menu-region [data-sec="contracts"]'));
            newButton = element(by.css('.js-new-draft'));
            modalWindow = element(by.css('#cm-draft-create-modal'));
            finalizeContractButton = modalWindow.element(by.css('.btn-success.js-finalize'));
        });

        it('should open new draft modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(newButton);

            browser.sleep(1000);
            
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });



        it('should check for empty fields', function () {
            generalHelper.waitElementClickableAndClick(finalizeContractButton);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).not.toEqual(0);
        });

        it('should test field restrictions', function () {
            var tabValues = element(by.css('li[role="values"]'));
            generalHelper.waitElementClickableAndClick(tabValues);

            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").val("asd")');
            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").blur()');

            //select payment interval unit time type :: Opcao selecionada "DAYS"
            var selectpickerPaymentIntervalUnitTimeType = modalWindow.element(by.css('#cm-values [data-id="paymentIntervalUnitTimeType"]'));
            var selectpickerPaymentIntervalUnitTimeTypeOption = modalWindow.all(by.css('#cm-values .dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeType);
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeTypeOption);

            //value amount
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").val("asd")');
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").blur()');

            generalHelper.waitElementClickableAndClick(finalizeContractButton);

            var errors = modalWindow.all(by.css('.form-group.error'));
            expect(errors.count()).not.toEqual(0);

        });


        it('should fill mandatory fields and submit', function() {
            var tabGeneral = element(by.css('li[role="information"]'));
            generalHelper.waitElementClickableAndClick(tabGeneral);

            var name = config.inputs.getRandomName(10).replace(/ /g, '');
            //NOTA: tem que ser um cliente que exista e ja tenha sido adicionado anteriormente
            var client = 'aloevera';

            //title
            var titleInput = modalWindow.element(by.css('#title'));
            generalHelper.waitElementDisplayed(titleInput);
            titleInput.sendKeys('Contract ' + name);

            //reference
            var referenceInput = modalWindow.element(by.css('#reference'));
            referenceInput.sendKeys('REF' + name);

            //select category
            var selectpickerCategory = modalWindow.element(by.css('#cm-information [data-id="category"]'));
            var selectpickerCategoryOption = modalWindow.all(by.css('#cm-information .dropdown-menu.open')).get(1).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerCategory);
            generalHelper.waitElementClickableAndClick(selectpickerCategoryOption);
            

            var tabContractors = element(by.css('li[role="contractors"]'));
            generalHelper.waitElementClickableAndClick(tabContractors);

            //select client
            var clientSelectContainer = element(by.css('div.js-customer-private-customer.js-customer-private-customer-input'));
            var radioClient = element(by.css('div.js-customer-private-customer label.radio span'));
            generalHelper.waitElementClickableAndClick(radioClient);
            generalHelper.selectTypeAheadItem(clientSelectContainer, 'input#toPrivate', client, 1);



            var tabDates = element(by.css('li[role="dates"]'));
            generalHelper.waitElementClickableAndClick(tabDates);


            //Specificites Date1

            var checkboxDate1 = modalWindow.all(by.css('td.values label.checkbox span')).get(0);
            generalHelper.waitElementClickableAndClick(checkboxDate1);

            var time = modalWindow.element(by.css('#duration'));
            time.sendKeys('1');

            var selectPickerDurationType = modalWindow.element(by.css('#cm-dates [data-id="durationUnitTimeType"]'));
            var selectPickerDurationOption = modalWindow.all(by.css('#cm-dates .dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectPickerDurationType);
            generalHelper.waitElementClickableAndClick(selectPickerDurationOption);

            //Specificites Date2

            var checkboxDate2 = modalWindow.all(by.css('td.values label.checkbox span')).get(1);
            generalHelper.waitElementClickableAndClick(checkboxDate2);


            browser.sleep(1000);

            var renewalLimit = modalWindow.element(by.css('#renewalLimit'));
            renewalLimit.sendKeys('1');

            browser.sleep(1000);

            var cancelLimit = modalWindow.element(by.css('#cancelLimit'));
            cancelLimit.sendKeys('1');
            

            var tabValues = element(by.css('li[role="values"]'));
            generalHelper.waitElementClickableAndClick(tabValues);

            //payment interval
            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").val(30)');
            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").blur()');

            //select payment interval unit time type :: Opcao selecionada "DAYS"
            var selectpickerPaymentIntervalUnitTimeType = modalWindow.element(by.css('#cm-values [data-id="paymentIntervalUnitTimeType"]'));
            var selectpickerPaymentIntervalUnitTimeTypeOption = modalWindow.all(by.css('#cm-values .dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeType);
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeTypeOption);

            //value amount
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").val(100)');
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").blur()');
            

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(finalizeContractButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });
            
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});