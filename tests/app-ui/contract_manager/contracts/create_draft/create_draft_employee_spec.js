describe('Contract Manager', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Contracts', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            saveDraftButton,
            recordCountEmployee;

        beforeEach(function() {
            menuIcon = element(by.css('#contract-manager-module'));
            tabItem = element(by.css('#contract-manager-menu-region [data-sec="contracts"]'));
            newButton = element(by.css('.js-new-draft'));
            modalWindow = element(by.css('#cm-draft-create-modal'));
            saveDraftButton = modalWindow.element(by.css('.btn-success.ok'));

        });


        it('login employee', function () {

            loginHelper.changeRoleTo('EMPLOYEE');

            generalHelper.returnToDashboard();

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);
            
            generalHelper.waitElementClickableAndClick(tabItem);

            var recordsEmployee = element(by.css('.js-total-res span'));
            recordCountEmployee = 0;
            recordsEmployee.getText().then(function(text) {
                recordCountEmployee = parseInt(text);
            });

        });

        it('should open new draft modal window', function () {

            loginHelper.changeRoleTo('MANAGER');

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);
            
            generalHelper.waitElementClickableAndClick(newButton);

            browser.sleep(1000);
            
            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(1000);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {
            
            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(saveDraftButton);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(5);
        });

        it('should test field restrictions', function () {
            var tabValues = element(by.css('li[role="values"]'));
            
            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(tabValues);

            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").val("asd")');
            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").blur()');

            //select payment interval unit time type :: Opcao selecionada "DAYS"
            var selectpickerPaymentIntervalUnitTimeType = modalWindow.element(by.css('#cm-values [data-id="paymentIntervalUnitTimeType"]'));
            var selectpickerPaymentIntervalUnitTimeTypeOption = modalWindow.all(by.css('#cm-values .dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeType);
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeTypeOption);

            //value amount
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").val("asd")');
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").blur()');

            generalHelper.waitElementClickableAndClick(saveDraftButton);

            var errors = modalWindow.all(by.css('.form-group.error'));
            expect(errors.count()).not.toEqual(0);

        });

        it('should fill mandatory fields and submit', function() {
            var tabGeneral = element(by.css('li[role="information"]'));
            
            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(tabGeneral);


            var name = config.inputs.getRandomName(10).replace(/ /g, '');
            //NOTA: tem que ser um cliente que exista e ja tenha sido adicionado anteriormente
            var employee = 'polo';

            //title
            var titleInput = modalWindow.element(by.css('#title'));
            generalHelper.waitElementDisplayed(titleInput);
            titleInput.sendKeys('Contract ' + name);

            //reference
            var referenceInput = modalWindow.element(by.css('#reference'));
            referenceInput.sendKeys('REF' + name);

            var tabContractors = element(by.css('li[role="contractors"]'));
            generalHelper.waitElementClickableAndClick(tabContractors);

            //select employee
            var employeeSelectContainer = element(by.css('div.js-customer-employee.js-customer-employee-input'));
            var radioEmployee = element(by.css('div.js-customer-employee label.radio span'));
            generalHelper.waitElementClickableAndClick(radioEmployee);
            generalHelper.selectTypeAheadItem(employeeSelectContainer, 'input#toEmployee', employee, 1);

            var tabValues = element(by.css('li[role="values"]'));
            generalHelper.waitElementClickableAndClick(tabValues);

            //payment interval
            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").val(40)');
            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").blur()');

            //select payment interval unit time type :: Opcao selecionada "DAYS"
            var selectpickerPaymentIntervalUnitTimeType = modalWindow.element(by.css('#cm-values [data-id="paymentIntervalUnitTimeType"]'));
            var selectpickerPaymentIntervalUnitTimeTypeOption = modalWindow.all(by.css('#cm-values .dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeType);
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeTypeOption);

            //value amount
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").val(200)');
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").blur()');

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(saveDraftButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(recordCount++);
            });

        });

        it('should check addiction contracts', function () {
            loginHelper.changeRoleTo('EMPLOYEE');

            generalHelper.returnToDashboard();

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var recordsEmployee = element(by.css('.js-total-res span'));
            recordsEmployee.getText().then(function(text) {
                expect(parseInt(text)).toEqual(recordCountEmployee);
            });

            loginHelper.changeRoleTo('MANAGER');
        });

    });

});