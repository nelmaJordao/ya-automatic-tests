describe('Contract Manager', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Contracts', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            companyModal,
            saveDraftButton;


        beforeEach(function() {
            menuIcon = element(by.css('#contract-manager-module'));
            tabItem = element(by.css('#contract-manager-menu-region [data-sec="contracts"]'));
            newButton = element(by.css('.js-new-draft'));
            modalWindow = element(by.css('#cm-draft-create-modal'));
            companyModal = element(by.css('#company-create-modal'));
            saveDraftButton = modalWindow.element(by.css('.btn-success.ok'));
        });

        it('should open new draft modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);
            
            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);
            
            generalHelper.waitElementClickableAndClick(newButton);

            browser.sleep(1000);
            
            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(1000);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {

            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(saveDraftButton);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).not.toEqual(0);
        });

        it('should test field restrictions', function () {
            var tabValues = element(by.css('li[role="values"]'));
            generalHelper.waitElementClickableAndClick(tabValues);

            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").val("asd")');
            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").blur()');

            //select payment interval unit time type :: Opcao selecionada "DAYS"
            var selectpickerPaymentIntervalUnitTimeType = modalWindow.element(by.css('#cm-values [data-id="paymentIntervalUnitTimeType"]'));
            var selectpickerPaymentIntervalUnitTimeTypeOption = modalWindow.all(by.css('#cm-values .dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeType);
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeTypeOption);

            //value amount
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").val("asd")');
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").blur()');

            generalHelper.waitElementClickableAndClick(saveDraftButton);

            var errors = modalWindow.all(by.css('.form-group.error'));
            expect(errors.count()).not.toEqual(0);

        });


        it('should fill mandatory fields and submit', function() {
            var tabGeneral = element(by.css('li[role="information"]'));
            generalHelper.waitElementClickableAndClick(tabGeneral);

            var name = config.inputs.getRandomName(10).replace(/ /g, '');
            //NOTA: tem que ser um cliente que exista e ja tenha sido adicionado anteriormente

            //title
            var titleInput = modalWindow.element(by.css('#title'));
            generalHelper.waitElementDisplayed(titleInput);
            titleInput.sendKeys('Contract ' + name);

            //reference
            var referenceInput = modalWindow.element(by.css('#reference'));
            referenceInput.sendKeys('REF' + name);

            //select category
            var selectpickerCategory = modalWindow.element(by.css('#cm-information [data-id="category"]'));
            var selectpickerCategoryOption = modalWindow.all(by.css('#cm-information .dropdown-menu.open')).get(1).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerCategory);
            generalHelper.waitElementClickableAndClick(selectpickerCategoryOption);

            var tabContractors = element(by.css('li[role="contractors"]'));
            generalHelper.waitElementClickableAndClick(tabContractors);

            //select client
            var clientSelectContainer = element(by.css('div.js-customer-private-customer.js-customer-private-customer-input'));
            var radioClient = element(by.css('div.js-customer-private-customer label.radio span'));
            generalHelper.waitElementClickableAndClick(radioClient);
            generalHelper.selectTypeAheadCreateNew(clientSelectContainer, 'input#toPrivate');

            browser.sleep(1000);

            var name = companyModal.element(by.css('#name'));
            name.clear().sendKeys('armanda');



            var successButton = companyModal.element(by.xpath('/html/body/div[3]/div[2]/div/div[3]/button[2]'));
            generalHelper.waitElementClickableAndClick(successButton);
            

            var tabValues = element(by.css('li[role="values"]'));
            generalHelper.waitElementClickableAndClick(tabValues);

            //payment interval
            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").val(30)');
            browser.executeScript('$("#cm-draft-create-modal #paymentInterval").blur()');

            //select payment interval unit time type :: Opcao selecionada "DAYS"
            var selectpickerPaymentIntervalUnitTimeType = modalWindow.element(by.css('#cm-values [data-id="paymentIntervalUnitTimeType"]'));
            var selectpickerPaymentIntervalUnitTimeTypeOption = modalWindow.all(by.css('#cm-values .dropdown-menu.open')).get(0).element(by.css('li[data-original-index="1"]'));
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeType);
            generalHelper.waitElementClickableAndClick(selectpickerPaymentIntervalUnitTimeTypeOption);

            //value amount
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").val(100)');
            browser.executeScript('$("#cm-draft-create-modal #valueAmount").blur()');
            

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(saveDraftButton);
            

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(recordCount++);
            });
            
        });
        
        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});