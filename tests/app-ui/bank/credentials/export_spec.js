describe('Bank', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Accounts', function ()
    {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstExportButton,
            menuIconDocs;


        beforeEach(function()
        {
            menuIcon = element(by.css('#bank-module'));
            tabItem = element(by.css('#bank-menu-region [data-sec="credentials"]'));
            firstExportButton = element.all(by.css('#bank-credentials-dtable a.js-export-credential')).get(1);
            menuIconDocs = element(by.css('#doc-viewer-module'));
        });

        it('should click exportButton', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            generalHelper.waitElementClickableAndClick(tabItem);
            
            browser.sleep(1000);
            
            generalHelper.waitElementClickableAndClick(firstExportButton);

            browser.sleep(1000);
            
            generalHelper.waitAlertSuccessPresent();
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

        it('should open documents module', function () {
            generalHelper.waitElementClickableAndClick(menuIconDocs);

            var recentDocsButton= element(by.css('div.dropdown a.js-latestfiles'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(recentDocsButton);

            var firstDoc = element.all(by.css('div.dropdown ul.docs-dashboard-dropdown li a h6')).get(0);


            firstDoc.getText().then(function(text){
                expect(text).toContain('.ofx');
            })
        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});