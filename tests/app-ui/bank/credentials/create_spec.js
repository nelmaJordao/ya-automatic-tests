describe('Bank', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    var bankAccount = { bankName : 'Linxo Test Bank',
                        username : '123456',
                        password : 'password' };

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Credentials', function () {
        
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButton;
        
        beforeEach(function()
        {
            menuIcon = element(by.css('#bank-module'));
            tabItem = element(by.css('#bank-menu-region [data-sec="credentials"]'));
            newButton = element(by.css('.js-new-credential'));
            modalWindow = element(by.css('#credential-create-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
        });

        it('should open new credentials modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(newButton);

            browser.sleep(1000);

            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });

        /*
        it('should check for empty fields', function ()
        {
            generalHelper.waitElementClickableAndClick(okButton);
            
            var errors = modalWindow.all(by.css('.form-group.error'));
            
            expect(errors.count()).toEqual(3);
        });
        */
        
        it('should fill mandatory fields and submit', function()
        {
            // verificar se foi bem introduzido
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            // Escolher instituicao bancaria
            var bankNameInput = modalWindow.element(by.css('#credential-create-modal [data-id="bank"]'));
            var bankName = element(by.css('div.bs-searchbox input.input-block-level'));
            var bankli = element.all(by.css('div.dropdown-menu ul.dropdown-menu li[data-original-index="71"]'));

            generalHelper.waitElementClickableAndClick(bankNameInput);
            bankName.sendKeys('Linxo Test Bank');
            generalHelper.waitElementClickableAndClick(bankli);
            
            
            
            // Preencher username do banco
            var userInput = modalWindow.element(by.css('#value'));
            userInput.sendKeys( bankAccount.username );
            
            // Preencher passe do banco
            var passInput = modalWindow.element(by.css('#label'));
            passInput.sendKeys( bankAccount.password );

            var btnConnection = modalWindow.element(by.css('.btn-success.js-account-specifics-submit-btn'));

            generalHelper.waitElementClickableAndClick(btnConnection);
            
            browser.sleep(25000);

            var table = modalWindow.element(by.css('.table.table-striped'));

            var lastRow = table.all(by.css('tr')).last();

            var lastRowLastTd = lastRow.all(by.css('td')).last();

            var checkbox = lastRowLastTd.element(by.css('label.checkbox span'));

            browser.sleep(1000);

            checkbox.click();
            //generalHelper.waitElementClickableAndClick(checkbox);

            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(recordCount++);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });
        
});
