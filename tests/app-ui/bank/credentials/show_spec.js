describe('bank', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Credentials', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow;
        
        beforeEach(function()
        {
            menuIcon = element(by.css('#bank-module'));
            tabItem = element(by.css('#bank-menu-region [data-sec="credentials"]'));
            modalWindow = element(by.css('#credential-read-modal'));
        });

        it('should open show credentials modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);
            
            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            var firstShowButton = element.all(by.css('#bank-credentials-dtable a.js-show-credential')).get(1);
            generalHelper.waitElementClickableAndClick(firstShowButton);

            browser.sleep(1000);
            
            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(1000);
            
            expect(modalWindow.isDisplayed()).toBe(true);

        });


        it('should check for empty fields', function ()
        {
            //nome do banco
            var bankName = modalWindow.all(by.css('.form-group p.value')).get(0);
            expect(bankName.getText()).not.toEqual('');
            
            //nome da agencia
            var agence = modalWindow.all(by.css('.form-group p.value')).get(1);
            expect(agence.getText()).not.toEqual('');
            
            //acronimo
            var alias = modalWindow.all(by.css('.form-group p.value')).get(2);
            expect(alias.getText()).not.toEqual('');
        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default'));
            generalHelper.waitElementClickableAndClick(closeButton);
            
            expect(modalWindow.isPresent()).toBe(false);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });
    
});