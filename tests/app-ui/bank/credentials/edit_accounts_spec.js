describe('Bank', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Credentials', function ()
    {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstEditButton,
            modalWindow,
            okButton,
            passwordInput,
            closeButton;

        beforeEach(function()
        {
            menuIcon = element(by.css('#bank-module'));
            tabItem = element(by.css('#bank-menu-region [data-sec="credentials"]'));
            firstEditButton = element.all(by.css('#bank-credentials-dtable a.js-edit-credential-accounts')).get(1);
            modalWindow = element(by.css('#credential-update-accounts-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            closeButton = modalWindow.element(by.css('.btn-default.cancel'));

            // campos para limpar e depois preencher
            passwordInput = modalWindow.element(by.css('#Password'));
        });

        it('should open edit credentials modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(firstEditButton);

            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(1000);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should close and reopen edit credentials modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(closeButton);
            
            browser.sleep(1000);
            
            generalHelper.waitElementClickableAndClick(firstEditButton);

            browser.sleep(1000);
            
            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(1000);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should fill mandatory fields and submit', function()
        {
            browser.sleep(1000);

            // editar username do banco
            passwordInput.sendKeys('password');

            var btnConnection = modalWindow.element(by.css('.js-account-specifics-submit-btn'));

            generalHelper.waitElementClickableAndClick(btnConnection);

            browser.sleep(22000);

            var table = modalWindow.element(by.css('.table.table-striped'));

            var secondRow = table.all(by.css('tr')).get(1);

            var secondRowLastTd = secondRow.all(by.css('td')).last();

            var checkbox = secondRowLastTd.element(by.css('label.checkbox span'));

            generalHelper.waitElementClickableAndClick(checkbox);
            //checkbox.click();

            generalHelper.waitElementClickableAndClick(okButton);

            //generalHelper.waitAlertSuccessPresentAndClose();
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});