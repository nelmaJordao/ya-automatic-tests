describe('Bank', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Credentials', function ()
    {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstEditButton,
            modalWindow,
            nameInput,
            okButton,
            closeButton;

        beforeEach(function()
        {
            menuIcon = element(by.css('#bank-module'));
            tabItem = element(by.css('#bank-menu-region [data-sec="credentials"]'));
            firstEditButton = element.all(by.css('#bank-credentials-dtable a.js-edit-credential')).get(1);
            modalWindow = element(by.css('#credential-edit-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            // campos para limpar e depois preencher
            nameInput = modalWindow.element(by.css('#name'));
        });
        
        it('should open edit credentials modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            
            browser.sleep(1000);
            
            generalHelper.waitElementClickableAndClick(firstEditButton);
            
            browser.sleep(1000);
            
            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(1000);
            
            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should close and reopen edit credentials modal window', function ()
        {
            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(firstEditButton);

            browser.sleep(1000);

            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(1000);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function ()
        {
            nameInput.clear();
            generalHelper.waitElementClickableAndClick(okButton);

            browser.sleep(1000);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(1);
        });


        it('should fill mandatory fields and submit', function()
        {//
            // editar username do banco
            nameInput.sendKeys('Editted Linxo Test Bank');

            generalHelper.waitElementClickableAndClick(okButton);
            //generalHelper.waitAlertSuccessPresentAndClose();
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });
    
});