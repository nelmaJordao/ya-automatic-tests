describe('Bank', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Credentials', function ()
    {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstEditButton,
            modalWindow,
            okButton,
            codeInput,
            passwordInput,
            closeButton;

        beforeEach(function()
        {
            menuIcon = element(by.css('#bank-module'));
            tabItem = element(by.css('#bank-menu-region [data-sec="credentials"]'));
            firstEditButton = element.all(by.css('#bank-credentials-dtable a.js-update-credential')).get(1);
            modalWindow = element(by.css('#credential-update-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            // campos para limpar e depois preencher
            codeInput = modalWindow.element(by.css('#value'));
            passwordInput = modalWindow.element(by.css('#label'));
        });

        it('should open edit credentials modal window', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(firstEditButton);

            browser.sleep(1000);

            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(1000);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should close and reopen edit credentials modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(firstEditButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function ()
        {
            //limpar campos

            generalHelper.waitElementClickableAndClick(okButton);

            var errors = modalWindow.all(by.css('.alert.alert-danger'));

            expect(errors.count()).toEqual(1);
        });


        it('should fill mandatory fields and submit', function()
        {
            browser.sleep(1000);

            // editar username do banco
            codeInput.sendKeys('123456');
            passwordInput.sendKeys('password');

            generalHelper.waitElementClickableAndClick(okButton);
            //generalHelper.waitAlertSuccessPresentAndClose();
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});