describe('Bank', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    // variavel para as credenciais do banco
    //var bankAccount = { bankName : 'Demonstration Bank 2',
        //username : "edit_user",
        //password : "edit_passe" };


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Accounts', function ()
    {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstEditButton,
            modalWindow,
            okButton,
            closeButton,
            nameInput;


        beforeEach(function()
        {
            menuIcon = element(by.css('#bank-module'));
            tabItem = element(by.css('#bank-menu-region [data-sec="accounts"]'));
            firstEditButton = element.all(by.css('#bank-accounts-dtable a.js-edit-account')).get(0);
            modalWindow = element(by.css('#account-update-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            closeButton = modalWindow.element(by.css('.btn-default.cancel'));

            // campos para limpar e depois preencher
            nameInput = modalWindow.element(by.css('#name'));
        });

        it('should open edit accounts modal window', function () {
            
            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(menuIcon);
            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(firstEditButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should close and reopen edit accounts modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(firstEditButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should fill mandatory fields and submit', function()
        {
            // editar/preencher acronimo da conta
            nameInput.clear();
            nameInput.sendKeys('Test Name');
            

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            var res = firstEditButton.element(by.xpath("../..")).all(by.css('td')).get(1);

            res.getText().then(function(text) {
                expect(text).toEqual('Test Name');
            });
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});