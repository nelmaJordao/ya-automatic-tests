describe('bank', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('Accounts', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow;
        
        beforeEach(function()
        {
            menuIcon = element(by.css('#bank-module'));
            tabItem = element(by.css('#bank-menu-region [data-sec="accounts"]'));
            modalWindow = element(by.css('#account-read-modal'));
        });

        it('should open show accounts modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            var firstShowButton = element.all(by.css('#bank-accounts-dtable a.js-show-account')).get(0);
            generalHelper.waitElementClickableAndClick(firstShowButton);

            generalHelper.waitElementPresent(modalWindow);
            
            expect(modalWindow.isDisplayed()).toBe(true);

        });


        it('should check for empty fields', function ()
        {
            //nome do banco
            var bankName = modalWindow.all(by.css('.form-group p.value')).get(1);
            expect(bankName.getText()).not.toEqual('');
        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default'));
            generalHelper.waitElementClickableAndClick(closeButton);
            
            expect(modalWindow.isPresent()).toBe(false);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });
    
});