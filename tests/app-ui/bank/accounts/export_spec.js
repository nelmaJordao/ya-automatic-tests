describe('Bank', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Accounts', function ()
    {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstExportButton,
            menuIconDocs;


        beforeEach(function()
        {
            menuIcon = element(by.css('#bank-module'));
            tabItem = element(by.css('#bank-menu-region [data-sec="accounts"]'));
            firstExportButton = element.all(by.css('#bank-accounts-dtable a.js-export-account')).get(0);
            menuIconDocs = element.all(by.css('.nav .js-app-menu-item')).get(8);
        });

        it('should click exportButton', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);


            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(firstExportButton);

            browser.sleep(2000);

            generalHelper.waitAlertSuccessPresent();
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

        it('should open documents module', function () {

            generalHelper.waitElementClickableAndClick(menuIconDocs);

            browser.sleep(1000);

            var recentDocsButton= element(by.css('div.dropdown a.js-latestfiles'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(recentDocsButton);

            var firstDoc = element.all(by.css('div.dropdown ul.docs-dashboard-dropdown li a h6')).get(0);

            browser.sleep(1000);

            firstDoc.getText().then(function(text){
                expect(text).toContain('.ofx');
            })
        });


        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});