describe('Bank', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Connectors', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            searchField,
            searchButton;

        beforeEach(function() {
            menuIcon = element(by.css('#bank-module'));
            tabItem = element(by.css('#bank-menu-region [data-sec="connectors"]'));
            searchField = element(by.css('#searchTable'));
            searchButton = element(by.css('.js-search-btn'));
        });

        it('should search', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);



            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            searchField.clear();
            searchField.sendKeys('demo');

            generalHelper.waitElementClickableAndClick(searchButton);

            browser.sleep(5000);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});