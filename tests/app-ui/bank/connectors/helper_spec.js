describe('Bank', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Connectors', function () {
        //declarar variaveis necessarias em mais do que um teste
        var
            menuIcon,
            tabItem,
            continueButton,
            closeButton,
            helperModal,
            helperButton;


        beforeEach(function() {
            menuIcon = element(by.css('#bank-module'));
            tabItem = element(by.css('#bank-menu-region [data-sec="connectors"]'));
            helperButton = element(by.css('a.uihelper-btn'));
            helperModal = element(by.css('#void-ui-helper'));
            continueButton = helperModal.element(by.css('button.pull-right'));
            closeButton = helperModal.element(by.css('button.pull-left'));
        });

        it('should use the helper', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);



            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(helperButton);
            generalHelper.waitElementPresent(helperModal);

            expect(helperModal.isPresent()).toBe(true);

            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(continueButton);

            browser.sleep(1000);
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(helperModal.isPresent()).toBe(false);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});
