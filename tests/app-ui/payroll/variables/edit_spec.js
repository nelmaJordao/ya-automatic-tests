describe('Payroll', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Variables', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            firstRow,
            editButton,
            modalWindow,
            cancelButton;

        beforeEach(function() {
            menuIcon = element(by.css('#payroll-module'));
            firstRow = element.all(by.css('#payroll-variables-dtable tr')).first();
            editButton = firstRow.element(by.css('td.options a.js-edit-variable'));
            modalWindow = element(by.css('#employee-modal'));
            cancelButton = modalWindow.element(by.css('.btn-default.cancel'));
        });

        it('should open variable modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

        });

        it('should cancel edit expense', function () {
            var cancelButton = element(by.css('button.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            browser.sleep(1000);
        });

        it('should open variable modal window', function () {
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should change values and submit variable', function () {
            var value1Input = modalWindow.element(by.css('#dyn_5'));
            var value2Input = modalWindow.element(by.css('#dyn_6'));
            var value3Input = modalWindow.element(by.css('#dyn_7'));
            var value4Input = modalWindow.element(by.css('#dyn_8'));
            var saveButton = modalWindow.element(by.css('.btn-success.ok'));

            var value1 = Math.floor(Math.random() * 10).toString();
            var value2 = Math.floor(Math.random() * 10).toString();
            var value3 = Math.floor(Math.random() * 10).toString();
            var value4 = Math.floor(Math.random() * 10).toString();

            value1Input.clear();
            value1Input.sendKeys(value1);

            value2Input.clear();
            value2Input.sendKeys(value2);

            value3Input.clear();
            value3Input.sendKeys(value3);

            value4Input.clear();
            value4Input.sendKeys(value4);

            generalHelper.waitElementClickableAndClick(saveButton);

            generalHelper.waitAlertSuccessPresentAndClose();

            var value1td = firstRow.all(by.css('td')).get(3);
            var value2td = firstRow.all(by.css('td')).get(4);
            var value3td = firstRow.all(by.css('td')).get(5);
            var value4td = firstRow.all(by.css('td')).get(6);

            expect(value1td.getText()).toEqual(value1);
            expect(value2td.getText()).toEqual(value2);
            expect(value3td.getText()).toEqual(value3);
            expect(value4td.getText()).toEqual(value4);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});