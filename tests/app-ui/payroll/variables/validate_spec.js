describe('Payroll', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Variables', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            firstRow,
            validateButton;

        beforeEach(function() {
            menuIcon = element(by.css('#payroll-module'));
            firstRow = element.all(by.css('#payroll-variables-dtable tr')).first();
            validateButton = firstRow.element(by.css('td.options a.js-validate-variable'));
        });

        it('should validate variable', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(validateButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            expect(firstRow.getAttribute('class')).toMatch('success');

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});