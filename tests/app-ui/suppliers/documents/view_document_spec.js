describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            firstRow,
            viewDocButton,
            modalWindow,
            closeButton;


        beforeEach(function() {
            menuIcon = element(by.css('#suppliers-module'));
            firstRow = element.all(by.css('#suppliers-documents-dtable tr')).get(0);
            viewDocButton = firstRow.element(by.css('td.options a.js-show-doc'));
            modalWindow = element(by.css('#document-read-modal'));
            closeButton = modalWindow.element(by.css('div.modal-footer button.cancel'))
        });

        it('should open document modal', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(viewDocButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(closeButton);

            });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});