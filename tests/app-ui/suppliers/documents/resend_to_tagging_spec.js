describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIconAchats,
            menuIconTraitement,
            dashboardButton,
            modalWindow;


        beforeEach(function() {
            menuIconAchats = element(by.css('#suppliers-module'));
            menuIconTraitement = element(by.css('#doc-tagging-module'));
            modalWindow = element(by.css('.modal-content'));
            dashboardButton = element(by.css('.js-brand'));

        });

        it('should open Achats Window', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIconAchats);

            browser.sleep(1000);

        });

        it('should resend document', function () {

            browser.sleep(2000);

            var resendButton = element.all(by.css('.js-delete-doc')).get(0);

            generalHelper.waitElementClickableAndClick(resendButton);

            browser.sleep(1000);

            var okResendButton = element(by.css('#invoicing-delete-invoice-modal .btn-success'));

            generalHelper.waitElementClickableAndClick(okResendButton);

            browser.sleep(1500);

        });

        it('should open Traitement Window', function ()
        {

            generalHelper.waitElementClickableAndClick(dashboardButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(menuIconTraitement);

            browser.sleep(1000);

        });


        it('should send document as avoir', function () {

            var traitementModal = element(by.css('#document-tagging-edit-document-modal'));

            var typeDocument = traitementModal.element(by.css('.dropdown-toggle'));

            generalHelper.waitElementClickableAndClick(typeDocument);

            var selectTypeDocument = traitementModal.all(by.css('.dropdown-menu li')).get(1);

            generalHelper.waitElementClickableAndClick(selectTypeDocument);

            browser.sleep(2000);

            var saveButton = element(by.css('.js-control-actions .btn-success'));
            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(1000);
        });

        it('should open Achats Window', function ()
        {

            generalHelper.waitElementClickableAndClick(dashboardButton);

            browser.sleep(2000);

            generalHelper.waitElementClickableAndClick(menuIconAchats);

        });

        it('should open Avoirs Window', function ()
        {
            browser.sleep(2000);

            var avoirsTab = element.all(by.css('.js-menu li')).get(1);
            generalHelper.waitElementClickableAndClick(avoirsTab);

            browser.sleep(1000);

        });


        it('should resend document', function () {

            browser.sleep(2000);

            var resendButton = element.all(by.css('.js-delete-doc')).get(0);
            generalHelper.waitElementClickableAndClick(resendButton);

            browser.sleep(1000);

            var okResendButton = element(by.css('#invoicing-delete-invoice-modal .btn-success'));
            generalHelper.waitElementClickableAndClick(okResendButton);

            browser.sleep(1500);

        });

        it('should open Traitement Window', function ()
        {
            generalHelper.waitElementClickableAndClick(dashboardButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(menuIconTraitement);

            browser.sleep(1000);

        });

        it('should send document as facture', function () {

            browser.sleep(2000);

            var traitementModal = element(by.css('#document-tagging-edit-document-modal'));
            var typeDocument = traitementModal.element(by.css('.dropdown-toggle'));
            generalHelper.waitElementClickableAndClick(typeDocument);

            browser.sleep(2000);

            var selectTypeDocument = traitementModal.all(by.css('.dropdown-menu li')).get(0);

            generalHelper.waitElementClickableAndClick(selectTypeDocument);

            browser.sleep(2000);

            var saveButton = element(by.css('.js-control-actions .btn-success'));
            generalHelper.waitElementClickableAndClick(saveButton);
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});
