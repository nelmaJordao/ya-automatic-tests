describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();
    
    describe('payment documents', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow;
        
        beforeEach(function()
        {
            menuIcon = element(by.css('#suppliers-module'));
            tabItem = element(by.css('#suppliers-menu-region [data-sec="paymentdocuments"]'));
            modalWindow = element(by.css('#suppliers-delete-payment-document-modal'));
        });
        
        
        it('should open delete payment document modal window', function ()
        {
            var firstDeleteButton = element.all(by.css('#suppliers-payment-documents-dtable a.js-delete-payment-document')).get(0);

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000)

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(firstDeleteButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        
        it('should confirm delete modal window', function()
        {
            var confirmDeleteButton = modalWindow.element(by.css('.btn-success'));
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(confirmDeleteButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text)
            {
                expect(parseInt(text)).toEqual(--recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
      
    });
    
});