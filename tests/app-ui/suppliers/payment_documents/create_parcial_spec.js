describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('payment documents', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButton,
            faturesTabItem;

        beforeEach(function()
        {
            menuIcon = element(by.css('#suppliers-module'));
            tabItem = element(by.css('#suppliers-menu-region [data-sec="paymentdocuments"]'));
            newButton = element(by.css('.js-new-payment-document'));
            modalWindow = element(by.css('#suppliers-create-payment-document-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ladda-button.ok'));
            faturesTabItem = element(by.css('#suppliers-menu-region [data-sec="documents"]'));
        });

        it('should open new payment document modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

        });

        it('should fill mandatory fields and submit', function()
        {
            // tem que existir decumentos tagados para esta entidade senão o script não funciona !!!
            var supplier = "JACA";

            //selecionar o fornecedor
            var supplierSelectContainer = element(by.css('div[data-key="supplier"]'));
            generalHelper.selectTypeAheadItem(supplierSelectContainer, 'input#company', supplier, 1);

            //selecionar o metodo de pagamento
            var selectpickerItemPaymentMethod = modalWindow.element(by.css('[data-id="paymentMethod"]'));
            var selectpickerItemPaymentMethodOption = modalWindow.all(by.css('.dropdown-menu.open')).get(0).element(by.css('li[data-original-index="5"]'));
            generalHelper.waitElementClickableAndClick(selectpickerItemPaymentMethod);
            generalHelper.waitElementClickableAndClick(selectpickerItemPaymentMethodOption);

            var numberInput = modalWindow.element(by.css('#paymentNumber'));
            numberInput.sendKeys('123456789');

            // Seleciona apenas um documento do fornecedor especificado
            var checkFirstInput = modalWindow.all(by.css('#payment-document-form-items tr input.js-pay')).last();
            generalHelper.waitElementClickableAndClick(checkFirstInput);

            var money = modalWindow.all(by.css('.js-invoice-value-pay')).last();
            money.clear();
            money.sendKeys('1');

            // verificar se foi bem introduzido
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });

        });

        it('should check facture state', function()
        {
            generalHelper.waitElementClickableAndClick(faturesTabItem);

            browser.sleep(1000);

            var searchButton = element(by.css('.js-search-btn'));

            var search = element(by.css('#searchTable'));
            search.sendKeys('BIP');

           generalHelper.waitElementClickableAndClick(searchButton);

            browser.sleep(1000);

            var payment = 'PARTIELLEMENT RÉGLÉ';

            var secondRow = element.all(by.css('#suppliers-documents-dtable tr')).get(0);

            var etat = secondRow.element(by.xpath("../..")).all(by.css('td')).get(5);

            etat.getText().then(function(text) {
                expect(text).toEqual(payment);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});