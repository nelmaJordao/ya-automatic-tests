describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('payment documents', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow;

        beforeEach(function()
        {
            menuIcon = element(by.css('#suppliers-module'));
            tabItem = element(by.css('#suppliers-menu-region [data-sec="paymentdocuments"]'));
            modalWindow = element(by.css('#suppliers-read-payment-document-modal'));
        });

        it('should open show payment document modal window', function ()
        {
            var firstShowButton = element.all(by.css('#suppliers-payment-documents-dtable a.js-show-payment-document')).get(0);

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(firstShowButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

        });

        it('should check for empty fields', function ()
        {
            //nome do fornecedor
            var supplierName = modalWindow.all(by.css('.form-group h3.name')).get(0);
            expect(supplierName.getText()).not.toEqual('nome');

            //data
            var date = modalWindow.all(by.css('.form-group p.value')).get(0);
            expect(date.getText()).not.toEqual('data');

            //metodo de pagamento
            var paymentMethod = modalWindow.all(by.css('.form-group p.value')).get(1);
            expect(paymentMethod.getText()).not.toEqual('metodo');

            browser.sleep(1000);
            
            //verificar se o número de item e superior a 0
            var numberOfItems = modalWindow.all(by.css('.table-striped.table-hover tbody tr'));
            numberOfItems.count().then(function( number )
            {
                expect(number).toBeGreaterThan(0);
            });

        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});