describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('payment documents', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow,
            rowList,
            sendMailButton,
            resendMailButton,
            emailInput;

        beforeEach(function() {
            menuIcon = element(by.css('#suppliers-module'));
            tabItem = element(by.css('#suppliers-menu-region [data-sec="paymentdocuments"]'));
            modalWindow = element(by.css('#payment-document-send-email'));

            var tBody = element(by.css('#suppliers-payment-documents-dtable'));
            rowList = tBody.all(by.css('tr'));
            sendMailButton = rowList.get(0).element(by.css('.js-send-payment-document'));
            resendMailButton = rowList.get(0).element(by.css('.js-send-payment-document i.icons8-rotate1'));
            emailInput = modalWindow.element(by.css('#email'));
        });

        it('should open send email modal', function ()
        {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(sendMailButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should send email', function ()
        {
            var email = config.inputs.getRandomName(7, false)  + '@xpto.biz';
            var sendButton = modalWindow.element(by.css('.btn-success.ok'));
            emailInput.clear().sendKeys(email);

            generalHelper.waitElementClickableAndClick(sendButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            browser.refresh();
        });

        it('should see email filled when resending', function ()
        {
            generalHelper.waitElementClickableAndClick(resendMailButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

            generalHelper.waitElementPresent(emailInput);
            generalHelper.waitElementDisplayed(emailInput);

            expect(emailInput.getAttribute('value')).not.toEqual('');

            var okButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(okButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});