describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Suppliers', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow,
            okButton,
            lastRow;

        beforeEach(function() {
            menuIcon = element(by.css('#suppliers-module'));
            tabItem = element(by.css('#suppliers-menu-region [data-sec="suppliers"]'));
            modalWindow = element(by.css('#company-read-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            lastRow = element.all(by.css('#suppliers-suppliers-dtable tr')).first();
        });

        it('should open show family modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            var showButton = lastRow.element(by.css('.js-show-supplier'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(showButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {
            var name = modalWindow.element(by.css('h3.name'));
            expect(name.getText()).not.toEqual('');


        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-default.cancel'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});