describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Suppliers', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButton,
            emailInput,
            siteInput;

        beforeEach(function() {
            menuIcon = element(by.css('#suppliers-module'));
            tabItem = element(by.css('#suppliers-menu-region [data-sec="suppliers"]'));
            newButton = element(by.css('.js-new-supplier'));
            modalWindow = element(by.css('#company-create-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            emailInput = modalWindow.element(by.css('input#email'));
            siteInput = modalWindow.element(by.css('input#website'));
        });

        it('should open new family modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should test field restrictions', function () {
            var companyName = 'as';

            var nameInput = element(by.css('div.js-typeahead-company'));

            generalHelper.selectTypeAheadItem(nameInput, 'input#name', companyName, 1);

            var expandButton =  modalWindow.element(by.css('.js-collapse-fields-address'));
            generalHelper.waitElementClickableAndClick(expandButton);

            generalHelper.waitElementClickableAndClick(emailInput);
            emailInput.clear();
            siteInput.clear();

            emailInput.sendKeys('a');
            siteInput.sendKeys('a');

            var errors = modalWindow.all(by.css('.form-group.error'));
            expect(errors.count()).toEqual(2);
        });

        it('should fill mandatory fields and submit', function() {

            browser.sleep(1000);

            emailInput.clear();
            siteInput.clear();
            emailInput.sendKeys('email@xpto.biz');
            siteInput.sendKeys('www.xpto.biz');

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(recordCount++);
            });

            var saveButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});