describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Suppliers', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            modalWindow, 
            editButton,
            okButton,
            lastRow,
            emailInput,
            siteInput,
            cancelButton;

        beforeEach(function() {
            menuIcon = element(by.css('#suppliers-module'));
            tabItem = element(by.css('#suppliers-menu-region [data-sec="suppliers"]'));
            modalWindow = element(by.css('#company-update-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ladda-button.ok'));
            lastRow = element.all(by.css('#suppliers-suppliers-dtable tr')).first();
            emailInput = modalWindow.element(by.css('input#email'));
            siteInput = modalWindow.element(by.css('input#website'));
            cancelButton = modalWindow.element(by.css('.btn-default.cancel'));
        });

        it('should open edit family modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            editButton = lastRow.element(by.css('.js-edit-supplier'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should cancel edit expense', function () {
            var cancelButton = element(by.css('button.cancel'));
            generalHelper.waitElementClickableAndClick(cancelButton);

            browser.sleep(1000);
        });

        it('should open edit family modal window', function () {
            editButton = lastRow.element(by.css('.js-edit-supplier'));

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should test field restrictions', function () {

            emailInput.clear();
            emailInput.sendKeys('a');

            generalHelper.waitElementClickableAndClick(okButton);

            var errors = modalWindow.all(by.css('.form-group.error'));
            expect(errors.count()).not.toEqual(0);
        });

        it('should fill mandatory fields and submit', function() {

            var streetInput = modalWindow.element(by.css('#streetLine1'));
            streetInput.clear();
            streetInput.sendKeys("Test street");

            var addressCompleteInput = modalWindow.element(by.css('#streetLine2'));
            addressCompleteInput.clear();
            addressCompleteInput.sendKeys("Test address");

            var postalCodeInput = modalWindow.element(by.css('#postalCode'));
            postalCodeInput.clear();
            postalCodeInput.sendKeys("1234-567");

            var cityInput = modalWindow.element(by.css('#city'));
            cityInput.clear();
            cityInput.sendKeys("Test town");

            var telephoneInput = modalWindow.element(by.css('#phone'));
            telephoneInput.clear();
            telephoneInput.sendKeys("123456789");

            emailInput.clear();
            emailInput.sendKeys('email@xpto.biz');
            

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();
            
        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});