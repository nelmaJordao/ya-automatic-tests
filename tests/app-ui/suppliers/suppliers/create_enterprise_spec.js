describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var sirenHelper = requireCommonHelper('sirenHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Suppliers', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButton,
            inputTva,
            numeroTva,
            verificar;


        beforeEach(function() {
            menuIcon = element(by.css('#suppliers-module'));
            tabItem = element(by.css('#suppliers-menu-region [data-sec="suppliers"]'));
            newButton = element(by.css('.js-new-supplier'));
            modalWindow = element(by.css('#company-create-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ladda-button.ok'));
            verificar = modalWindow.element(by.css('.js-check-vat'));
            numeroTva = modalWindow.element(by.css('input#vatNumber'));
        });

        it('should open new family modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should fill mandatory fields and submit', function() {

            var container = element(by.css('div.js-typeahead-company'));

            generalHelper.selectTypeAheadCreateNew(container, 'input#name');

            inputTva = modalWindow.element(by.css('#radio-vat'));

            generalHelper.waitElementClickableAndClick(inputTva);

            numeroTva.sendKeys(sirenHelper.getTVA());

            generalHelper.waitElementClickableAndClick(verificar);

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });
});