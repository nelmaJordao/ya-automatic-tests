describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Suppliers', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            newButton,
            modalWindow,
            okButton,
            radioAutres,
            fiscalNumber,
            street,
            address,
            email,
            telephoneFixe,
            telephoneSecond,
            name;

        beforeEach(function() {
            menuIcon = element(by.css('#suppliers-module'));
            tabItem = element(by.css('#suppliers-menu-region [data-sec="suppliers"]'));
            newButton = element(by.css('.js-new-supplier'));
            modalWindow = element(by.css('#company-create-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ladda-button.ok'));
            fiscalNumber = modalWindow.element(by.css('#fiscalNumber'));
            street = modalWindow.element(by.css('#streetLine1'));
            address = modalWindow.element(by.css('#streetLine2'));
            email = modalWindow.element(by.css('#email'));
            telephoneFixe = modalWindow.element(by.css('#phone'));
            telephoneSecond = modalWindow.element(by.css('#phoneSecondary'));
            name = modalWindow.element(by.css('#name'));
        });

        it('should open new family modal window', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(tabItem);
            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should test field restrictions', function () {

            var container = element(by.css('div.js-typeahead-company'));
            generalHelper.selectTypeAheadCreateNew(container, 'input#name');

            radioAutres = element(by.css('#radio-private'));

            generalHelper.waitElementClickableAndClick(radioAutres);

            browser.sleep(2000);

            email.clear();

            email.sendKeys('a');


            var errors = modalWindow.all(by.css('.form-group.error'));
            expect(errors.count()).toEqual(1);
        });

        it('should fill mandatory fields and submit', function() {

            name.clear();
            name.sendKeys(config.inputs.getRandomName(10, true));

            var number = Math.floor(999999999 * Math.random());

            fiscalNumber.sendKeys(number);
            street.sendKeys('Rua Santo Antonio');
            address.sendKeys('Leiria');
            email.sendKeys('email@xpto.biz');
            telephoneFixe.sendKeys('244512376');
            telephoneSecond.sendKeys('244987654');

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });
});