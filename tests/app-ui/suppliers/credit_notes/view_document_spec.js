describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            tabItem,
            firstRow,
            viewDocButton,
            modalWindow,
            closeButton;


        beforeEach(function() {
            menuIcon = element(by.css('#suppliers-module'));
            tabItem = element(by.css('#suppliers-menu-region [data-sec="creditnotes"]'));
            firstRow = element.all(by.css('#suppliers-credit-notes-dtable tr')).get(0);
            viewDocButton = firstRow.element(by.css('td.options a.js-show-credit-note'));
            modalWindow = element(by.css('#creditnote-read-modal'));
            closeButton = modalWindow.element(by.css('div.modal-footer button.cancel'))
        });

        it('should open document modal', function () {
            
            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(tabItem);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(viewDocButton);

            expect(modalWindow.isDisplayed()).toBe(true);

            generalHelper.waitElementClickableAndClick(closeButton);

            });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});