describe('Info', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Info', function () {

        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            feedList;

        beforeEach(function() {
            menuIcon = element(by.css('#legal-info-module'));
            feedList = element(by.css('#feeds-list'));
        });

        it('should check first info date', function () {

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(5000);

            var dateTextField = feedList.all(by.css('div div.row div.legal-info-feed-item h5')).get(0);

            dateTextField.getText().then(function(text){
                var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
                var currDate = new Date();
                var dateArray = text.split(" ");
                var day = dateArray[0];
                var year = dateArray[2];

                var month = 0;
                for(var i = 0; i < 12; i++){
                    if(months[i] === dateArray[2]){
                        month = i;
                    }
                }

                var infoDate = new Date(year, month, day);
                var oneWeekAgo = new Date();
                oneWeekAgo.setDate(currDate.getDate() - 7);

                browser.sleep(1000);

                expect(infoDate < oneWeekAgo).toBe(true);
            });
        });


        it('should open info', function() {
            var openInfo = feedList.all(by.css('div div.row div.legal-info-feed-item a.js-show-details')).get(0);
            var infoTitleField = feedList.all(by.css('div div.row div.legal-info-feed-item a.js-show-details h3')).get(0);
            generalHelper.waitElementClickableAndClick(openInfo);

            var openInfoFeed = element(by.css('#feed-details-region'));
            var openInfoTitleField = openInfoFeed.all(by.css('div p.titre')).get(0);

            browser.sleep(1000);

            infoTitleField.getText().then(function(text){
                openInfoTitleField.getText().then(function(secondText){
                   expect(text.toLowerCase()).toEqual(secondText.toLowerCase());
                });
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});