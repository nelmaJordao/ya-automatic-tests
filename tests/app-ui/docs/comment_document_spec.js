describe('Docs', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            searchField,
            selectDoc,
            searchButton,
            randomDoc = Math.floor(Math.random() * 5);

        beforeEach(function() {
            menuIcon = element(by.css('#doc-viewer-module'));
            searchField = element(by.css('.js-search-field'));
            searchButton = element (by.css('.js-search-btn'));
            selectDoc = element.all(by.css('#docs-explorer-results-region a.file')).get(randomDoc);
        });

        it('should search documents', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            searchField.sendKeys('fac');

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(searchButton);

            generalHelper.waitElementClickableAndClick(selectDoc);

            browser.sleep(1000);

        });

        it('should create and move area of comment', function(){

            var doc = element(by.css('div.docs-viewer-page.js-viewer-page'));

            generalHelper.selectArea(doc,{x:-100, y:-100},{x:100, y:100});

            browser.sleep(3000);

        });

        it('should comment a document', function(){

            var textField = element(by.css('#page_annot_xpto .js-new-text'));
            var saveButton = element(by.css('#page_annot_xpto .js-new-save'));

            textField.sendKeys('Hello World');

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(5000);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});