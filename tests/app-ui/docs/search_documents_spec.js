describe('Docs', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            searchField,
            searchButton;

        beforeEach(function() {
            menuIcon = element(by.css('#doc-viewer-module'));
            searchField = element(by.css('.js-search-field'));
            searchButton = element (by.css('.js-search-btn'));

        });

        it('should search documents', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            searchField.sendKeys('fac');

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(searchButton);

            var selectDoc = element.all(by.css('#docs-explorer-results-region a.file')).get(0);
            generalHelper.waitElementClickableAndClick(selectDoc);

            browser.sleep(2000);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });



    });

});
