describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            recentDoc,
            listDoc,
            fullModeButton,
            shareButton;

        beforeEach(function () {
            menuIcon = element(by.css('#doc-viewer-module'));
            fullModeButton = element(by.css('.js-fullscreen-btn'));
            shareButton = element(by.css('.js-publiclink-btn'));
            recentDoc = element(by.css('.js-latestfiles'));
            listDoc = element.all(by.css('.dropdown.open li')).get(3);

        });

        it('should open recent document', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(recentDoc);
            generalHelper.waitElementClickableAndClick(listDoc);

            browser.sleep(1000);

        });

        it('should show the previous and the next document', function () {

            var previousButton = element(by.css('.js-prev-file'));
            var nextButton = element(by.css('.js-next-file'));

            for (var i = 0; i < 2; i++) {

                generalHelper.waitElementClickableAndClick(previousButton);

                browser.sleep(1500);
            }

            for (var i = 0; i < 2; i++) {

                generalHelper.waitElementClickableAndClick(nextButton);

                browser.sleep(1500);
            }

        });

        it('should expand the window and share the document', function () {

            generalHelper.waitElementClickableAndClick(fullModeButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(shareButton);

            var sendTo = element(by.css('#emails'));
            sendTo.sendKeys('test@xpto.biz');

            browser.sleep(1000);

            var sendButton = element(by.css('.ladda-button'));
            generalHelper.waitElementClickableAndClick(sendButton);

            browser.sleep(1000);

            generalHelper.waitAlertSuccessPresentAndClose();

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(fullModeButton);

        });

        it('should show the previous and the next page and resize the document', function () {

            var nextPageDocumentButton = element(by.css('.js-paging-next-btn'));
            var previousPageButton = element(by.css('.js-paging-previous-btn'));
            var firstPageButton = element(by.css('.js-paging-first-btn'));
            var lastPageButton = element(by.css('.js-paging-last-btn'));

            browser.sleep(1000);

            browser.executeScript('$("#docs-viewer-main-region").scrollTop(1000000);');

            browser.sleep(1000);

            browser.executeScript('$("#docs-viewer-main-region").scrollTop(0);');

            for (var i = 0; i < 2; i++) {

                generalHelper.waitElementClickableAndClick(nextPageDocumentButton);

                browser.sleep(1500);
            }

            for (var i = 0; i < 2; i++) {

                generalHelper.waitElementClickableAndClick(previousPageButton);

                browser.sleep(1500);
            }

            generalHelper.waitElementClickableAndClick(lastPageButton);

            browser.sleep(1500);

            generalHelper.waitElementClickableAndClick(firstPageButton);

            browser.sleep(1000);

        });

        it('should show the previous and the next page and resize the document', function () {

            var zoomInButton = element(by.css('.js-zoomin-btn'));
            var zomomOutButton = element(by.css('.js-zoomout-btn'));
            var fullScreenButton = element(by.css('.js-actualsize-btn'));
            var fitsizeButton = element(by.css('.js-fitsize-btn'));
            var fullwidthButton = element(by.css('.js-fullwidth-btn'));

            for (var i = 0; i < 4; i++) {

                generalHelper.waitElementClickableAndClick(zomomOutButton);

                browser.sleep(1500);
            }

            for (var i = 0; i < 4; i++) {

                generalHelper.waitElementClickableAndClick(zoomInButton);

                browser.sleep(1500);
            }

            generalHelper.waitElementClickableAndClick(fullwidthButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(fitsizeButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(fullScreenButton);

            browser.sleep(1000);

        });

        it('should hide the sidebar which contains information, show metadata, show information', function () {

            var sideBar = element(by.css('.js-inspector-btn'));
            var infoButton = element(by.css('.docs-viewer-inspector-menu-info'));
            var metadataButton = element(by.css('.docs-viewer-inspector-menu-metadata'));
            var annotationButton = element(by.css('.docs-viewer-inspector-menu-annotations'));
            var showSideBar = element(by.css('.js-inspector-btn'));

            generalHelper.waitElementClickableAndClick(sideBar);
            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(sideBar);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(showSideBar);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(annotationButton);

            browser.sleep(1000);

            if(element.all(by.css('#docs-viewer-inspector-menu .docs-viewer-inspector-menu-metadata')).length > 0) {
                generalHelper.waitElementClickableAndClick(metadataButton);
            }

            browser.sleep(2000);

            generalHelper.waitElementClickableAndClick(infoButton);

            browser.sleep(1500);

        });

    });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

});

