describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 3600000;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;
    });

    requireLoginSpec();

    describe('Documents', function () {

        var menuIcon,
            expandButton,
            filesList;

        beforeEach(function() {
            menuIcon = element(by.css('#doc-viewer-module'));
            expandButton = element(by.css('a.js-tree-master-collapse'));
            filesList = element.all(by.css('a.file'));

        });

        it('should open all the documents', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(expandButton);

            var levelDetailButton = element(by.css('.btn-xs'));
            var detailList = element.all(by.css('.js-lvldetail-option'));

            detailList.each(function (option) {

                generalHelper.waitElementClickableAndClick(levelDetailButton);

                browser.sleep(500);

                generalHelper.waitElementClickableAndClick(option);

                browser.sleep(500);

            });

            filesList.each(function(file){
                generalHelper.waitElementClickableAndClick(file);

                browser.sleep(1000);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});

