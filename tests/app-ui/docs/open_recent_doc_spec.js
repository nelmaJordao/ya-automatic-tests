describe('Suppliers', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            recentDoc,
            listDoc,
            closeDocButton;

        beforeEach(function() {
            menuIcon = element(by.css('#doc-viewer-module'));
            recentDoc = element(by.css('.js-latestfiles'));
            listDoc = element.all(by.css('.dropdown.open ul')).get(0);
            closeDocButton = element(by.css('.js-close-btn'));

        });

        it('should open recent document', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(recentDoc);
            generalHelper.waitElementClickableAndClick(listDoc);

            browser.sleep(2500);

            generalHelper.waitElementClickableAndClick(closeDocButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});


