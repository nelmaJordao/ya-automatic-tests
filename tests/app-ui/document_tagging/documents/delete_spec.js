describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            closeButton,
            firstDeleteButton,
            modalWindow,
            firstRow,
            okButton,
            editButton,
            searchButton;


        beforeEach(function() {
            menuIcon = element(by.css('#doc-tagging-module'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            firstRow = element.all(by.css('#document-tagging-documents-dtable tr')).get(0);
            firstDeleteButton = firstRow.element(by.css('a.js-delete-document'));
            modalWindow = element(by.css('#document-tagging-delete-document-modal'));
            okButton = element(by.css('.btn-success.ok'));
            editButton = firstRow.element(by.css('.js-edit-document'));
            searchButton = element(by.css('.js-search-btn'));
        });


        it('should open delete document modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(searchButton);
            generalHelper.waitElementClickableAndClick(editButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(searchButton);
            firstDeleteButton.click();
            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(2000);

            expect(modalWindow.isDisplayed()).toBe(true);
        });


        it('should confirm delete modal window', function()
        {
            var confirmDeleteButton = modalWindow.element(by.css('.btn-success'));
            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(confirmDeleteButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text)
            {
                expect(parseInt(text)).toEqual(--recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});