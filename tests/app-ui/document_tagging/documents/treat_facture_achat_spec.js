describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();
    var sirenHelper = requireCommonHelper('sirenHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            modalWindow,
            addVatButton,
            firstRow,
            editButton,
            closeButton;

        beforeEach(function() {
            menuIcon = element(by.css('#doc-tagging-module'));
            modalWindow = element(by.css('#document-tagging-edit-document-modal'));
            addVatButton = modalWindow.element(by.css('.js-add-vat'));
            firstRow = element.all(by.css('#document-tagging-documents-dtable tr')).first();
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            editButton = firstRow.element(by.css('.js-edit-document'));
            
        });

        it('should fill form and submit', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(2000);

            var docTypeSelect = modalWindow.element(by.css('button.selectpicker[data-id="docType"]'));
            var docTypeOption = modalWindow.element(by.css('li[data-original-index="3"] a.js-doctype-option'));
            generalHelper.waitElementClickableAndClick(docTypeSelect);
            generalHelper.waitElementClickableAndClick(docTypeOption);

            var inputNumber = modalWindow.element(by.css('#number'));
            inputNumber.sendKeys(config.inputs.getRandomName(10, false));

            browser.sleep(1000);

            var picker = modalWindow.element(by.css('#calfield-documentDate'));

            picker.clear();

            var date = dateHelper.getTodayDate();

            picker.sendKeys(date['day'] + '-' + date['month'] + '-' + date['year']);

            browser.sleep(1000);

            var parent = modalWindow.element(by.css('.js-auto-complete'));
            var nameInput = modalWindow.element(by.css('#supplier'));
            nameInput.clear();
            generalHelper.selectTypeAheadItem(parent, '#supplier', 'a', 1);

            var createCompanyModal = element(by.css('#company-create-modal'));
            var saveCompanyButton = createCompanyModal.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveCompanyButton);

            var firstRowHT = modalWindow.all(by.css('input.js-ht-value')).get(0);
            var secondRowHT = modalWindow.all(by.css('input.js-ht-value')).get(1);
            firstRowHT.clear();
            secondRowHT.clear();
            firstRowHT.sendKeys('100');
            secondRowHT.sendKeys('200');

            var clickSelectPicker = modalWindow.all(by.css('.dropdown-toggle[data-id="pickVat"]')).get(0);
            var clickOption = modalWindow.all(by.css('.dropdown-menu li[data-original-index="2"]')).get(1);
            generalHelper.waitElementClickableAndClick(addVatButton);
            generalHelper.waitElementClickableAndClick(clickSelectPicker);
            generalHelper.waitElementClickableAndClick(clickOption);

            var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);
            thirdRowHT.clear();
            thirdRowHT.sendKeys('300');

            browser.sleep(1000);

            var clickSelectPickerPlus = modalWindow.all(by.css('.dropdown-toggle[data-id="pickVat"]')).get(1);
            var clickOptionPlus = modalWindow.all(by.css('.dropdown-menu li[data-original-index="3"]')).get(2);
            generalHelper.waitElementClickableAndClick(addVatButton);
            generalHelper.waitElementClickableAndClick(clickSelectPickerPlus);
            generalHelper.waitElementClickableAndClick(clickOptionPlus);

            var fourthRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
            fourthRowHT.clear();
            fourthRowHT.sendKeys('400');

            browser.sleep(1000);

            var clickSelectPickerPlusPlus = modalWindow.all(by.css('.dropdown-toggle[data-id="pickVat"]')).get(2);
            var clickOptionPlusPlus = modalWindow.all(by.css('.dropdown-menu li[data-original-index="1"]')).get(3);
            generalHelper.waitElementClickableAndClick(addVatButton);
            generalHelper.waitElementClickableAndClick(clickSelectPickerPlusPlus);
            generalHelper.waitElementClickableAndClick(clickOptionPlusPlus);

            var fifthRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
            fifthRowHT.clear();
            fifthRowHT.sendKeys('500');

            browser.sleep(1000);

            var designationInput = modalWindow.element(by.css('#designation'));
            designationInput.sendKeys(config.inputs.getDescription(300));

            var additionalTab = modalWindow.element(by.css('a[aria-controls="tagging-additional-data"]'));
            generalHelper.waitElementClickableAndClick(additionalTab);

            var paymentDate = modalWindow.element(by.css('#calfield-paymentDate'));
            generalHelper.waitElementClickableAndClick(paymentDate);
            paymentDate.sendKeys('27-05-2016');

            browser.sleep(1000);

            var purchaseNumberInput = modalWindow.element(by.css('#purchaseOrderNumber'));
            purchaseNumberInput.sendKeys('123456789');

            var currencySelect = modalWindow.element(by.css('button[data-id="currencyType"]'));
            var currencyOption = modalWindow.element(by.css('li[data-original-index="48"] a'));
            generalHelper.waitElementClickableAndClick(currencySelect);
            generalHelper.waitElementClickableAndClick(currencyOption);

            var currencyAmountInput = modalWindow.element(by.css('#currencyAmount'));
            currencyAmountInput.sendKeys('asd');

            expect(currencyAmountInput.getText()).not.toEqual('asd');

            currencyAmountInput.sendKeys('123456789');

            var commentInput = modalWindow.element(by.css('#comment'));
            commentInput.sendKeys(config.inputs.getDescription(300));

            //Guardar
            var saveButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);
            generalHelper.waitAlertSuccessPresentAndClose();
            generalHelper.waitElementClickableAndClick(closeButton);

            });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});