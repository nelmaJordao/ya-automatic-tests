describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();
    var dateHelper = requireCommonHelper('dateHelper');


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            modalWindow,
            closeButton;

        beforeEach(function() {
            menuIcon = element(by.css('#doc-tagging-module'));
            modalWindow = element(by.css('#document-tagging-edit-document-modal'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));

        });

        it('should fill form and submit', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(2000);

        });

        it('should fill fields', function(){

            var documentType = modalWindow.element(by.css('.selectpicker[data-id="docType"]'));
            generalHelper.waitElementClickableAndClick(documentType);

            var documentOption = modalWindow.element(by.css('.dropdown-menu li[data-original-index="4"]'));
            generalHelper.waitElementClickableAndClick(documentOption);

            var factureNumber = modalWindow.element(by.css('#number'));
            generalHelper.waitElementClickableAndClick(factureNumber);
            factureNumber.sendKeys(config.inputs.getRandomName(10, false));

            var documentDate = modalWindow.element(by.css('#calfield-documentDate'));

            picker.clear();

            var date = dateHelper.getTodayDate();

            picker.sendKeys(date['day'] + '-' + date['month'] + '-' + date['year']);

            var inputCustomer = modalWindow.element(by.css('#customer'));
            var customerDiv = modalWindow.all(by.css('.js-auto-complete')).first();
            inputCustomer.clear();
            generalHelper.selectTypeAheadItem(customerDiv, 'input#customer', 'aa', 2);

            browser.sleep(1000);

            var selectionButton = modalWindow.element(by.css('button[data-id="itemFamily"][title="Sélectionnez"]'));
            generalHelper.waitElementClickableAndClick(selectionButton);

            var selectOption = modalWindow.all(by.css('.dropdown-menu li[data-original-index="5"] a')).get(1);
            generalHelper.waitElementClickableAndClick(selectOption);

            var tvaButton = modalWindow.element(by.css('button[data-id="pickVat"][title="Sélectionnez"]'));
            generalHelper.waitElementClickableAndClick(tvaButton);

            var tvaOption = modalWindow.all(by.css('.dropdown-menu li[data-original-index="3"] a')).get(2);
            generalHelper.waitElementClickableAndClick(tvaOption);

            var firstRowHT = modalWindow.all(by.css('.js-vat-input')).get(0);
            generalHelper.waitElementClickableAndClick(firstRowHT);
            firstRowHT.clear();
            firstRowHT.sendKeys('100');

            var firstRowTVA = modalWindow.all(by.css('.js-vat-input')).get(1);
            generalHelper.waitElementClickableAndClick(firstRowTVA);
            firstRowTVA.clear();
            firstRowTVA.sendKeys('75');

            var additionalTab = modalWindow.element(by.css('.js-add-vat'));
            generalHelper.waitElementClickableAndClick(additionalTab);

            generalHelper.waitElementClickableAndClick(selectionButton);
            var selectOptionSecondRow = modalWindow.all(by.css('.dropdown-menu li[data-original-index="5"] a')).get(3);
            generalHelper.waitElementClickableAndClick(selectOptionSecondRow);

            generalHelper.waitElementClickableAndClick(tvaButton);
            var tvaOptionSecondRow = modalWindow.all(by.css('.dropdown-menu li[data-original-index="2"] a')).get(4);
            generalHelper.waitElementClickableAndClick(tvaOptionSecondRow);

            var secondRowHT = modalWindow.all(by.css('.js-vat-input')).get(2);
            generalHelper.waitElementClickableAndClick(secondRowHT);
            secondRowHT.clear();
            secondRowHT.sendKeys('250');

            var secondRowTVA = modalWindow.all(by.css('.js-vat-input')).get(3);
            generalHelper.waitElementClickableAndClick(secondRowTVA);
            secondRowTVA.clear();
            secondRowTVA.sendKeys('30');

        });

        it('should test the rotation', function(){

        var rotationButton = element(by.css('.js-rotate-image'));

        for(var i = 0; i < 4; i++){

            generalHelper.waitElementClickableAndClick(rotationButton);

            browser.sleep(1000);
        }
        });

        it('should create area of comment', function(){

            var document = element(by.css('#doctag-edit-document-viewer-region'));
            var commentField = element(by.css('.js-annot-image'));
            generalHelper.waitElementClickableAndClick(commentField);
            generalHelper.selectArea(document,{x: -300, y: -400},{x: 100, y: 100});

            browser.sleep(1000);

        });

        it('should comment a document', function(){

            var textField = element(by.css('#page_annot_xpto .js-new-text'));
            var saveButton = element(by.css('#page_annot_xpto .js-new-save'));

            textField.sendKeys('Hello World');

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(saveButton);

        });

        it('should save', function(){

            var saveButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);
            generalHelper.waitAlertSuccessPresentAndClose();
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});