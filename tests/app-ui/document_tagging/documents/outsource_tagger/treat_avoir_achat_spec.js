describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTTAG.subdomain);

    it('should login with ' + config.users.TESTTAG.username + ' as ' + config.users.TESTTAG.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTTAG);
        loginHelper.changeRoleTo("OUTSOURCETAGGER");
    });

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            closeButton,
            firstRow,
            editButton,
            modalWindow,
            okButton;

        beforeEach(function() {
            
            menuIcon = element(by.css('#doc-tagging-module'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            firstRow = element.all(by.css('#document-tagging-documents-dtable tr')).first();
            editButton = firstRow.element(by.css('.js-edit-document'));
            modalWindow = element(by.css('#document-tagging-edit-document-modal'));
            okButton = element(by.css('.btn-success.ok'));
        });

        it('should fill form and submit', function () {

            var docTypeSelect = modalWindow.element(by.css('button.selectpicker[data-id="docType"]'));
            var docTypeOption = modalWindow.element(by.css('li[data-original-index="1"] a.js-doctype-option'));
            generalHelper.waitElementClickableAndClick(docTypeSelect);
            generalHelper.waitElementClickableAndClick(docTypeOption);

            var inputNumber = modalWindow.element(by.css('#number'));
            inputNumber.sendKeys(config.inputs.getRandomName(10, false));

            var resetButton = modalWindow.element(by.css('.js-reset-typeahead'));
            if(resetButton.isDisplayed()){
                var supplierDiv = modalWindow.element(by.css('div.js-auto-complete'));

                generalHelper.selectTypeAheadItem(supplierDiv, 'input#supplier', 's', 1);
            }

            var documentDate = modalWindow.element(by.css('#calfield-documentDate'));
            documentDate.clear();
            documentDate.sendKeys('27-05-2016');

            var addVatButton = modalWindow.element(by.css('.js-add-vat'));
            var vatDropdown = modalWindow.element(by.css('button[data-id="pickVat"]'));
            var vatOption = modalWindow.element(by.css('.dropdown-menu.inner.selectpicker li[data-original-index="4"] a.js-choose-vat span.text'));

            generalHelper.waitElementClickableAndClick(addVatButton);
            generalHelper.waitElementClickableAndClick(vatDropdown);
            generalHelper.waitElementClickableAndClick(vatOption);

            var firstRowHT = modalWindow.all(by.css('input.js-ht-value')).get(0);
            var secondRowHT = modalWindow.all(by.css('input.js-ht-value')).get(1);
            var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);

            firstRowHT.clear();
            secondRowHT.clear();
            thirdRowHT.clear();

            firstRowHT.sendKeys('100');
            secondRowHT.sendKeys('200');
            thirdRowHT.sendKeys('300');

            var designationInput = modalWindow.element(by.css('#designation'));

            designationInput.sendKeys(config.inputs.getDescription(300));

            var additionalTab = modalWindow.element(by.css('a[aria-controls="tagging-additional-data"]'));

            generalHelper.waitElementClickableAndClick(additionalTab);

            var paymentDate = modalWindow.element(by.css('#calfield-paymentDate'));
            paymentDate.sendKeys('27-05-2016');

            var purchaseNumberInput = modalWindow.element(by.css('#purchaseOrderNumber'));
            purchaseNumberInput.sendKeys('123456789');

            var currencySelect = modalWindow.element(by.css('button[data-id="currencyType"]'));
            var currencyOption = modalWindow.element(by.css('li[data-original-index="48"] a'));
            generalHelper.waitElementClickableAndClick(currencySelect);
            generalHelper.waitElementClickableAndClick(currencyOption);

            var currencyAmountInput = modalWindow.element(by.css('#currencyAmount'));
            currencyAmountInput.sendKeys('asd');

            expect(currencyAmountInput.getText()).not.toEqual('asd');

            currencyAmountInput.sendKeys('123456789');

            var commentInput = modalWindow.element(by.css('#comment'));
            commentInput.sendKeys(config.inputs.getDescription(3005));

            //Testar rotação
            var rotateButton = modalWindow.element(by.css('a.js-rotate-image'));

            generalHelper.waitElementClickableAndClick(rotateButton);
            generalHelper.waitElementClickableAndClick(rotateButton);
            generalHelper.waitElementClickableAndClick(rotateButton);
            generalHelper.waitElementClickableAndClick(rotateButton);

            //Testar Associação de páginas
            if(!modalWindow.isElementPresent(by.css('.viewer-pages'))){
                var btnPages = modalWindow.element(by.css('a.js-open-pages'));
                generalHelper.waitElementClickableAndClick(btnPages);

                var associatePage = modalWindow.all(by.css('a.js-thumb-check')).get(0);
                generalHelper.waitElementClickableAndClick(associatePage);

                var closePages = modalWindow.element(by.css('a.js-viewer-editing-close'));
                generalHelper.waitElementClickableAndClick(closePages);
            }

            //Guardar
            var saveButton = modalWindow.element(by.css('.btn-success.ok'));

            generalHelper.waitElementClickableAndClick(saveButton);
            generalHelper.waitAlertSuccessPresentAndClose();

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});