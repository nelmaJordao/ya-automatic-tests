describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTTAG.subdomain);

    it('should login with ' + config.users.TESTTAG.username + ' as ' + config.users.TESTTAG.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTTAG);
        loginHelper.changeRoleTo("OUTSOURCETAGGER");
    });

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            closeButton,
            firstRow,
            editButton,
            modalWindow,
            okButton;

        beforeEach(function() {
            
            menuIcon = element(by.css('#doc-tagging-module'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            firstRow = element.all(by.css('#document-tagging-documents-dtable tr')).get(0);
            editButton = firstRow.element(by.css('.js-edit-document'));
            modalWindow = element(by.css('#document-tagging-edit-document-modal'));
            okButton = element(by.css('.btn-success.ok'));
        });

        it('should search', function () {
            var docTypeSelect = modalWindow.element(by.css('button.selectpicker[data-id="docType"]'));
            var docTypeOption = modalWindow.element(by.css('li[data-original-index="3"] a.js-doctype-option'));
            generalHelper.waitElementClickableAndClick(docTypeSelect);
            generalHelper.waitElementClickableAndClick(docTypeOption);

            var documentDate = modalWindow.element(by.css('#calfield-documentDate'));
            documentDate.clear();
            documentDate.sendKeys('28-05-2016');

            var supplierDiv = modalWindow.element(by.css('div.js-auto-complete'));
            generalHelper.selectTypeAheadItem(supplierDiv, 'input#supplier', 's', 1);


            var radioType = modalWindow.element(by.css('#radio-payable'));
            generalHelper.waitElementClickableAndClick(radioType);

            var designationInput = modalWindow.element(by.css('#description'));
            designationInput.sendKeys(config.inputs.getDescription(300));


            //Testar rotação
            var rotateButton = modalWindow.element(by.css('a.js-rotate-image'));

            for (var i = 0; i <= 3; i++) {

                generalHelper.waitElementClickableAndClick(rotateButton);

                browser.sleep(500);
            }

            //Testar Associação de páginas
            if(!modalWindow.isElementPresent(by.css('.viewer-pages'))){
                var btnPages = modalWindow.element(by.css('a.js-open-pages'));
                generalHelper.waitElementClickableAndClick(btnPages);

                var associatePage = modalWindow.all(by.css('a.js-thumb-check')).get(0);
                generalHelper.waitElementClickableAndClick(associatePage);

                var closePages = modalWindow.element(by.css('a.js-viewer-editing-close'));
                generalHelper.waitElementClickableAndClick(closePages);
            }

            //Guardar
            var saveButton = modalWindow.element(by.css('.btn-success.ok'));

            generalHelper.waitElementClickableAndClick(saveButton);
            generalHelper.waitAlertSuccessPresentAndClose();

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});


