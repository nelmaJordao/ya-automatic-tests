describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTTAG.subdomain);

    it('should login with ' + config.users.TESTTAG.username + ' as ' + config.users.TESTTAG.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTTAG);
        loginHelper.changeRoleTo("OUTSOURCETAGGER");
    });

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            closeButton,
            searchField,
            searchButton;

        beforeEach(function() {
            menuIcon = element(by.css('#customer-admin-module'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            searchField = element(by.css('#searchTable'));
            searchButton = element(by.css('.js-search-btn'));
        });

        it('should search', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            searchField.clear();
            searchField.sendKeys('1');

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(searchButton);
            browser.sleep(1000);

            records.getText().then(function(text) {
                expect(parseInt(text)).not.toEqual(recordCount);
            });

            });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });


    });

});