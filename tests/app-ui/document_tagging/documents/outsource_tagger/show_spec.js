describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.TESTTAG.subdomain);

    it('should login with ' + config.users.TESTTAG.username + ' as ' + config.users.TESTTAG.role, function () {
        loginHelper.loginWith(baseURL, config.users.TESTTAG);
        loginHelper.changeRoleTo("OUTSOURCETAGGER");
    });

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            closeButton,
            modalWindow,
            firstShowButton,
            fechar;

        beforeEach(function() {
            menuIcon = element(by.css('#contract-manager-module'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            modalWindow = element(by.css('#document-tagging-view-document-modal'));
            firstShowButton = element.all(by.css('#document-tagging-documents-dtable a.js-show-document')).get(0);
            fechar = element(by.css('.btn-default.cancel'));
        });

        it('should open show document modal window', function ()
        {

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(firstShowButton);
            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(2000);

            expect(modalWindow.isDisplayed()).toBe(true);

        });

        it('should close modal window', function() {
            generalHelper.waitElementClickableAndClick(fechar);

            expect(modalWindow.isPresent()).toBe(false);

            });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});