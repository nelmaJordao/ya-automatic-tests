describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            modalWindow,
            addVatButton,
            firstRow,
            editButton,
            closeButton;

        beforeEach(function() {
            menuIcon = element(by.css('#doc-tagging-module'));
            modalWindow = element(by.css('#document-tagging-edit-document-modal'));
            addVatButton = modalWindow.element(by.css('.js-add-vat'));
            firstRow = element.all(by.css('#document-tagging-documents-dtable tr')).first();
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            editButton = firstRow.element(by.css('.js-edit-document'));

        });

        it('should fill form and submit', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);

            browser.sleep(2000);

            var docTypeSelect = modalWindow.element(by.css('button.selectpicker[data-id="docType"]'));
            var docTypeOption = modalWindow.element(by.css('li[data-original-index="2"] a.js-doctype-option'));
            generalHelper.waitElementClickableAndClick(docTypeSelect);
            generalHelper.waitElementClickableAndClick(docTypeOption);

            var titleInput = modalWindow.element(by.css('#contractTitle'));
            titleInput.sendKeys('Tagging test my ext');

            var numberInput = modalWindow.element(by.css('#contractReference'));
            numberInput.sendKeys(config.inputs.getRandomName(10, 0));

            var societeDiv = modalWindow.all(by.css('div.js-auto-complete')).get(1);
            generalHelper.selectTypeAheadItem(societeDiv, '#toExternal', 'bip', 1);

            var paymentIntervalInput = modalWindow.element(by.css('#contractPaymentInterval'));
            paymentIntervalInput.sendKeys('1');

            var paymentIntervalSelect = modalWindow.element(by.css('button.selectpicker[data-id="contractPaymentIntervalUnitTimeType"]'));
            var paymentIntervalOption = modalWindow.all(by.css('li[data-original-index="2"] a')).get(1);
            generalHelper.waitElementClickableAndClick(paymentIntervalSelect);
            generalHelper.waitElementClickableAndClick(paymentIntervalOption);

            var currencySelect = modalWindow.element(by.css('button.selectpicker[data-id="contractCurrency"]'));
            var currencyOption = modalWindow.all(by.css('li[data-original-index="1"] a')).get(2);
            generalHelper.waitElementClickableAndClick(currencySelect);
            generalHelper.waitElementClickableAndClick(currencyOption);

            //Guardar
            var saveButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(1000);

            generalHelper.waitAlertSuccessPresentAndClose();

            generalHelper.waitElementClickableAndClick(closeButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});