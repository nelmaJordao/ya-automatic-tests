describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();
    var dateHelper = requireCommonHelper('dateHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            modalWindow,
            addVatButton,
            clickSelectPicker,
            vatOption,
            closeButton,
            firstRow,
            editButton;

        beforeEach(function() {
            menuIcon = element(by.css('#doc-tagging-module'));
            modalWindow = element(by.css('#document-tagging-edit-document-modal'));
            addVatButton = modalWindow.element(by.css('.js-add-vat'));
            clickSelectPicker = element(by.css('.dropdown-toggle.selectpicker.btn-default[data-id="pickVat"][title="Sélectionnez"]'));
            vatOption = modalWindow.element(by.css('.dropdown-menu.open li[data-original-index="5"] a.js-choose-vat'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            firstRow = element.all(by.css('#document-tagging-documents-dtable tr')).first();
            editButton = firstRow.element(by.css('.js-edit-document'));
        });

        it('should fill form and submit', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(editButton);

            browser.sleep(2000);

            var docTypeSelect = modalWindow.element(by.css('button.selectpicker[data-id="docType"]'));
            var docTypeOption = modalWindow.element(by.css('li[data-original-index="5"] a.js-doctype-option'));
            generalHelper.waitElementClickableAndClick(docTypeSelect);
            generalHelper.waitElementClickableAndClick(docTypeOption);

            var expenseTypeSelect = modalWindow.element(by.css('button[data-id="expenseType"]'));
            var expenseTypeOption = modalWindow.element(by.css('li[data-original-index="1"] a.js-expensetype-option'));
            generalHelper.waitElementClickableAndClick(expenseTypeSelect);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(expenseTypeOption);

            browser.sleep(1000);

            var picker = modalWindow.element(by.css("#calfield-documentDate"));

            picker.clear();

            var date = dateHelper.getTodayDate();

            picker.sendKeys(date['day'] + '-' + date['month'] + '-' + date['year']);

            browser.sleep(1000);


            var employee = Math.round(Math.random() * 3);
            var employeeSelect = modalWindow.element(by.css('button.selectpicker[data-id="user"]'));
            var employeeOption = modalWindow.element(by.css('li[data-original-index="'+employee+'"] a.js-user-option'));
            generalHelper.waitElementClickableAndClick(employeeSelect);
            generalHelper.waitElementClickableAndClick(employeeOption);

            var keywordsInput = modalWindow.element(by.css('div.tags-input-box span input.tt-input'));
            keywordsInput.sendKeys('asdasdasd');

            var paymentSelect = modalWindow.element(by.css('button[data-id="paymentMethod"]'));
            var paymentOption = modalWindow.element(by.css('li[data-original-index="1"] a.js-paymethod-option'));

            generalHelper.waitElementClickableAndClick(paymentSelect);
            generalHelper.waitElementClickableAndClick(paymentOption);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(addVatButton);
            generalHelper.waitElementClickableAndClick(clickSelectPicker);
            generalHelper.waitElementClickableAndClick(vatOption);

            var firstRowHT = modalWindow.all(by.css('input.js-ht-value')).get(0);
            var secondRowHT = modalWindow.all(by.css('input.js-ht-value')).get(1);
            var thirdRowHT = modalWindow.all(by.css('input.js-vat-input')).get(0);

            firstRowHT.clear();
            secondRowHT.clear();
            thirdRowHT.clear();

            firstRowHT.sendKeys('100');
            secondRowHT.sendKeys('200');
            thirdRowHT.sendKeys('300');


            browser.sleep(1000);

            var vatOptionSecondRowAdd = modalWindow.all(by.css('.dropdown-menu.open li[data-original-index="3"] a.js-choose-vat')).get(1);
            generalHelper.waitElementClickableAndClick(addVatButton);
            generalHelper.waitElementClickableAndClick(clickSelectPicker);
            generalHelper.waitElementClickableAndClick(vatOptionSecondRowAdd);

            browser.sleep(1000);

            var fourthRowHT = modalWindow.all(by.css('input.js-vat-input')).get(2);
            fourthRowHT.clear();
            fourthRowHT.sendKeys('400');

            browser.sleep(1000);

            var vatOptionThirdRowAdd = modalWindow.all(by.css('.dropdown-menu.open li[data-original-index="1"] a.js-choose-vat')).get(2);
            generalHelper.waitElementClickableAndClick(addVatButton);
            generalHelper.waitElementClickableAndClick(clickSelectPicker);
            generalHelper.waitElementClickableAndClick(vatOptionThirdRowAdd);

            var fifthRowHT = modalWindow.all(by.css('input.js-vat-input')).get(4);
            fifthRowHT.clear();
            fifthRowHT.sendKeys('500');

            browser.sleep(1000);

            var vatOptionFourthRowAdd = modalWindow.all(by.css('.dropdown-menu.open li[data-original-index="6"] a.js-choose-vat')).get(3);
            generalHelper.waitElementClickableAndClick(addVatButton);
            generalHelper.waitElementClickableAndClick(clickSelectPicker);
            generalHelper.waitElementClickableAndClick(vatOptionFourthRowAdd);

            var sixthRowHT = modalWindow.all(by.css('input.js-vat-input')).get(6);
            sixthRowHT.clear();
            sixthRowHT.sendKeys('600');

            browser.sleep(1000);

            //Ver se o icon do comentário está ativo
            var commentInactive = modalWindow.element(by.css('.js-annot-image'));
            var commentActive = modalWindow.element(by.css('.js-annot-image.active'));
            generalHelper.waitElementClickableAndClick(commentInactive);

            browser.sleep(1000);

            expect(commentActive.isDisplayed()).toBe(true);

            //Ver se o icon da lupa está ativa

            var zoomInactive = modalWindow.element(by.css('.js-zoom-image'));
            var zoomActive = modalWindow.element(by.css('.js-zoom-image.active'));
            generalHelper.waitElementClickableAndClick(zoomInactive);

            browser.sleep(1000);

            expect(zoomActive.isDisplayed()).toBe(true);

            //Testar rotação
            var rotateButton = modalWindow.element(by.css('a.js-rotate-image'));

            for (var i = 0; i < 4; i++) {

                generalHelper.waitElementClickableAndClick(rotateButton);

                browser.sleep(1000);

            }

            var designationInput = modalWindow.element(by.css('#description'));
            designationInput.sendKeys(config.inputs.getDescription(300));

            //Testar Associação de páginas
            if(modalWindow.all(by.css('a.js-open-pages')).length > 0){
                var btnPages = modalWindow.element(by.css('a.js-open-pages'));
                generalHelper.waitElementClickableAndClick(btnPages);

                var associatePage = modalWindow.all(by.css('a.js-thumb-check')).get(0);
                generalHelper.waitElementClickableAndClick(associatePage);

                var closePages = modalWindow.element(by.css('a.js-viewer-editing-close'));
                generalHelper.waitElementClickableAndClick(closePages);
            }

            //Guardar
            var saveButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(closeButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });
        
    });

});