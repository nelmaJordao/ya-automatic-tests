describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            modalWindow;

        beforeEach(function() {
            menuIcon = element.all(by.css('#doc-tagging-module'));
            modalWindow = element(by.css('#document-tagging-edit-document-modal'));

        });

        it('should open show document modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(2000);

            expect(modalWindow.isDisplayed()).toBe(true);

        });

        it('should select only 3 page of document and tagging as pieces justificatives', function () {

            for (var i = 0; i < 10; i++) {

                browser.sleep(1000);

                var numberOfPages = element(by.css('.js-viewer-pages-indicator'));

                browser.sleep(1000);

                numberOfPages.getText().then(function (text) {

                    if (text != '1 de 1') {

                        var splitPagesButton = element(by.css('.js-open-pages'));
                        generalHelper.waitElementClickableAndClick(splitPagesButton);

                        var selectedPages = element.all(by.css('.isAssociated'));
                        selectedPages.each(function (page) {

                            var checkButton = page.element(by.css('.js-thumb-check'));

                            generalHelper.waitElementClickableAndClick(checkButton);

                        });

                        var checkButtons = element.all(by.css('.js-thumb-check'));

                        for (var i = 0; i < 2; i++) {

                            var check = checkButtons.get(i);

                            generalHelper.waitElementClickableAndClick(check);

                            browser.sleep(2000);
                        }

                        var closePages = element(by.css('.js-viewer-editing-close'));

                        generalHelper.waitElementClickableAndClick(closePages);

                        browser.sleep(1000);

                        var documentType = modalWindow.element(by.css('.selectpicker[data-id="docType"]'));
                        generalHelper.waitElementClickableAndClick(documentType);

                        browser.sleep(1000);

                        var documentOption = modalWindow.element(by.css('.dropdown-menu li[data-original-index="4"]'));
                        generalHelper.waitElementClickableAndClick(documentOption);

                        browser.sleep(1000);

                        var saveButton = modalWindow.element(by.css('.btn-success.ok'));

                        generalHelper.waitElementClickableAndClick(saveButton);

                        browser.sleep(1000);

                    }

                    else {

                        var nextPageButton = modalWindow.element(by.css('.js-next-doc'));
                        generalHelper.waitElementClickableAndClick(nextPageButton);

                    }

                });
                browser.sleep(1000);
            }

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});