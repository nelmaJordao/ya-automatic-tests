describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();
    var dateHelper = requireCommonHelper('dateHelper');


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            closeButton,
            firstRow,
            editButton,
            modalWindow,
            okButton;

        beforeEach(function() {
            menuIcon = element(by.css('#doc-tagging-module'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            firstRow = element.all(by.css('#document-tagging-documents-dtable tr')).get(0);
            editButton = firstRow.element(by.css('.js-edit-document'));
            modalWindow = element(by.css('#document-tagging-edit-document-modal'));
            okButton = element(by.css('.btn-success.ok'));
        });

        it('should search', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(closeButton);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(editButton);

            browser.sleep(2000);

            var docTypeSelect = modalWindow.element(by.css('button.selectpicker[data-id="docType"]'));
            var docTypeOption = modalWindow.element(by.css('li[data-original-index="6"] a.js-doctype-option'));
            generalHelper.waitElementClickableAndClick(docTypeSelect);
            generalHelper.waitElementClickableAndClick(docTypeOption);

            var picker = modalWindow.element(by.css('#calfield-documentDate'));

            picker.clear();

            var date = dateHelper.getTodayDate();

            picker.sendKeys(date['day'] + '-' + date['month'] + '-' + date['year']);

            var supplierDiv = modalWindow.all(by.css('div.js-auto-complete')).first();
            generalHelper.selectTypeAheadCreateNew(supplierDiv, 'input#supplier');

            browser.sleep(1000);

            var createCompanyModal = element(by.css('#company-create-modal'));
            var companyName = createCompanyModal.element(by.css('#name'));
            companyName.clear();
            companyName.sendKeys('Test');

            browser.sleep(1000);

            var saveCompanyButton = createCompanyModal.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveCompanyButton);

            var radioType = modalWindow.element(by.css('#radio-payable'));
            generalHelper.waitElementClickableAndClick(radioType);

            var designationInput = modalWindow.element(by.css('#description'));
            designationInput.sendKeys(config.inputs.getDescription(300));

            //Testar rotação
            var rotateButton = modalWindow.element(by.css('a.js-rotate-image'));

            for (var i = 0; i <= 3; i++) {

                generalHelper.waitElementClickableAndClick(rotateButton);

                browser.sleep(500);

            }

            //Testar Associação de páginas
            if(modalWindow.all(by.css('a.js-open-pages')).length > 0){
                var btnPages = modalWindow.element(by.css('a.js-open-pages'));
                generalHelper.waitElementClickableAndClick(btnPages);

                var associatePage = modalWindow.all(by.css('a.js-thumb-check')).get(0);
                generalHelper.waitElementClickableAndClick(associatePage);

                var closePages = modalWindow.element(by.css('a.js-viewer-editing-close'));
                generalHelper.waitElementClickableAndClick(closePages);
            }

            //Guardar
            var saveButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);
            generalHelper.waitAlertSuccessPresentAndClose();
            generalHelper.waitElementClickableAndClick(closeButton);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});