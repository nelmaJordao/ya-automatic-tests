describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            modalWindow;


        beforeEach(function() {
            menuIcon = element(by.css('#doc-tagging-module'));
            modalWindow = element(by.css('.modal-content'));
        });

        it('should open show document modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(menuIcon);

            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(2000);

            expect(modalWindow.isDisplayed()).toBe(true);

        });

        it('should select only one page of document, rotate document, crop original document, crop the crop document', function () {

            var numberOfPages = element(by.css('.js-viewer-pages-indicator'));
            var firstRow = element.all(by.css('#document-tagging-documents-dtable tr')).first();
            var editButton = firstRow.element(by.css('.js-edit-document'));
            var closeEditModal = element(by.css('.modal-header .close'));
            generalHelper.waitElementClickableAndClick(closeEditModal);

            var filterBySource = element(by.css('.js-sort[data-field="source"]'));
            generalHelper.waitElementClickableAndClick(filterBySource);

            browser.sleep(1000);

            generalHelper.waitElementClickableAndClick(editButton);

            browser.sleep(1000);

            numberOfPages.getText().then(function(text){

                if(text != '1 de 1'){

                    var splitPagesButton = element(by.css('.js-split-document'));
                    generalHelper.waitElementClickableAndClick(splitPagesButton);

                    browser.sleep(1000);

                    var splitModal = element(by.css('#document-tagging-split-document-modal'));
                    var cancelSplitButton = splitModal.element(by.css('.btn-default'));
                    generalHelper.waitElementClickableAndClick(cancelSplitButton);

                    browser.sleep(1000);

                    generalHelper.waitElementClickableAndClick(splitPagesButton);

                    var confirmationSplitButton = splitModal.element(by.css('.btn-success'));
                    generalHelper.waitElementClickableAndClick(confirmationSplitButton)

                    browser.sleep(1000);

                    generalHelper.waitElementClickableAndClick(editButton);

                    browser.sleep(1000);
                }

                var rotationButton = element(by.css('.js-rotate-image'));
                var document = element(by.css('#doctag-edit-document-viewer-region'));

                for(var i = 0; i < 4; i++){

                    generalHelper.waitElementClickableAndClick(rotationButton);

                    browser.sleep(1000);
                }

                var cropButton = element(by.css('.js-crop-image'));
                generalHelper.waitElementClickableAndClick(cropButton);

                browser.sleep(1000);

                generalHelper.selectArea(document,{x: -300, y: -400},{x: 100, y: 100});

                generalHelper.selectArea(document,{x: -100, y: -400},{x: 100, y: 100});

                generalHelper.selectArea(document,{x: 100, y: -400},{x: 100, y: 100});

                generalHelper.selectArea(document,{x: -200, y: -200},{x: 100, y: 100});


                var okCropButton = element(by.css('.js-viewer-cropping-close'));
                generalHelper.waitElementClickableAndClick(okCropButton);

                browser.sleep(500);

                var confimationCropModal = element(by.css('#document-tagging-crop-document-modal'));

                expect(confimationCropModal.isDisplayed()).toBe(true);

                var confirmationButton = confimationCropModal.element(by.css('.btn-success'));
                generalHelper.waitElementClickableAndClick(confirmationButton);

                browser.sleep(1000);

                browser.refresh();

                browser.sleep(1000);

                generalHelper.waitElementClickableAndClick(closeEditModal);

                browser.sleep(1000);

                var closeSourceFilter = element(by.css('.pos span'));

                generalHelper.waitElementClickableAndClick(closeSourceFilter);

                browser.sleep(1000);

                generalHelper.waitElementClickableAndClick(editButton);

                browser.sleep(1000);

                generalHelper.waitElementClickableAndClick(cropButton);

                browser.sleep(1000);

                generalHelper.selectArea(document,{x: 0, y: -200},{x: 100, y: 100});

                generalHelper.waitElementClickableAndClick(okCropButton);

                browser.sleep(1000);

                expect(confimationCropModal.isDisplayed()).toBe(true);

                generalHelper.waitElementClickableAndClick(confirmationButton);

                browser.sleep(1000);

            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});