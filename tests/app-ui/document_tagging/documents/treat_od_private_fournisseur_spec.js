describe('Document Tagging', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();
    var dateHelper = requireCommonHelper('dateHelper');


    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    requireLoginSpec();

    describe('Documents', function () {
        //declarar variaveis necessarias em mais do que um teste
        var menuIcon,
            closeButton,
            firstRow,
            editButton,
            modalWindow,
            okButton;

        beforeEach(function() {
            menuIcon = element(by.css('#doc-tagging-module'));
            closeButton = element(by.css('#document-tagging-edit-document-modal div div div button.close'));
            firstRow = element.all(by.css('#document-tagging-documents-dtable tr')).get(0);
            editButton = firstRow.element(by.css('.js-edit-document'));
            modalWindow = element(by.css('#document-tagging-edit-document-modal'));
            okButton = element(by.css('.btn-success.ok'));
        });

        it('should search', function () {

            generalHelper.waitElementClickableAndClick(menuIcon);
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(editButton);

            browser.sleep(2000);

            var docTypeSelect = modalWindow.element(by.css('button.selectpicker[data-id="docType"]'));
            var docTypeOption = modalWindow.element(by.css('li[data-original-index="6"] a.js-doctype-option'));
            generalHelper.waitElementClickableAndClick(docTypeSelect);
            generalHelper.waitElementClickableAndClick(docTypeOption);

            var picker = modalWindow.element(by.css('#calfield-documentDate'));

            picker.clear();

            var date = dateHelper.getTodayDate();

            picker.sendKeys(date['day'] + '-' + date['month'] + '-' + date['year']);

            var supplierDiv = modalWindow.element(by.css('.twitter-typeahead'));
            generalHelper.selectTypeAheadCreateNew(supplierDiv, "#supplier");

            browser.sleep(1000);

            var createCompany = element(by.css('#company-create-modal'));
            var name = createCompany.element(by.css('#name'));
            name.clear().sendKeys('androidddddd');

            var save = createCompany.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(save);

            var radioType = modalWindow.element(by.css('#radio-payable'));
            generalHelper.waitElementClickableAndClick(radioType);

            var designationInput = modalWindow.element(by.css('#description'));
            designationInput.sendKeys(config.inputs.getDescription(300));

            //Testar rotação
            var rotateButton = modalWindow.element(by.css('a.js-rotate-image'));

            for (var i = 0; i <= 3; i++) {

                generalHelper.waitElementClickableAndClick(rotateButton);

                browser.sleep(500);
            }

            //Testar Associação de páginas
            if(modalWindow.element(by.css('a.js-open-pages')).isDisplayed()){
                var btnPages = modalWindow.element(by.css('a.js-open-pages'));
                generalHelper.waitElementClickableAndClick(btnPages);

                var associatePage = modalWindow.all(by.css('a.js-thumb-check')).get(0);
                generalHelper.waitElementClickableAndClick(associatePage);

                var closePages = modalWindow.element(by.css('a.js-viewer-editing-close'));
                generalHelper.waitElementClickableAndClick(closePages);
            }

            //Guardar
            var saveButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(saveButton);
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.returnToDashboard();

            var menuIconSupplier = element(by.css('#suppliers-module'));
            var tabItemSupplier = element(by.css('#suppliers-menu-region [data-sec="suppliers"]'));
            generalHelper.waitElementClickableAndClick(menuIconSupplier);
            generalHelper.waitElementClickableAndClick(tabItemSupplier);

            browser.sleep(1000);

            var searchField = element(by.css('#searchTable'));
            var searchButton = element(by.css('.js-search-btn'));

            searchField.clear();
            searchField.sendKeys('androidddddd');

            var records = element(by.css('.js-total-res span'));
            generalHelper.waitElementClickableAndClick(searchButton);
            browser.sleep(1000);

            records.getText().then(function(text) {
                expect(parseInt(text)).not.toEqual(0);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});