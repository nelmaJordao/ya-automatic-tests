describe('Outsource manager', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.OUTSOURCEMANAGER.subdomain);

    it('should login with ' + config.users.OUTSOURCEMANAGER.username + ' as ' + config.users.OUTSOURCEMANAGER.role, function () {
        loginHelper.loginWith(baseURL, config.users.OUTSOURCEMANAGER);
    });

    describe('Outsource manager', function () {

        //declarar variaveis necessarias em mais do que um teste
        var modalWindow,
            firstRow;

        beforeEach(function() {
           
            modalWindow = element(by.css('.modal.in'));
            firstRow = element.all(by.css('#taggers-dtable tr')).get(0);
        });

        it('should open show family modal window', function () {

            var activateButton = firstRow.element(by.css('.js-email-tagger'));
            generalHelper.waitElementClickableAndClick(activateButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should close modal window', function() {
            var closeButton = modalWindow.element(by.css('.btn-success.ok'));
            generalHelper.waitElementClickableAndClick(closeButton);

            expect(modalWindow.isPresent()).toBe(false);

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});