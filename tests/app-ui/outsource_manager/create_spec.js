describe('Outsource_manager', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.OUTSOURCEMANAGER.subdomain);

    it('should login with ' + config.users.OUTSOURCEMANAGER.username + ' as ' + config.users.OUTSOURCEMANAGER.role, function () {
        loginHelper.loginWith(baseURL, config.users.OUTSOURCEMANAGER);
    });

    describe('Outsource_manager', function () {

        //declarar variaveis necessarias em mais do que um teste
        var newButton,
            modalWindow,
            okButton,
            name,
            nickname,
            email;

        beforeEach(function() {
            
            newButton = element(by.css('.js-new-tagger'));
            modalWindow = element(by.css('#tagger-create-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            name = modalWindow.element(by.css('#firstName'));
            nickname = modalWindow.element((by.css('#lastName')));
            email = modalWindow.element(by.css('#email'));

        });

        it('should open new tagger modal window', function () {

            generalHelper.waitElementClickableAndClick(newButton);
            generalHelper.waitElementPresent(modalWindow);

            browser.sleep(1000);

            expect(modalWindow.isDisplayed()).toBe(true);
        });

        it('should check for empty fields', function () {

            generalHelper.waitElementClickableAndClick(okButton);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(3);
        });

        it('should test field restrictions', function () {

            generalHelper.waitElementClickableAndClick(okButton);

            name.sendKeys('123');

            nickname.sendKeys('123');

            email.sendKeys('abc')

            expect(name.getText()).not.toEqual('123');
            expect(nickname.getText()).not.toEqual('123');
            expect(email.getText()).not.toEqual('abc');

            name.clear();
            nickname.clear();
            email.clear();
        });

        it('should fill mandatory fields and submit', function() {

            name.clear();
            name.sendKeys('Tagger');

            nickname.clear();
            nickname.sendKeys('Holding');

            email.clear();
            email.sendKeys('test.tag' + Math.floor((Math.random() * 8000) + 4000) + '@xpto.biz');


            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;

            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

            records.getText().then(function(text) {
                expect(parseInt(text)).toEqual(++recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});