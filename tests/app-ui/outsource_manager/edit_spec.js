describe('Outsource manager', function ()
{
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();



    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.OUTSOURCEMANAGER.subdomain);

    it('should login with ' + config.users.OUTSOURCEMANAGER.username + ' as ' + config.users.OUTSOURCEMANAGER.role, function () {
        loginHelper.loginWith(baseURL, config.users.OUTSOURCEMANAGER);
    });

    describe('Outsource manager', function ()
    {

        //declarar variaveis necessarias em mais do que um teste
        var firstRow,
            editButton,
            modalWindow,
            okButton,
            closeButton,
            name,
            nickname,
            email;

        beforeEach(function()
        {
            firstRow = element.all(by.css('#taggers-dtable tr')).get(0);
            editButton = firstRow.element(by.css('a.js-edit-tagger'));
            modalWindow = element(by.css('#tagger-update-modal'));
            okButton = modalWindow.element(by.css('.btn-success.ok'));
            closeButton = modalWindow.element(by.css('.btn-default.cancel'));

            //campos para limpar e depois preencher
            name = modalWindow.element(by.css('#firstName'));
            nickname = modalWindow.element(by.css('#lastName'));
            email = modalWindow.element(by.css('#email'));

        });

        it('should open edit tagger modal window', function () {

            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);
        });


        it('should check for empty fields', function ()
        {

            browser.sleep(2000);

            name.clear();
            nickname.clear();

            generalHelper.waitElementClickableAndClick(okButton);

            var errors = modalWindow.all(by.css('.form-group.error'));

            expect(errors.count()).toEqual(2);

        });


        it('should close and reopen edit tagger modal window', function ()
        {
            generalHelper.waitElementClickableAndClick(closeButton);
            generalHelper.waitElementClickableAndClick(editButton);
            generalHelper.waitElementPresent(modalWindow);

            expect(modalWindow.isDisplayed()).toBe(true);

        });


        it('should fill mandatory fields and submit', function()
        {
            browser.sleep(2000);

            //editar name
            name.clear();
            name.sendKeys('TAGGER');

            //editar nickname
            nickname.clear();
            nickname.sendKeys('Rogger');

            generalHelper.waitElementClickableAndClick(okButton);
            generalHelper.waitAlertSuccessPresentAndClose();

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});