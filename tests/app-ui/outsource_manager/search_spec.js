describe('Outsource manager', function () {
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var config = requireConfig();

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.OUTSOURCEMANAGER.subdomain);

    it('should login with ' + config.users.OUTSOURCEMANAGER.username + ' as ' + config.users.OUTSOURCEMANAGER.role, function () {
        loginHelper.loginWith(baseURL, config.users.OUTSOURCEMANAGER);
    });

    describe('Outsource manager', function () {
        //declarar variaveis necessarias em mais do que um teste
        var searchField,
            searchButton;

        beforeEach(function() {
            
            searchField = element(by.css('#searchTable'));
            searchButton = element(by.css('.js-search-btn'));
        });

        it('should search', function () {

            browser.sleep(1000);

            searchField.clear();
            searchField.sendKeys('1');

            var records = element(by.css('.js-total-res span'));
            var recordCount = 0;
            records.getText().then(function(text) {
                recordCount = parseInt(text);
            });

            generalHelper.waitElementClickableAndClick(searchButton);
            browser.sleep(1000);

            records.getText().then(function(text) {
                expect(parseInt(text)).not.toEqual(recordCount);
            });

        });

        it('should return to dashboard', function () {
            generalHelper.returnToDashboard();
            browser.refresh();
        });

    });

});