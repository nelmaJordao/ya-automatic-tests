describe('Admin', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.SUPERADMIN.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.SUPERADMIN.role, function () {
        loginHelper.loginWith(baseURL, config.users.SUPERADMIN);
    });

    
    var accountingFirm = {  siren : '524070281',
                            alias : 'Sene',
                            subDomain : 'testsene1',
                            firstName : 'test',
                            lastName : 'sene',
                            email : 'testsene@xpto.biz',
                            type : 'partner'};


    seedHelper.createAccountingFirm( accountingFirm, baseURL + '#logout' );
    // dá timeout

});