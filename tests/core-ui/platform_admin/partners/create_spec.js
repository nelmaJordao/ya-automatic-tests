describe('Admin', function () {
    var config = requireConfig();
    var generalHelper = requireCommonHelper('generalHelper');
    var loginHelper = requireCommonHelper('loginHelper');
    var seedHelper = requireCommonHelper('seedHelper');
    var baseURL = generalHelper.baseUrlWithSubdomain(config.users.PLATFORMADMIN.subdomain);

    beforeEach(function () {
        browser.ignoreSynchronization = true;
    });

    afterEach(function () {
        browser.ignoreSynchronization = false;
    });

    it('should login with ' + config.username + ' as ' + config.users.PLATFORMADMIN.role, function () {
        loginHelper.loginWith(baseURL, config.users.PLATFORMADMIN);
    });

    var partner = { name : "partner um",
                    firstName : "partner",
                    lastName : "um",
                    email : "test.par1@xpto.biz" };

    seedHelper.createPartnerOrHolding( partner, baseURL + '#logout' );

});