# YesAccount Tests

## Installation
Steps to install and run the tests.

### Software Requirements

1. Node NPM
2. Protractor
3. Selenium Webdriver

### Install Protractor

```sh
npm install -g protractor
```

```sh
webdriver-manager update
```

### Start Selenium from terminal

```sh
webdriver-manager start
```

(the process keeps running in the terminal window)

### Run the tests

Run all tests available. The tests must be configured in the `protractor.conf.js` file.

```sh
protractor
```

Run one single test.

```sh
protractor --specs ./path/to/file/test_spec.js
```


## Preparation
Preparation needed before EACH test.

### Invoices

#### Clients
1. Create "Romain Blue" client

### Document tagging
1. Tag a document from "Orange"
